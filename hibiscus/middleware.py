# -*- coding:utf-8 -*-
from hibiscus.timer import HibiscusTask


class MenuActiveMiddleware(object):

    TOP_MENU = "TopMenu"
    SECOND_MENU = "SecondMenu"
    THIRD_MENU = "ThirdMenu"

    def process_request(self, request):
        if not request.is_ajax():
            top_menu = request.GET.get(self.TOP_MENU, None)

            second_menu = request.GET.get(self.SECOND_MENU, None)
            third_menu = request.GET.get(self.THIRD_MENU, None)

            top_menu_has = request.session.get(self.TOP_MENU, None)
            second_menu_has = request.session.get(self.SECOND_MENU, None)
            third_menu_has = request.session.get(self.THIRD_MENU, None)
            if third_menu:
                request.session[self.THIRD_MENU] = third_menu

            if second_menu:
                if second_menu_has :
                    if self.THIRD_MENU in request.session:
                        del request.session[self.THIRD_MENU]
                request.session[self.SECOND_MENU] = second_menu

            if top_menu:
                if top_menu_has :
                    if self.THIRD_MENU in request.session:
                        del request.session[self.THIRD_MENU]
                    if self.SECOND_MENU in request.session:
                        del request.session[self.SECOND_MENU]
                request.session[self.TOP_MENU] = top_menu


class MobileResponseMiddleware(object):

    def process_response(self, request, response):
        if request.path.startswith("/mobile"):
            response['Access-Control-Allow-Origin'] = "*"
            response['Access-Control-Allow-Credentials'] = 'true'
            response['Access-Control-Allow-Methods'] = "GET, POST, OPTIONS"
            response['Access-Control-Allow-Headers'] = "Origin, X-Requested-With, Content-Type, Accept, huiche-x-token"
            response['Access-Control-Max-Age'] = "3600"
        return response


class PortalResponseMiddleware(object):

    def process_response(self, request, response):
        if request.path.startswith("/portal"):
            response['Access-Control-Allow-Origin'] = "*"
            response['Access-Control-Allow-Methods'] = "GET, POST, OPTIONS"
            response['Access-Control-Allow-Headers'] = "Origin, X-Requested-With, Content-Type, Accept, huiche-x-token"
            response['Access-Control-Max-Age'] = "3600"
        return response


class TaskStartMiddleware(object):

    def process_request(self, request):
        HibiscusTask().run()
