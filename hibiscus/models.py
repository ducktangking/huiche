# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_delete


# Create your models here.


class SupportedArea(models.Model):
    """
        支持的地区
        province: 省
        city:     市
        region:   区县
    """
    province = models.CharField(max_length=16)
    province_initial = models.CharField(max_length=4, default='')
    city_initial = models.CharField(max_length=4, default='')
    city = models.CharField(max_length=16)
    region = models.CharField(max_length=16)
    created_at = models.DateTimeField(default=timezone.now)


class VerifyKey(models.Model):
    """
        发送的随机
            如果是手机发送,那么为random为六位,如果是email发送则为32位
        random: 随机串
        phone: 手机号码
        email: 邮箱地址
    """
    random = models.CharField(max_length=32)
    phone = models.CharField(max_length=16)
    email = models.EmailField(null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class Picture(models.Model):
    """
        图片表
        name 图片的原始名称
        path 图片在服务器上的路径
    """
    name = models.CharField(max_length=32)
    path = models.CharField(max_length=128)
    created_at = models.DateTimeField(default=timezone.now)


class Logo(models.Model):
    """
        品牌,车型图标
    """
    picture = models.ForeignKey(Picture)
    is_default = models.BooleanField(default=False)


class Portrait(models.Model):
    """
        头像
    """
    picture = models.ForeignKey(Picture)
    is_default = models.BooleanField(default=False)


class UserAddress(models.Model):
    """
        用户的收货地址
        添加经销商店址的坐标信息,经度（longitude）纬度（latitude）
    """
    address = models.CharField(max_length=128)
    longitude = models.FloatField(null=True)
    latitude = models.FloatField(null=True)
    is_default = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class UserInformation(models.Model):
    """
        用户信息
        addresses: 拥有很多收货地址
        portrait: 拥有一个头像
        name: 姓名,当用户的身份为经销商时，name指公司简称
        sex: 性别
        birthday: 出生年月
        tel: 座机
        qq: qq号码
        weibo: 微博地址
        weixin: 微信地址
    """
    SEX_CHOICES = (('m', "male"), ('f', "female"))
    addresses = models.ManyToManyField(UserAddress)
    city_info = models.ForeignKey(SupportedArea, null=True)
    portrait = models.ForeignKey(Portrait, null=True)
    name = models.CharField(max_length=16, null=True)
    email = models.EmailField(null=True)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES, default='m')
    birthday = models.DateField(null=True)
    tel = models.CharField(max_length=16, null=True)
    qq = models.CharField(max_length=16, null=True)
    weibo = models.CharField(max_length=128, null=True)
    weixin = models.CharField(max_length=128, null=True)


class Users(models.Model):
    """注册用户表"""
    information = models.ForeignKey(UserInformation, null=True)
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=128)
    phone = models.CharField(max_length=16, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    last_ip = models.GenericIPAddressField(blank=True, null=True)
    article_types = models.ManyToManyField("ArticleType")
    is_active = models.BooleanField(default=True)
    fav_series = models.ManyToManyField("Series")
    fav_cars = models.ManyToManyField("Car")
    created_at = models.DateTimeField(default=timezone.now)


class HcAdmin(models.Model):
    """
        管理员表
        information: 管理员信息
        username: 用户名
        phone: 手机号码
        type: 类型 管理员,操作员以及审计员
        password: 密码,sha256算法保存
        last_login: 上一次登录时间
        last_ip: 上一次登录ip
        is_active: 是否激活
        created_at: 创建时间
    """
    ADMIN_CHOICES = (
        ('o', 'operator'),
        ('a', 'auditor'),
        ('m', 'manager')
    )
    area = models.ForeignKey(SupportedArea, null=True)
    information = models.ForeignKey(UserInformation, null=True)
    username = models.CharField(max_length=16)
    phone = models.CharField(max_length=16)
    type = models.CharField(max_length=1, default='o', choices=ADMIN_CHOICES)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    last_ip = models.GenericIPAddressField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)

    def get_user_name(self):
        return self.username

    def get_user_id(self):
        return self.id

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True


class Producer(models.Model):
    """
        供应商表
        area: 供应商服务地区
        series_quote: 供应商的车系保养报价
        car_quote: 供应商车款报价
        creator: 经销商角色创建者
    """
    PRODUCER_TYPES = (
        ('c', 'compost'),
        ('4', '4s'),
        ('b', 'brand'),
        ('q', 'quick repair'),
        ('z', 'direct store')
    )
    area = models.ForeignKey(SupportedArea, null=True)
    brand = models.ManyToManyField('brand', through='ProducerCarSeries',
                                    through_fields=('producer', 'brand'), related_name="producer_series")
    series_quote = models.ManyToManyField('SeriesUpkeepInfo', through='SeriesUpkeepQuote',
                                          through_fields=('producer', 'series_upkeep_info'),
                                          related_name="series_quotes")
    car_quote = models.ManyToManyField('Car', through='CarQuote',
                                       through_fields=('producer', 'car'), related_name="car_quotes")
    information = models.OneToOneField(UserInformation, null=True, on_delete=models.SET_NULL)
    type = models.CharField(max_length=1, default='c', choices=PRODUCER_TYPES)
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=128)
    phone = models.CharField(max_length=16)
    repair_phone = models.CharField(max_length=16)
    full_name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=32)
    last_login = models.DateTimeField(blank=True, null=True)
    last_ip = models.GenericIPAddressField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_recommend = models.BooleanField(default=False)
    creator = models.ForeignKey(HcAdmin, null=True)
    created_at = models.DateTimeField(default=timezone.now)

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True


class Adviser(models.Model):
    """
        供应商服务顾问表
        type: 类型,市场顾问(负责新车买卖),服务顾问(负责维修保养),事故处理(负责钣金做漆)
        weight: 顾问的比重,0~1000,数值越高,排序越靠前
        is_loop: 顾问是否加入询问机制(一旦产生新车,维修保养以及钣金做漆的订单,那么会自动轮询将此订单发送到顾问)
    """
    ADVISER_CHOICES = (
        (0, 'market'),
        (1, 'service'),
        (2, 'fault deal'),
    )
    LEVEL_CHOICES = (
        (0, 'low'),
        (1, 'middle'),
        (2, 'high')
    )
    type = models.IntegerField(default=0, choices=ADVISER_CHOICES)
    producer = models.ForeignKey(Producer, on_delete=models.CASCADE)
    weight = models.IntegerField(default=500)
    is_loop = models.BooleanField(default=True)
    information = models.OneToOneField(UserInformation, null=True, on_delete=models.SET_NULL)
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=128)
    phone = models.CharField(max_length=16)
    level = models.IntegerField(default=1, choices=LEVEL_CHOICES)
    last_login = models.DateTimeField(blank=True, null=True)
    last_ip = models.GenericIPAddressField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class BrandType(models.Model):
    """
        车款生产品牌类型
        name: 名称, 初始化包含:自主 合资 进口
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class BrandLevel(models.Model):
    """
        车款生产品牌类型
        name: 名称, 初始化包含:高端 中端 低端
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class BrandCountry(models.Model):
    """
        平拍国家类型
        name： 国别, 初始化包含： 日系 德系 法系 欧系 美系 国产
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class BrandCallOut(models.Model):
    """
       品牌标注
        name: 初始化为，热门品牌， 日系品牌， 美德法韩， 国产品牌， 高端品牌
    """
    name = models.CharField(max_length=64)
    created_at = models.DateTimeField(default=timezone.now)


class Brand(models.Model):
    """
        品牌表
        parent: 父品牌
        type: 品牌类型
        logo: 拥有一个品牌图标
        name: 名称
        initial: 品牌首字母
        is_hot: 是否为热门品牌
        story: 品牌历史
        logo_story: 品牌图标历史
        created_at: 创建时间
    """
    parent = models.ForeignKey('Brand', null=True)
    type = models.ManyToManyField(BrandType)
    level = models.ForeignKey(BrandLevel)
    country = models.ForeignKey(BrandCountry, null=True)
    logo = models.ForeignKey(Logo, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=16)
    initial = models.CharField(max_length=1)
    is_hot = models.ManyToManyField(BrandCallOut)
    story = models.CharField(max_length=512, null=True)
    logo_story = models.CharField(max_length=512, null=True)
    is_app = models.BooleanField(default=False)
    is_pc = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class SeriesCallOut(models.Model):
    """
        车型标注
        name: 新车， 推荐车， 热门车， 主打车， 猜您喜欢， 平行进口
    """
    name = models.CharField(max_length=64)
    created_at = models.DateTimeField(default=timezone.now)


class SeriesType(models.Model):
    """
        车款生产品牌类型
        name: 名称, 初始化包含:热销 平行进口 主打车
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)



class SeriesKind(models.Model):
    """
        车型中的车辆类型
        name: 轿车 SUV MPV 跑车 面包车 客车
    """
    parent = models.ForeignKey('SeriesKind', null=True)
    name = models.CharField(max_length=64)
    created_at = models.DateTimeField(default=timezone.now)


class SeriesPortalAd(models.Model):
    """
    设置前端八个展示框
    """
    link = models.CharField(max_length=512)
    pic = models.ForeignKey(Picture, on_delete=models.CASCADE, null=True)
    position = models.IntegerField()
    created_at = models.DateTimeField(default=timezone.now)


class SeriesNewCar(models.Model):
    """
        设置新车上市
        预留 位置六个
    """
    name = models.CharField(max_length=32)
    position = models.IntegerField()
    created_at = models.DateTimeField(default=timezone.now)


class SeriesCharacterShow(models.Model):
    """
        前端车型文字投放位置
        row : 行
        row_name: 行名 , 价格区间无
        col: 列
        sheet: 页名
        page: 页数
        created_at: 创建时间
    """
    row = models.IntegerField()
    row_name = models.CharField(max_length=32, null=True)
    col = models.IntegerField()
    sheet = models.CharField(max_length=32)
    page= models.IntegerField()
    about_price = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)


class SeriesPictureShow(models.Model):
    """
        pc前端焦点图片展示
    """
    sheet = models.CharField(max_length=32)
    page= models.IntegerField()
    about_price = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)


class Series(models.Model):
    """
        车系(车型)
        brand: 车系隶属于一个品牌
        logo: 车系拥有一个图标
        name: 名称
        is_hot: 是否为热门车系(车型)
        created_at: 创建时间
        position:前端广告页，对应1-8八个位置，没有为0
        page:前端首页右侧大图展示， 价格相关对应1个页面，12345对应热门车型，热销车，新车，价格相关，suv
        kind: 车型 车辆类型 大小类型
        portal_ad: 前端对应广告位8个
    """
    brand = models.ForeignKey(Brand, null=True)
    type = models.ForeignKey(SeriesType)
    logo = models.ForeignKey(Logo, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=64)
    is_hot = models.ManyToManyField(SeriesCallOut)
    is_app = models.BooleanField(default=False)
    is_pc = models.BooleanField(default=False)
    rel_series = models.ManyToManyField("RelationSeries")
    related_series = models.ManyToManyField('Series', related_name='related_s')
    position = models.IntegerField(default=0, null=True)
    page = models.IntegerField(default=0, null=True)
    kind= models.ForeignKey(SeriesKind, null=True)
    series_ad = models.ForeignKey(SeriesPortalAd, on_delete=models.SET_NULL, null=True)
    new_car = models.ForeignKey(SeriesNewCar, null=True)
    pc_character = models.ForeignKey(SeriesCharacterShow, null=True)
    pc_picture = models.ForeignKey(SeriesPictureShow, null=True)
    created_at = models.DateTimeField(default=timezone.now)


class RelationSeries(models.Model):
    """
    关联车型
    """
    level_one_brand = models.ForeignKey(Brand, null=True, related_name="level_one_brand")
    level_two_brand = models.ForeignKey(Brand, null=True, related_name="level_two_brand")
    detail_series = models.ManyToManyField("Series")
    is_all = models.BooleanField(default=False)


class ProducerCarSeries(models.Model):
    """
        供应商与代理车系(车型)关联表
        type: 类型, 市场车型的代理,维修保养车型的代理,钣金做漆车型的代理
        material_type: 代理材料的类型，能够代理全部、只代理原厂、只代理普通的材料
        created_at: 创建时间
    """
    PRODUCER_SERIES_CHOICES = (
        (0, 'market'),
        (1, 'service'),
        (2, 'fault deal'),
    )
    PRODUCER_MATETIAL_TYPE = (
        (0, 'all'),
        (1, 'origianl'),
        (2, 'general'),
    )
    producer = models.ForeignKey(Producer, on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    type = models.IntegerField(default=0, choices=PRODUCER_SERIES_CHOICES)
    material_type = models.IntegerField(default=0, choices=PRODUCER_MATETIAL_TYPE)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeGrade(models.Model):
    """
        车款级别
        name: 名称, 初始化包含:微型车 小型车 紧凑型车 中型车 中大型车 豪华车 MPV SUV 跑车 面包车 电动车 混动车 皮卡 进口车
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeBody(models.Model):
    """
        车款车身类型
        name: 名称, 初始化包含:两厢 三厢 旅行版
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeEnergy(models.Model):
    """
        车款油类型
        name: 名称, 初始化包含:汽油 柴油 纯电动 油电混合 油气混合
    """
    parent = models.ForeignKey('CarTypeEnergy', null=True)
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeCountry(models.Model):
    """
        车款国别
        name: 名称,初始化包含:德系 日系 韩系 美系 欧系 非日系
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeGearbox(models.Model):
    """
        车款变速箱类型
        name: 名称,初始化包含:手动 半自动（AMT）自动（AT）手自一体 无极变速（CVT）双离合（DSG）
    """
    name = models.CharField(max_length=32)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeCC(models.Model):
    """
        车款排量类型
        name: 名称,初始化包含:1.3L以下 1.3-1.6L 1.7-2L 2.1-3L 3.1-5L 5L以上
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeDrive(models.Model):
    """
        车款发动机类型
        name: 名称,初始化包含:前驱 后驱 全时四驱 分时四驱 适时四驱
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeDischarge(models.Model):
    """
        车款排量标准类型
        name: 名称,初始化包含:国4 国5 京5 欧4 欧5 国4/欧4 国5/欧5
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeDoor(models.Model):
    """
        车款车门数类型
        name: 名称,初始化包含:2-3门 4-6门
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeSeat(models.Model):
    """
        车款座位数类型
        name: 名称,初始化包含:2座 4-5座 6座7座 7座以上
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeExtension(models.Model):
    """
        车款额外配置类型
        name: 名称,初始化包含:天窗 GPS导航 倒车影像 儿童锁 涡轮增压 无钥匙启动 四轮碟刹 真皮座椅 ESP
        氙气大灯 定速巡航 自动空调 胎压监测 自动泊车 空气净化器 换挡拨片 电动座椅 儿童座椅接口
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarTypeColor(models.Model):
    """
        车款颜色类型
        type： 外观颜色 or 内饰颜色
        name: 名称,初始化包含:珍珠白 闪电白 玛瑙红 魔力红 波尔红 晶石黑 冰晶蓝 摩卡棕
        rgd: RGB颜色值
    """
    COLOR_CHOICES = (
        (0, 'outside'),
        (1, 'inside')
    )
    side = models.IntegerField(default=0, choices=COLOR_CHOICES)
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)
    rgb = models.CharField(max_length=16)


class CarTypePicture(models.Model):
    """
        车款图片类型
        name: 名称,初始化包含:外观 内饰 空间 图解 官方 车展
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarItemTypeDetail(models.Model):
    """
        车款详情分类
        name: 名称,初始化包含:基本信息 车体 发动机 变速箱 底盘转向 车轮制动 安全装备 操控配置 外部配置 内部配置 座椅配置 多媒体配置 灯光配置 门窗/后视镜  空调/冰箱 高科技配置
    """
    name = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=timezone.now)


class CarItemDetail(models.Model):
    """
        车款详情项目
        name: 具体项目名称,初始化包含:
        基本参数（厂商、级别、发动机、变速箱、长*宽*高(mm)、车身结构、最高车速(km/h)、官方0-100km/h加速(s)、实测0-100km/h加速(s)、实测0-100km/h制动(s)、实测油耗(L/100km)、认证车主平均油耗(L/100km)、工信部综合油耗(L/100km)）、
        车身（长度(mm)、宽度(mm)、高度(mm)、轴距(mm)、前轮距(mm)、后轮距(mm)、最小离地间隙(mm)、整备质量(kg)、车身结构、车门数(个)、油箱容积(L)、行李厢容积(L)）、
        发动机（发动机型号、排量(ml)、进气形式、气缸排列形式、气缸数（个）、每缸气门数（个）、压缩比、配气机构、缸径（mm）、行程（mm）、最大马力（ps）、最大功率（kw）、最大功率转速（rpm）、最大扭矩转速（rpm）、发动机特有技术、燃料形式、燃油标号、供油方式、缸盖材料、缸体材料、环保标准）、
        变速箱（简称、档位个数、变速箱类型）、
        底盘转向（驱动方式、前悬挂类型、后悬挂类型、主力类型、车体结构）、
        车轮制动（前制动器类型、后制动器类型、驻车制动类型、前轮胎规格、后轮胎规格、备胎规格）、
        安全装备（主/副驾驶座安全气囊、前/后排侧气囊、前/后排头部气囊（气帘）、膝部气囊、胎压监测装置、零胎压继续行驶、安全带未系提示、ISOFIX儿童座椅接口、发动机电子防盗、车内中控锁、遥控钥匙、无钥匙启动系统、无钥匙进入系统）、
        操控配置（ABS防抱死、制动力分配(EBD/CBC等)、刹车辅助(EBA/BAS/BA等)、牵引力控制(ASR/TCS/TRC等)、车身稳定控制(ESC/ESP/DSC等)、上坡辅助、自动驻车、陡坡缓降、可变悬架、空气悬架、可变转向比、前桥限滑差速器/差速锁、中央差速器锁止功能、后桥限滑差速器/差速锁）、
        外部配置（电动天窗、全景天窗、运动外观套件、铝合金轮圈、电动吸合门、侧滑门、电动后备厢、感应后备厢、车顶行李架）、
        内部配置（真皮方向盘、方向盘调节、方向盘电动调节、多功能方向盘、方向盘换挡、方向盘加热、方向盘记忆、定速巡航、前/后驻车雷达、倒车视频影像、行车电脑显示屏、全液晶仪表盘、HUD抬头数字显示）、
        座椅配置（座椅材质、运动风格座椅、座椅高低调节、腰部支撑调节、肩部支撑调节、主/副驾驶座电动调节、第二排靠背角度调节、第二排座椅移动、后排座椅电动调节、电动座椅记忆、前/后排座椅加热、前/后排座椅通风、前/后排座椅按摩、第三排座椅、后排座椅放倒方式、前/后中央扶手、后排杯架）、
        多媒体配置（GPS导航系统、定位互动服务、中控台彩色大屏、蓝牙/车载电话、车载电视、后排液晶屏、220v/230v电源、外接音源接口、CD支持MP3/WMA、多媒体系统、扬声器品牌、扬声器数量）、
        灯光配置（近光灯、远光灯、日间行车灯、自适应远近光、自动头灯、转向辅助灯、转向头灯、前雾灯、大灯高度可调、大灯清洗装置、车内氛围灯）、
        门窗/后视镜（前/后电动车窗、车窗防夹手功能、防紫外线/隔热玻璃、后视镜电动调节、后视镜加热、内/外后视镜自动防炫目、后视镜记忆、后风挡遮阳帘、后排侧遮阳帘、后排侧隐私玻璃、遮阳板化妆镜、后雨刷、感应雨刷）、
        空调/冰箱（空调控制方式、后排独立空调、后座出风口、温度分区控制、车内空气调节/花粉过滤、车载冰箱）、
        高科技配置（自动泊车入位、发动机启停技术、并线辅助、车道偏离预警系统、主动刹车/主动安全系统、整体主动转向系统、夜视系统、中控液晶屏分屏显示、自适应巡航、全景摄像头）
        url: 项目的解释路径,譬如什么叫"最小离地间隙"的一个解释url
    """
    type = models.ForeignKey(CarItemTypeDetail)
    name = models.CharField(max_length=32)
    url = models.CharField(max_length=128)
    created_at = models.DateTimeField(default=timezone.now)


class CarStall(models.Model):
    '''
    排档：4档，5档，6档，7档，8档，9档
    '''
    name = models.CharField(max_length=32)
    created_at = models.DateTimeField(default=timezone.now)


class Car(models.Model):
    """
        车款表
        extension: 车款包含哪些额外配置
        details: 车款详情包含哪些
        pictures: 车款包含哪些图片
        color: 车款包含哪些颜色
        series: 车款属于哪个车系(型)
        grade: 车款级别
        body: 车款车身类型
        energy: 车款发动机油品类型
        country: 车款国别
        gearbox: 车款变速箱类型
        cc: 车款排量类型
        driver: 车款驱动类型
        discharge: 车款排量标准
        door: 车款门数
        seat: 车款座位数
        name: 车款名称
        style: 车款款式
        power: 车款功率
        produce_year: 车款生产年份
        produce_price: 出厂价
        price: 车款指导价
        price_min: 车款报价最低值
        price_max: 车款报价最高值
        is_sell: 车款是否在售
        is_hot: 车款是否是热门车款
        is_new: 是否是新车款
        car_color: 车款禁用颜色 0618修改， 保留原来字段，现在意为车款弃用颜色
    """
    extensions = models.ManyToManyField(CarTypeExtension, related_name="extensions")
    details = models.ManyToManyField(CarItemDetail, through='CarDetail',
                                     through_fields=('car', 'item'), related_name="details")
    pictures = models.ManyToManyField(Picture, through='CarPicture',
                                      through_fields=('car', 'picture'), related_name="pictures")
    colors = models.ManyToManyField(CarTypeColor, related_name="colors")
    series = models.ForeignKey(Series, related_name="series")
    grade = models.ForeignKey(CarTypeGrade, related_name="grade")
    body = models.ForeignKey(CarTypeBody, related_name="body")
    energy = models.ForeignKey(CarTypeEnergy, related_name="energy")
    country = models.ForeignKey(CarTypeCountry, related_name="country")
    gearbox = models.ForeignKey(CarTypeGearbox, related_name="gearbox")
    cc = models.ForeignKey(CarTypeCC, related_name="cc")
    drive = models.ForeignKey(CarTypeDrive, related_name="drive")
    discharge = models.ForeignKey(CarTypeDischarge, related_name="discharge")
    door = models.ForeignKey(CarTypeDoor, related_name="door")
    seat = models.ForeignKey(CarTypeSeat, related_name="seat")
    name = models.CharField(max_length=64)
    style = models.CharField(max_length=64)
    power = models.IntegerField()
    real_cc = models.CharField(max_length=16)
    cc_details = models.FloatField(default=1.0)
    stall = models.ForeignKey(CarStall)
    produce_year = models.TextField()
    produce_price = models.IntegerField(default=1.0)
    price = models.FloatField()
    price_min = models.FloatField()
    price_max = models.FloatField()
    car_color = models.ManyToManyField(CarTypeColor)
    is_sell = models.BooleanField(default=True)
    is_hot = models.BooleanField(default=False)
    is_new = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class CarDetail(models.Model):
    """
        车款详情关联表
        cart: 指明哪个车款
        item: 指明是详情中的哪一项
        value: 车款具体详情项目的值
        option: 项目不是数值类的,则有三个选择,无,标配,选配
    """
    OPTION_CHOICES = (
        ('0', "None"),
        ('1', "Standard"),
        ('2', "Option")
    )
    car = models.ForeignKey(Car)
    item = models.ForeignKey(CarItemDetail)
    value = models.CharField(max_length=64)
    option = models.IntegerField(choices=OPTION_CHOICES, default=1)


class CarPicture(models.Model):
    """
        车款图片关联表
        cart: 指明哪个车款
        picture: 具体哪张图片
        type: 图片归属的类型
        color: 图片归属的颜色
    """
    car = models.ForeignKey(Car)
    picture = models.ForeignKey(Picture)
    type = models.ForeignKey(CarTypePicture, related_name="type", null=True)
    color = models.ForeignKey(CarTypeColor, related_name="color", null=True)
    created_at = models.DateTimeField(default=timezone.now)


class ArticleType(models.Model):
    """
        文章类型、分为两级
        新闻（新闻速递、车型资讯）
        厂商动态测评（安全评测、对比评测、试驾评测）
        说客（说客、驴友、自驾、提车、其他）
        爱车讲堂（新手上路、交通法规、保养常识、驾驶技巧、事故处理技巧）
        商户资讯（新闻速递、车型资讯）

    """
    parent = models.ForeignKey("ArticleType", null=True)
    name = models.CharField(max_length=16)
    created_at = models.DateTimeField(default=timezone.now)


class ArticleStyleType(models.Model):
    """
        文章类型，普通，图示，视频
    """
    name = models.CharField(max_length=30)


class ArticleShow(models.Model):
    """
    文章页面展示, 包括pc前端和app展示, 将所有的展示放到一起,以便查询, 具体区别在type
    name: 展示名称, 包括:
    app轮播, 首页轮播, 焦点轮播, 首页文字推送,
    首页说客, 首页视频, 资讯头条, 资讯首页, 视频首页,
    分站头条,分站轮播, 分站文字推送, 人物访谈
    desc: 相关描述
    position: 对应位置
    type: 类型, 对应name中的相应类别, 1-13
    area: 对应投放区域, 只有部分投放方式需要
    page: 页数,用于轮播
    is_app: 是否用于投放app端
    """
    name = models.CharField(max_length=64)
    desc = models.CharField(max_length=256)
    position = models.IntegerField(null=True)
    type = models.IntegerField(null=True)
    area = models.ForeignKey(SupportedArea, null=True)
    page = models.IntegerField(null=True)
    is_app = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class Article(models.Model):
    """
        文章
        type: 文章属于哪个类型
        brand: 文章属于哪个品牌
        cart: 文章属于哪个车款
        area: 文章隶属于哪个地区
        producer: 文章是哪个供应商创建的
        admin: 文章是哪个管理员创建的
        auditor: 文章是哪个管理员审核通过的
        title: 文章标题
        content: 文章内容
        clicks: 文章浏览次数
        status: 文章状态,一个是未审核,一个是已审核
        created_at: 文章被创建时间
        updated_at: 文章被修改时间
        audited_at: 文章被审核时间
        article_style_type: 样式类型普通，图示，视频
        position:插入轮播图位置，第一二三四页，对应五个位置
        is_show: 轮播
        is_headline: 上头条
        on_top: 置顶
    """
    INDEX_CHOICES = (
        (False, "Not show in index"),
        (True, "Show in index")
    )
    STATUS_CHOICES = (
        (False, "No Audited"),
        (True, "Audited")
    )
    CREATE_TYPES = (
        ("o", "original created"),
        ("r", "reprint")
    )
    STYLE_TYPES = (
        (0, 'Normal'),
        (1, 'Picture'),
        (2, 'Video')
    )
    created_type = models.CharField(max_length=1, default="o", choices=CREATE_TYPES)
    type = models.ForeignKey(ArticleType, null=True)
    brand = models.ForeignKey(Brand, null=True, on_delete=models.SET_NULL)
    # brand = models.ManyToManyField(Brand)
    series = models.ForeignKey(Series, null=True, on_delete=models.SET_NULL)
    area = models.ForeignKey(SupportedArea, null=True)
    producer = models.ForeignKey(Producer, null=True, on_delete=models.SET_NULL)
    admin = models.ForeignKey(HcAdmin, null=True, on_delete=models.SET_NULL, related_name="admin")
    auditor = models.ForeignKey(HcAdmin, null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=128)
    content = models.TextField()
    clicks = models.IntegerField(default=0)
    is_new = models.BooleanField(default=False)
    default_picture = models.ForeignKey(Picture, null=True)
    status = models.BooleanField(default=False, choices=STATUS_CHOICES)
    is_index = models.IntegerField(default=False, choices=INDEX_CHOICES)
    # is_show = models.BooleanField(default=False)
    is_headline = models.BooleanField(default=False)
    position = models.IntegerField(default=0, null=True)
    article_style_type = models.IntegerField(default=0, choices=STYLE_TYPES)
    reprint_addr = models.CharField(max_length=512, default="")
    on_top = models.BooleanField(default=False)
    #
    # app_carousel = models.ForeignKey()
    # index_carousel = models.ForeignKey
    # focus_carousel = models.ForeignKey
    # index_char_push = models.ForeignKey
    # index_customer_say = models.ForeignKey
    # index_video = models.ForeignKey
    # news_headlines = models.ForeignKey
    # news_index = models.ForeignKey
    # video_index = models.ForeignKey
    # sub_site_headlines = models.ForeignKey
    # sub_site_carousel = models.ForeignKey
    # sub_site_push = models.ForeignKey
    # famous_talk = models.ForeignKey
    article_show = models.ManyToManyField(ArticleShow, default=None)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)
    audited_at = models.DateTimeField(default=timezone.now)

    # def delete(self, using=None, keep_parents=False):
    #     pro_sale = ProducerPreSale.objects.filter(article=self)
    #     pr = ProducerPreSale.objects.get(article=self) if pro_sale else None
    #     models.Model.delete(self, using=using, keep_parents=keep_parents)
    #     if pr:
    #         pr.delete()


class Location(models.Model):
    """
        广告位
        index: 所处门户页面名称
        name: 广告位名称
        width: 广告位宽度(单位px)
        height: 广告位高度(单位px)
        is_app: 广告位是否终端app上的
    """
    index = models.CharField(max_length=32)
    name = models.CharField(max_length=32)
    width = models.IntegerField()
    height = models.IntegerField()
    is_app = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class Ad(models.Model):
    """
        广告
        area: 广告隶属于哪个地区
        picture: 广告图片
        location: 广告放在哪个广告位上
        name: 广告名称
        brief: 广告简短描述
        link: 广告点击后的链接地址
        consumer: 广告投资人
        time: 广告展示时长(单位 天)
        is_timeout: 广告是否过期
    """
    area = models.ForeignKey(SupportedArea)
    picture = models.ForeignKey(Picture, on_delete=models.CASCADE)
    location = models.OneToOneField(Location, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=16)
    brief = models.CharField(max_length=64)
    link = models.CharField(max_length=128)
    consumer = models.CharField(max_length=64)
    time = models.IntegerField(default=30)
    is_timeout = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class GroupPurchase(models.Model):
    """
        团购
        area: 团购隶属于哪个地区
        name: 团购名称
        brief: 团购简短描述
        picture: 团购有哪些图片
        article: 团购具体内容,此处以一个文章的形式展示
        is_timeout: 团购是否过期
        time_begin: 团购开始时间
        time_end: 团购结束时间
        status: 团购启动状态
        is_left: 推送大图展示
    """
    series = models.ForeignKey(Series)
    area = models.ForeignKey(SupportedArea)
    name = models.CharField(max_length=32)
    brief = models.CharField(max_length=64)
    picture = models.ManyToManyField(Picture)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True)
    is_timeout = models.BooleanField(default=False)
    time_begin = models.DateTimeField(default=timezone.now)
    time_end = models.DateTimeField()
    status = models.BooleanField(default=False)
    is_left = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class GroupPurchaseUser(models.Model):
    """
        团购参与人信息
        group_purchase: 指明哪一个团购的人数
        name: 参与团购人姓名
        phone: 参与团购人手机号码
        city: 参与团购人城市
        buy_type: 参与团购人购买方式: 前台限制,只能从"全款","贷款","置换"
    """
    group_purchase = models.ForeignKey(GroupPurchase, on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    phone = models.CharField(max_length=16)
    city = models.CharField(max_length=16)
    buy_type = models.CharField(max_length=32)
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepType(models.Model):
    """
        维修保养类型
        name: 类型名称,目前初始化为:维修保养,钣金做漆两个类型

    """
    name = models.CharField(max_length=16)
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepPackage(models.Model):
    """
        维修保养套餐
        type: 属于哪个保养类型
        name: 套餐名称,目前初始化为: 维修保养(节气门清洗、刹车油、空调滤芯、火花塞、雨刮器、后刹车片、
            冷却液、大保养、电瓶、小保养、刹车片、变速箱油),钣金做漆(全车喷漆、全车门板内衬、全车改色、前保险杠、
            发送机舱盖、左前翼子板、右前翼子板、左反光镜、右反光镜、左前门、右前门、做底大边、右底大边、车顶、左后门、
            右后门、左后翼子板、右后翼子板、行李厢盖、后保险杠)
    """
    type = models.ForeignKey(UpkeepType, on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    created_at = models.DateTimeField(default=timezone.now)


class Upkeep(models.Model):
    """
        保养项目
        package: 属于哪个保养套餐
        name: 保养项目名称,初始化为: 节气门清洗(),刹车油()等
        price_low: 低端品牌保养项目的报价,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        price_middle: 中端品牌保养项目的报价,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        price_high: 高端品牌保养项目的报价,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        price_min: 项目报价最低价格,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        price_max: 项目报价最高价格,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        labor_price_original: 原厂工时费指导价
        labor_price_general： 普通工时费知道价格
        labor_price_min: 工时费报价最低价格
        labor_price_max: 工时费报价最高价格
        特殊的部分为机油，在绑定材料的时候还需要添加机油用量
    """
    package = models.ManyToManyField(UpkeepPackage)
    name = models.CharField(max_length=16)
    price_low = models.IntegerField(default=0)
    price_middle = models.IntegerField(default=0)
    price_high = models.IntegerField(default=0)
    price_min = models.IntegerField(default=0)
    price_max = models.IntegerField(default=0)
    labor_price_original = models.IntegerField(default=0)
    labor_price_general = models.IntegerField(default=0)
    labor_price_min = models.IntegerField(default=0)
    labor_price_max = models.IntegerField()
    is_oil = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=timezone.now)


class Oil(models.Model):
    cc = models.CharField(default=1.0, max_length=30)
    capacity = models.CharField(max_length=16)
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepMaterial(models.Model):

    """
        保养材料
        upkeep: 属于哪个保养项目
        picture: 材料的图片
        area: 材料在哪些地区是默认的,如果存在area就说明本材料在这个area上面是默认选择的
        model: 材料的型号
        name: 材料名称
        content: 材料说明,这里目前只支持文字
        type: 代表这个材料是否是原厂"O"还是普通"G"
        price: 材料的指导价
        price_min: 材料报价的最低价格
        price_max: 材料报价的最高价格

        default_area: 默认显示地区 supportedArea foreign key null=true
        default_series: 默认车型

    """

    brand = models.CharField(max_length=32)
    upkeep = models.ForeignKey(Upkeep, on_delete=models.CASCADE)
    picture = models.ForeignKey(Picture, null=True)
    # area = models.ManyToManyField(SupportedArea, related_name="area")
    # model = models.CharField(max_length=64)
    # name = models.CharField(max_length=32)
    # content = models.CharField(max_length=256)
    # type = models.CharField(max_length=1, default="G", choices=TYPE_CHOICES)
    # price = models.IntegerField()
    # price_min = models.IntegerField()
    # price_max = models.IntegerField()
    # default_area = models.ForeignKey(SupportedArea, null=True)
    # default_series = models.ForeignKey(Series, null=True)
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepMaterialLevel(models.Model):
    """
        材料型号适用级别，全部，高端，终端，低端, 指定车型,指定品牌
    """
    name = models.CharField(max_length=30)


class UpkeepMaterialModel(models.Model):
    """
    time_labor_price: 工时指导价
    time_labor_min_price: 最低价
    time_labor_max_price: 最高价

    material_price: 材料指导价
    material_min_price: 最低价
    material_max_price： 最高价
    """
    TYPE_CHOICES = (
        ("G", "GENERAL"),
        ("O", "ORIGINAL")
    )
    picture = models.ForeignKey(Picture, null=True)
    material = models.ForeignKey(UpkeepMaterial)
    level = models.ManyToManyField(UpkeepMaterialLevel)
    code = models.CharField(max_length=30)
    area = models.ManyToManyField(SupportedArea, related_name="area")
    model = models.CharField(max_length=64)
    name = models.CharField(max_length=32)
    content = models.CharField(max_length=256)
    type = models.CharField(max_length=1, default="G", choices=TYPE_CHOICES)

    time_labor_price = models.IntegerField(default=0)
    time_labor_min_price = models.IntegerField(default=0)
    time_labor_max_price = models.IntegerField(default=0)
    material_price = models.IntegerField(default=0)
    material_min_price = models.IntegerField(default=0)
    material_max_price = models.IntegerField(default=0)
    default_area = models.ForeignKey(SupportedArea, null=True)
    default_series = models.ForeignKey(Series, null=True)
    created_at = models.DateTimeField(default=timezone.now)


class CarYear(models.Model):
    year = models.IntegerField()
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepSeries(models.Model):
    """
    车型保养管理
    """
    series = models.ForeignKey(Series)
    produce_year = models.ForeignKey(CarYear, null=True)
    cc = models.CharField(default="1L", max_length=30)
    created_at = models.DateTimeField(default=timezone.now)


class SeriesUpkeepInfo(models.Model):
    """
        车系(车型)保养信息
        series: 指明是哪个车系
        upkeep: 指明是哪个保养项目,用于钣金做漆,因为钣金做漆没有材料的选择,直接针对于项目进行的报价
        upkeep_material: 指明使用的是哪个保养材料
        cc: 指明是哪个排量
        produce_year: 指明是哪个年份
        capacity: 指明材料用量
        all_series: 标记是否为全部车型, True 是全部车型， False 不是全部车型
        更改：删除绑定保养车系、年份、排量，统一报价
    """
    upkeep_series = models.ForeignKey(UpkeepSeries, on_delete=models.CASCADE, null=True)
    all_series = models.BooleanField(default=False)
    series = models.ManyToManyField(Series)
    brands = models.ManyToManyField(Brand)
    # 用于维修保养，材料选择机油
    upkeep_material_model = models.ForeignKey(UpkeepMaterialModel, null=True)
    # 用于钣金做漆，直接对项目
    upkeep = models.ForeignKey(Upkeep, null=True)
    is_quote = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class CustomerInfo(models.Model):
    """
        name: 姓名
        phone: 手机号码
        is_defeat: 是否战败
        status: 状态,处理,未处理
        level: 级别
        sexual:性别
    """
    SEX_CHOICES = (
        (1, '男'),
        (2, '女')
    )
    name = models.CharField(max_length=32)
    phone = models.CharField(max_length=16)
    sexual = models.IntegerField(SEX_CHOICES, default=1)
    user = models.ForeignKey(Users, null=True)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    buy_year = models.IntegerField(default=2000, null=True)
    tag = models.CharField(max_length=16, null=True)


class CarInquiry(models.Model):
    """
        车款询价订单
        cart: 指明是哪款车的询价订单
        area: 指明是在哪个地区进行的询价
        producer: 指明是向哪些厂商进行的询价
        is_token:订单,抢单True,未抢单
        adviser: 指明是向哪些具体服务顾问的询价
        is_order: 是否是订单
        content: 询价描述
        manner: 联系方式
        created_at: 询价创建时间
        resource:来源
        order_created_at: 订单未被处理后，转化成被抢单时的时间,用于抢单排序
    """
    STATUS_CHOICES = (
        (False, "Not Deal"),
        (True, "Done")
    )
    MANNER_CHOICES = (
        ('P', 'phone call'),
        ('T', 'text mail'),
        ('B', 'both')
    )
    LEVEL_CHOICES = (
        ('H', "7"),
        ('A', '15'),
        ('B', '30'),
        ('C', '90'),
    )
    area = models.ForeignKey(SupportedArea, null=True)
    producer = models.ManyToManyField(Producer)
    adviser = models.ForeignKey(Adviser, null=True)
    content = models.CharField(max_length=256, null=True)
    is_order = models.BooleanField(default=False)
    is_defeat = models.BooleanField(default=False)
    is_token = models.BooleanField(default=True)
    status = models.BooleanField(default=False, choices=STATUS_CHOICES)
    resource = models.CharField(max_length=16, default='huiche', null=True)
    record_date = models.DateTimeField(null=True)
    level = models.CharField(choices=LEVEL_CHOICES, max_length=8, default='H', null=True)
    created_at = models.DateTimeField(default=timezone.now)
    order_created_at = models.DateTimeField(null=True)
    manner = models.CharField(max_length=8, choices=MANNER_CHOICES, default='P')
    customer = models.ForeignKey(CustomerInfo, null=True)


class CarUpkeepInquiry(models.Model):
    """
        车款保养订单
        cart: 指明是哪款车的保养订单
        area: 指明是在哪个地区进行的保养提交
        producer: 指明是向哪些厂商进行的询价
        adviser: 指明是向哪些具体服务顾问的询价
        name: 姓名
        phone: 手机号码
        status: 状态,处理,未处理
        token:订单,抢单,未抢单
        content: 询价描述
        created_at: 询价创建时间
    """
    STATUS_CHOICES = (
        (False, "Not Deal"),
        (True, "Done")
    )
    MANNER_CHOICES = (
        ('P', 'phone call'),
        ('T', 'text mail'),
        ('B', 'both')
    )

    LEVEL_CHOICES = (
        ('H', "7"),
        ('A', '15'),
        ('B', '30'),
        ('C', '90'),
    )
    type = models.ForeignKey(UpkeepType, on_delete=models.CASCADE)
    upkeep = models.ManyToManyField(Upkeep)
    upkeep_material = models.ManyToManyField(UpkeepMaterial)
    value = models.FloatField(default=0)
    is_token = models.BooleanField(default=True)
    area = models.ForeignKey(SupportedArea, on_delete=models.CASCADE)
    status = models.BooleanField(default=False, choices=STATUS_CHOICES)
    is_order = models.BooleanField(default=False)
    is_defeat = models.BooleanField(default=False)
    record_date = models.DateTimeField(null=True)
    level = models.CharField(choices=LEVEL_CHOICES, max_length=8, default='H', null=True)
    producer = models.ManyToManyField(Producer)
    order_created_at = models.DateTimeField(null=True)
    adviser = models.ForeignKey(Adviser, null=True)
    resource = models.CharField(max_length=16, default='huiche', null=True)
    manner = models.CharField(max_length=8, choices=MANNER_CHOICES, default='P')
    content = models.CharField(max_length=256, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    customer = models.ForeignKey(CustomerInfo, null=True)

#
# class Textmail(models.Model):
#     """
#     短信内容
#     status:使用/未使用
#     text: 短信内容
#     """
#     USE_CHOICES = (
#         (1, "buy_car"),
#         (2, "fix_upkeep"),
#         (3, "paint_upkeep"),
#         (4, "insurance"),
#     )
#     status = models.BooleanField(default=False)
#     text = models.TextField(max_length=256)
#     usefor = models.IntegerField(USE_CHOICES, default=1)
#     created_at = models.DateTimeField(default=timezone.now)


class SeriesUpkeepQuote(models.Model):
    """
        车系保养报价
        series_upkeep_material: 指明是哪个车系的类型进行的报价
        producer: 指明是哪个供应商进行的报价
        labor: 供应商工时费的报价
        price: 供应商的材料报价
        weight: 供应商报价比重,0~1000,数值越高,排序越靠前
        created_at: 报价时间
        sale_prie_low/middle/high: 相对板金的低中高报价
        sale_labor_price:相对优惠工时报价
        sale_material_price: 相对优惠材料报价
        sale_price_low:钣金喷漆， 低端报价
        sale_price_middle:钣金喷漆， 中端报价
        sale_price_high: 钣金喷漆， 高端报价
        sale_labor_price: 维修保养， 工时报价
        sale_material_price: 维修保养， 材料报价
    """

    series_upkeep_info = models.ForeignKey(SeriesUpkeepInfo, on_delete=models.CASCADE)
    producer = models.ForeignKey(Producer)
    package = models.ForeignKey(UpkeepPackage)
    # labor = models.IntegerField(null=True)
    # sale_labor = models.IntegerField(null=True)
    # sale_price = models.IntegerField(null=True)
    # price = models.IntegerField(null=True)
    sale_price_low = models.IntegerField(null=True)
    sale_price_middle = models.IntegerField(null=True)
    sale_price_high = models.IntegerField(null=True)
    sale_labor_price = models.IntegerField(null=True)
    sale_material_price = models.IntegerField(null=True)
    weight = models.IntegerField(default=500)
    created_at = models.DateTimeField(default=timezone.now)


class CarQuote(models.Model):
    """
        车款报价
        cart: 指明是哪个车款的报价
        producer: 指明是哪个供应商进行的报价
        price: 供应商的报价
        weight: 供应商报价比重,0~1000,数值越高,排序越靠前
        created_at: 报价时间
    """
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    producer = models.ForeignKey(Producer, related_name="producer")
    price = models.FloatField()
    mail_price = models.FloatField(default=0)
    sale_price = models.FloatField(default=0)
    weight = models.IntegerField(default=500)
    mail_time = models.DateTimeField(null=True)
    sale_time = models.DateTimeField(null=True)
    created_at = models.DateTimeField(default=timezone.now)


class UserToken(models.Model):
    """
        app登录token，目前不考虑中间人攻击的情况
        user 绑定的用户
        token 临时token
        timeout_at 超时时间
        status 是否可用
    """
    user = models.ForeignKey(Users)
    token = models.CharField(max_length=64)
    timeout_at = models.DateTimeField()
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class AdviserToken(models.Model):
    """
        app登录token，目前不考虑中间人攻击的情况
        user 绑定的用户
        token 临时token
        timeout_at 超时时间
        status 是否可用
    """
    user = models.ForeignKey(Adviser)
    token = models.CharField(max_length=64)
    timeout_at = models.DateTimeField()
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(default=timezone.now)


class FeedBack(models.Model):
    user = models.ForeignKey(Users)
    content = models.TextField()
    created_at = models.DateTimeField(default=timezone.now)


class UpkeepOrders(models.Model):
    """
        name: 用户名
        phone: 电话
        car_license: 车牌号
        payment: 付费方式， 0 自费； 1 保险
        adviser: 保养顾问
        in_time: 来店时间
        out_time： 取车时间
        user: 提交订单的用户信息
        status: 状态,处理,未处理
        series: 保养订单针对的车
    """
    STATUS_CHOICES = (
        (False, "Not Deal"),
        (True, "Done")
    )
    name = models.CharField(max_length=64)
    phone = models.CharField(max_length=32)
    car_license = models.CharField(max_length=24)
    payment = models.BooleanField(default=0)
    adviser = models.ForeignKey(Adviser, null=True)
    in_time = models.DateField()
    out_time = models.DateField(null=True)
    user = models.ForeignKey(Users, null=True)
    upkeeps_pkgs = models.ManyToManyField(UpkeepPackage)
    material_model = models.ManyToManyField(UpkeepMaterialModel)
    upkeep_series = models.ForeignKey(UpkeepSeries, null=True, on_delete=models.CASCADE)
    producer = models.ForeignKey(Producer, null=True)
    status = models.BooleanField(default=False, choices=STATUS_CHOICES)

#
# class Insurance(models.Model):
#     owner_infor = models.ManyToManyField(OwnerInformation)
#     type = models.ManyToManyField(InsuranceType)
#     name = models.CharField()
#     price = models.FloatField()
#     detail = models.TextField()
#     time_begin = models.DateTimeField(default=timezone.now())
#
#
# class OwnerInformation(models.Model):
#     name = models.CharField()
#     phone = models.IntegerField()
#     tag = models.CharField()
#     used_car = models.ForeignKey(Car)
#     ex_company = models.ForeignKey(Company)
#     time_end = models.DateTimeField(default=timezone.now())
#     created_at = models.DateTimeField(default=timezone.now())
#
#
# class InsuranceType(models.Model):
#     name = models.CharField()
#
#
# class Favorable(models.Model):
#     method  = models.CharField()
#     time_begin = models.DateTimeField()
#     time_end = models.DateTimeField()
#
#
# class Company(models.Model):
#     name = models.CharField()
#     Insurance = models.ManyToManyField(Insurance)
#     logo = models.ForeignKey(Picture)
#     favor = models.ManyToManyField(Favorable)


class InsuranceCompany(models.Model):
    name = models.CharField(max_length=64)
    logo = models.ForeignKey(Picture)


class InsuranceActivity(models.Model):
    name = models.CharField(max_length=64)
    content = models.CharField(max_length=256)
    company = models.ForeignKey(InsuranceCompany)


class BrandUpkeepArea(models.Model):
    """
    车型品牌 使用的材料 在各区域置顶操作
    brand：车型品牌
    area：区域
    upkeep_material_model：材料
    """
    brand = models.ForeignKey(Brand)
    area = models.ForeignKey(SupportedArea)
    upkeep_material_model = models.ForeignKey(UpkeepMaterialModel)


class SeriesYearColor(models.Model):
    """
    车型年份颜色管理
    series: 车型
    year： 车款年份
    color: 年份车款颜色
    """
    series = models.ForeignKey(Series)
    year = models.TextField()
    color = models.ManyToManyField(CarTypeColor)
    created_at = models.DateTimeField(default=timezone.now)


class GiftBagType(models.Model):
    """
        gift_name:礼包类型名称
        has_way: 是否含有实现方式
    """
    HAS_WAY_CHOICES =(
        (True, "true"),
        (False, "false")
    )
    gift_name = models.CharField(max_length=16)
    has_way = models.BooleanField()
    created_at = models.DateTimeField(default=timezone.now)



# class GiftBagValue(models.Model):
#     """
#         gift_type: 礼包类型
#         ways： 礼包兑现方式
#         text_value: 对应值
#     """
#     gift_type = models.ForeignKey(GiftBagType)
#     ways = models.IntegerField(null=True)
#     text_value = models.CharField(max_length=512, null=True)
#     created_at = models.DateTimeField(default=timezone.now)


class GiftBag(models.Model):
    """
        car_good： 随车赠品 有无
        gift_value: 礼包总价值（元）
        oil_card: 油卡，具体表示为金额价值，单位：元
        business_insurance： 商业险，单位年
        traffic_insurance： 交强险， 单位年
        buy_tax： 增值税，单选100% 或50%
        upkeep_way: 赠送的保养方式
        upkeep_val： 保养具体值，多值时用/ 隔开
        else_sale： 其他促销事项
    """
    # gift_bag_type = models.ManyToManyField(GiftBagValue)
    BUY_TAX_CHOICE = (
        (1,"50%"),
        (2,"100%")
    )
    UP_WAYS = (
        (1, "money"),
        (2, "times"),
        (3, "year divide 10kkm")
    )
    car_good = models.BooleanField(default=False)
    gift_value = models.FloatField()
    oil_card = models.FloatField(null=True)
    business_insurance = models.IntegerField(null=True)
    traffic_insurance = models.IntegerField(null = True)
    buy_tax = models.IntegerField(null=True, choices=BUY_TAX_CHOICE)
    upkeep_way = models.IntegerField(null=True, choices=UP_WAYS)
    upkeep_val = models.FloatField(null=True)
    else_sale = models.TextField(null=True)
    created_at = models.DateTimeField(default=timezone.now)


class SaleCarAndColor(models.Model):
    """
        car ： 促销车款
        color： 促销车款颜色
        inventory： 库存状态
    """
    INVENTORY_CHOICES = (
        (0, "shortage"),
        (1, "need order"),
        (2, "enough car")
    )
    car = models.ForeignKey(Car)
    color = models.ManyToManyField(CarTypeColor)
    origin_price = models.FloatField(null=True)
    discount_price = models.FloatField(null=True)
    finaly_price = models.FloatField(null=True)
    inventory = models.IntegerField(choices=INVENTORY_CHOICES)
    created_at = models.DateTimeField(default=timezone.now)


class SaleSeriesAndColor(models.Model):
    """
        series： 外键 经销商代理车型
        year： 选定车款生产年份
        series_color： 车型选定颜色
        car_and_color： 促销车款， 及库存状态， 车款颜色
    """
    series = models.ForeignKey(Series)
    # year = models.CharField(max_length=256)
    # series_color = models.ManyToManyField(CarTypeColor, null=True)
    car_and_color = models.ManyToManyField(SaleCarAndColor)
    created_at = models.DateTimeField(default=timezone.now)


class ProducerPreSale(models.Model):
    """
        producer : 关联经销商
        series：外键 选定促销车型
        sale_start ： 促销开始时间
        sale_end: 促销结束时间
        sale_style： 促销方式
        sale_num： 促销数额，金额/折扣
        gift： 是否赠送礼包
        keep_up_in_buttom： 底部保留保养信息
        company_adr_in_buttom： 底部显示公司地址
        map_in_buttom： 底部显示公司地图
        sale_tel_in_buttom： 底部显示促销电话
        hd_pic： 高清大图
        small_pic： 小图 四张
        article： 生成文章 一对一关联
    """
    SALE_STYLE_CHOICES = (
        (1, 'by reduce money'),
        (2, 'by discount')
    )
    producer = models.ForeignKey(Producer)
    bind_series = models.ForeignKey(SaleSeriesAndColor, on_delete=models.SET_NULL, null=True)
    sale_start = models.DateTimeField(default=timezone.now)
    sale_end = models.DateTimeField(default=timezone.now)
    # sale_style = models.IntegerField(choices=SALE_STYLE_CHOICES)
    # sale_num = models.FloatField()
    gift = models.ForeignKey(GiftBag, null=True, on_delete=models.SET_NULL)
    keep_up_in_buttom = models.BooleanField(default=True)
    company_adr_in_buttom = models.BooleanField(default=True)
    map_in_buttom = models.BooleanField(default=True)
    sale_tel_in_buttom = models.BooleanField(default=True)
    hd_pic = models.ForeignKey(Picture, related_name="hd_pic", null=True, on_delete=models.SET_NULL)
    small_pic = models.ManyToManyField(Picture, related_name="small_pic")
    title = models.TextField(null=True)
    introduction = models.TextField(null=True)
    article = models.OneToOneField(Article, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)

    def delete(self, using=None, keep_parents=False):
    #     if self.bind_series:
    #         for obj in self.bind_series.car_and_color.all():
    #             obj.color.clear()
    #             obj.delete()
    #         self.bind_series.delete()
    #     # self.bind_series.car_and_color.delete()
        article = self.article if self.article else None
    #     self.small_pic.clear()
        models.Model.delete(self, using=using, keep_parents=keep_parents)
        if article:
            article.delete()





@receiver(pre_delete, sender=Article)
def delete_sale(sender, **kwargs):
    article = kwargs['instance']
    ProducerPreSale.objects.filter(article=article).delete()
    # print u"删除文章顺便删除促销信息"


@receiver(pre_delete, sender=ProducerPreSale)
def delete_sale(sender, **kwargs):
    pre_sale = kwargs['instance']
    if pre_sale.bind_series:
        for obj in pre_sale.bind_series.car_and_color.all():
            obj.color.clear()
            obj.delete()
        pre_sale.bind_series.delete()
    # article = pre_sale.article if pre_sale.article else None
    pre_sale.small_pic.clear()
