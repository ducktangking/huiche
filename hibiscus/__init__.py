# -*- coding:utf-8 -*-
__author__ = 'tomjun'
__version__ = '1.0.1'
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

LOCALE_PATHS = [os.path.join(BASE_DIR, "locale")]