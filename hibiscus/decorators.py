# -*- coding:utf-8 -*-
__author__ = 'tom'

import simplejson
from functools import wraps
from django.utils.decorators import available_attrs
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.conf import settings
from hibiscus.models import UserToken, AdviserToken
from django.db.models import Q


def data_table(columns, model):

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            table_data = {}
            table_data["start"] = int(request.GET["start"])
            table_data["length"] = request.GET["length"]
            table_data["draw"] = request.GET["draw"]
            table_data["filter_data"] = request.GET["search[value]"]
            table_data["order_cloumn"] = int(request.GET["order[0][column]"])
            table_data["order_dir"] = request.GET["order[0][dir]"]
            if table_data["order_dir"] == "desc":
                table_data["order_cl"] = "-" + columns[table_data["order_cloumn"]]
            else:
                table_data["order_cl"] = columns[table_data["order_cloumn"]]
            table_data["end"] = table_data["start"] + int(table_data["length"])
            if table_data["filter_data"]:
                table_data["key_word"] = table_data["filter_data"]
            else:
                table_data["key_word"] = ""
            request.table_data = table_data

            table_st = {}
            data = view_func(request, *args, **kwargs)
            if type(data) == tuple:
                table_st["draw"] = request.table_data["draw"]
                table_st["recordsTotal"] = len(data[0])
                table_st["recordsFiltered"] = data[1]
                table_st["data"] = data[0]
            else:
                table_st["draw"] = request.table_data["draw"]
                table_st["recordsTotal"] = len(data)
                table_st["recordsFiltered"] = model.objects.count()
                table_st["data"] = data
            return HttpResponse(simplejson.dumps(table_st))
        return _wrapped_view
    return decorator


def page(func):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        size = int(request.GET.get("size", "20"))
        page = int(request.GET.get("page", "1"))
        res = func(request, *args, **kwargs)

        p = Paginator(res, size)
        cur = p.page(page)
        return {"page": {
                    "size": p.per_page,
                    "totalPages": p.num_pages,
                    "totalElements": p.count,
                    "number": page
                }, "data": cur.object_list}
    return inner


def singleton(cls, *args, **kw):
    instances = {}

    def _singleton():
        if cls not in instances:
            instances[cls] = cls(*args, **kw)
        return instances[cls]
    return _singleton


def require_app_login(func):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        res = {"res": "fail"}
        token = request.META.get(settings.HUICHE_X_TOKEN, None)
        if token:
            user_token = UserToken.objects.filter(Q(token=token), Q(status=True))
            if user_token:
                user = user_token[0].user
                request.app_user = user
                request.app_token = user_token[0]
                return func(request, *args, **kwargs)
            else:
                res["description"] = "用户登录超时"
        else:
            res["description"] = "用户未登录"
        return res
    return inner


def login_info(func):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        token = request.META.get(settings.HUICHE_X_TOKEN, None)
        if token:
            user_token = UserToken.objects.filter(Q(token=token), Q(status=True))
            if user_token:
                user = user_token[0].user
                request.app_user = user
                request.app_token = user_token[0]
        return func(request, *args, **kwargs)
    return inner


def require_adviser_login(func):
    @wraps(func, assigned=available_attrs(func))
    def inner(request, *args, **kwargs):
        res = {"res": "fail"}
        token = request.META.get(settings.HUICHE_X_TOKEN, None)
        if token:
            user_token = AdviserToken.objects.filter(Q(token=token), Q(status=True))
            if user_token:
                user = user_token[0].user
                request.app_user = user
                request.app_token = user_token[0]
                return func(request, *args, **kwargs)
            else:
                res["description"] = "用户登录超时"
        else:
            res["description"] = "用户未登录"
        return res
    return inner


def user_authorized(request):
    res = False
    token = request.META.get(settings.HUICHE_X_TOKEN, None)
    if token:
        user_token = UserToken.objects.filter(Q(token=token), Q(status=True))
        if user_token:
            res = True
            user = user_token[0].user
            request.app_user = user
            return res, user
    return res, None