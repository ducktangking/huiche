function adBind(o) {
    var _url = $(o).attr("href");
    var _aId = $("#aId").val();
    var _lId = $(o).attr("lId");
    if(confirm("确认投放？注意：如果所选广告位已经投放其他广告，则会覆盖！")){
        $.ajax({
            url: _url,
            method: "POST",
            data: {"aId": _aId, "lId": _lId},
            success: function(data){
                if(data == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
        });
    }
    return false;
}

$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [
            {
                "targets": 4,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["status"]){
                        return "已投放";
                    }else{
                        return "未投放";
                    }
                }
            },
            {
                "targets": 6,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    var txt = "<a onclick='return adBind(this)' href='/background/ad/adetail/bind/bind/' lId="+row['DT_RowId']+">绑定</a>";
                    return txt;
                }
            }
        ],
        "columns": [
                    { "data": "index" },
                    { "data": "name" },
                    { "data": "width"},
                    { "data": "height"},
                    { "data": "status"},
                    { "data": "created_at" },
                    { "data": "action" }
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
})