$(document).ready(function () {
     var api;
    var selected = [];
    var _url = $("#adminNewCarQuote").val();
    $('#carQuoteTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET'
        },
        "columnDefs":[{
            "targets": 6,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                return "<a href='#' onclick='set_weight("+row['DT_RowId']+")'>设置权重</a>";
            }
        }],
        "columns": [
                    { "data": "brand_name" },
                    { "data": "series_name" },
                    {"data": "car_name"},
                    {"data": "producer_username"},
                    {"data": "price"},
                    {"data": "weight"},
                    {"data": "action"}
        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });

    $('#quoteWeightSub').click(function () {
        $.ajax({
            url: $("#carQuoteWeight").val(),
            method: "post",
            dataType: "json",
            data: $("#weightSetting").serialize(),
            success: function(data){
                if(data['res'] == "success"){
                    window.location.href = $("#adminCarQuoteIndex").val();
                }
            }
        });
    });
});