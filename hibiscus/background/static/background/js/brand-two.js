/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    var api;
    var selected = [];
    var brandId = $("#brandId").val();
    var _url = $("#dataAction").attr("action");
    $('#brandTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET',
            data: {"brandId": brandId}
        },
        "columnDefs": [
            {
                "targets": 4,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><img  style='width:128px; height:128px;' src='"+row["pic_path"]+"' /></div>";
                }
            },{
                "targets": 5,
                "ordering": false,
                "orderable": false,
            },{
                "targets": 6,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    if(row['is_app']){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 7,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    if(row['is_pc']){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },
            {
                "targets": 8,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    var two_brand = "";
                    var is_app = "";
                    var is_pc = "";
                    var series_txt = "<a href='/background/series/?brandId="+row["DT_RowId"]+"&SecondMenu=seriesManage' " +
                        "title='Car Series'>车型管理</a>&nbsp;&nbsp;&nbsp;";
                    if(row["parent"] == "no"){
                        two_brand = "<a href='/background/brand/two/index/?brandId="+row["DT_RowId"]+"' " +
                            "title='Two Brand'>二级品牌管理</a>&nbsp;&nbsp;&nbsp;"
                    }
                    //  if(row['parent'] != "no"){
                    //     if(!row['is_pc']){
                    //         is_app = "<a href='#' onclick='brand_set_pc("+row["DT_RowId"]+")'>设置pc</a>&nbsp;&nbsp;&nbsp;";
                    //     }else{
                    //         is_app = "<a href='#' onclick='brand_set_pc("+row["DT_RowId"]+")'>取消pc</a>&nbsp;&nbsp;&nbsp;";
                    //     }
                    //     if(!row['is_app']){
                    //         is_pc = "<a href='#' onclick='brand_set_app("+row['DT_RowId']+")'>设置app</a>&nbsp;&nbsp;&nbsp;";
                    //     }else{
                    //         is_pc = "<a href='#' onclick='brand_set_app("+row['DT_RowId']+")'>取消app</a>&nbsp;&nbsp;&nbsp;";
                    //     }
                    // }
                    var edit_url = "<a href='/background/brand/edit/?brandId="+row["DT_RowId"]+"'>编辑</a>&nbsp;&nbsp;&nbsp;<br/>";
                    return series_txt + two_brand + edit_url + is_app + is_pc;
                }
            }
        ],
        "columns": [
                    { "data": "initial" },
                    { "data": "name" },
                    {"data": "type"},
                    {"data": "brand_level"},
                    {"data": "pic_path"},
                    {"data": "parent"},
            {"data": "is_app"},
            {"data": "is_pc"}
                    //{"data": "pic_path"}
        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });
    $('#brandTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#brandTable').dataTable().api();
    $("#delBrand").click(function(){
        if(selected.length == 0){
            $("#delBrandModal").modal('show');
        }else{
            $("#brandModel").modal('show');
        }
    });

    $("#delBrandForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#delBrandItems").val(ids);
            var _url = $("#brandDelAction").attr("action");
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#delBrandForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#brandModel").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("a[class='chr_search']").each(function () {
        $(this).click(function() {
            $("a[class*='on']").each(function () {
               $(this).removeClass('on');
            });
            $(this).addClass("on");
            $('#brandTable').dataTable().api().search($(this).text()).draw();
        });
    });



});