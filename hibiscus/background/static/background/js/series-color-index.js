/**
 * Created by lf on 17-6-11.
 */
$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var series_id = $("#series_id").val();

    $('#DataTable').dataTable({
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"series_id": series_id}
        },
        "language": {
            "url": $("#lanFile").val()
        },
        "columnDefs": [
            {
                "targets": 0,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row) {
                    return "<div>" + row['year'] + "</div>"
                }
            },{
                "targets": 1,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row){
                    if (row['side'] == 1){

                        return "<div id='side_"+ row['DT_RowId'] +"' side='" + row['side']+ "'>内饰</div>";
                    }else{
                        return "<div id='side_"+ row['DT_RowId'] +"' side='" + row['side']+ "'>外观</div>";
                    }
                }
            },{
                "targets": 2,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row){
                    console.log(row['color'][0]);
                    var color_list = "";
                    for(var i=0;i <row['color'].length; i++){
                        var color_name = row['color'][i]['name'];
                        var color_id = row['color'][i]['id'];
                        var color_rgb = row['color'][i]['rgb'];
                        color_list += "<i>"+ color_name +"</i><div style='display:inline-block; height:15px;width:15px;background-color: "+color_rgb+"'></div>&nbsp;&nbsp;";
                    }
                    return "<div style='display: inline'>"+ color_list +"</div>";
                }
            },{
            "targets": 3,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row){
                    return row['created_at']
                }
            },{
            "targets": 4,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row){
                    return "<a href='"+ $("#color_edit_url").val() +
                        "?side="+row['side']+"&series_year_id="+row['DT_RowId']+"'>编辑</a>"
                }
            }
        ],
        "columns": [
            {"data": "year"},
            {"data":"side"},
            {"data": "color"},
            {"data": "created_at"},
            // {"data":"row_id"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function (row, data) {
            console.log(data);
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');

            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if (index === -1) {
            selected.push(id);
        } else {
            selected.splice(index, 1);
        }
        console.log(selected);
        $(this).toggleClass('selected');
    });

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function () {
        if (selected.length == 0) {
            $("#ConfirmModal").modal('show');
        } else {
            $("#DeleteModal").modal('show');
        }
    });

      $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                var side = $("#side_"+selected[i]).attr("side");
                ids += side + "," + selected[i] + "_";
            }
            console.log(ids);
            $("#DeleteItems").val(ids);
            var _url = $("#color_delete_url").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteItems").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);
                    }
                }
            });
            return false;
        }

    });


});