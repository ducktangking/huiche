$(document).ready(function () {
    var api;

    var selected = [];
    var _url = $("#upkeepQuoteIndex").val();
    console.log(_url);
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 3,
                "ordering": false,
                "searching": false,
                "orderable": false,
                "render": function(data, type, row){
                    if(row["upkeep_type_id"] == "all"){
                         var t = "<a href='/background/user/producer/upkeep/quote/list/?type_id=1&upkeep_series_id="+row["DT_RowId"]+"'" +">维修报价</a>&nbsp;&nbsp;&nbsp;";
                         var t1 = "<a href='/background/user/producer/upkeep/quote/list/?type_id=2&upkeep_series_id="+row["DT_RowId"]+"'" +">板金报价</a>&nbsp;&nbsp;&nbsp;";
                         return t + t1;
                    }else if(row["upkeep_type_id"] == 1){
                         var t = "<a href='/background/user/producer/upkeep/quote/list/?type_id=1&upkeep_series_id="+row["DT_RowId"]+"'" +">维修报价</a>&nbsp;&nbsp;&nbsp;";
                          return t;
                    }else{
                        var t1 = "<a href='/background/user/producer/upkeep/quote/list/?type_id=2&upkeep_series_id="+row["DT_RowId"]+"'" +">板金报价</a>&nbsp;&nbsp;&nbsp;";
                        return t1;
                    }
                }
            }
        ],
        "columns": [
            // {"data": "brand_name"},
            {"data": "series_name"},
            {"data": "produce_year"},
            { "data": "cc" },
            {"data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
});