/**
 * Created by lf on 16-12-3.
 */
$(document).ready(function () {
    pkg_detail();
    if($('#typeId').val()==1){
       /* var tx = $('tbody').html();
        console.log(tx);*/
/*
        var num = null;
        for (var i=1;i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var pkg_id = tr_obj.attr('model');
            if(pkg_id == num){
            continue;}
            else{
                var pkg_name = tr_obj.find("input[class='pkgName']").val();
                /!*var up_len = tr_obj.find("input[class='upLength']").val();
                var ma_len = tr_obj.find("input[class='maLength']").val();*!/
                var j=0;
                $('tbody').find("tr[class='model_"+pkg_id+"']").each(function () {
                    j+=1;
                });
                if(j!=0){
                var txt = "<td rowspan='"+j+"'>"+pkg_name+"</td>";
                $('tbody').find("tr[class='model_"+pkg_id+"']").eq(0).prepend(txt);
                var txt2 = "<td rowspan='"+j+"' style='vertical-align: middle'><input type='button' id='btnUpkeepQuote_"+pkg_id+"' onclick='subUpkeepQuote(this)' class='btn btn-primary' value='提交'></td>";
                $('tbody').find("tr[class='model_"+pkg_id+"']").eq(0).append(txt2);
                }
                num = pkg_id;
                }
        }
*/
        var u_num = -1;
        var m_num = -1;
        for (var i=1;i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var upkeep_id = tr_obj.attr('upkeepId');
            var material_id = tr_obj.attr('materialId');
            if(material_id == m_num){
                continue
            }
            else{
                var upkeep_name = tr_obj.attr('upkeepName');
                var material_brand = tr_obj.attr('materialbrand');
                var sum_len = tr_obj.attr('sumLen');
                var ma_len = tr_obj.attr('maLen');
                var txt1 = "<td rowspan='"+ ma_len +"' valign='middle'>"+ material_brand +"</td>";
                tr_obj.prepend(txt1);
                if(upkeep_id == u_num){
                    continue
                }else {
                    var txt2 = "<td rowspan='"+ sum_len +"' valign='middle'>"+ upkeep_name +"</td>";
                    tr_obj.prepend(txt2);
                    u_num = upkeep_id;
                }
                m_num = material_id;
            }
        }
    }else{
        var num = null;
        for (var i=1;i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var pkg_id = tr_obj.attr('banjin');
            if(pkg_id == num){
            continue;}
            else{
                var pkg_name = tr_obj.find("input[class='pkgName']").val();
                /*var up_len = tr_obj.find("input[class='upLength']").val();
                var ma_len = tr_obj.find("input[class='maLength']").val();*/
                var j=0;
                $('tbody').find("tr[class='banjin_"+pkg_id+"']").each(function () {
                    j+=1;
                });
                if(j!=0){
                var txt = "<td rowspan='"+j+"'>"+pkg_name+"</td>";
                $('tbody').find("tr[class='banjin_"+pkg_id+"']").eq(0).prepend(txt);
                var txt2 = "<td rowspan='"+j+"'  style='vertical-align: middle'><input type='button' id='btnUpkeepQuote_"+pkg_id+"' onclick='subUpkeepQuotebanjin(this)' class='btn btn-primary' value='提交'></td>";
                $('tbody').find("tr[class='banjin_"+pkg_id+"']").eq(0).append(txt2);
                }
                num = pkg_id;

            }
        }
    }
    $('#batchQuote').click(function () {$('#batchQuoteDiv').toggle();});
    $('#laborTimeSure').click(function () {
        var val = $('#laborTimeVal').val();
        if(val){
            if($('#laborTimeChiose').val()==1){
                $('tbody').find("td[class='laborTimePrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide - val;
                    $(this).parent().find("input[name='sale_labor_price']").val(parseInt(sale));
                })
            }else{
                $('tbody').find("td[class='laborTimePrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide * val / 100 ;
                    $(this).parent().find("input[name='sale_labor_price']").val(parseInt(sale));
                })
            }
        }
    });
    $('#materialSure').click(function () {
        var val = $('#materialVal').val();
        if(val){
            if($('#materialChiose').val()==1){
                $('tbody').find("td[class='materialPrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide - val;
                    $(this).parent().find("input[name='sale_material_price']").val(parseInt(sale));
                })
            }else{
                $('tbody').find("td[class='materialPrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide * val/100;
                    $(this).parent().find("input[name='sale_material_price']").val(parseInt(sale));
                })
            }
        }
    });

    $('#paintQuoteSure').click(function () {
        var val = $('#paintQuoteVal').val();
        if(val){
            if($('#paintQuoteChioce').val() == 1){
                $('tbody').find("td[class='paintGuidePrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide - val;
                    $(this).parent().find("input[class='sale_price']").val(parseInt(sale));
                });
            }else {
                $('tbody').find("td[class='paintGuidePrice']").each(function () {
                    var guide = $(this).text();
                    var sale = guide * val / 100;
                    $(this).parent().find("input[class='sale_price']").val(parseInt(sale));
                });
            }
        }
    });

    judge_remind_text1();
    judge_remind_text2()
    $('#laborTimeChiose , #materialChiose').change(function () {judge_remind_text1();});
    $('#paintQuoteChioce').change(function () {judge_remind_text2()});
    
    
/*    $('#batchQuoteSure1').click(function () {
        var num = null;
        var data_list = '';
        for (var i=1;i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var pkg_id = tr_obj.attr('model');
            if(pkg_id == num){
            continue;}
            else{
                var form = $('tbody').find("tr[class='model_"+pkg_id+"']");
                    var data = '';
                form.each(function () {
                    console.log(pkg_id);
                    var sale_labor_price = $(this).find('input[name=sale_labor_price]').val();
                    var sale_material_price = $(this).find('input[name=sale_material_price]').val();
                    var upkeepId = $(this).find('input[name=upkeepId]').val();
                    var mtype = $(this).find('input[name=materialType]').val();
                    var model_id = $(this).find('input[name=modelId]').val();
                    //1 材料型号id； 2 项目id; 3 材料类型id, G 通用；O 原厂; 4 工时优惠价 5 材料优惠价
                    var pdata =  model_id + "," + upkeepId + "," + mtype + "," + sale_labor_price + "," + sale_material_price;
                    data += pdata + "#";
                });
                data_list += pkg_id +"%"+data + "?";
                num = pkg_id;
            }
        }
        console.log(data_list);
        var _url = $("#upkeepBatchFixQuoteAction").val();
    $.ajax({
        url: _url,
        method: "POST",
        dataType: "json",
        data: {"data_list": data_list},
        success: function(data){
            if(data['res'] == "success"){
                alert("报价成功!");
                var jump_url = $('#upkeepFixQuoteIndex').val();
                window.location.href = jump_url;
                //return false;
            }else{
                alert("报价失败!");
                console.log(data['msg']);
                return false;
            }
        }
    });
    });*/
    $('#batchQuoteSure1').click(function () {
        var data_list = '';
        for (var i=1;i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var sale_labor_price = tr_obj.find('input[name="sale_labor_price"]').val();
            var sale_material_price = tr_obj.find('input[name="sale_material_price"]').val();
            var upkeep_id = tr_obj.attr('upkeepId');
            var model_id = tr_obj.attr('modelId');
            //项目id+ 材料id + 工时优惠价 + 材料优惠价
            var t = upkeep_id + "%" + model_id + "%" + sale_labor_price  + "%" + sale_material_price;
            data_list += t + "?";
        }
        var _url = $("#upkeepBatchFixQuoteAction").val();
        $.ajax({
            url: _url,
            method: "POST",
            dataType: "json",
            data: {"data_list": data_list},
            success: function(data){
                if(data['res'] == "success"){
                    alert("报价成功!");
                    var jump_url = $('#upkeepFixQuoteIndex').val();
                    window.location.href = jump_url;
                    //return false;
                }else{
                    alert("报价失败!");
                    console.log(data['msg']);
                    return false;
                }
            }
        });
    });
    $('#batchQuoteSure2').click(function () {
        var data_list = '';
        var num = null;
        for(var i=1; i<$('tbody tr').length;i++){
            var tr_obj = $('tbody tr').eq(i);
            var pkg_id = tr_obj.attr('banjin');
            if(pkg_id == num){
                continue;
            }else{
                var form = $('tbody').find("tr[class='banjin_"+pkg_id+"']");
                var sale_price_low = '';
                var sale_price_middle = '';
                var sale_price_high = '';
                var k = 1;
                var data = '';
                form.each(function () {
                    var model_obj = $(".banjin_" + $(this).val());
                    sale_price_low = $(this).find('input[name=sale_price_low]').val() || sale_price_low;
                    sale_price_middle = $(this).find('input[name=sale_price_middle]').val() || sale_price_middle;
                    sale_price_high = $(this).find('input[name=sale_price_high]').val() || sale_price_high;
                    var upkeepId = $(this).find('input[name=banjinUpkeepId]').val();
                    if(k%3==0){
                    //1 低端优惠价；2 中端优惠价； 3 高端优惠价; 4 项目id;
                    var pdata =  sale_price_low + "," + sale_price_middle + "," + sale_price_high + "," + upkeepId;
                    data += pdata + "#";}
                    console.log(data);
                    k+=1;
                });
                data_list += pkg_id +"%"+ data + "?";
                num = pkg_id;
            }
        }
        console.log(data_list);
        var _url = $("#upkeepBatchPaintQuoteAction").val();
        $.ajax({
            url: _url,
            method: "POST",
            dataType: "json",
            data: {"data_list": data_list},
            success: function(data){
                if(data['res'] == "success"){
                    alert("报价成功!");
                    var jump_url = $('#upkeepPaintQuoteIndex').val();
                    window.location.href = jump_url;
                    //return false;
                }else{
                    alert("报价失败!");
                    console.log(data['msg']);
                    return false;
                }
            }
        });
    })


});

function judge_remind_text1() {
    var laval = $('#laborTimeChiose').val();
    var maval = $('#materialChiose').val();
    if (laval == 1){
        $('#laborTimeRemind').text('单位：元');
    }else{
        $('#laborTimeRemind').text(' % ');
    }
    if(maval == 1){
        $('#materialRemind').text('单位：元');
    }else{
        $('#materialRemind').text(' % ');
    }
}
function judge_remind_text2() {
    var paval = $('#paintQuoteChioce').val();
    if(paval == 1){
        $('#paintQuoteRemind').text('单位：元');
    }else {
        $('#paintQuoteRemind').text('%');
    }
}

function fixMaterialQuote(obj) {
    var model_id = $(obj).attr('modelId');
    var tr_obj = $(obj).parent().parent();
    var sale_labor_price = tr_obj.find('input[name="sale_labor_price"]').val();
    var sale_material_price = tr_obj.find('input[name="sale_material_price"]').val();
    var upkeep_id = tr_obj.attr('upkeepId');
    var _url = $("#upkeepQuoteActionUrl").val();
    //alert(model_id + "," + sale_labor_price + "," + sale_material_price + "," + upkeep_id + "," + _url);
    $.ajax({
        url: _url,
        method: "POST",
        dataType: "json",
        data: {"model_id": model_id,
                "sale_labor_price": sale_labor_price,
                "sale_material_price": sale_material_price,
                "upkeep_id": upkeep_id
                },
        success: function(data){
            if(data['res'] == "success"){
                alert("报价成功!");
                var jump_url = $('#upkeepFixQuoteIndex').val();
                window.location.href = jump_url;
                //return false;
            }else{
                alert("报价失败!");
                console.log(data['msg']);
                return false;
            }
        }
    });

}