/**
 * Created by lf on 16-12-9.
 */
$(document).ready(function () {
    var car_id = $('#carId').val();
    var type_id = $('#typeId').val();
    if(type_id ==1 || type_id ==2){
        var txt = "<select style='float: right' id='carColor_"+type_id+"'><option value='-1'>全部颜色</option></select>";
                                $('#picType_'+type_id).append(txt);
    }
    $.ajax({
            url:$('#picDataUrl').val(),
            type: 'post',
            datatype:'json',
            data:({'car_id':car_id,'type_id':type_id}),
            success:function (data) {
                console.log(data);
                if(!$('#picType_'+type_id+' span').text()){
                    if(type_id ==1||type_id == 2) {
                        if(data['car_color_list']){
                            for (var i = 0; i < data['car_color_list'].length; i++) {
                                var txt1 = "<option value='"+data['car_color_list'][i]['color_id']+"'>"+data['car_color_list'][i]['color']+"</option>";
                                var txt2 = "<div><br><br><div class='col-sm-2'><span style='font-weight: 500'>"+data['car_color_list'][i]['color']+
                                    "</span></div><br><div id='type_"+type_id+"color_"+data['car_color_list'][i]['color_id']+"' class='col-sm-12'></div><div class='col-sm-12'><hr style='height:1px;border:none;border-top:1px solid #555555;' ></div></div>";
                                $('#carColor_'+type_id).append(txt1);
                                $('#picType_'+type_id).append(txt2);
                            }
                        }
                        if(data['car_pics_list']){
                            for(var j=0;j<data['car_pics_list'].length;j++){
                                var txt3 = "<div class='col-sm-2'><img src='"+data['car_pics_list'][j]['pic']+"' width='128px' height='128px'><span style='position: relative;left: 50px'>"+data['car_pics_list'][j]['name']+"</span></div>";
                                $('#type_'+type_id+'color_'+data['car_pics_list'][j]['color_id']).append(txt3);
                            }
                        }
                    }
                    else{
                        if(data['car_pics_list']){
                            var txt4 = "<br><div class='col-sm-12'>";
                            for(var k=0;k<data['car_pics_list'].length;k++){
                                var txt5 = "<div class='col-sm-2'><img src='"+data['car_pics_list'][k]['pic']+"' width='128px' height='128px'><span style='position: relative;left: 50px'>"+data['car_pics_list'][k]['name']+"</span></div>"
                                txt4 +=txt5;
                            }
                            $('#picType_'+type_id).append(txt4+"</div>");
                        }
                    }
                }
            }
        });

        $('#carColor_'+type_id).change(function () {
        var color_val = $('#carColor_'+type_id).val();
        if (color_val == -1){
            $('#picType_'+type_id).children().show();
        }else{
            $(this).children().each(function () {
                $('#type_'+type_id+'color_'+$(this).val()).parent().hide();
            });
            $('#type_'+type_id+'color_'+color_val).parent().show();
        }
    })

});