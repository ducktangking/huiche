$(document).ready(function () {

    $("#upSeriesEditForm").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: $("#editActionUrl").val(),
            method: "post",
            dataType: "json",
            data: $("#upSeriesEditForm").serialize(),
            success: function (data) {
                if(data['res'] == "success"){
                    location.href = $("#UpSeriesIndexUrl").val();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.readyState);
                console.log(textStatus);
                console.log(errorThrown);
            }

        });
    });


    $('#upSeriesEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            manageCC: {
                validators: {
                    notEmpty: {
                        message: '排量不得为空'
                    }
                }
            },
        }
    });
});