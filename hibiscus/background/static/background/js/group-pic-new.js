/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var options = {
        url: $("#groupPicNewAction").val(),
        type: "post",
        success: function (data) {
            if(data == "success"){
                location.href = $("#groupPicIndexUrl").val();
            }
        }
    };
    // ajaxForm
    $("#groupPicNewForm").ajaxForm(options);
    // ajaxSubmit
    $("#btnGroupPicNew").submit(function (event) {
        event.preventDefault();
        $("#groupPicNewForm").ajaxSubmit(options);
    });

});