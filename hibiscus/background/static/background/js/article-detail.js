/**
 * Created by gmj on 7/9/16.
 */

$(document).ready(function () {
    $.ajax({
        url: $("#articleDetail").val(),
        method: "GET",
        dataType: "json",
        data: {"arId": $("#articleId").val()},
        success: function (data) {
            console.log(data);
            var title = data['title'];
            var author = "";
            if(data['producer'] != ""){
                author = data['producer'];
            }
            if(data['admin']){
                author = data['admin'];
            }
            var status = "待审核";
            if(data['status']){
                status = "审核通过";
            }
            var content = data['content'];
            var article_type = data['type'];
            $("#articleTitle").append(title);
            $("#articleAuthor").append(author);
            $("#articleStatus").append(status);
            $("#articleContent").append(content);
            $("#articleType").append(article_type);
        }
    });
});