/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var options = {
        url: $("#seriesEditAction").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#cIndexAction").val();
            }
        }
    };
/*
    // ajaxForm
    $("#SeriesEditForm").ajaxForm(options);
    // ajaxSubmit
    $("#btnSeriesEdit").submit(function (event) {
        event.defaultPrevented;
        $("#SeriesEditForm").ajaxSubmit(options);
    });
*/



    $('#serName').css({display:'inline-block'}).attr({maxlength:'32',placeholder:"输入车型名称"});

    $('#SeriesEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            serName: {
                validators: {
                    notEmpty: {
                        message: '车型名称不得为空'
                    }
                }
            },
            logo:{
                validators:{
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            }
        }
    }).on('success.form.bv',function (event) {
        event.defaultPrevented;
         /*if($('#relatedSeries option:eq(0)').val()){
            var txt = $('#relatedSeries option:eq(0)').val();
            for(var i=1;i<$('#relatedSeries').children().length;i++){
            txt += ',' + $('#relatedSeries option:eq('+i+')').val();
        }
            $('#relatedfor').val(txt);}*/
            $("#SeriesEditForm").ajaxForm(options);
            //return false;
    });
var topbrandid = $('#topBrand').val();
    related_series_ajax(null,topbrandid,null);
    $('#toRight').click(function () {
        $('#addSeries option:selected').appendTo($('#relatedSeries'));
    });
    $('#toLeft').click(function () {
        $('#relatedSeries option:selected').appendTo($('#addSeries'));
    });

    

    if($('#relatedSeries option:eq(0)').val()){
        var txt = $('#relatedSeries option:eq(0)').val();
        for(var i=1;i<$('#relatedSeries').children().length;i++){
            txt += ',' + $('#relatedSeries option:eq('+i+')').val();
        }
            $('#relatedfor').val(txt);}
        else{
            $('#relatedfor').val('');
        }
    $('#toRight,#toLeft').click(function () {
        if($('#relatedSeries option:eq(0)').val()){
        var txt = $('#relatedSeries option:eq(0)').val();
        for(var i=1;i<$('#relatedSeries').children().length;i++){
            txt += ',' + $('#relatedSeries option:eq('+i+')').val();
        }
            $('#relatedfor').val(txt);}
        else{
            $('#relatedfor').val('');
        }
    });


    var brand_id=$('#seriesBrand').val();
    var brand_text = $('#seriesBrand option:selected').text();
    $('#action_url').html('<input type="hidden" id="cIndexAction" value="/background/series/?brandId='+brand_id+'&SecondMenu=brandMange">');
    $('#seriesBrand').children().remove();
    $('#seriesBrand').append("<option value='"+brand_id+"'>"+brand_text+"</option>");


    if(!$("#series_kind").val()){
        write_in_series_kind()
    }

    show_ad_content("#has_ad_status");
    show_new_car_position("#is_new_car");
});


function show_ad_content(obj){
    var checked = $(obj).prop("checked");
    if(checked){
        $("#ad_content").show()
    }else{
        $("#ad_content").hide()
    }
}

function show_new_car_position(obj) {
    var checked = $(obj).prop("checked");
    if(checked){
        $("#new_car_position").show();
    }else{
        $("#new_car_position").hide();
    }
}
