/**
 * Created by gmj on 4/9/16.
 */

function render_area_info(){
    var area_data = area_info();
    for(var i=0; i<area_data.length; i++){
        var item = area_data[i];
        var txt = "<option>"+ item["province"] +"</option>";
        $("#province").append(txt);
    }
}

function display_cities(obj){
    $("#selectAll").attr("style", "display:block;");
    $("#subBtn").attr("style", "display:block;");
    var province = $(obj).val();
    var cities_data = area_info();
    var cities_pos = $("#level_two");
    cities_pos.empty();
    for(var i=0; i<cities_data.length; i++){
        var item = cities_data[i];
        if(item["province"] == province){
            for(var j=0; j<item["city"].length; j++){
                var txt = "<input type='checkbox' name='cities' value='"+ item["city"][j] +"'>" + item["city"][j];
                cities_pos.append(txt);
            }
        }
    }
}

function validate_unique(){
    var _url = $("#unique_url").attr("action");
    var res = "";
    var city = "";
    var province = $("#province").find("option:selected").val();
    $("input[name=cities]").each(function(){
        if($(this).prop("checked")){
            city = $(this).val();
        }
    });
    $.ajax({
        url: _url,
        dataType: "text",
        method: "get",
        async: false,
        data: {"province": province,
                "city": city},
        success: function(data){
            console.log(data)
            res = data;
        }
    });
    if(res == "no"){
        return true;
    }
    return false;
}

$("#areaData").submit(function(event){
    event.preventDefault();
    //return false;
    var val_unique = validate_unique();
    if(val_unique){
        var _url = $("#action_val").attr("action");
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#areaData").serialize(),
            success: function(data){
                console.log(data);
                if(data["res"] == "success"){
                    var index_url = $("#index_url").attr("action");
                    window.location.href = index_url;
                }
            }
        });
    }else{
        alert("已存在");
        console.log("Same info exists");
    }

});

function init_area_datatable(){
    var selected = [];
    var _url = $("#areaIndex").attr("action");
    $('#areaTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "columns": [
                    { "data": "province" },
                    { "data": "city" },
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#areaTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
        console.log(selected);
    } );
}


function switch_select(){
    $("#all").click(function(){
        $("input[name=cities]").prop("checked", $(this).prop("checked"));
    });
}

function delete_all(){
    $("#deleteAll").click(function () {
        var res = confirm("Are you sure to delete selected items");
        if(res == true){

        }
    });
}


$(document).ready(function(){
    //init_area_datatable();
    var api;
    var selected = [];
    var _url = $("#areaIndex").attr("action");
    $('#areaTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 0,
            "render": function (data, type, row) {
                if(row["province"] == "-1"){
                    return "全国";
                }else{
                    return row['province'];
                }
            }
        },{
            "targets": 1,
            "render": function (data, type, row) {
                if(row['city'] == "-1"){
                    return "全国";
                }else{
                    return row['city'];
                }
            }
        }],
        "columns": [
                    { "data": "province" },
                    { "data": "city" },
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#areaTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#areaTable').dataTable().api();
    $("#delArea").click(function(){
        if(selected.length == 0){
            $("#delModal").modal('show');
        }else{
            $("#myModal").modal('show');
        }
    });

    $("#delAreaForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#delItems").val(ids);
            var _url = $("#areaDelAction").attr("action");
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#delAreaForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#myModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
    render_area_info();
    switch_select();

});