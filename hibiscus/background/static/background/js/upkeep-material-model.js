$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var material_id = $("#materialId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"material_id": material_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 0,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                var t = "<div><img width='128' height='128' src='"+row['pic_path']+"'></div>";
                return t;
            }
        },{
            "targets": 5,
            "ordering": false,
            "searching": false,
            "render": function (data, type, row) {
                if(row['type'] == "G"){
                    return "通用";
                }
                if(row['type'] == "O"){
                    return "原厂";
                }
            }
        },{
            "targets": 7,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {

                var edit_t = "<a href='/background/upkeep/material/model/edit/?model_id="+row["DT_RowId"]+
                                "&material_id="+material_id+"'>编辑</a>&nbsp&nbsp&nbsp";
                var area_t = "<a href='#' onclick='set_material_default_area("+row["DT_RowId"]+")'>地区优先</a>"
                return edit_t + area_t;

            }
        }],
        "columns": [
            { "data": "pic_path"},
            { "data": "name" },
            {"data": "model"},
            // { "data": "price"},
            { "data": "time_labor_price" },
            {"data": "material_price"},
            {"data": "type"},
            {"data": "code"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
                if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                    $(row).addClass('selected');
                }
            }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#clearCon").click(function () {
        var url = $("#IndexempUrl").val();
        location.href = url;
    });

    $("#areaSetting").submit(function (event) {
        event.preventDefault();
        var url = $("#setModelDefaultAction").val();
        $.ajax({
            url: url,
            method: "post",
            data: $("#areaSetting").serialize(),
            success: function (data) {
                if(data == "success"){
                    window.location = $("#IndexempUrl").val();
                }
            }
        })
    });

});