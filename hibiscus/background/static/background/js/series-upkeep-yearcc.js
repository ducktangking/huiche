/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var series_id = $("#seriesYearCc").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET',
            data: {
                "series_id": series_id
            }
        },
        "columnDefs":[{
            "targets": 3,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                var t = "<a href='/background/upkeep/series/upkeep/edit/?upseries_id="+row['DT_RowId']+"'>编辑</a>";
                return t;
            }}],
        "columns": [
            {"data": "name"},
            { "data": "cc" },
            {"data": "produce_year"},
            {"data": "action"}

        ],
    
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
         var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();

    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#delUSeriesUpUrl").val();

            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("a[class='chr_search']").each(function () {
        $(this).click(function() {
            $("a[class*='on']").each(function () {
               $(this).removeClass('on');
            });
            $(this).addClass("on");
            $('#DataTable').dataTable().api().search($(this).text()).draw();
        });
    });



});