$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var car_id = $("#carId").val();

    $('#DataTable').dataTable({
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"car_id": car_id}
        },
        "language": {
            "url": $("#lanFile").val()
        },
        "columnDefs": [
            {
                "targets": 2,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div style='height:15px;width:15px;background-color: "+row["rgb"]+"'></div>";
                }
            },{
                "targets": 1,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if (row['side'] == 1){
                        return "<div>内饰</div>";
                    }else{
                        return "<div>外观</div>";
                    }
                }
            },{
                "targets": 4,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if (row['forbid'] == 1){
                        return "<a href='#' color_id='"+row['DT_RowId']+"' forbid='1' onclick='forbid_color(this)'>启用</a>";
                    }else{
                        return "<a href='#' color_id='"+row['DT_RowId']+"' forbid='0' onclick='forbid_color(this)'>禁用</a>";
                    }
                }
            }
        ],
        "columns": [
            {"data": "name"},
            {"data":"side"},
            {"data": "rgb"},
            {"data": "created_at"},

        ],
        "pagingType": "full_numbers",
        "rowCallback": function (row, data) {
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');
            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if (index === -1) {
            selected.push(id);
        } else {
            selected.splice(index, 1);
        }

        $(this).toggleClass('selected');
    });

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function () {
        if (selected.length == 0) {
            $("#ConfirmModal").modal('show');
        } else {
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if (ids_l != 0) {
            for (var i = 0; i < selected.length; i++) {
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function (data) {
                    console.log(data);
                    if (data["res"] == "success") {
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
/*

    $("#NewForm").submit(function (event) {
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function (data) {
                if (data["res"] == "success") {
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                }
            }
        });

    });*/




    $("#clearCon").click(function () {
        var url = $("#carIndex").val();
        location.href = url;
    });


    $('#CarPrice,#CarPriceMax,#CarPriceMin,#CarPower').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
        if (eventObj.ctrlKey == true || (keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 9 || keyCode == 46 || keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
            return true;
        else
            return false;
    });



    $('#realC').css({display: "inline-block"}).attr({placeholder: "排量", maxLength: '8'});
    $('#CarName').css({display: "inline-block"}).attr({placeholder: "请输入16位以内的款式名称"});
    $('#CarPrice').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入指导价",
        maxLength: '8'
    });
    $('#CarPriceMin').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入最低价",
        maxLength: '8'
    });
    $('#CarPriceMax').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入最高价",
        maxLength: '8'
    });
    $('#CarPower').css({display: "inline-block"}).attr({placeholder: "请输入功率", maxLength: '8'});


    $('#NewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function(validator, form, submitButton) {},
        fields: {
            realCC: {
                validators: {
                    notEmpty: {
                        message: '排量不得为空'
                    }
                }
            },
            CarName: {
                validators: {
                    notEmpty: {
                        message: '款式不得为空'
                    }
                }
            },
            CarPrice: {
                validators: {
                    notEmpty: {
                        message: '指导价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPriceMin: {
                validators: {
                    notEmpty: {
                        message: '最低价格不得为空',
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPriceMax: {
                validators: {
                    notEmpty: {
                        message: '最高价格不得为空',
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPower: {
                validators: {
                    notEmpty: {
                        message: '功率不得为空'
                    },
                    regexp: {
                        regexp:/^([1-9])([0-9]{0,2})$/,
                        message: '功率格式错误的',
                    },
                }
            }
        },

    }).on('success.form.bv',function (event) {
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function (data) {
                if (data["res"] == "success") {
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                }
            }
        });
    });

});

function forbid_color(obj) {
    // $(obj).event.preventDefault();
    var color_id = $(obj).attr("color_id");
    var forbid = $(obj).attr("forbid");
    var car_id = $("#carId").val();
    $.ajax({
        url:$("#colorForbidUrl").val(),
        type:"get",
        dataType: "json",
        data:{
                "color_id":color_id,
                "car_id":car_id
            },
        success: function (data) {
            if(data["res"] == "success"){
                if(forbid == 1){
                    alert("已启用");
                    $(obj).html("禁用");
                    $(obj).attr("forbid", 0);
                }else{
                    $(obj).html("启用");
                    alert("已禁用");
                    $(obj).attr("forbid", 1);
                }
                // location.href = $("#IndexUrl").val();
            }else{
                alert("操作已失败");
            }
        }
    })
}