$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    function render_bind_area_info(){
        var area_data = area_info();
        for(var i=0; i<area_data.length; i++){
            var item = area_data[i];
            var txt = "<option>"+ item["province"] +"</option>";
            $("#bindProvince").append(txt);

        }
    }
    render_bind_area_info();
    var _url = $("#areaIndex").val();
    $('#MaterialAreaDataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 0,
            "render": function (data, type, row) {
                if(row["province"] == "-1"){
                    return "全国";
                }else{
                    return row['province'];
                }
            }
        },{
            "targets": 1,
            "render": function (data, type, row) {
                if(row['city'] == "-1"){
                    return "全国";
                }else{
                    return row['city'];
                }
            }
        },{
            "targets": 2,
            "render": function (data, type, row) {
                var t = "<a href='/background/upkeep/material/brand/bind/?areaId="+ row["DT_RowId"] +"'>车型查看</a>";
                return t
            }
        }],
        "columns": [
                    { "data": "province" },
                    { "data": "city" },
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });

    
});