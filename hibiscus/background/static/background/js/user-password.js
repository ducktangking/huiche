/**
 * Created by gmj on 8/12/16.
 */

 var userId = $("#adminId").val();
 var role = $("#adminRole").val();
 var options = {
        beforeSubmit: validate_password_data,
        url: $("#changePwdAction").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data["res"] == "success"){
                if(role == "admin"){
                    location.href = $("#indexUrlAdmin").val();
                }else{
                    location.href = $("#indexUrlProducer").val();
                }
            }
        }
    };
        // ajaxForm
        $("#passwordChangeForm").ajaxForm(options);
        // ajaxSubmit
        $("#passwordBtn").submit(function (event) {
            event.defaultPrevented;
            $("#passwordChangeForm").ajaxSubmit(options);
            return false;
        });

function validate_password_data() {
    var opwd = $("#originalPwd").val();
    var npwd = $("#NewPwd").val();
    var cpwd = $("#confirmPwd").val();
    var res = "";
    if(opwd == ""){
        alert("请输入原始密码!");
        return false;
    }
    $.ajax({
        url: $("#validateUrl").val(),
        method: "GET",
        dataType: "text",
        async: false,
        data: {
            'role': $("#adminRole").val(),
            'userId': $("#adminId").val(),
            'pwd': opwd
        },
        success: function(data){
            if(data == 'not equal'){
                res = data;
                return false;
            }
        }
    });

    if(res == "not equal"){
        alert("原始密码有误，请重新输入");
        return false;
    }

    if(npwd == ""){
        alert("请输入新密码");
        return false;
    }
    if(cpwd == ""){
        alert("请输入确认密码");
        return false;
    }
    if(npwd != cpwd){
        alert("新密码与确认密码不一致，请重新输入");
        return false;
    }
    return true;
}