$(document).ready(function () {
    $("#unbindArticle").click(function () {
        var _url = $(this).attr("href");
        var _gId = $("#gId").val();
        if(confirm("确认重新绑定吗？")){
            $.ajax({
                url: _url,
                method: "POST",
                data: {"gId": _gId},
                success: function(data){
                    if(data == "success"){
                        var index_url = $("#BindUrl").val();
                        window.location.href = index_url+"?gId="+_gId;
                        }
                    }
            });
        }
        return false;
    });
});