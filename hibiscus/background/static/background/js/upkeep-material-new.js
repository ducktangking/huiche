$(document).ready(function () {


    $("#NewForm").submit(function(event){
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });

    });
    
    $.ajax({
        url: $("#AreaData").val(),
        method: "GET",
        dataType: "json",
        success: function (data) {
            var area_data = data;
            for(var j=0; j<area_data.length; j++){
                var d = "";
                if(area_data[j]["province"] == -1 || area_data[j]["city"] == -1){
                    d = "全国";
                }else{
                    d = area_data[j]["province"] + "/" + area_data[j]["city"];
                }
                var txt = "<input type='checkbox' name='area' value='"+area_data[j]["id"]+"'>" + d;
                $("#materialArea").append(txt);
            }
        }
    });


    console.log($("#typeId").val());
    if($("#typeId").val() == ""){
        $.ajax({
        url: $("#UpkeepData").val(),
        method: "GET",
        dataType: "json",
        data: {"type_id": 1},
        success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<option value='"+data[i]["id"]+"'>"+data[i]["name"]+"</option>";
                    $("#upkeep").append(txt);
                }
            }
        });
    }

    var options = {
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data["res"]);
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };
        // ajaxForm
    console.log($("#IndexUrl").val());
        // ajaxSubmit




    $('#materialNewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            materialBrand: {
                validators: {
                    notEmpty: {
                        message: '品牌名称不得为空'
                    }
                }
            },
            materialPic: {
                validators: {
                    notEmpty: {
                        message: '请上传图片'
                    },
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            },
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
            $("#materialNewForm").ajaxForm(options);
            //return false;
        });
});