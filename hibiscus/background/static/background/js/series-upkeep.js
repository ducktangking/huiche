/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    // for (var i=1;i<$('#DataTable tbody tr').length; i++){
    //     $('#DataTable tbody tr').eq(i).addClass("hehehehe")
    // }
    // var api;
    // var selected = [];
    // var _url = $("#DataUrl").val();
     $('#DataTable').dataTable();
/*
    // $('#DataTable').dataTable( {
    //     serverSide: true,
    //     "language": {
    //             "url": $("#lanFile").val()
    //         },
    //     ajax: {
    //         url: _url,
    //         type: 'GET'
    //     },
    //     "columnDefs": [{
    //         "targets": 3,
    //         "ordering": false,
    //         "searching": false,
    //         "render": function(data, type, row){
    //             return "<a href='/background/upkeep/series/upkeep/detail/?up_series_id="+row["DT_RowId"]+"'>详情</a>";
    //         }
    //     }],
    //     "columns": [
    //                 { "data": "series_name" },
    //                 { "data": "produce_year" },
    //                 {"data": "cc"}
    //     ],
    //
    //     "pagingType": "full_numbers",
    //     "rowCallback": function( row, data ) {
    //         if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
    //             $(row).addClass('selected');
    //         }
    //     }
    // });
    $('#DataTable tbody').on('click', 'tr', function () {
        // var id = this.id;
        var ids = $(this).find(".delete_ids").attr("ids");
        var index = $.inArray(ids, selected);


        if ( index === -1 ) {
            selected.push( ids );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
        console.log(selected);
    } );

    api = $('#DataTable').dataTable().api();

    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        console.log(selected);
        return false;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#delSeriesUpUrl").val();
            console.log("#################################3");
            console.log(_url);

            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("a[class='chr_search']").each(function () {
        $(this).click(function() {
            $("a[class*='on']").each(function () {
               $(this).removeClass('on');
            });
            $(this).addClass("on");
            $('#DataTable').dataTable().api().search($(this).text()).draw();
        });
    });

*/
    api = $('#DataTable').dataTable().api();

    $("a[class='chr_search']").each(function () {
        $(this).click(function() {
            $("a[class*='on']").each(function () {
               $(this).removeClass('on');
            });
            $(this).addClass("on");
            $('#DataTable').dataTable().api().search($(this).text()).draw();
        });
    });


});
function bindSeriesAction(obj) {
    var model_id = $(obj).attr('modelId');
    var up_id = $(obj).attr('upId');
    var text = $(obj).text();
    if(text=='绑定'){
        var bind_status = 1
    }else {
        var bind_status = null
    }
    var url = $('#bindActionUrl').val();
    $.ajax({
        url:url,
        data:{
            "modelId":model_id,
            "upkeepSeriesId": up_id,
            "status": bind_status
        },
        type:"get",
        dataType: "json",
        success:function (data) {
            if(data=='success'){
                window.location.href=window.location.href;
            }else{
                alert(text + "失败");
            }
        }
    })
}
