$(document).ready(function () {

    $("#upSeriesYearForm").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: $("#yearNewActionUrl").val(),
            method: "post",
            dataType: "json",
            data: $("#upSeriesYearForm").serialize(),
            success: function (data) {
                if(data['res'] == "success"){
                    location.href = $("#yearIndexUrl").val();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.readyState);
                console.log(textStatus);
                console.log(errorThrown);
            }

        });
    });

    var str = {};

    for (var i = 1950; i <= 2050; i++) {
            if (i == 2016)
                str += "<option value=" + i + " selected='selected'>" + i + "</option>";
            else
                str += "<option value=" + i + ">" + i + "</option>";
    }
    $('#car_year').html(str);

});