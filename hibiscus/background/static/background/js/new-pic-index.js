/**
 * Created by lf on 16-12-8.
 */
$(document).ready(function () {
    $('#picType_1').show().siblings().hide();
    var car_id = $('#carId').val();
    new_pics_index_ajax(car_id,1);
    $('#newPicContent li').each(function () {
        //var tx = $(this).children().text();
        $(this).click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            var type_id = $(this).attr('typeId');
           $('#picType_'+type_id).show().siblings().hide();
            new_pics_index_ajax(car_id,type_id)
        })
    });



    $('#newPicContent div:contains(外观图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });
    $('#newPicContent div:contains(内饰图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });
    $('#newPicContent div:contains(空间图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });
    $('#newPicContent div:contains(图解图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });
    $('#newPicContent div:contains(官方图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });
    $('#newPicContent div:contains(车展图片)').click(function () {
        $(this).addClass('classPicShow').siblings().removeClass('classPicShow');
        var type_id = $(this).attr('typeId');
        $('#picType_'+type_id).show().siblings().hide();
        new_pics_index_ajax(car_id,type_id)
    });

    $('#carColor_1').change(function () {
        var color_val = $('#carColor_1').val();
        if (color_val == -1){
            $('#picType_1').children().show();
        }else{
            $(this).children().each(function () {
                $('#type_1color_'+$(this).val()).parent().hide();
            });
            $('#type_1color_'+color_val).parent().show();
        }
    })

    $('#carColor_2').change(function () {
        var color_val = $('#carColor_2').val();
        if (color_val == -1){
            $('#picType_2').children().show();
        }else{
            $(this).children().each(function () {
                $('#type_2color_'+$(this).val()).parent().hide();
            });
            $('#type_2color_'+color_val).parent().show();
        }
    })

});

    function new_pics_index_ajax(car_id,type_id) {
        $.ajax({
            url:$('#picDataUrl').val(),
            type: 'post',
            datatype:'json',
            data:({'car_id':car_id,'type_id':type_id}),
            success:function (data) {
                console.log(data);
                if(!$('#picType_'+type_id+' span').text()){
                    if(type_id ==1||type_id == 2) {
                        if(data['car_color_list']){
                            for (var i = 0; i < data['car_color_list'].length; i++) {
                                var txt1 = "<option value='"+data['car_color_list'][i]['color_id']+"'>"+data['car_color_list'][i]['color']+"</option>";
                                var txt2 = "<div><br><br><div class='col-sm-2'><span style='font-weight: 500'>"+data['car_color_list'][i]['color']+
                                    "</span></div><br><div id='type_"+type_id+"color_"+data['car_color_list'][i]['color_id']+"' class='col-sm-12'></div><div class='col-sm-12'><hr style='height:1px;border:none;border-top:1px solid #555555;' ></div></div>";
                                $('#carColor_'+type_id).append(txt1);
                                $('#picType_'+type_id).append(txt2);
                            }
                        }
                        if(data['car_pics_list']){
                            for(var j=0;j<data['car_pics_list'].length;j++){
                                var txt3 = "<div class='col-sm-2'><input type='checkbox' value='"+data['car_pics_list'][j]['id']+
                                    "'><img src='"+data['car_pics_list'][j]['pic']+"' width='100px' height='100px'><span style='position: relative;left: 50px'>"+data['car_pics_list'][j]['name']+"</span></div>";
                                $('#type_'+type_id+'color_'+data['car_pics_list'][j]['color_id']).append(txt3);
                            }
                        }
                    }
                    else{
                        if(data['car_pics_list']){
                            var txt4 = "<br><div class='col-sm-12'>";
                            for(var k=0;k<data['car_pics_list'].length;k++){
                                var txt5 = "<div class='col-sm-2'><input type='checkbox' value='"+data['car_pics_list'][k]['id']+
                                    "'><img src='"+data['car_pics_list'][k]['pic']+"' width='100px' height='100px'><span style='position: relative;left: 50px'>"+data['car_pics_list'][k]['name']+"</span></div>"
                                txt4 +=txt5;
                            }
                            $('#picType_'+type_id).append(txt4+"</div>");
                        }
                    }
                }
            }
        })
    }

    function uploadPic(obj) {
        var car_id = $('#carId').val();
        var type_id = $(obj).attr('typeId');
        var index_url = $('#uploadPicUrl').val()+'?carId='+car_id+'&typeId='+type_id;
        window.location.href = index_url;
    }

    function batchUploadPic(obj) {
        var car_id = $('#carId').val();
        var type_id = $(obj).attr('typeId');
        var index_url = $('#batchUploadPicUrl').val()+'?carId='+car_id+'&typeId='+type_id;
        window.location.href = index_url;
    }

    function deleteCarPic(obj) {
        var car_id = $('#carId').val();
        var type_id = $(obj).attr('typeId');
        var delete_url = $('#carPicDeleteAct').val();
        var car_pic_id_list = '';
        var index_url = $('#carPicIndexUrl').val()+'?carId='+car_id;
        $('#picType_'+type_id).find('input[type="checkbox"]').each(function () {
            if($(this).is(':checked')){
            car_pic_id_list += $(this).val()+",";
            }
        });
        console.log(car_pic_id_list);
        if(car_pic_id_list) {
            $.ajax({
                url: delete_url,
                type: 'post',
                dataType: 'json',
                data: ({'car_pic_id_list': car_pic_id_list}),
                success: function (data) {
                    console.log(data['res']);
                    if(data['res'] == "success"){
                    window.location.href = index_url;
                    }else {
                        alert('删除失败');
                    }
                }
            });
        }
    }