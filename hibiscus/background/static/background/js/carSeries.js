/**
 * Created by server on 4/14/16.
 */

$(document).ready(function(){
    var api;
    var selected = [];
    var _url = $("#dataSeries").attr("action");
    var brand_id = $("#brandId").val();
    var conditionVal = $('#conditionVal').html();
    $('#seriesTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"brand_id": brand_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [
            {
                "targets": 2,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function(data, type, row){

                    return "<div><img  style='width:150px; height:100px;' src='"+row["pic_path"]+"' /></div>";
                }
            },{
                "orderable":false,
                "targets": 3,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    if(row['is_app']){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 4,
                "orderable":false,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    if(row['is_pc']){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 5,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function (data, type, row) {
                    var is_app = "";
                    var is_pc = "";
                    var is_un ='';
                    var is_ad8 = '';
                    var model_txt = "<a href='/background/car/?seriesId="+row["DT_RowId"]+"&SecondMenu=modelManage' " +
                        "title='Model Manage'>车款管理</a>&nbsp;&nbsp;&nbsp;";
                    var relation_series = "<a href='/background/series/relation/?seriesId="+row["DT_RowId"]+"'>关联车型</a>&nbsp;&nbsp;&nbsp;";
                    if(conditionVal){
                    var edit_txt = "<a href='/background/series/edit/?seriesId="+row["DT_RowId"]+"&source=brand'>编辑</a>&nbsp;&nbsp;&nbsp;";}
                    else{
                    var edit_txt = "<a href='/background/series/edit/?seriesId="+row["DT_RowId"]+"'>编辑</a>&nbsp;&nbsp;&nbsp;";}

                    if(conditionVal) {
                        if (!row['is_pc']) {
                            is_pc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='series_set_pc(" + row["DT_RowId"] + "," + brand_id+","+1+")'>设置pc</a>&nbsp;&nbsp;&nbsp;";
                        } else {
                            is_pc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='series_set_pc(" + row["DT_RowId"] + "," + brand_id+","+1+")'>取消pc</a>&nbsp;&nbsp;&nbsp;";
                        }
                    }else {
                        if (!row['is_pc']) {
                            is_pc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='series_set_pc(" + row["DT_RowId"] + "," + 0 +","+0+")'>设置pc</a>&nbsp;&nbsp;&nbsp;";
                        } else {
                            is_pc = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick='series_set_pc(" + row["DT_RowId"] + "," + 0 +","+0+")'>取消pc</a>&nbsp;&nbsp;&nbsp;";
                        }
                    }

                    if(conditionVal) {
                        if(!row['is_app']){
                            is_app = "<a href='#' onclick='series_set_app("+row['DT_RowId']+"," + brand_id+","+1+")'>设置app</a>&nbsp;&nbsp;&nbsp;";
                        }else{
                            is_app = "<a href='#' onclick='series_set_app("+row['DT_RowId']+"," + brand_id+","+1+")'>取消app</a>&nbsp;&nbsp;&nbsp;";
                        }}
                    else{
                        if(!row['is_app']){
                            is_app = "<a href='#' onclick='series_set_app("+row['DT_RowId']+"," + 0 +","+0+")'>设置app</a>&nbsp;&nbsp;&nbsp;";
                        }
                        else{
                            is_app = "<a href='#' onclick='series_set_app("+row['DT_RowId']+"," + 0 +","+0+")'>取消app</a>&nbsp;&nbsp;&nbsp;";
                        }
                    }
                    var color = "<a href='#' onclick='set_car_color("+row['DT_RowId']+")'>颜色管理</a>";
                    if(conditionVal){
                            is_un = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' seriesId='"+ row['DT_RowId'] +"' onclick='series_set_index(this)'>设置前端首页</a>&nbsp;&nbsp;&nbsp;";
                    }else {
                            is_un = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:void(0)' seriesId='"+ row['DT_RowId'] +"' onclick='series_set_index(this)'>设置前端首页</a>&nbsp;&nbsp;&nbsp;";
                    }
//                            is_un = "<a href='#' seriesId='"+ row['DT_RowId'] +"' onclick='series_set_un("+row['DT_RowId']+"," + 0 +","+0+")'>推送首页</a>&nbsp;&nbsp;&nbsp;";
                    return model_txt + edit_txt + is_app +"<br>"+ is_pc+color +"<br>"+ is_un;
                }
            }
        ],
        "columns": [
            {"data": "brand"},
            { "data": "name" },
            {"data": "logo"},
            {"data": "is_app"},
            {"data": "is_pc"},
            {"data": "action"}

        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#seriesTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );
    api = $("#seriesTable").dataTable().api();

    $("#delCarS").click(function(){
        if(selected.length == 0){
            $("#delCarSerModal").modal('show');
        }else{
            $("#carSerModal").modal('show');
        }
    });

    $("#delCarSerForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#delCarSerItems").val(ids);
            var _url = $("#carSerDelAction").attr("action");
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#delCarSerForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#carSerModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#clearCon").click(function () {
        var url = $("#seriesIndex").val();
        location.href = url;
    });
  //  $('#setSeriesAd').modal("show");
    $('#indexShow_2, #indexShow_3, #indexShow_4, #indexShow_5').hide();
    $('#pageNums').children().each(function () {
        var page_id = $(this).attr('pageId');
        $(this).mousemove(function () {
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().parent().find('div[class="indexShow"]').each(function () {
                $(this).hide();
            });
            $('#indexShow_'+page_id).show();
        });

    });
});


function series_set_index(obj) {
    var series_id = $(obj).attr('seriesId');
    $.ajax({
        url: $("#seriesPcPageShow").val(),
        method:"get",
        data:{"series_id": series_id},
        dataType:"html",
        success:function (data) {
            $("#setSeriesAd").html(data);
            $("#setSeriesAd").modal("show")
        }
    })
}


function set_car_color(series_id) {
    console.log(series_id);
    window.location.href=$("#seriesColorIndex").val() +"?series_id=" + series_id;
}

function active_series_position(obj_temp) {
    var obj = $(obj_temp);
    var checkbox = obj.parent().find("input[type='checkbox']");
    var checked = checkbox.prop("checked");
    console.log(checked);
    if(checked){
        checkbox.prop("checked", false);
        obj.removeClass("active");
    }else{

        var old_active_a = $("*[id^=pc_page_]").find("a");
        old_active_a.removeClass("active");
        console.log(old_active_a);
        var old_checkbox = old_active_a.parent().find("input[type='checkbox']");
        old_checkbox.removeClass("active");

        checkbox.prop("checked", true);
        obj.addClass("active");

    }
}

function change_page_show(obj_temp) {
    var obj = $(obj_temp);
    var page_id = obj.attr("pageId");
    obj.addClass("active").siblings().removeClass("active");
    $("#pc_page_" + page_id).css({"display":"inline-flex"});
    $("div[id^='pc_page_']").hide();
    $("#pc_page_" + page_id).show();
}

function change_page_pic_status(obj_temp){
    var obj = $(obj_temp);
    var checkbox = obj.find("input[type='checkbox']");
    if(checkbox.prop("checked")){
        obj.removeClass("div_active");
        checkbox.prop("checked", false);
    }else{
        var old_div = $("#setSeriesAd").find(".div_active");
        old_div.removeClass("div_active");
        var old_input = old_div.find("input");
        old_input.prop("checked", false);
        obj.addClass("div_active");
        checkbox.prop("checked", true);
    }

}


function pc_page_action(obj_temp){
    var obj = $(obj_temp);
    var series_id = obj.attr("seriesId");
    var t_checkbox = $("#setSeriesAd table input");
    var active_div = $("#setSeriesAd").find(".div_active");

    var char_id = -1;
    var pic_id = -1;

    for (var i=0; i<t_checkbox.length; i++){
        var che = t_checkbox[i];
        if($(che).prop("checked")){
            char_id = $(che).val();
        }
    }
    // if(active_div){
    var pic_che = active_div.find("input[type='checkbox']");
    pic_id = pic_che.val() || -1;
    // }

    console.log(char_id + "---" + pic_id + "---"  +series_id );

    $.ajax({
        url:$("#seriesPcPageAction").val(),
        method:"post",
        data:{
            "series_id":series_id,
            "char_id":char_id,
            "pic_id":pic_id
        },
        dataType:"json",
        success:function (data) {
            if(data["res"] == "success"){
                $("#setSeriesAd").modal("hide")
            }else{
                $("#setSeriesAd").modal("hide")
                alert("未能设置成功")
            }
        }

    })
}