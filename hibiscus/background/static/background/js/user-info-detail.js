/**
 * Created by gmj on 4/24/16.
 */

$(document).ready(function () {
    var user_id = $("#user_id").val();
    var _url = $("#detailData").val();
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "json",
        data: {"user_id": user_id},
        success: function (data) {
            var t1 = "<img style='width:128px;height:128px;' src='"+data["por_path"]+"'>";
            $("#userPor").append(t1);
            var t2 = ""
            for(var i=0; i<data["address"].length; i++){
                t2 += "<span>"+data["address"][i]+"</span>" + "&nbsp;&nbsp;";
            }
            $("#userAddr").append(t2);
            $("#userName").text(data["name"]);
            if(data["sex"] == "m"){
                $("#userSex").text("男");
            }else{
                $("#userSex").text('女');
            }
            $("#email").text(data['email']);
            var birthday = data['birthday'].split(" ")[0];
            $("#userBir").text(birthday);
            $("#userTel").text(data["tel"]);
            $("#userQQ").text(data["qq"]);
            $("#userWeibo").text(data['weibo']);
            $("#userWeixin").text(data["weixin"]);
            $("#loginName").text(data['username']);
            $("#phone").text(data['tel']);
            $("#originalPhone").text(data['phone']);
            $("#area").text(data['area']);
            if(data['type'] == 'o'){
                $("#adminRole").text("操作员");
            }
            if(data['type'] == 'a'){
                $("#adminRole").text("审计员");
            }
            if(data['type'] == 'm'){
                $("#adminRole").text("管理员");
            }

        }
    });
    
});