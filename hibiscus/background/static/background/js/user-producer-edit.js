/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    // console.log($("#editUrl").val());
    var _url = $("#editUrl").val();
    // var role = $("#adminRole").val();
    var options = {
        url: _url,
        type: "POST",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#indexUrl").val();
            }
        }
    };

   $("#producerEditForm").ajaxForm(options);
   $("#producerEditSub").submit(function (event) {

        event.defaultPrevented;
        $("#producerEditForm").ajaxSubmit(options);
        return false;
    });

/*    $.ajax({
        url: $('#baidumap').val(),
        dataType: 'html',
        method: 'POST',
        success:function (data) {
            $('#baiduMapHtml').html(data)
        }
    })*/

    
    
    var producer_area = $('#producerArea').val();
	var producer_address = $('#producerAddress').val();
	var addr_longitude = $('#addrLongitude').val() || 118.725437;
	var addr_latitude = $('#addrLatitude').val() || 32.144502;
	console.log(producer_address + " , " + producer_area);
	//alert($('#suggestId').val());

// 百度地图插件导入 js部分
    	// 百度地图API功能
	// 百度地图API功能
    var map = new BMap.Map("allmap", {enableMapClick:false});// 创建Map实例
	map.centerAndZoom(new BMap.Point(addr_longitude,addr_latitude), 17);  // 初始化地图,设置中心点坐标和地图级别
	map.addControl(new BMap.MapTypeControl());   //添加地图类型控件
	map.setCurrentCity(producer_area);          // 设置地图显示的城市 此项是必须设置的
	map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    var myGeo = new BMap.Geocoder();
	    // 将地址解析结果显示在地图上,并调整地图视野
	if(producer_address){
        myGeo.getPoint(producer_address, function(point){
            if (point) {
                map.centerAndZoom(point, 16);
                map.addOverlay(new BMap.Marker(point));
				$('#addrLongitude').val(point.lng);
				$('#addrLatitude').val(point.lat);
//				alert(point.lng + " ----" + point.lat);
	$('#suggestId').val(producer_address);
            }else{
                alert("您选择地址没有解析到结果!");
            }
	    }, producer_area);
	}
	var geoc = new BMap.Geocoder();
	map.addEventListener("click", function(e){
		var pt = e.point;
		geoc.getLocation(pt, function(rs){
			var addComp = rs.addressComponents;
			$('#suggestId').val(addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber);
			//alert(addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber);
			$('#addrLongitude').val(e.point.lng);
			$('#addrLatitude').val(e.point.lat);
		});
	});


		function G(id) {
		return document.getElementById(id);
	}
	var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
		{"input" : "suggestId"
		,"location" : map
	});

	ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
	var str = "";
		var _value = e.fromitem.value;
		var value = "";
		if (e.fromitem.index > -1) {
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}
		str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

		value = "";
		if (e.toitem.index > -1) {
			_value = e.toitem.value;
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}
		str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
		G("searchResultPanel").innerHTML = str;
	});

	var myValue;
	ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
	var _value = e.item.value;
		myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		G("searchResultPanel").innerHTML ="onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;

		setPlace();
	});
	function setPlace(){
		map.clearOverlays();    //清除地图上所有覆盖物
		function myFun(){
			var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
			map.centerAndZoom(pp, 18);
			map.addOverlay(new BMap.Marker(pp));    //添加标注
			$('#addrLongitude').val(pp.lng);
			$('#addrLatitude').val(pp.lat);
			//alert(pp.lng + "++++++++++++" + pp.lat);
		}
		var local = new BMap.LocalSearch(map, { //智能搜索
		  onSearchComplete: myFun
		});
		local.search(myValue);
	}
});