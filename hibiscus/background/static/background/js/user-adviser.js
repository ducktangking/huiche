$(document).ready(function () {
    var api;
    var selected = [];
    var adviser_type = $("#adviserType").val();
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"adviser_type": adviser_type}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 3,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_active"] == 1){
                        return "在职";
                    }else{
                        return "离职";
                    }
                }
            },{
            "targets": 2,
            "ordering": false,
            "searching": false,
            "render": function (data, type, row) {
                return "上 |下";
            }
        },{
                "targets": 4,
                "ordering": false,
                "searching": false,
                "orderable": false,
                "render": function(data, type, row){
                    var ss;
                    var txt = "<a href='/background/user/adviser/info/detail" +
                        "/?user_id="+row["DT_RowId"]+"'>查看详情</a>&nbsp;&nbsp;";
                    if(row['is_active'] == 1){
                        ss = "<a href='/background/user/adviser/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>禁用</a>";
                    }else{
                        ss = "<a href='/background/user/adviser/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>激活</a>";
                    }
                    return txt + ss;
                }
            }
        ],

        "columns": [
            { "data": "username" },
            {"data": "phone"},
            {"data": "order"},
            {"data": "status"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });


    
});