/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    var api;
    var selected = [];
    var _url = $("#clueCarDataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET'
        },
        "columnDefs": [
            {
                "targets": 4,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row['status'] == 0){
                        return "未处理";
                    }else{
                        return "已处理";
                    }
                }
            },
            {
                "targets": 5,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    var t = "";
                    if(row['status'] == 0){
                        t = "<a href='#' onclick='get_producer_adviser("+row["DT_RowId"]+")'>分配</a>"
                    }
                    return t;
                }
            }
        ],
        "columns": [
                    { "data": "car_name" },
                    { "data": "username" },
                    {"data": "phone"},
                    {"data": "adviser_name"}
        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });
    $('#brandTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#delBrand").click(function(){
        if(selected.length == 0){
            $("#delBrandModal").modal('show');
        }else{
            $("#brandModel").modal('show');
        }
    });

    $("#delBrandForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#delBrandItems").val(ids);
            var _url = $("#brandDelAction").attr("action");
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#delBrandForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#brandModel").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#clueAdviserForm").submit(function (event) {
        event.defaultPrevented;
        $.ajax({
            url: $("#disAdviserAction").val(),
            method: "post",
            dataType: "json",
            data: $("#clueAdviserForm").serialize(),
            success: function (data) {
                if(data['res'] == "success"){
                    location.href = $("#carClueIndexUrl").val();
                }
            }
        });
    });

});