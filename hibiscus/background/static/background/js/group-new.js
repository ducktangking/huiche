$(document).ready(function () {
    var data_url = $("#adDataUrl").val();
    $.ajax({
        url: $("#AreaData").val(),
        method: "GET",
        dataType: "json",
        success: function (data) {
            var area_data = data;
            for(var j=0; j<area_data.length; j++){
                var d = "";
                if(area_data[j]["province"] == -1 || area_data[j]["city"] == -1){
                    d = "全国";
                }else{
                    d = area_data[j]["province"] + "/" + area_data[j]["city"];
                }
                var txt = "<option value='"+area_data[j]["id"]+"'>" + d + "</option>";
                $("#groupArea").append(txt);
            }
        }
    });
    var options = {
        // beforeSubmit: validate_brand_data,
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };
    // ajaxForm
    $("#NewForm1").ajaxForm(options);
    // ajaxSubmit
    $("#picSub1").submit(function (event) {
        event.defaultPrevented;
        $("#NewForm1").ajaxSubmit(options);
        //return false;
    });
});