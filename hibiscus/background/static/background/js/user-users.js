$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 5,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_active"] == 1){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 6,
                "ordering": false,
                "orderable":false,
                "searching": false,
                "render": function (data, type, row) {
                    var ss;
                    if(row['is_active'] == 1){
                        ss = "<a href='/background/user/users/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>禁用</a>&nbsp;&nbsp;";
                    }else{
                        ss = "<a href='/background/user/users/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>激活</a>&nbsp;&nbsp;";
                    }
                    var reset_pwd = "<a href='#' onclick='reset_user_pwd("+row["DT_RowId"]+")'>重置密码</a> &nbsp;&nbsp;";
                    var edit = "<a href='/background/user/users/update/?user_id="+row["DT_RowId"]+"'>编辑</a>";
                    return ss + reset_pwd + edit;
                }
            }
        ],

        "columns": [
            { "data": "username" },
            { "data": "realname" },
            { "data": "phone" },
            { "data": "login_time" },
            { "data": "register_time" },
            {"data": "is_active"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });


    
});