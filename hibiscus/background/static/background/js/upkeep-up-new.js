$(document).ready(function () {


    $('#projName').css({display:'inline-block'}).attr({maxlength:'16',placeholder:"输入16位以内的项目名称"});
    $('#lowPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入低端品牌报价"});
    $('#middlePrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入中端品牌报价"});
    $('#highPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入高端品牌报价"});
    $('#minPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入项目最低价格"});
    $('#maxPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入项目最高价格"});
    $('#origPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入原厂工时费指导价格"});
    $('#genPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入普通工时费指导价格"});
    $('#labMinPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入工时费最低价格"});
    $('#labMaxPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入工时费最高价格"});

    $('#lowPrice,#middlePrice,#highPrice,#minPrice,#maxPrice,#origPrice,#genPrice,#labMinPrice,#labMaxPrice').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
            if (eventObj.ctrlKey == true||(keyCode >= 48 && keyCode <= 57)||keyCode == 8||keyCode == 46||keyCode == 9|| keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
                return true;
            else
                return false;
    });


    function get_up_type() {
        var _url = $("#typeUrl").val();
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            success: function (data) {
                var type_data = data;
                for(var i=0; i<type_data.length; i++){
                    var t = "<option value='"+type_data[i]["id"]+"'>"+type_data[i]["name"]+"</option>";
                    $("#upT").append(t);
                }
            }
        });

        _url = $("#pkgUrl").val();
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"type_id": type_id},
            success: function (data) {
                var pkg_data = data;
                for(var j=0; j<pkg_data.length; j++){
                    var t1 = "<input type='checkbox' name='upPkg' value='"+pkg_data[j]["id"]+"'>" + pkg_data[j]["name"];
                    $("#upPkg").append(t1);
                }
            }
        });
        $("#upParams1").hide();
        $("#upParams2").show();

        $("#upT").change(function () {
            type_id = $("#upT").find("option:selected").val();
            console.log(type_id);
            $.ajax({
                url: _url,
                method: "GET",
                dataType: "json",
                data: {"type_id": type_id},
                success: function (data) {
                    $("#upPkg").empty();
                    var pkg_data = data;
                    for(var j=0; j<pkg_data.length; j++){
                        var t1 = "<input type='checkbox' name='upPkg' value='"+pkg_data[j]["id"]+"'>" + pkg_data[j]["name"];
                        $("#upPkg").append(t1);
                    }
                    if(type_id == 1){
                        $("#upParams1").hide();
                        $("#upParams2").show();
                    }else{
                        $("#upParams2").hide();
                        $("#upParams1").show();
                    }
                }
            });
        });

    }

    var pkg_id = $("#pkgId").val();
    var type_id = $("#typeId").val();
    console.log(pkg_id);
    console.log(type_id);
    if(!(pkg_id && type_id)){
        get_up_type();
        console.log("init upkeep new");
    }
    var all = "";
    $("#upkeepNewForm").submit(function(event){
        event.preventDefault();
        //return false;
        if(!$("#pkgId").val()){
            $("#upPkg").find(":checkbox:checked").each(function () {
            console.log($(this).attr("value"));
            all = $(this).attr("value") + "," + all;
            });
            if(all == ""){
                alert("请至少选择一个参与的套餐！");
                return false;
            }
            $("#selectedPackages").val(all.substr(0, all.length-1));
        }else {
            $("#selectedPackages").val($("#pkgId").val());
        }

    });


    var reg1 = /^[0-9]+\.+([0-9]{2})$/;
    var reg2 = /^([1-9])([0-9]{0,7})$/;


    $('#upkeepNewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            projName: {
                validators: {
                    notEmpty: {
                        message: '项目名称不得为空'
                    }
                }
            },
            lowPrice: {
                validators: {
                    notEmpty: {
                        message: '低端品牌报价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            middlePrice: {
                validators: {
                    notEmpty: {
                        message: '中端品牌报价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                },
            },
            highPrice: {
                validators: {
                    notEmpty: {
                        message: '高端品牌报价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            minPrice: {
                validators: {
                    notEmpty: {
                        message: '项目最低价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            maxPrice: {
                validators: {
                    notEmpty: {
                        message: '项目最高价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            origPrice: {
                validators: {
                    notEmpty: {
                        message: '原厂工时费指导价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            genPrice: {
                validators: {
                    notEmpty: {
                        message: '普通工时费指导价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            labMinPrice: {
                validators: {
                    notEmpty: {
                        message: '工时费最低价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            labMaxPrice: {
                validators: {
                    notEmpty: {
                        message: '工时费最高价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
        }
    }).on('success.form.bv',function () {
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#upkeepNewForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });
    });

});