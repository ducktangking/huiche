$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 1,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_default"] == 0){
                        return "非"
                    }else{
                        return "是"
                    }
                }
            }],
        "columns": [
                    { "data": "address" },
                    {"data": "is_default"},
                    { "data": "created_at" },
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
        event.preventDefault();
        var addr = $("#userAddr").val();
        var va_url = $("#validateUrl").val();
        var is_exists = "";
        if(addr == ""){
            alert("Please insert user Address!");
            return false;
        }else{
            $.ajax({
                url: va_url,
                method: "GET",
                dataType: "json",
                data: {"userAddr": addr},
                async: false,
                success: function(data){
                    is_exists = data['res'];
                }
            });
            if(is_exists == "yes"){
                var msg = "The same address has been exists";
                alert(msg);
                return false;
            }
            if(is_exists == "error"){
                var msg = "Error happened.";
                alert(msg);
                return false;
            }
        }
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function(data){
                console.log(data);
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    console.log(index_url);
                    window.location.href = index_url;
                    }
                }
            });

        });
});