/**
 * Created by lf on 17-6-11.
 */
$(document).ready(function () {
        $(".pick-a-color").pickAColor({
        showSpectrum            : true,
        showSavedColors         : true,
        saveColorsPerElement    : true,
        fadeMenuToggle          : true,
        showAdvanced			: true,
        showBasicColors         : true,
        showHexInput            : true,
        allowBlank				: true,
        inlineDropdown			: true
    });

    $("#submit_color_new").click(function (event) {
        var tbody = $("#tbody");
    // console.log(tbody.children().length);
    // console.log(tbody.children().eq(0).attr("color_name"));
        var year = $("#producer_year").val();
        var series_id = $("#series_id").val();
        var color_str = "";
        for(var i=0; i < tbody.children().length; i++){
            // console.log("hhhhh" + i);
            var tr = tbody.children().eq(i);
            // console.log(tr);
            var color_name = tr.attr("color_name");
            var color_type = tr.attr("cyname");
            var color_value = tr.attr("cvalue");
            console.log(color_name + "+" + color_type + "+" +color_value);
            var color_con = color_name + "," + color_type + "," + color_value;
            color_str += color_con + ";"
        };
        // tbody.children().each(function () {
        //     console.log(this.getAttribute("color_name"));
        // });
        event.defaultPrevented;
        event.preventDefault();
        $.ajax({
            url:$("#color_new_action_url").val(),
            type:"post",
            dataType: "json",
            data:{
                "series_id":series_id,
                "year":year,
                "color_str":color_str
            },
            success: function (data) {
                if(data["res"] == "success"){
                    // console.log($("#color_index_url").val());
                    location.href = $("#color_index_url").val();
                }
            }

        })
    });
});

function add_color_in_table() {
    var color_name = $("#color_name").val();
    var color_value = $("#color_value").val();
    var color_type = $("#color_type").val();
    console.log(color_name+"," + color_value + ","+ color_type);
    $("#color_new_table").show();
    $("#submit_color_new").show();
    $("#plugin").hide();

    if(color_name && color_value){
        var color_type_name = "";
            if(color_type == 0){
                color_type_name = "外观"
        }else{
                    color_type_name = "内饰"
            }
        var txt = "<tr color_name='"+color_name+"' cyname='"+color_type +"' cvalue='#"+color_value+"'>"+
                "<td>" + color_name + "</td>"+
                "<td>"+ color_type_name +"</td>"+
                "<td>"+"<div style='height:15px;width:15px;background-color: #"+color_value+"'></div>"+"</td>" +
                "<td><a href='#' onclick='delete_color(this)'>删除</a></td>"
                +"</tr>";
    $("#tbody").append(txt);
    $("#color_name").val("");
    $("#color_value").val("");
    // $("#color_type").val("1");
    }
}

function delete_color(obj) {
    $(obj).parent().parent().remove();
    var tr_len = $("#tbody").children().length;
    if(tr_len == 0){
    $("#color_new_table").hide();
    $("#submit_color_new").hide();
    $("#plugin").show();

    }
}