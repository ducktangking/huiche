/**
 * Created by server on 4/18/16.
 */
$(document).ready(function(){
    var type_url = $("#tbrTyAction").attr("action");
    $.ajax({
        url: type_url,
        dataType: "json",
        method: "get",
        success: function(data){
            var type_data = data["type_l"];
            var level_data = data["level"];
            for(var i=0; i<type_data.length; i++){
                var txt = "<option value='"+type_data[i]["id"]+"'>"+type_data[i]["name"]+"</option>";
                $("#tbrandType").append(txt);
            }
            for(var j=0; j<level_data.length; j++){
                var txt = "<option value='"+level_data[j]["id"]+"'>"+level_data[j]["name"]+"</option>";
                $("#tbrandLevel").append(txt);
            }

        }

    });
    var options = {
        beforeSubmit: validate_brand_data,
        url: $("#tbrandAction").attr("action"),
        type: "post",
        success: function (data) {
            if(data == "success"){
                location.href = $("#tbIndexAction").attr("action");
            }
        }
    };
        // ajaxForm
        $("#tbrandDataForm").ajaxForm(options);
        // ajaxSubmit
        $("#tbtnAjaxForm").submit(function (event) {
            event.defaultPrevented;
            $("#tbrandDataForm").ajaxSubmit(options);
            //return false;
        });

    function validate_brand_data(){
        var unique_url = $("#tuniqueUrl").attr("action");
        var name = $("#tbrandName").val();
        if(name == ""){
            var msg = "Please input brand name";
            alert(msg);
            return false;
        }

        var exists = "";
        $.ajax({
            url: unique_url,
            method: "GET",
            dataType: "json",
            async: false,
            data: {"name": name, "level": "two"},
            success: function(data){
                exists = data["res"];
            }
        });
        if(exists == "no"){
            console.log("not exists");
            return true;
        }else{
            var msg = "系统中已存在相同名称的二级品牌";
            alert(msg);
            return false;
        }
    }

});