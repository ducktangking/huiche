$(document).ready(function () {

    function in_array(search,array){
        for(var i in array){
            if(array[i]==search){
                return true;
            }
        }
        return false;
    }

    $.ajax({
        url: $("#AreaData").val(),
        method: "GET",
        dataType: "json",
        success: function (data) {
            var selectedAreasArray = $("#selectedAreas").val().split(',');
            var area_data = data;
            for(var j=0; j<area_data.length; j++){
                if (in_array(area_data[j]["id"], selectedAreasArray)){
                    var txt = "<input type='checkbox' name='area' checked='checked' value='"+area_data[j]["id"]+"'>" +
                    area_data[j]["province"] + "/" + area_data[j]["city"];
                }else{
                    var txt = "<input type='checkbox' name='area' value='"+area_data[j]["id"]+"'>" +
                    area_data[j]["province"] + "/" + area_data[j]["city"];
                }
                $("#materialArea").append(txt);
            }
        }
    });

    upkeepId = $("#upkeepId").val();
    console.log(upkeepId);
    $.ajax({
    url: $("#UpkeepData").val(),
        method: "GET",
        dataType: "json",
        data: {"type_id": 1},
        success: function (data) {
                for(var i=0; i<data.length; i++){
                    if(data[i]["id"] == upkeepId){
                        var txt = "<option selected='selected' value='"+data[i]["id"]+"'>"+data[i]["name"]+"</option>";
                    }else{
                        var txt = "<option value='"+data[i]["id"]+"'>"+data[i]["name"]+"</option>";
                    }

                    $("#upkeep").append(txt);
                }
            }
    });

    var options = {
        url: $("#ActionEditUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data["res"]);
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };

        // ajaxSubmit
/*

    var source = $('#SourceMaterial').val();
    var actionback = $('#ActionBack').val();
    var upkeep_id = $('#upkeep').val();
    if(source){
        $('#IndexUrl').val(actionback+"?up_id="+upkeep_id+"&SecondMenu=upMaterialManage");
        $('#upkeep').change(function () {
            upkeep_id = $('#upkeep').val();
        $('#IndexUrl').val(actionback+"?up_id="+upkeep_id+"&SecondMenu=upMaterialManage");
        })
    }
*/
    var actionback = $('#ActionBack').val();
    var upkeep_id = $('#upkeep').val();
        $('#IndexUrl').val(actionback+"?up_id=all&SecondMenu=upMaterialManage");



    $('#materialEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            materialBrand: {
                validators: {
                    notEmpty: {
                        message: '品牌名称不得为空'
                    }
                }
            },
            materialPic: {
                validators: {
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            },
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
            $("#materialEditForm").ajaxForm(options);
            //return false;
        });
});