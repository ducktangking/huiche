/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    $("#addWords").click(function () {
        var txt ="<div style='clear:both;'></div><div><div class='form-group input_div_manu' style='float:left;width:40%;'><label class='col-sm-2 control-label'>文字</label>" +
            "<textarea class='article1 form-control input_manual' rows='3' name='articleContent'></textarea>" +
            "</div>" +
                "<div style='float: left;' class='btn btn-danger btn-xs' onclick='reduce_el(this)'>去除</div>" +
            "</div>";
        $("#content").append(txt);
    });
    $("#addPic").click(function () {
        var txt_p = "<div style='clear: both;'></div>" +
            "<div id='picPos'>" +
            "<div class='form-group input_div_manu' style='float: left;width: 40%;'>" +
                "<label class='col-sm-2 control-label'>图片</label>" +
            "<div class='col-sm-10'><input name='img' id='img' type='file'></div>" +
            "</div>" +
            "<div class='btn btn-danger btn-xs' style='float: left;' onclick='reduce_el(this)'>去除</div>" +
            "<div style='float: left;margin-left: 10px;'><input class='btn btn-primary btn-xs' type='submit' id='picOne' value='上传'></div>" +
            "</div>";
        $("#content").append(txt_p);
    });

    var options = {
        beforeSubmit: select_data,
        url: $("#picActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data["res"] == "success" && data["pic_name"] != "") {
                var img_p = $("#img").parent();
                img_p.remove();
                $("#content").append(data["pic_name"]);
                var txt = '<input type="hidden" class="article1" name="articleContent" value="'+data["pic_str"]+'">';
                $("#content").append(txt);
                $("#picPos").css("display", "none");
            }
            if(data["res"] == "success" && data["finish"] == "done"){
                // var t = "<input type='hidden' id='arId' value='"+data["id"]+"'>";
                // $("#contentArea").css("display", "none");
                // $("#articlePri").append(t);
                // $("#articlePri").css("display", "block");
                window.location.href=$("#IndexUrl").val();
            }
        }
    };

    function select_data(formData) {
        // return false;
        $("input[name=allData]").val("");
        var cont = "";
        $(".article1").each(function () {
            var str = '<p>'+$(this).val()+'</p>';
            cont += str;
        }   );
        console.log(formData);
        $("input[name=allData]").val(cont);
        formData[14]["value"] = cont;
        return true;
    }
    // ajaxForm
    $("#NewForm1").ajaxForm(options);

    
    // ajaxSubmit
    $("#picSub1").submit(function (event) {
        event.defaultPrevented;
        $("#NewForm").ajaxSubmit(options);
    });
    
      $("#picOne").submit(function (event) {
        event.defaultPrevented;
        $("#NewForm").ajaxSubmit(options);
    });
    
    $("#priAction").click(function () {
        var url = $("#priUrl").val();
        var ar_id = $("#arId").val();
        $.ajax({
            url: url,
            method: "GET",
            dataType: "json",
            data: {"ar_id": ar_id},
            success: function (data) {
                if(data["res"] == "success"){
                    $("#articlePri").empty();
                    $("#articlePri").append(data["content"]);
                }else{
                     $("#articlePri").append(data["reason"]);
                }
            }
        });
    });

});