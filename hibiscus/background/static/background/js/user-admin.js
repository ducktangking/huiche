$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var username = $('#theUserName').val();
    $('#RoleDataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 0,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                if(row['portrait']){
                    var p = "<img src='"+row['portrait']+"' width='128' height='128' />";
                    return p;
                }else{
                    return "暂无头像";
                }

            }
        },{
                "targets": 2,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["type"] == "o"){
                        return "操作员";
                    }else if(row["type"] == "a"){
                        return "审计员";
                    }else{
                        return "管理员";
                    }
                }
            },{
                "targets": 6,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_active"] == 1){
                        return "在职";
                    }else{
                        return "离职";
                    }
                }
            }
            ,{
                "targets": 7,
                "ordering": false,
                "searching": false,
                "orderable": false,
                "render": function(data, type, row){
                    var ss;
                    var edit = "<a href='/background/user/admin/info/edit/?user_id="+row["DT_RowId"]+"'>编辑</a>&nbsp;";
                    var del = "<a onclick='confirm_sure_delete(this)' user_id="+row["DT_RowId"]+" href='#'>删除</a><br/>";
                    var txt = "<a href='/background/user/admin/info/detail/?user_id="+row["DT_RowId"]+"'>查看详情</a><br/>";
                    var reset_pwd = "<a href='#' onclick='reset_admin_pwd("+row["DT_RowId"]+")'>重置密码</a><br/>";
                    if(row['is_active'] == 1){
                        ss = "&nbsp;&nbsp;&nbsp;<a href='/background/user/admin/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>禁用</a>";
                    }else{
                        ss = "&nbsp;&nbsp;&nbsp;<a href='/background/user/admin/active/?user_id="+row["DT_RowId"]+"&is_active="+row['is_active']+"'>激活</a>";
                    }
                    if(username != 'admin'){
                        if(row['type'] == "m"){
                            return txt + reset_pwd;
                        }else{
                            return edit + del + txt + reset_pwd+ ss;
                        }
                    }else{
                            if(row['name'] == 'admin'){
                                return txt + reset_pwd;
                            }
                            return edit + del + txt + reset_pwd+ ss;
                    }
                }
            }
        ],
        "columns": [
            {"data": "portrait"},
            {"data": "name" },
            {"data": "type"},
            {"data": "phone"},
            {"data": "created_at"},
            {"data": "last_login"},
            {"data": "is_active"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#RoleDataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#RoleDataTable').dataTable().api();
/*    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });
        
    }*/

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
            var user_id = $('#deleteUserId').val();
            var _url = $("#actionDeleteOboUrl").val();
            $.ajax({
                url: _url,
                method: "GET",
                dataType: "json",
                data:{'user_id':user_id},
                success: function(data){
                    console.log(data);
                    if(data == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
    });




    
});


function confirm_sure_delete(obj) {
    var user_id = $(obj).attr('user_id');
    $("#DeleteModal").modal('show');
    $('#deleteUserId').val(user_id);
}