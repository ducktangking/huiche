function article_bind(o) {
    var _url = $(o).attr("href");
    var _arId = $(o).attr("arId");
    var _gId = $(o).attr("gId");
    if(confirm("确认绑定吗？")){
        $.ajax({
            url: _url,
            method: "POST",
            data: {"arId": _arId, "gId": _gId},
            success: function(data){
                if(data == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
        });
    }
    return false;
}

$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var _gId = $("#gId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 1,
            "render": function (data, type, row) {
                if(row["created_type"] == "o"){
                    return "原创";
                }else{
                    return "转载";
                }
            }
        },{
            "targets": 3,
            "render": function (data, type, row) {
                var txt = "<a onclick='return article_bind(this);' href='/background/group/article/bind/' arId="+row['DT_RowId']+" gId="+_gId+">绑定</a>";
                txt += "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='/background/article/detail/?arId="+row['DT_RowId']+"'>详情</a>";
                return txt;
            }
        }],
        "columns": [
            { "data": "title" },
            { "data": "created_type"},
            { "data": "created_at" },
            { "data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
});