/**
 * Created by server on 4/14/16.
 */

$(document).ready(function () {
    // function get_relation_series() {
    //     var _url = $("#relationDataUrl").val();
    //     $.ajax({
    //         url: _url,
    //         method: "GET"
    //         dataType: "html",
    //         success: function (data) {
    //             $("#relationSeries").html(data);
    //         }
    //     })
    // }
    // get_relation_series();
    var options = {
        beforeSubmit: validate_series_data,
        url: $("#seriesAction").attr("action"),
        type: "post",
        success: function (data) {
            console.log(data);
            if(data == "success"){
                location.href = $("#cIndexAction").attr("action");
            }
            if(data == "file format error"){
                alert("LOGO 格式错误。");
            }
        }
    };
        // ajaxForm
        // ajaxSubmit

    function validate_series_data(){
        var unique_url = $("#uniqueUrl").attr("action");
        var brand_id = $("#brandId").val();
        if(brand_id == ""){
            brand_id = $("#selectBrandId").val();
        }
        var name = $("#serName").val();

        if(name == ""){
            var msg = "请输入车型名称";
            alert(msg);
            return false;
        }

        var exists = "";
        $.ajax({
            url: unique_url,
            method: "GET",
            dataType: "json",
            async: false,
            data: {"name": name, "brand_id": brand_id},
            success: function(data){
                exists = data["res"];
            }
        });
        if(exists == "no"){
            console.log("not exists");
            return true;
        }else{
            var msg = "相同名称的车型已存在";
            alert(msg);
            return false;
        }
    }


    $('#serName').css({display:'inline-block'}).attr({maxlength:'32',placeholder:"输入车型名称"});

    $('#carSeriesForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            seriesLogo: {
                validators: {
                    notEmpty: {
                        message: '请上传图片'
                    },
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            },
            serName: {
                validators: {
                    notEmpty: {
                        message: '车型名称不得为空'
                    }
                }
            },
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
            $("#carSeriesForm").ajaxForm(options);
            //return false;
        });
var topbrandid = $('#topBrand').val();
    related_series_ajax(null,topbrandid,null);
    $('#toRight').click(function () {
        $('#addSeries option:selected').appendTo($('#relatedSeries'));
    })
    $('#toLeft').click(function () {
        $('#relatedSeries option:selected').appendTo($('#addSeries'));
    });
    $('#toRight,#toLeft').click(function () {
        if($('#relatedSeries option:eq(0)').val()){
        var txt = $('#relatedSeries option:eq(0)').val();
        for(var i=1;i<$('#relatedSeries').children().length;i++){
            txt += ',' + $('#relatedSeries option:eq('+i+')').val();
        }
            $('#relatedfor').val(txt);}
        else{
            $('#relatedfor').val('');
        }
    });

    write_in_series_parent_kind();
    write_in_series_kind();
    show_new_car_position("#is_new_car");
    show_ad_content("#has_ad")
});

function write_in_series_parent_kind() {
    var data_json_str =$("#series_kind_value").val();
    var data_json = JSON.parse(data_json_str);
    var text = "";
    for(var i=0;i<data_json.length; i++)    {
        var data = data_json[i];
        var txt = "<option value='"+ data['id'] +"'> " +data['name']+ "</option>";
        text += txt;
    }
    $("#series_kind_parent").html(text);

}




function show_ad_content(obj){
    var checked = $(obj).prop("checked");
    if(checked){
        $("#ad_content").show()
    }else{
        $("#ad_content").hide()
    }
}


function show_new_car_position(obj) {
    var checked = $(obj).prop("checked");
    if(checked){
        $("#new_car_position").show();
    }else{
        $("#new_car_position").hide();
    }
}