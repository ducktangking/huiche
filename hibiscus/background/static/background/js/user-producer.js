$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 6,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_recommend"] == 1){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 7,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_active"] == 1){
                        return "是";
                    }else{
                        return "否";
                    }
                }
            },{
                "targets": 0,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    var p = "<div><img width='128' height='128' src='"+row['pic']+"'/></div>";
                    return p;
                }
            },{
                "targets": 2,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row['rank'] == '4'){
                        return "4S店"
                    }
                    if(row['rank'] == 'c'){
                        return "综合销售";
                    }
                    if(row['rank'] == 'b'){
                        return '维修连锁';
                    }
                    if(row['rank'] == 'q'){
                        return "快修店";
                    }
                }
            },{
                "targets": 8,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    var txt = "<a href='/background/user/producer/info/detail" +
                        "/?user_id="+row["DT_RowId"]+"'>查看详情</a><br/>";
                    var producer_series = "<a href='/background/user/producer/car/series/index/?producer_id="+
                        row["DT_RowId"]+"'>代理车系</a><br/>";
                    var reset_pwd = "<a href='#' onclick='reset_producer_pwd("+row["DT_RowId"]+")'>重置密码</a><br/>";
                    var status_set;
                    if(row['is_active'] == 1){
                        status_set = "&nbsp;&nbsp;&nbsp;<a href='/background/user/producer/active/?producer_id="+row['DT_RowId']+"&is_active="+row['is_active']+"'>禁用</a>&nbsp;&nbsp;";
                    }else{
                        status_set = "&nbsp;&nbsp;&nbsp;<a href='/background/user/producer/active/?producer_id="+row['DT_RowId']+"&is_active="+row['is_active']+"'>激活</a>&nbsp;&nbsp;";
                    }
                    var edit_url = "<br/>&nbsp;&nbsp;&nbsp;<a href='/background/user/producer/admin/edit/?producer_id="+row['DT_RowId']+"'>编辑</a>";
                    return txt + producer_series + reset_pwd + status_set + edit_url;
                }
            }
        ],
        "columns": [
            {"data": "pic"},
            {"data": "realname"},
            {"data": "type"},
            { "data": "username" },
            {"data": "phone"},
            {"data": "repair_phone"},
            {"data": "is_recommend"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
});
