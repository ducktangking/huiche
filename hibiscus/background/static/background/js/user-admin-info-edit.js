/**
 * Created by lf on 16-12-16.
 */
/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){

    $('select').children("option[value='2']").remove();

    var addr_url = $("#AddrUrl").val();
    $("#showAddr").click(function () {
        $("#addrList").empty();
        $.ajax({
            url: addr_url,
            method: "GET",
            dataType: "json",
            success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<input type='checkbox' name='addrList' " +
                        "value='"+data[i]["ad_id"]+"'>" + data[i]["address"] + "&nbsp;&nbsp;&nbsp;";
                    $("#addrList").css("display", "block");
                    $("#addrList").append(txt);
                }
            }
        });
    });

    var options = {
        beforeSubmit: validate_username,
        url: $("#ActionEditUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };

    birthday_date();

        $('#NewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            excluded:['select', 'portrait'],
        fields: {
            salePhone:{
                validators: {
                    notEmpty: {
                        message: '销售热线不得为空',
                    },
                }
            },
            repairPhone:{
                validators: {
                    notEmpty: {
                        message: '售后热线不得为空',
                    },
                }
            },
            username: {
                validators: {
                    notEmpty: {
                        message: '用户名不得为空'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: '手机号码不得为空',
                    },
                    regexp: {
                        regexp: /^1(3|4|5|7|8)\d{9}$/,
                        message: '手机号码格式错误',
                        },
                    }
                },
            password: {
                validators: {
                    notEmpty: {
                        message: '密码不得为空'
                    }
                }
            },
/*            portrait:{
                validators: {
                    notEmpty:{
                      message:'请上传图片'
                    },
                    regexp:{
                    regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                    message:'图片格式错误'
                        },
                    }
            },*/
            infoName: {
                validators: {
                    notEmpty: {
                        message: '姓名不得为空'
                    }
                }
            },
            InfoAddr: {
                validators: {
                    notEmpty: {
                        message: '地址信息不得为空'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '邮箱不得为空'
                    },
                    regexp: {
                        regexp: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)?([a-zA-Z0-9]{2,4})$/,
                        message: '邮箱格式错误',
                    },
                },
            },
            tel: {
                validators: {
                    notEmpty: {
                        message: '电话不得为空'
                    }
                }
            },
            qq: {
                validators: {
                    notEmpty: {
                        message: 'QQ号不得为空'
                    },
                    numeric:{
                    message: 'QQ号格式不正确',
                    },
                    stringLength:{
                        min:6,
                        max:12,
                    message: 'QQ号格式不正确',
                    }
                }
            },
            weibo: {
                validators: {
                    notEmpty: {
                        message: '微博不得为空'
                    }
                }
            },
            weixin: {
                validators: {
                    notEmpty: {
                        message: '微信不得为空'
                    }
                }
            }
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
            $("#NewForm").ajaxForm(options);
            // return false;
        });
});