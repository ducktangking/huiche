/**
 * Created by lf on 17-3-18.
 */
/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){

    $('select').children("option[value='2']").remove();

    var addr_url = $("#AddrUrl").val();
    $("#showAddr").click(function () {
        $("#addrList").empty();
        $.ajax({
            url: addr_url,
            method: "GET",
            dataType: "json",
            success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<input type='checkbox' name='addrList' " +
                        "value='"+data[i]["ad_id"]+"'>" + data[i]["address"] + "&nbsp;&nbsp;&nbsp;";
                    $("#addrList").css("display", "block");
                    $("#addrList").append(txt);
                }
            }
        });
    });

    var options = {
        beforeSubmit: validate_username,
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };

   birthday_date();

        $('#NewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            excluded:['select',':disabled'],
        fields: {
            shortName:{
                validators: {
                    notEmpty: {
                        message: '公司简称不得为空',
                    },
                }
            },
            fullName:{
                feedbackIcons: 'false',
            },
            InfoAddr:{
                validators: {
                    notEmpty: {
                        message: '经销商地址不得为空',
                    },
                }
            },
            salePhone:{
                validators: {
                    notEmpty: {
                        message: '销售热线不得为空',
                    },
                }
            },
            repairPhone:{
                validators: {
                    notEmpty: {
                        message: '售后热线不得为空',
                    },
                }
            },
            portrait:{
                validators: {
                    notEmpty:{
                      message:'请上传图片'
                    },
                    regexp:{
                    regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                    message:'图片格式错误'
                        },
                    }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '邮箱不得为空'
                    },
                    regexp: {
                        regexp: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)?([a-zA-Z0-9]{2,4})$/,
                        message: '邮箱格式错误',
                    },
                },
            },
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;

            $("#NewForm").ajaxForm(options);
        });


});
