$(document).ready(function () {
    $(".upQuoteForm").submit(function (event) {
        event.preventDefault();
        var action_url = $("#upkeepQuoteAction").val();
        $.ajax({
            url: action_url,
            method: "post",
            dataType: "json",
            data: $(this).serialize(),
            success: function(data){
                location.href = $("#upkeepQuoteIndex").val();
            }
        });
    });
    
    $("#weixiuAction").click(function () {
        $("#banjin").hide();
        $("#weixiu").show();
        $("#weixiuAction").attr("style", "float: left;background: red;");
        $("#banjinAction").attr("style", "float: left;background: none;margin-left: 50px;");
    });

    $("#banjinAction").click(function () {
        $("#weixiu").hide();
        $("#banjin").show();
         $("#banjinAction").attr("style", "float: left;background: red;margin-left: 50px;");
        $("#weixiuAction").attr("style" , "float: left;background: none;");
    });
    
    $(".quoteSpanData1").each(function () {
       $(this).click(function () {
           $(this).parent().find(".quoteInputData1").show();
           $(this).hide();
       }); 
    });

    $(".quoteSpanData2").each(function () {
       $(this).click(function () {
           $(this).parent().find(".quoteInputData2").show();
           $(this).hide();
       });
    });
    
});