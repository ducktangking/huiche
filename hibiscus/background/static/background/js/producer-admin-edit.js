/**
 * Created by tom on 16-10-16.
 */
/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var addr_url = $("#AddrUrl").val();
    $("#showAddr").click(function () {
        $("#addrList").empty();
        $.ajax({
            url: addr_url,
            method: "GET",
            dataType: "json",
            success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<input type='checkbox' name='addrList' " +
                        "value='"+data[i]["ad_id"]+"'>" + data[i]["address"] + "&nbsp;&nbsp;&nbsp;";
                    $("#addrList").css("display", "block");
                    $("#addrList").append(txt);
                }
            }
        });
    });

    var options = {
        // beforeSubmit: validate_username,
        url: $("#editUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#adminDetail").val();
            }
        }
    };


        $('#salePhone').css({display: "inline-block",imeMode:'disabled'});
        $('#repairPhone').css({display: "inline-block",imeMode:'disabled'});



    $('#adminPhone').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
            if (eventObj.ctrlKey == true||(keyCode >= 48 && keyCode <= 57)||keyCode == 8||keyCode == 46||keyCode == 9|| keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
                return true;
            else
                return false;
    });

//$('#fullName').data('bootstrapValidator').disableSubmitButtons(false);
    $('#adminEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        excluded:['select',':disabled'],
        fields: {
            shortName:{
                validators: {
                    group: '.group',
                    notEmpty: {
                        message: '公司简称不得为空',
                    },
                }
            },
            fullName:{
                feedbackIcons: 'false',
            },
            InfoAddr:{
                validators: {
                    notEmpty: {
                        message: '经销商地址不得为空',
                    },
                }
            },
            salePhone:{
                validators: {
                    notEmpty: {
                        message: '销售热线不得为空',
                    },
                }
            },
            repairPhone:{
                validators: {
                    notEmpty: {
                        message: '售后热线不得为空',
                    },
                }
            },
            portrait:{
                validators: {
                    regexp:{
                    regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                    message:'图片格式错误'
                        },
                    }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '邮箱不得为空'
                    },
                    regexp: {
                        regexp: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)?([a-zA-Z0-9]{2,4})$/,
                        message: '邮箱格式错误',
                    },
                },
            },
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
        if($('#addrLongitude').val()){
            $("#adminEditForm").ajaxForm(options);
        }else{
            alert("该地址无法解析，不能获得坐标参数");
            return false
        }
            // return false;
        });
    //birthday_date();
    
    
    
    // 百度地图 api

        var producer_area = $('#producerArea').val();
	var producer_address = $('#producerAddress').val();
	var addr_longitude = $('#addrLongitude').val() || 118.725437;
	var addr_latitude = $('#addrLatitude').val() || 32.144502;
	console.log(producer_address + " , " + producer_area);
	//alert($('#InfoAddr').val());

// 百度地图插件导入 js部分
    	// 百度地图API功能
	// 百度地图API功能
    var map = new BMap.Map("allmap", {enableMapClick:false});// 创建Map实例
	map.centerAndZoom(new BMap.Point(addr_longitude,addr_latitude), 17);  // 初始化地图,设置中心点坐标和地图级别
	map.addControl(new BMap.MapTypeControl());   //添加地图类型控件
	map.setCurrentCity(producer_area);          // 设置地图显示的城市 此项是必须设置的
	map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    var myGeo = new BMap.Geocoder();
	    // 将地址解析结果显示在地图上,并调整地图视野
	if(producer_address){
        myGeo.getPoint(producer_address, function(point){
            if (point) {
                map.centerAndZoom(point, 16);
                map.addOverlay(new BMap.Marker(point));
				$('#addrLongitude').val(point.lng);
				$('#addrLatitude').val(point.lat);
//				alert(point.lng + " ----" + point.lat);
	$('#InfoAddr').val(producer_address);
            }else{
                alert("您选择地址没有解析到结果!");
            }
	    }, producer_area);
	}
	var geoc = new BMap.Geocoder();
	// map.addEventListener("click", function(e){
	// 	var pt = e.point;
	// 	geoc.getLocation(pt, function(rs){
	// 		var addComp = rs.addressComponents;
	// 		$('#InfoAddr').val(addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber);
	// 		//alert(addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber);
	// 		$('#addrLongitude').val(e.point.lng);
	// 		$('#addrLatitude').val(e.point.lat);
	// 	});
	// });


		function G(id) {
		return document.getElementById(id);
	}

	var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
		{"input" : "InfoAddr"
		,"location" : map
	});

	ac.addEventListener("onhighlight", function(e) {  //鼠标放在下拉列表上的事件
	var str = "";
		var _value = e.fromitem.value;
		var value = "";
		if (e.fromitem.index > -1) {
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}
		str = "FromItem<br />index = " + e.fromitem.index + "<br />value = " + value;

		value = "";
		if (e.toitem.index > -1) {
			_value = e.toitem.value;
			value = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		}
		str += "<br />ToItem<br />index = " + e.toitem.index + "<br />value = " + value;
		$("#searchResultPanel").html(str);
	});

	var myValue;
	ac.addEventListener("onconfirm", function(e) {    //鼠标点击下拉列表后的事件
	var _value = e.item.value;
		myValue = _value.province +  _value.city +  _value.district +  _value.street +  _value.business;
		$("#searchResultPanel").html("onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue);
		setPlace();
	});
	function setPlace(){
		map.clearOverlays();    //清除地图上所有覆盖物
		function myFun(){
			var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
			map.centerAndZoom(pp, 18);
			map.addOverlay(new BMap.Marker(pp));    //添加标注
			$('#addrLongitude').val(pp.lng);
			$('#addrLatitude').val(pp.lat);
			//alert(pp.lng + "++++++++++++" + pp.lat);
		}
		var local = new BMap.LocalSearch(map, { //智能搜索
		  onSearchComplete: myFun
		});
		local.search(myValue);
	}

    $("#InfoAddr").focusout(function () {
        var this_address = $('#InfoAddr').val();
        	// 创建地址解析器实例
        var myGeo = new BMap.Geocoder();
        // 将地址解析结果显示在地图上,并调整地图视野
        myGeo.getPoint(this_address, function(point){
            if (point) {
                map.centerAndZoom(point, 16);
                map.addOverlay(new BMap.Marker(point));
   			$('#addrLongitude').val(point.lng);
			$('#addrLatitude').val(point.lat);
         }
        });
    });
});

// 表单验证 冗余
/*            tel: {
                validators: {
                    notEmpty: {
                        message: '电话不得为空'
                    }
                }
            },
            qq: {
                validators: {
                    notEmpty: {
                        message: 'QQ号不得为空'
                    },
                    numeric:{
                    message: 'QQ号格式不正确',
                    }
                }
            },
            weibo: {
                validators: {
                    notEmpty: {
                        message: '微博不得为空'
                    }
                }
            },
            weixin: {
                validators: {
                    notEmpty: {
                        message: '微信不得为空'
                    }
                }
            }*/
