/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var options = {
        // beforeSubmit: select_data,
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data['res'] == 'success'){
                window.location.href=$("#IndexUrl").val();
            }
        }
    };

    // ajaxForm
    $("#NewForm1").ajaxForm(options);

    
    // ajaxSubmit
    $("#picSub1").submit(function (event) {
        event.defaultPrevented;
        $("#NewForm1").ajaxSubmit(options);
    });


    $("#title").keydown(function () {
        $("#chars_leave").text(Math.abs(20-$(this).val().length));
    })

    $(".char_select").each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            var href = $(this).attr("href");
            var original_top_t = '<option value="-1">全部品牌</option>';
            var original_t = '<option value="-1">全部品牌</option>';
            var original_car_t = '<option value="-1">全部车款</option>';
            $("#articleTopBrand").empty();
            $("#articleBrand").empty();
            $("#carticleCar").empty();
            $("#articleTopBrand").append(original_top_t);
            $("#articleBrand").append(original_t);
            $("#carticleCar").append(original_car_t);

            $.ajax({
                url: href,
                method: "get",
                dataType: "json",
                success: function (data) {
                    for(var i=0; i<data.length; i++){
                        var t = "<option value='"+data[i]["id"]+"'>"+data[i]["name"]+"</option>";
                        $("#articleTopBrand").append(t);
                    }

                }
            });

        });

    });

    // $('#articleOne').change(function () {
    // $('#articleTwo option:contains(促销)').remove();
    // $('#articleTwo option:contains(热门访谈)').remove();
    // });
    // area_change_with();
    // $('#articleOne').change(function () {
    //     area_change_with();
    // });
    article_web_change();
    $('#articleOne').change(function () {
        article_web_change();
    });
});
    //
    // function area_change_with() {
    // var the_area = $("#articleArea").attr("area");
    // var the_area_id = $('#articleArea').attr("area_id");
    //     if($('#articleOne').val()!=45){
    //         $("#articleAreaInput").val("全国");
    //         $('#articleArea').val(2)
    //     }else{
    //         $("#articleAreaInput").val(the_area);
    //         $('#articleArea').val(the_area_id);
    //     }
    // }
function article_web_change() {
    var the_area = $("#articleArea").attr("area");
    var the_area_id = $('#articleArea').attr("area_id");
    if($('#operatorId').val()){
        $('#articleTwo option:contains(销售促销)').remove();
        $('#articleTwo option:contains(售后促销)').remove();
        if($('#articleOne').val()!=45){
            $("#articleAreaSelect").val("2");
            $('#articleArea').val(2)
        }else{
            $("#articleAreaSelect").val("1");
            $('#articleArea').val(the_area_id);
        }
    }else{
        $('#articleOne option:contains(新闻)').siblings().remove();
    }
}