/**
 * Created by lf on 16-10-18.
 */
$(document).ready(function () {

    var model_id = $("#model_id").val();
    if (model_id === undefined) {
        model_id = "";
    }

    var options = {
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data["res"]);
            if (data["res"] == "success") {
                location.href = $("#IndexUrl").val();
            }
        }
    };
    // ajaxForm
    console.log($("#IndexUrl").val());
    // ajaxSubmit

    $('#price').css({display: 'inline-block', imeMode: "disabled"});
    $('#priceMin').css({display: 'inline-block', imeMode: "disabled"});
    $('#priceMax').css({display: 'inline-block', imeMode: "disabled"});
    $('#price,#priceMax,#priceMin').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
        if (eventObj.ctrlKey == true || (keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 46 || keyCode == 9 || keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
            return true;
        else
            return false;
    });


    $('#materialModelNewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            materialModelPic: {
                validators: {
                    regexp: {
                        regexp: /\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message: '图片格式错误'
                    },
                },
            },
            name: {
                validators: {
                    notEmpty: {
                        message: '名称不得为空'
                    }
                }
            },
            modal: {
                validators: {
                    notEmpty: {
                        message: '型号不得为空'
                    }
                }
            },
            price: {
                validators: {
                    notEmpty: {
                        message: '价格不得为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            priceMin: {
                validators: {
                    notEmpty: {
                        message: '最低价格不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            priceMax: {
                validators: {
                    notEmpty: {
                        message: '最高价格不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            origPrice: {
                validators: {
                    notEmpty: {
                        message: '原厂工时指导价不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            genPrice: {
                validators: {
                    notEmpty: {
                        message: '普通工时指导价不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            code: {
                validators: {
                    notEmpty: {
                        message: '编码不得为空'
                    },
                },
            },
            content: {
                validators: {
                    notEmpty: {
                        message: '内容不得为空'
                    },
                },
            }
        }
    }).on('success.form.bv', function (event) {
        event.defaultPrevented;
        $("#materialModelNewForm").ajaxForm(options);
        //return false;
    });


    if (!$('#is_all_and_chosen').val()) {
        $('#addSeriesBox').hide();
    }
    if (!$('#is_chosen_series').val()) {
        $('#chosenSeries').hide();
    }

    $('#toRight').click(function () {
        $('#addSeries option:selected').appendTo($('#bindFor'));
    })
    $('#toLeft').click(function () {
        $('#bindFor option:selected').appendTo($('#addSeries'));
    });
    $('#toRight,#toLeft').click(function () {
        if ($('#bindFor option:eq(0)').val()) {
            var txt = '';
            for (var i = 0; i < $('#bindFor').children().length; i++) {
                txt += $('#bindFor option:eq(' + i + ')').val() + ',';
            }
            $('#bindSeries').val(txt);
        }
        else {
            $('#bindSeries').val('');
        }
    });

    $('#toRightBrand').click(function () {
        $('#addBrands option:selected').appendTo($('#bindForBrands'));
    });
    $('#toLeftBrand').click(function () {
        $('#bindForBrands option:selected').appendTo($('#addBrands'));
    });

    $('#allAndChosen').change(function () {
        judge_all_and_chosen();
    })


});