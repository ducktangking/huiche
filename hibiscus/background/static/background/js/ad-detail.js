function adUnBind(o) {
    var _url = $(o).attr("href");
    var _aId = $(o).attr("aId");
    if(confirm("确认下架？")){
        $.ajax({
            url: _url,
            method: "POST",
            data: {"aId": _aId},
            success: function(data){
                if(data == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
        });
    }
    return false;
}

$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [
            {
                "targets": 5,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["is_timeout"]){
                        return "过期";
                    }else{
                        return "未过期";
                    }
                    // return "<div><img  style='width:128px; height:128px;' src='"+row["pic_path"]+"' /></div>";
                }
            },
            {
                "targets": [4, 6, 8],
                "orderable": false
            },
            {
                "targets": 7,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><img  style='width:128px; height:128px;' src='"+row["picture"]+"' /></div>";
                }
            },
            {
                "targets": 9,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["location"] == ""){
                        var txt = "<a href='/background/ad/adetail/bind/?aId="+row['DT_RowId']+"'>投放</a>";
                        return txt;
                    }else{
                        return "<a onclick='return adUnBind(this)' href='/background/ad/adetail/bind/unbind/' aId="+row['DT_RowId']+">下架</a>";
                    }
                    // return "<div><img  style='width:128px; height:128px;' src='"+row["pic_path"]+"' /></div>";
                }
            }],
        "columns": [
                    {"data": "name" },
                    {"data": "location"},
                    {"data": "link"},
                    {"data": "consumer"},
                    {"data": "time"},
                    {"data": "is_timeout"},
                    {"data": "brief"},
                    {"data": "picture"},
                    { "data": "created_at" },
                    { "data": "action" }
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
        console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2");
        event.preventDefault();
        var wid = $("#adWidth").val();
        var heig =$("#adHeight").val();
        if(isNaN(wid)){
            alert("宽度请输入数字");
            return false;
        }
        if(isNaN(heig)){
            alert("高度请输入数字");
            return false;
        }
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });

    });
})