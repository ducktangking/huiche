/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    var data_url = $("#DataUrl").val();
    var _url = data_url;
            var api;
        var selected = [];
        $('#DataTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET'
        },
        "columnDefs":[{
            "targets": 6,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                var t = "<a href='/background/upkeep/year/cc?series_id="+row['DT_RowId']+"'>年份排量管理</a>";
                return t;
            }
        }],
        "columns": [
            {"data": "initial"},
            {"data": "first_brand"},
            { "data": "second_brand" },
            {"data": "series_name"},
            {"data": "country"},
            {"data": "level"},
            {"data": "action"}

        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
         var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();

    $("a[class='chr_search']").each(function () {
        $(this).click(function() {
            $("a[class*='on']").each(function () {
               $(this).removeClass('on');
            });
            $(this).addClass("on");
            $('#DataTable').dataTable().api().search($(this).text()).draw();
        });
    });

/*
    $('#DataTable tbody').on('click', 'tr', function () {
         var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
*/

    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#delUSeriesUpUrl").val();

            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });


});

function filter_series_by_initial(msg) {
    var this_page_url = $('#manageIndex').val();
    console.log(this_page_url);
    window.location.href = this_page_url + "?SecondMenu=upkeepSeriesManage&initial=" + msg;
}
