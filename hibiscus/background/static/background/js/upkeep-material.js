$(document).ready(function () {
    var up_new_id;
    function get_upkeep_list(){

        var _url = $("#upkeepForMaterial").val();
        var t;
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"up_id": $("#upId").val()},
            success: function (data) {
                console.log(data);
                $("#upkeep_proj_for_material").empty();
                    $("#upkeep_proj_for_material").append("<option value='all'>全部</option>");
                for(var i=0; i<data.length; i++){
                    if(i == 0){
                        up_new_id = data[i]['id'];
                        $("#upNewId").val(up_new_id);
                    }
                    if(data[i]['is_checked']){
                        t = "<option value='"+data[i]['id']+"' selected>"+data[i]["name"]+"</option>";
                    }else{
                        t = "<option value='"+data[i]['id']+"'>"+data[i]["name"]+"</option>";
                    }
                    $("#upkeep_proj_for_material").append(t);
                }
            }
            
        });
    }
    get_upkeep_list();
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var up_id = $("#upId").val();
    if(up_id == ""){
        up_id = $("#upNewId").val();
    }
    var type_id = $("#typeId").val();
    var pkg_id = $("#pkgId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"up_id": up_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columns": [
            { "data": "upkeep"},
            { "data": "brand" },
            {"data": "pic"},
            {"data": "action"}

        ],
        "columnDefs": [{
            "targets": 2,
            "ordering": false,
            "orderable": false,
            "searching": false,
            "render": function (data, type, row) {
                var t = "<div><img width='128' height='128' src='"+row['pic']+"'></div>";
                return t;
            }
        },
            {
            "targets": 3,
            "ordering": false,
            "orderable": false,
            "render": function (data, type, row) {
                var modt = "<a href='/background/upkeep/material/model/manage/?material_id="+row['DT_RowId']+"'>型号管理</a>&nbsp;&nbsp;&nbsp;";
                var edit_t = "<a href='/background/upkeep/material/edit/?material_id="+row["DT_RowId"]+"'>编辑</a>";
                return modt + edit_t;
            }
        }],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
                if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                    $(row).addClass('selected');
                }
            }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
    event.preventDefault();
    //return false;
    var _url = $("#ActionNewUrl").val();
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#NewForm").serialize(),
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#IndexUrl").val();
                window.location.href = index_url;
                }
            }
        });

    });

    $.ajax({
        url: $("#UpkeepData").val(),
        method: "GET",
        dataType: "json",
        data: {"type_id": 1},
        success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<a href='"+data[i]["id"]+"'>"+data[i]["name"]+"</a>";
                    var txt = "<a href='/background/upkeep/material/?up_id="+data[i]["id"]+"&type_id="+type_id+"&pkg_id="+pkg_id+"&SecondMenu=upMaterialManage'>"+data[i]["name"]+"</a>";
                    $("#upkeeps").append(txt);
                }
            }
    });

    $("#clearCon").click(function () {
        var url = $("#IndexUrl").val();
        location.href = url;
    });

});