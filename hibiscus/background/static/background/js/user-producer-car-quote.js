$(document).ready(function () {

    $(".edit_quote").each(function () {
        $(this).click(function () {
            $(this).hide();
            $(this).parent().find("input[type='text']").show();
        });
    });



    $('#batchSaleQuote').click(function () {$('#saleFrame').toggle();});
    $('#batchMailQuote').click(function () {$('#mailFrame').toggle();});



    $('#saleSure').click(function () {
        var sale = $('#saleNum').val();
        if(sale){
        if($('#saleSelect').val()==1){
            $('.batchsale').each(function () {
                var price = $(this).parent().find("td[class='batchprice']").children().val();
                $(this).children().val((price-sale).toFixed(2));
            })
        }else{
            $('.batchsale').each(function () {
                var price = $(this).parent().find("td[class='batchprice']").children().val();
                $(this).children().val((price*sale/100).toFixed(2));
            })
        }
        }
    });



        $('#mailSure').click(function () {
        var mail = $('#mailNum').val();
        if(mail){
        if($('#mailSelect').val()==1){
            $('.batchmail').each(function () {
                var price = $(this).parent().find("td[class='batchprice']").children().val();
                $(this).children().val((price-mail).toFixed(2));
            })
        }else{
            $('.batchmail').each(function () {
                var price = $(this).parent().find("td[class='batchprice']").children().val();
                $(this).children().val((price*mail/100).toFixed(2));
            })
        }
        }
    });



        $('#batchQuote').click(function () {
            $('.batchprice').each(function () {
                var price = $(this).parent().find("td[class='batchprice']").children().val();
                $(this).parent().find("td[class='batchsale']").children().val(price);
                $(this).parent().find("td[class='batchmail']").children().val(price);
            })
        })
    $('#batchSure').click(function () {
            batch_sure_request();/*
        $('.batchprice').each(function () {
            var id = $(this).parent().find("button[class='quoteSub']").attr('carId');
            var action_url = $("#carQuoteAction").val();
            $.ajax({
                url: action_url,
                dataType: "json",
                method: "POST",
                async:false,
                data: $("#carQuoteForm_" + id).serialize(),
                success: function(data){
                    if(data['res'] != "success"){
                        alert("error happened");
                    }
                }
            });
        });*/
         
    });
    if($("#saleSelect").val()==1){
        $('#saleRemind').text('单位：万元')
    }else{
        $('#saleRemind').text(" % ")
    }
    if($("#mailSelect").val()==1){
        $('#mailRemind').text('单位：万元')
    }else{
        $('#mailRemind').text(" % ")
    }
    $('#saleSelect').change(function () {
        if($("#saleSelect").val()==1){
            $('#saleRemind').text('单位：万元')
        }else{
            $('#saleRemind').text(" % ")
        }
    });
    $('#mailSelect').change(function () {
        if($("#mailSelect").val()==1){
            $('#mailRemind').text('单位：万元')
        }else{
            $('#mailRemind').text(" % ")
        }
    });



    function batch_sure_request() {
        var data = '';
        var _url = $('#batchCarquoteUrl').val();
        $('.batchprice').each(function () {
            var car_id = $(this).parent().find("input[name='carId']").val();
            var producer_id = $(this).parent().find("input[name='producer_id']").val();
            var producer_type = $(this).parent().find("input[name='producer_type']").val();
            var material_type = $(this).parent().find("input[name='material_type']").val();
            var quote_price = $(this).parent().find("input[name='quotePrice']").val();
            var sale_price = $(this).parent().find("input[name='salePrice']").val();
            var mail_price = $(this).parent().find("input[name='mailPrice']").val();
            var sale_time = $(this).parent().find("input[name='saleTime']").val();
            var mail_time = $(this).parent().find("input[name='mailTime']").val();

            // 经销商id 车款id 报价类型 材料类型 指导价 优惠价 秋波价 优惠期限 秋波期限
            var pdata = producer_id + "," + car_id + "," + producer_type + "," + material_type + "," + quote_price + "," + sale_price + "," + mail_price + "," + sale_time + "," + mail_time;
            data += pdata+"#";
        });
        console.log(data);
        $.ajax({
            url: _url,
            method: "POST",
            dataType: "json",
            data: { "model_data": data },
            success: function(data){
            if(data['res'] == "success"){
                alert("报价成功!");
                // var jump_url = $('#thisPageUrl').val();
                // window.location.href = jump_url;
                var index_url = $("#carQuoteIndexUrl").val();
                window.location.href = index_url;
                return false;
            }else{
                alert("报价失败!");
                return false;
                }
            }
        });
    }

    $('#batchDelete').click(function () {
        var data_list = '';
        var producer_id = '';
        $('#quotedtable').find("button[class='deleteQuoted']").each(function () {
            var car_id = $(this).attr('carId');
            producer_id = $(this).attr("producerId");
            data_list += car_id +","
        });
        var url = $("#batchCarQuoteDel").val();
        var role = $("#actionRole").val();
        var index_url = "";
        $.ajax({
            url: url,
            method: "get",
            dataType: "json",
            data: {"data_list": data_list,
                    "producer_id": producer_id
                },
            success: function (data) {
                if(data['res'] == "success"){
                    if(role == "producer"){
                        index_url = $("#carQuoteIndexUrl").val();

                    }else{
                        index_url = $("#adminCarQuoteIndex").val();
                    }
                    location.href = index_url;

                }else{
                    alert("error happened when delete car quote");
                }
            }

        })
    });

/*
function delQuote(obj){
    var url = $("#carQuoteDel").val();
    var role = $("#actionRole").val();
    var index_url = "";
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"carId": $(obj).attr("carId"),
                "producerId": $(obj).attr("producerId")
            },
        success: function (data) {
            if(data['res'] == "success"){
                if(role == "producer"){
                    index_url = $("#carQuoteIndexUrl").val();

                }else{
                    index_url = $("#adminCarQuoteIndex").val();
                }
                location.href = index_url;

            }else{
                alert("error happened when delete car quote");
            }
        }
    });
}
*/


});

