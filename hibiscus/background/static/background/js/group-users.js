$(document).ready(function () {

    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var _gId = $("#gId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url+"?gId="+_gId,
            type: 'GET',
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columns": [
            { "data": "name" },
            { "data": "phone"},
            { "data": "city" },
            { "data": "buy_type" },
            { "data": "created_at"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
});