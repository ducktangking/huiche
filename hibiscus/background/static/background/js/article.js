$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var parent_id = $("#articleParentId").val();
    var producer_id = $("#authProducerId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"parent_id": parent_id,
                "producer_id": producer_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 1,
            "render": function (data, type, row) {
                if(row["created_type"] == "o"){
                    return "原创";
                }else{
                    return "转载";
                }
            }
        },{
            "targets": 3,
            "render": function (data, type, row) {
                var txt = "<a href='/background/article/detail/?producer_id="+row["producer_id"]+"&arId="+row['DT_RowId']+"'>编辑</a>";
                return txt;
            }
        }],
        "columns": [
            { "data": "title" },
            {"data": "created_type"},
            { "data": "created_at" },
            {"data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
    event.preventDefault();
    //return false;
    var _url = $("#ActionNewUrl").val();
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#NewForm").serialize(),
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#IndexUrl").val();
                window.location.href = index_url;
                }
            }
        });

    });
});