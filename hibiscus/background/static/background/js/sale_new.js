/**
 * Created by lf on 17-8-23.
 */
var series_id = -1;
$(document).ready(function () {
    // var car_url = $("#car_url").val();
    var producer_id = $("#producer_id").val();
    $("#datetimeStart").datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    }).on("click",function(){
        console.log($("#datetimeEnd").val());
        console.log(new Date().toLocaleDateString());
        $("#datetimeStart").datetimepicker("setEndDate",$("#datetimeEnd").val());
        $("#datetimeStart").datetimepicker("setStartDate", new Date())
    });

    // $("#datetimeStart").datetimepicker("setEndDate",new Date());

    $("#datetimeEnd").datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    }).on("click",function(){
        // console.log("hehhehehehehe");
        $("#datetimeEnd").datetimepicker("setStartDate",$("#datetimeStart").val())
    });

    var sale_way_val = $("#sale_ways").val();
    if(sale_way_val == 1){
        $("#sale_way_2").hide();
        $("#sale_value_2").hide();
        $("#sale_way_1").show();
        $("#sale_value_1").show();
    }else{
        $("#sale_way_1").hide();
        $("#sale_value_1").hide();
        $("#sale_way_2").show();
        $("#sale_value_2").show();
    }

    $(".select_series").children().click(function () {
        series_id =  $(this).parent().find('input[type="radio"]').val();
        console.log(series_id);
        get_car_html(series_id, producer_id);
        get_series_color(series_id);
        $("#sale_value_1").prop("disabled", false);
        $("#sale_value_2").prop("disabled", false);

    });

    var radio_arr = $(".select_series :radio");
    var init_series = -1;
    radio_arr.each(function () {
        if($(this).prop("checked")){
            init_series = $(this).val();
            series_id = init_series;
            console.log(init_series);
        }
    });

    if(init_series != -1){
        get_car_html(init_series, producer_id);
        get_series_color(init_series);
        $("#sale_value_1").prop("disabled", false);
        $("#sale_value_2").prop("disabled", false);
    }
    switch_page_state(0);
        // ati_control_color()
});

// 从后台获取车款信息
function get_car_html(series_id, producer_id){
        var car_url = $("#car_url").val();
        $.ajax({
            url:car_url,
            method:"get",
            dataType:"html",
            data:{
                "producer_id":producer_id,
                "series_id":series_id
            },
            success:function (data) {
                $("#sale_car_label").show();
                $("#car_table").html(data);
                if($("#detail_edit").is(":hidden")){
                    $("#sale_car_simple_table").hide();
                    $("#sale_car_table").show()
                }else{
                    $("#sale_car_table").hide();
                    $("#sale_car_simple_table").show()
                }
            }
        })
}

//从后台获取车型颜色
function get_series_color(series_id) {
       var series_color_url = $("#series_color_url").val();
       $.ajax({
            url:series_color_url,
            method:"get",
            dataType:"json",
            data:{
                "series_id":series_id
            },
            success:function (data) {
                var text = "";
                // console.log(data.notEmpty);
                if(data.length != 0){
                    for(var i=0; i<data.length; i++){
                        var color_text = "<input type='checkbox' year='" +
                            data[i]['year'] +  "' value='" +data[i]['id']+
                            "' onclick='ati_control_color()'>" + data[i]['name'] + "&nbsp&nbsp";
                        text += color_text;
                    }
                    // $("#color_json").text(JSON.stringify(data));
                    $("#car_color_checkbox").html(text);
                    $("#car_color_div").show();
                    control_color();
                    var edit_data = new Object();
                    for(var j=0; j<data.length; j++){
                        var year = data[j]["year"];
                        if(!edit_data[year]){
                            edit_data[year] = new Object();
                        }
                        var id = data[j]['id'];
                        var name = data[j]['name'];
                        edit_data[year][id] = name
                    }
                    $("#color_json").text(JSON.stringify(edit_data))
                }else{
                    $("#car_color_div").hide();
                }
            }
        })
}

// 车型点击文字相当于点击按钮
function change_radio(obj) {
    var parent_div = $(obj).parent();
    var input = parent_div.find("input");
    if(!input.prop('checked')){
        input.get(0).checked = true;
    }
}

// 优惠两种方式的切换
function hide_one_unit(obj){
    var select_obj = $(obj);
    var select_value = select_obj.val();
    if(select_value == 1){
        $("#sale_way_2").hide();
        $("#sale_value_2").hide();
        $("#sale_way_1").show();
        $("#sale_value_1").show();
    }else{
        $("#sale_way_1").hide();
        $("#sale_value_1").hide();
        $("#sale_way_2").show();
        $("#sale_value_2").show();
    }
}

// 全选/反选颜色
function control_color() {
        // console.log($("#all_color_control").prop("checked"));
    var checkbox_arr = $("#car_color_checkbox :checkbox");
    if ($("#all_color_control").prop("checked")) {
        checkbox_arr.prop("checked", true);
        // checkbox_arr.attr("checked", true);
    } else {
        checkbox_arr.attr("checked", false);
    }
}

// $("#car_color_checkbox").children().click(ati_control_color());
// 反控制 <颜色齐全> 按钮的选中撤销
function ati_control_color(){
    var checkbox_arr = $("#car_color_checkbox :checkbox");
    var num = 0;
    var checkbox_len = checkbox_arr.length;
    for(var i=0; i<checkbox_len; i++){
        if($(checkbox_arr[i]).prop("checked")){
            num += 1
        }
    }
    var all_color_control = $("#all_color_control");
    if(num == checkbox_len){
        all_color_control.prop("checked", true);
    }else{
        all_color_control.prop("checked", false);
    }
    console.log(num);
}

// 促销页面 --- 控制选中某年款的车款
function choose_produce_year(obj){
    var year_obj = $(obj);
    var year = year_obj.val();
    if(year_obj.prop("checked")){
        $("#sale_car_simple_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", true)
        });
        $("#sale_car_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", true)
        })

    }else{
        $("#sale_car_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", false)
        })
        $("#sale_car_simple_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", false)
        });
    }
}

// 促销页面 --- 反控制 选择车年款 按钮
function ati_control_year(obj) {
    var checkbox_obj = $(obj);
    var year = checkbox_obj.attr("year");
    var car_id = checkbox_obj.val();
    if(checkbox_obj.prop("checked")){
        // var car_num = same_year_cars.length;
        var change = true;
        if($("#detail_edit").is(":hidden")){
            var same_year_cars = $("#sale_car_table :input[year='"+year+"']");
            same_year_cars.each(function () {
                if(!$(this).prop("checked")){
                    change = false
                }
            });
        }else{
            $("#car_" + car_id).prop("checked", true);
            var same_year_cars = $("#sale_car_simple_table :input[year='"+year+"']");
            same_year_cars.each(function () {
                if(!$(this).prop("checked")){
                    change = false
                }
            });
        }
        if(change){
            $("#year_control :input[value='" + year+ "']").prop("checked", true);
        }
    }else{
        $("#year_control :input[value='" + year+ "']").prop("checked", false);
        if(!$("#detail_edit").is(":hidden")){
            $("#car_" + car_id).prop("checked", false);
        }
    }
}

// 点击编辑详情后的页面动作
function detail_sale_info(obj){
    var producer_id = $("#producer_id").val();
    // console.log($("#sale_value_1").val());
    if(series_id != -1 ){
        if($("#sale_value_1").val()||$("#sale_value_2").val()) {
            $(obj).hide();
            set_sale_price();
            set_inventory_state();
            $("#sale_way_div").hide();
            $("#sale_car_simple_table").hide();
            switch_page_state(1)
            record_car_color();
            judge_car_color($("#all_color_control").prop("checked"));
            batch_write_color();
            $("#sale_car_table").show();
            add_detail_html(producer_id, series_id);
            $("#detail_div").show();
            $(".popover_huiche").each(function () {
                var _this = this;
                $(_this).popover({html : true }).on("click", function () {
                    var tr_parent = $(_this).parents("tr");
                    tr_parent.siblings().find(".popover_huiche").popover("hide")
                })
            })
        }
    }
}

function switch_page_state(status){
    if(status == 1){
        $("#inventory_state").hide();
        $("#simple_preview").hide();
        $("#detail_preview").show();
        $("#simple_submit").hide();
        $("#detail_submit").show();
    }else{
        $("#inventory_state").show();
        $("#simple_preview").show();
        $("#detail_preview").hide();
        $("#simple_submit").show();
        $("#detail_submit").hide();
    }
}

// 设置每个车款的促销价格
function set_sale_price(){
    var sale_way = $("#sale_ways").val();
    var sale_value = $("#sale_value_" + sale_way).val();
    if(sale_value>0){
        if(sale_way == 1){
            $(".car_discount_price").each(function () {
                $(this).val(sale_value);
                var origin_value = $(this).parent().parent().find(".origin_price").html();
                var finally_price_obj = $(this).parent().parent().find(".finally_price");
                var price = origin_value - sale_value;
                finally_price_obj.html(price)
            })
        }else{
            $(".origin_price").each(function () {
                var origin_value = $(this).html();
                var discount_value = (origin_value * sale_value/100).toFixed(2);
                $(this).parent().parent().find(".car_discount_price").val(discount_value);
                var finally_price_obj = $(this).parent().parent().find(".finally_price");
                var price = origin_value - discount_value;
                finally_price_obj.html(price.toFixed(2));
                // console.log(origin_value);
            })
        }
    }
}

// 设置每个车款的库存状态
function set_inventory_state() {
    // console.log($("#inventory_div :input").find(":checked"));
    var state = -1;
    $("#inventory_div :input").each(function () {
        if($(this).prop("checked")){
            state = $(this).val()
        }
    });
    console.log(state);
    $(".car_inventory").each(function () {
        $(this).val(state)
    })
}

//编辑详情部分html
function add_detail_html(producer_id, series_id) {
    $.ajax({
        url:$("#sale_new_detail").val(),
        data:{"producer_id":producer_id, "series_id":series_id},
        method:"get",
        dataType:"html",
        success:function (data) {
            $("#detail_div").html(data);
            var high_price = find_max_value();
            write_into_title(high_price);
        }
    })
}

//写入新闻标题和导语
function write_into_title(high_price) {
    var title_tmp = $("#title_tmp").val();
    var introduction_tmp = $("#introduction_tmp").val();
    var title = title_tmp.replace("<price>", high_price);
    var introduction = introduction_tmp.replace("<price>", high_price);
    $("#news_title").val(title);
    $("#news_introduction").val(introduction);
}

//找到优惠的最大值
function find_max_value(){
    var max = 0;
    $(".car_discount_price").each(function () {
        var discount_val = $(this).val() ;
        max = (max>discount_val)? max : discount_val;
    });
    return max
}


// 展开礼包信息
function show_gift_bag(obj){
    if($(obj).prop("checked")){
        $("#gift_bag_div").show();
    }else{
        $("#gift_bag_div").hide();
        $("#gift_bag_choise :input").prop("checked", false);
        $(".gift-bag-content").each(function () {
            var id = $(this).prop("id");
            gift_bag_content(id, false);
        })
    }
}

// 展开礼包具体条目
function gift_bag_detail_show(obj) {
    var bind_id = $(obj).attr("bind");
    var state = $(obj).prop("checked");
    gift_bag_content(bind_id, state);
}

// 某一礼包条目内容
function gift_bag_content(id, state){
    if(state){
        $("#" + id).show()
    }else{
        $("#" + id).hide();
        if(id=="upkeep_way_div"){
            console.log("hehe");
            $("#" + id +" :input[type='text']").val("");
            var first_radio = $("#" + id +" :input[type='radio']").eq(0);
            change_upkeep_way(first_radio);
        }else if(id=="buy_tax_div"){
            var radio_list = $("#" + id +" :input[type='radio']");
            radio_list.eq(0).prop("checked", true);
        }else{
            $("#" + id +" :input[type='text']").val("");
        }
    }
}

// 保养方式替换
function change_upkeep_way(obj) {
    var way_div = $(obj).parent();
    way_div.find("input[type='text']").prop("disabled",false);
    way_div.siblings().find("input[type='text']").prop("disabled", true);
}

// 批量改变 车型颜色 是否齐全图标
function judge_car_color(all_color){
    if(all_color){
        $(".show-color").hide();
        $(".single_car_all_color").show();
    }else{
        $(".single_car_all_color").hide();
        $(".show-color").show();
    }
}

// 将选中颜色记录到单个车型中
function record_car_color() {
    var result = new Object();
    $("#car_color_checkbox :input").each(function () {
        if($(this).prop("checked")){
            var year = $(this).attr("year");
            if(!result[year]){
                result[year] = [];
            }
            result[year].push($(this).val())
        }
    });
    $(".edit-car-color").each(function () {
        var pro_year = $(this).attr("year");
        // $(this).attr("data-content", "*****************");
        if(result[pro_year]){
            $(this).attr("chosen_color", result[pro_year])
        }
    })
}



function simple_article_modal_show(){
    record_car_color();
    set_sale_price();
    set_inventory_state();
    var _url = $("#preview_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "is_simple": 1,
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val()
        },
        success:function (data) {
            // console.log("hehhehe");
            // console.log(data);
            $("#article_body").html(data['html']);
            $("#article_model").modal("show");
        }
    })
}


function detail_article_modal_show() {
    var _url = $("#preview_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "is_simple":0,
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val(),
            "headline":$("#news_title").val(),
            "introduction":$("#news_introduction").val(),
            "big_pic_id":$("#big_pic_val").val(),
            "small_pic_str":$("#small_pic_val").val(),
            "is_upkeep":$("#is_upkeep").prop("checked"),
            "is_addr":$("#is_addr").prop("checked"),
            "is_map":$("#is_map").prop("checked"),
            "is_tel":$("#is_tel").prop("checked")
        },
        success:function (data) {
            $("#article_body").html(data['html']);
            $("#article_model").modal("show");
        }
    })
}

//
// function gift_big_value() {
//     var data = {};
//     var is_gift_bag = $("#is_send_gift_bag").prop("checked");
//     if(is_gift_bag){
//         var total = $("#gift_bag_total").val();
//
//     }
//     return data
//
// }


function sale_simple_submit() {
    var sale_way = $("#sale_ways").val();
    var sale_value = $("#sale_value_" + sale_way).val();

    if(series_id != -1 && sale_value && sale_value>0){
    record_car_color();
    set_sale_price();
    set_inventory_state();
    var _url = $("#sale_action_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "is_simple": 1,
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val()
        },
        success:function (data) {
            if(data['res'] == "success"){
                window.location.href= $("#sale_action_href").val() + "?producer_id";
            }
        }
    })

    }
}


function sale_detail_submit(){
    var _url = $("#sale_action_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "is_simple":0,
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val(),
            "headline":$("#news_title").val(),
            "introduction":$("#news_introduction").val(),
            "big_pic_id":$("#big_pic_val").val(),
            "small_pic_str":$("#small_pic_val").val(),
            "is_upkeep":$("#is_upkeep").prop("checked"),
            "is_addr":$("#is_addr").prop("checked"),
            "is_map":$("#is_map").prop("checked"),
            "is_tel":$("#is_tel").prop("checked")
        },
        success:function (data) {
            if(data['res'] == "success"){
                window.location.href= $("#sale_action_href").val() + "?producer_id";
            }
        }
    })

}





// 促销页面---批量写入颜色展示浮动窗
function batch_write_color() {
    console.log("aaaa")
    var data_str = $("#color_json").val();
    console.log(data_str);
    if (data_str) {
        var data = JSON.parse(data_str);
        $(".edit-car-color").each(function () {
            single_car_color_popover(this, data);
            $(this).attr("data-content", change_popover_html(this, data))
        })
    }
}


// 促销页面---改变显示颜色的弹窗显示内容 方法内部直接修改
function single_car_color_popover(obj, data) {
    console.log($(obj).attr("year"));
    var table_td = $(obj).parent().parent();
    var color_obj = table_td.find(".show-color");
    // console.log(color_obj);
    var color = $(obj).attr("chosen_color");
    var year = $(obj).attr("year");
    var color_num = 0;
    var text = "<ul style='width: 100%'>";
    if (color) {
        var color_list = color.split(",");
        color_num = color_list.length;
        console.log("color_num" + color_num);
        for (var i = 0; i < color_num; i++) {
            var name = data[year][color_list[i]];
            // console.log(name);
            var txt = "<li style='list-style-type:none; width: 50%;display: run-in'><span>" + name + "</span></li>";
            text += txt;
        }
    }
    color_obj.attr("data-content", text + "</ul>");
    var title = "<div style='width: 80px;text-align: center'><span>";
    if (color_num > 0) {
        color_obj.attr("data-original-title", title +color_num + "色</span></div>");
    }else{
        color_obj.attr("data-original-title", title + "无颜色</span></div>");
    }
}



// 促销页面---改变修改颜色弹窗显示内容 返回txt
function change_popover_html(obj, color_json) {
    // console.log("popover_html");
    // console.log(obj);
    var year = $(obj).attr("year");
    var chosen_color_str = $(obj).attr("chosen_color");
    var a_id = $(obj).prop("id");
    var chosen_color_list = chosen_color_str.split(",");
    var color_obj = color_json[year];
    var text = "";
    var all_color_list = [];
    console.log(a_id+ "a_id");
    for (var id in color_obj){
        var name = color_obj[id];
        all_color_list.push(id);
        if($.inArray(id, chosen_color_list)>= 0){
            var txt = "<div style='padding-right: 10px;display: inline; padding-bottom: 5px' >" +
                "<input type='checkbox' value='"+ id
                +"' checked='checked' class='single_color' onclick='single_car_ati_color_control(this)'><span>"+ name
                +"</span></div>"
        }else{
            var txt = "<div style='padding-right: 10px;display: inline; padding-bottom: 5px'>" +
                "<input type='checkbox' value='"+ id
                +"' class='single_color' onclick='single_car_ati_color_control(this)'><span>"+ name
                +"</span></div>"
        }
        text += txt;
    }
    text += "</div>";
    var html = "";

    if(all_color_list.length>chosen_color_list.length){
        html = "<div style='width: 150px;'>" +
            "<div style='text-align: center'>" +
            "<input type='checkbox' onclick='single_car_color_control(this)' class='all_color'>" +
            "<span>全部颜色</span></div>"+
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" + text +
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" +
            "<div style='text-align: center; padding-top:15px'>" +
            "<a class='btn btn-sm btn-warning' bind_id='"+ a_id +
            "' onclick='ensure_car_color(this)'>确定</a></div>";
        ;
    }else{
        html = "<div style='width: 150px;'>" +
            "<div style='text-align: center'>" +
            "<input type='checkbox' checked='checked' onclick='single_car_color_control(this)' class='all_color'>" +
            "<span>全部颜色</span></div>"+
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" + text +
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" +
            "<div style='text-align: center; padding-top:15px'>" +
            "<a class='btn btn-sm btn-warning' bind_id='"+ a_id +
            "' onclick='ensure_car_color(this)'>确定</a></div>";
    }
    // console.log(html);
    return html
}

