/**
 * Created by lf on 17-1-25.
 */
$(document).ready(function () {
});
function area_stick(obj) {
    var model_id = $(obj).attr("modelId");
    var brand_id = $(obj).attr("brandId");
    var area_id = $(obj).attr("areaId");
    var text = $(obj).text();
    if (text == "置顶"){
        var status = 1
    }else{
        var status = null
    }
    $.ajax({
        url:$('#stickActionUrl').val(),
        dataType:"json",
        type:"get",
        data:{
            "model_id":model_id,
            "brand_id":brand_id,
            "area_id":area_id,
            "status": status
        },
        success:function (data) {
            if(data=="success"){
                window.location.href=window.location.href;
            }else{
                alert(text + "失败")
            }
        }
    })
}