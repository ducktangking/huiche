$(document).ready(function () {
    var data_url = $("#adDataUrl").val();
    $.ajax({
        url: data_url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            console.log(data);
            var areas = data["areas"];
            var location = data["location"];

            for(var i=0; i< areas.length; i++){
                if(areas[i]["id"] == 2){
                    var txt = "<option value='"+areas[i]["id"]+"'>全国</option>";
                $("#adArea").append(txt);
                }else{
                var txt = "<option value='"+areas[i]["id"]+"'>"+areas[i]["province"] +
                    "/"+ areas[i]["city"] +"</option>";
                $("#adArea").append(txt);
            }
            }
            // for(var j=0; j < location.length; j++){
            //     var txt1 = "<option value='"+location[j]["id"]+"'>"+location[j]["name"]+"</option>";
            //     $("#adLocation").append(txt1);
            // }
        }
    });
    var options = {
        // beforeSubmit: validate_brand_data,
        url: $("#ActionNewUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#IndexUrl").val();
            }
        }
    };
    // ajaxForm
    $("#NewForm").ajaxForm(options);
    // ajaxSubmit
    $("#picSub").submit(function (event) {
        event.defaultPrevented;
        $("#NewForm").ajaxSubmit(options);
        //return false;
    });

    // $("#adlocationSelect").click(function () {
    //     if($(this).is(":checked")){
    //         $("#adLocation").attr("disabled", "disabled");
    //     }else{
    //         $("#adLocation").removeAttr("disabled");
    //     }
    // });
});