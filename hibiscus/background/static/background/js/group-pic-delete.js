/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){

    $(".deleteGroupPicture").on("click", function () {

        var _url = $(this).attr("href");
        var _groupPicId = $(this).attr("groupPicId");
        var _gId = $(this).attr("gId");
        $.ajax({
            url: _url,
            method: "POST",
            data: {"groupPicId": _groupPicId, "gId": _gId},
            success: function(data){
                if(data == "success"){
                    var index_url = $("#groupPicIndexUrl").val();
                    window.location.href = index_url;
                }
            }
        });

        return false;
    });



});