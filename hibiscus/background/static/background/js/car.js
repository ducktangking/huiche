$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var series_id = $("#carSeriesId").val();
    var car_id = $('#series').val();
    var str = {};
    var edit_year = $('#CarProducerYear').val();
    var conditionVal = $('#conditionVal').text();
    for (var i = 1950; i <= 2050; i++) {
        if (edit_year) {
            if (i == edit_year)
                str += "<option value=" + i + " selected='selected'>" + i + "</option>";
            else
                str += "<option value=" + i + ">" + i + "</option>";
        }
        else {
            if (i == 2016)
                str += "<option value=" + i + " selected='selected'>" + i + "</option>";
            else
                str += "<option value=" + i + ">" + i + "</option>";
        }
    }
    $('#year').html(str);


    $('#action_url').html('<input type="hidden" id="IndexUrl" value="/background/car/?seriesId=' + car_id + '&SecondMenu=modelManage">');
    $('#series').change(function () {
        car_id = $('#series').val();
        $('#action_url').html('<input type="hidden" id="IndexUrl" value="/background/car/?seriesId=' + car_id + '&SecondMenu=modelManage">');
    });

    $('#DataTable').dataTable({
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"series_id": series_id}
        },
        "language": {
            "url": $("#lanFile").val()
        },
        "columnDefs": [
            {
                "targets": 7,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    if (row['is_hot']) {
                        return "是";
                    } else {
                        return "否";
                    }
                }
            }, {
                "targets": 8,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function (data, type, row) {
                    if (conditionVal) {
                        var edit_t = "<a href='/background/car/edit/?carId=" + row['DT_RowId'] + "&source=car'>编辑基本信息</a><br/>";
                    }
                    else {
                        var edit_t = "<a href='/background/car/edit/?carId=" + row['DT_RowId'] + "'>编辑基本信息</a><br/>";
                    }
                    var txt = "<a href='/background/car/params/?carId=" + row['DT_RowId'] + "'>车款参数管理</a><br/>";
                    var newpic = "<a href='/background/car/new/pic/index/?carId=" + row['DT_RowId'] + "'>车款图片管理</a><br/>";
                    var color_manage = "<a href='/background/car/color?car_id=" + row["DT_RowId"] + "'>颜色管理</a>";
                    return edit_t + txt + newpic + color_manage;
                }
            }
        ],
        "columns": [
            {"data": "brand"},
            {"data": "series"},
            {"data": "produce_year"},
            {"data": "cc"},
            {"data": "gearbox"},
            {"data": "name"},
            {"data": "price"},
            {"data": "is_hot"},
            {"data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function (row, data) {
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');
            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if (index === -1) {
            selected.push(id);
        } else {
            selected.splice(index, 1);
        }

        $(this).toggleClass('selected');
    });

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function () {
        if (selected.length == 0) {
            $("#ConfirmModal").modal('show');
        } else {
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if (ids_l != 0) {
            for (var i = 0; i < selected.length; i++) {
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function (data) {
                    console.log(data);
                    if (data["res"] == "success") {
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
    /*

     $("#NewForm").submit(function (event) {
     event.preventDefault();
     //return false;
     var _url = $("#ActionNewUrl").val();
     $.ajax({
     url: _url,
     dataType: "json",
     method: "POST",
     data: $("#NewForm").serialize(),
     success: function (data) {
     if (data["res"] == "success") {
     var index_url = $("#IndexUrl").val();
     window.location.href = index_url;
     }
     }
     });

     });*/


    $("#clearCon").click(function () {
        var url = $("#carIndex").val();
        location.href = url;
    });


    $('#CarPrice,#CarPriceMax,#CarPriceMin,#CarPower').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
        if (eventObj.ctrlKey == true || (keyCode >= 48 && keyCode <= 57) || keyCode == 8 || keyCode == 9 || keyCode == 46 || keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
            return true;
        else
            return false;
    });


    $('#realC').css({display: "inline-block"}).attr({placeholder: "排量", maxLength: '8'});
    $('#CarName').css({display: "inline-block"}).attr({placeholder: "请输入16位以内的款式名称"});
    $('#CarPrice').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入指导价",
        maxLength: '8'
    });
    $('#CarPriceMin').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入最低价",
        maxLength: '8'
    });
    $('#CarPriceMax').css({display: "inline-block", imeMode: "disabled"}).attr({
        placeholder: "请输入最高价",
        maxLength: '8'
    });
    $('#CarPower').css({display: "inline-block"}).attr({placeholder: "请输入功率", maxLength: '8'});


    $('#NewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        submitHandler: function (validator, form, submitButton) {
        },
        fields: {
            realCC: {
                validators: {
                    notEmpty: {
                        message: '排量不得为空'
                    }
                }
            },
            CarName: {
                validators: {
                    notEmpty: {
                        message: '款式不得为空'
                    }
                }
            },
            CarPrice: {
                validators: {
                    notEmpty: {
                        message: '指导价不得为空'
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPriceMin: {
                validators: {
                    notEmpty: {
                        message: '最低价格不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPriceMax: {
                validators: {
                    notEmpty: {
                        message: '最高价格不得为空',
                    },
                    regexp: {
                        regexp: /^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,4})$/,
                        message: '价格格式错误',
                    },
                }
            },
            CarPower: {
                validators: {
                    notEmpty: {
                        message: '功率不得为空'
                    },
                    regexp: {
                        regexp: /^([1-9])([0-9]{0,2})$/,
                        message: '功率格式错误的',
                    },
                }
            }
        },

    }).on('success.form.bv', function (event) {
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function (data) {
                if (data["res"] == "success") {
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                }
            }
        });
    });
    if(!$("#parent_energy").val()){
    write_in_parent_energy();
    }
    // write_in_energy();
    if(!$("#energy").val()){
    write_in_energy();
    }
});




function write_in_parent_energy() {
    var data_json_str =$("#energy_data").val();
    console.log(data_json_str);
    if(data_json_str) {
        var data_json = JSON.parse(data_json_str);
        var text = "";
        for (var i = 0; i < data_json.length; i++) {
            var data = data_json[i];
            var txt = "<option value='" + data['id'] + "'> " + data['name'] + "</option>";
            text += txt;
        }
        $("#parent_energy").html(text);
    }
}


function write_in_energy() {
    var data_json_str =$("#energy_data").val();
    if(data_json_str ){
        var data_json = JSON.parse(data_json_str);
        var text = "";
        var chosen_kind =  parseInt($("#parent_energy").val());
        for(var i=0;i<data_json.length; i++)    {
            var data = data_json[i];
            console.log(data['id'] + chosen_kind);
            if (data['id'] == chosen_kind){
                var child_data_arr = data['energy'];
                for(var j=0;j<child_data_arr.length; j++) {
                    var child_data = child_data_arr[j];
                    var txt = "<option value='" + child_data['id'] + "'> " + child_data['name'] + "</option>";
                    text += txt;
                }
            }
        }
        console.log(text);
        $("#energy").html(text);
    }
}