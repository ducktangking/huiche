/**
 * Created by gmj on 4/11/16.
 */

$(document).ready(function(){
    var api;
    var selected = [];
    var _url = $("#carPicIndexData").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        "language": {
                "url": $("#lanFile").val()
            },
        ajax: {
            url: _url,
            type: 'GET'
        },
        "columnDefs": [
/*            {
                "ordering":false,
                "orderable":false,
                "targets":2
            },{
                "ordering":false,
                "orderable":false,
                "targets":3
            },*/
            {
                "targets": 4,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=1'>外观</a></div>";
                }
            },   {
                "targets": 5,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=2'>内饰</a></div>";
                }
            }, {
                "targets": 6,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=3'>空间</a></div>";
                }
            }, {
                "targets": 7,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=4'>图解</a></div>";
                }
            }, {
                "targets": 8,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=5'>官方</a></div>";
                }
            }, {
                "targets": 9,
                "ordering": false,
                "orderable": false,
                "searching": false,
                "render": function(data, type, row){
                    return "<div><a href='/background/car/pic/detail/?carId="+row["DT_RowId"]+"&typeId=6'>车展</a></div>";
                }
            }
        ],
        "columns": [
                    { "data": "brand" },
                    { "data": "series" },
            {"data": "produce_year"},
                    {"data": "name"},
                    {"data": "exterior  "},
                    {"data": "interior"},
            {"data": "space"},
            {"data": "diagram"},
            {"data": "offical"},
            {"data": "auto_show"}
                    // {"data": "action"}
        ],

        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

});