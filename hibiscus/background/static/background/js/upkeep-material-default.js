$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    function render_bind_area_info(){
        var area_data = area_info();
        for(var i=0; i<area_data.length; i++){
            var item = area_data[i];
            var txt = "<option>"+ item["province"] +"</option>";
            $("#bindProvince").append(txt);

        }
    }
    render_bind_area_info();


    $('#MaterialAreaDataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [
            {
                "targets": 5,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    var t;
                    console.log(row);
                    if(!row["is_default"]){
                        t = "<div value='"+row["DT_RowId"]+"' onclick='set_material_default_area(this)'>设为默认</div>"
                    }else{
                        t = "<span style='color:red;'>默认显示</span>";
                    }
                    return t;

                }
            },   {
                "targets": 4,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    if(row["default_area"]){
                        if(row['default_area'] == "All Areas"){
                            return "全国";
                        }
                        return row["default_area"];
                    }else{
                        return "无";
                    }

                }
            },{
                "targets": 5,
                "ordering": false,
                "searching": false,
                "render": function (data, type, row) {
                    var t = "<a href='/background/upkeep/material/area/config/?materialId="+row['DT_RowId']+"'>默认信息设置</a>";
                    return t;
                }
            }],
        "columns": [
            { "data": "brand" },
            {"data": "code"},
            {"data": "model"},
            {"data": "price"},
            {'data': "default_area"},
            { "action": "action" }
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });

    
});