$(document).ready(function () {
    var url = $("#producerCarSeriesUrl").val();
    var producer_id = $("#producerId").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data:{"producer_id": producer_id,
            "producer_series_type": 0},
        success: function (data) {
            var other_series = data[1];

            if(other_series.length != 0){
                for(var j=0; j<other_series.length; j++){
                    var t = "<div style='float:left;margin-left: 15px;'><input type='checkbox' name='other_series' " +
                        "value='"+other_series[j]["series_id"]+"'>" + other_series[j]["series_name"] + "</div>";
                    $("#" +other_series[j]['initial']+"_pos").append(t);
                    $("#" +other_series[j]['initial']+"_all").show();
                }
            }
        }
    });

$("#producerCarSeriesForm").submit(function(event){
    event.preventDefault();
    var _url = $("#producerCarSeriesAction").val();
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#producerCarSeriesForm").serialize(),
        success: function(data){
            console.log(data);
            if(data["res"] == "success"){
                var index_url = $("#producerCarSeriesIndexUrl").val();
                window.location.href = index_url;
            }
        }
    });
});



//    datatable 展示已代理品牌
    var api;
    var selected = [];
    var _url = $("#producerCarSeriesData").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
                "targets": 2,
                "ordering": false,
                "searching": false,
                "render": function(data, type, row){
                    // var t1 = "<div series_id='"+row['DT_RowId']+"' onclick='del_producer_car_series(this)'>删除</div>"
                    // var t = "<a href='/background/user/producer/car/series/delete/?producer_car_series_id="+row["DT_RowId"]+"'>删除</a>";
                   return "";
                }
            },{
            "targets": 0,
            "ordering": false,
            "searching": false,
            "render": function (data, type, row) {
                if(row['type'] == 0){
                    return "新车";
                }
                if(row['type'] == 1){
                    return "保养";
                }
                if(row['type'] == 2){
                    return "板金";
                }
            }
        }
        ],
        "columns": [
            // {"data": "producer_name"},
            {"data": "type"},
            {"data": "brand_name"},
            {"data": "action"}

        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#producerCarSeriesDelUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: {"ids": ids,
                        "producer_id": $("#producerId").val()},
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });
});