/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    if($("#carPicNewAction").val()){
        var options = {
            url: $("#carPicNewAction").val(),
            type: "post",
            success: function (data) {
                if(data == "success"){
                    location.href = $("#carPicIndexUrl").val();
                }
            }
        };
    }
    else {
        var options = {
            url: $("#batchCarPicNewAction").val(),
            type: "post",
            success: function (data) {
                if(data == "success"){
                    location.href = $("#carPicIndexUrl").val();
                }
            }
        }
    }
    // ajaxForm
    $("#carPicNewForm").ajaxForm(options);
    // ajaxSubmit
    $("#btnCarPicNew").submit(function (event) {
        event.preventDefault();
        $("#carPicNewForm").ajaxSubmit(options);
    }); 
    

});

/*   $('#carPicNewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            carPic:{
                validators:{
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                    notEmpty: {
                        message: '请上传图片'
                    },
                }
            },
            carPicName:{
                validators:{
                    notEmpty: {
                        message: '图片名称不得为空'
                    },
                }
            }
        }

        });*/