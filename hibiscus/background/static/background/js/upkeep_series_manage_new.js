$(document).ready(function () {

    $("#upSeriesForm").submit(function (event) {
        event.preventDefault();
        var year = [];
        $("input[name=manageProduceYear]").each(function () {
           if($(this).prop("checked")){
               year.push($(this).val());
           }
        });
        if(year.length == 0){
            alert("请选择年份");
            return false;
        }
        $.ajax({
            url: $("#UpSeriesNewUrl").val(),
            method: "post",
            dataType: "json",
            data: $("#upSeriesForm").serialize(),
            success: function (data) {
                if(data['res'] == "success"){
                    location.href = $("#UpSeriesIndexUrl").val();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.readyState);
                console.log(textStatus);
                console.log(errorThrown);
            }

        });
    });

    $('#upSeriesForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            manageCC: {
                validators: {
                    notEmpty: {
                        message: '排量不得为空'
                    }
                }
            },
        }
    });
});