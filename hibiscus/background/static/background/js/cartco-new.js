/**
 * Created by gmj on 4/20/16.
 */

$("#cartcoData").submit(function(event){
    event.preventDefault();
    //return false;
    var _url = $("#cartco_action_val").attr("action");
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#cartcoData").serialize(),
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#cartco_index_url").attr("action");
                window.location.href = index_url;
            }
        }
    });

});
$(document).ready(function () {
    $(".pick-a-color").pickAColor({
        showSpectrum            : true,
        showSavedColors         : true,
        saveColorsPerElement    : true,
        fadeMenuToggle          : true,
        showAdvanced			: true,
        showBasicColors         : true,
        showHexInput            : true,
        allowBlank				: true,
        inlineDropdown			: true
    });

});