/**
 * Created by lf on 17-8-23.
 */

$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var edit_url = $("#editUrl").val();
    // var parent_id = $("#articleParentId").val();
    var producer_id = $("#authProducerId").val();
    $('#DataTable').dataTable({
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"producer_id": producer_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 1,
            "render": function (data, type, row) {
                if(row["type"] == 0){
                    return "降价促销";
                }else{
                    return "礼包促销";
                }
            },
            "orderable" : false
        },{
            "targets": 2,
            "render": function (data, type, row) {
               if (row["sale_status"] == 0){
                   return "未开始"
               }else if(row["sale_status"] == -1){
                   return "已结束"
               }else{
                   return "进行中"
               }
            }
        },{
            "targets": 6,
            "orderable" : false,
            "render": function (data, type, row) {
               return "<a href='"+edit_url+"?producer_id&sale_id="+row['DT_RowId']+"' >编辑</a>"
            }
        }],
        "columns": [
            { "data": "title" },
            {"data": "type"},
            {"data": "sale_status"},
            { "data": "sale_time" },
            {"data": "publish_time"},
            {"data": "view_count"},
            {"data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
    })
        $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

});
