/**
 * Created by lf on 17-6-18.
 */
var delete_arr = [];
$(document).ready(function () {
    $(".pick-a-color").pickAColor({
        showSpectrum: true,
        showSavedColors: true,
        saveColorsPerElement: true,
        fadeMenuToggle: true,
        showAdvanced: true,
        showBasicColors: true,
        showHexInput: true,
        allowBlank: true,
        inlineDropdown: true
    });


    $("#submit_color_edit").click(function (event) {
        event.defaultPrevented;
        event.preventDefault();

        var series_color_id = $("#series_color_id").val();
        var tbody = $("#tbody");
    // console.log(tbody.children().length);
    // console.log(tbody.children().eq(0).attr("color_name"));
    //     var year = $("#producer_year").val();
    //     var series_id = $("#series_id").val();
        var color_str = "";
        for(var i=0; i < tbody.children().length; i++){
            // console.log("hhhhh" + i);
            var tr = tbody.children().eq(i);
            // console.log(tr);
            var color_id = tr.attr("id");
            var color_name = tr.attr("color_name");
            var color_type = tr.attr("cyname");
            var color_value = tr.attr("cvalue");
            console.log(color_id +"+"+color_name + "+" + color_type + "+" +color_value);
            var color_con = color_id + "," +color_name + "," + color_type + "," + color_value;
            color_str += color_con + ";"
        };
        var delete_id_str = delete_arr.join(",");
        console.log("delete:"+delete_id_str);
        $.ajax({
            url:$("#color_edit_action_url").val(),
            type:"post",
            dataType: "json",
            data:{
                "series_color_id":series_color_id,
                // "year":year,
                "color_str":color_str,
                "delete_id":delete_id_str
            },
            success: function (data) {
                if(data["res"] == "success"){
                    console.log($("#color_index_url").val());
                    location.href = $("#color_index_url").val();
                }
            }

        })


    });
});


function show_name_edit(obj) {
    var color_id = $(obj).attr("color_id");
    console.log(color_id);
    $(obj).hide();
    $("#show_"+ color_id).hide();
    $("#input_" + color_id).show();
    $("#save_"+ color_id).show();
}

function save_edit_name(obj) {
    var color_id = $(obj).attr("color_id");
    var color_name = $("#input_"+color_id).val();
    if (color_name) {
        console.log(color_name);
        // console.log($("#show_"+color_id).html());
        $("#show_" + color_id).html(color_name);

        $(obj).hide();
        $("#show_" + color_id).show();
        $("#edit_" + color_id).show();
        $("#input_" + color_id).hide();
        $("#" + color_id).attr("color_name", color_name)
    }
}

function color_side_edit(obj) {
    var side = $(obj).val();
    var color_id = $(obj).attr("color_id");
    $("#"+ color_id).attr("cyname", side);
}

function color_value_edit(obj) {
    var color_id = $(obj).attr("color_id");
    $(obj).hide();
    $("#color_value_div_"+ color_id).show();
}

function color_value_save(obj) {
    var color_id = $(obj).attr("color_id");
    var value = $(obj).val();
    console.log(value);
    if (value) {
        $("#" + color_id).attr("cvalue", "#"+value);
        $("#blank_" + color_id).css("background-color", "#"+value);
        $("#color_value_div_" + color_id).hide();
        $("#blank_" + color_id).show();
    }
}


function delete_color(obj) {
    var tr = $(obj).parent().parent();
    var id = tr.attr("id");
    delete_arr.push(id);
    $("#"+id).remove();
    // console.log(delete_arr);
}