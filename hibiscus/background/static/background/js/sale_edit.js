$(document).ready(function () {
    var producer_id = $("#producer_id").val();
    $("#datetimeStart").datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    }).on("click",function(){
        console.log($("#datetimeEnd").val());
        console.log(new Date().toLocaleDateString());
        $("#datetimeStart").datetimepicker("setEndDate",$("#datetimeEnd").val());
        $("#datetimeStart").datetimepicker("setStartDate", new Date())
    });

    // $("#datetimeStart").datetimepicker("setEndDate",new Date());

    $("#datetimeEnd").datetimepicker({
        language: 'zh-CN',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    }).on("click",function(){
        // console.log("hehhehehehehe");
        $("#datetimeEnd").datetimepicker("setStartDate",$("#datetimeStart").val())
    });
    get_series_color_json(series_id);

    batch_write_color();
    $(".popover_huiche").each(function () {
                var _this = this;
                $(_this).popover({html : true }).on("click", function () {
                    var tr_parent = $(_this).parents("tr");
                    tr_parent.siblings().find(".popover_huiche").popover("hide")
                })
            })
});

// 促销页面 --- 控制选中某年款的车款
function choose_produce_year(obj){
    var year_obj = $(obj);
    var year = year_obj.val();
    if(year_obj.prop("checked")){
        $("#sale_car_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", true)
        })
    }else{
        $("#sale_car_table :input[year='"+year+"']").each(function () {
            $(this).prop("checked", false)
        })
    }
}


// 促销页面 --- 反控制 选择车年款 按钮
function ati_control_year(obj) {
    var checkbox_obj = $(obj);
    var year = checkbox_obj.attr("year");
    var car_id = checkbox_obj.val();
    if(checkbox_obj.prop("checked")){
        // var car_num = same_year_cars.length;
        var change = true;
            var same_year_cars = $("#sale_car_table :input[year='"+year+"']");
            same_year_cars.each(function () {
                if(!$(this).prop("checked")){
                    change = false
                }
            });
        if(change){
            $("#year_control :input[value='" + year+ "']").prop("checked", true);
        }
    }else{
        $("#year_control :input[value='" + year+ "']").prop("checked", false);
        $("#car_" + car_id).prop("checked", false);
    }
}


function get_series_color_json(series_id) {
       var series_color_url = $("#series_color_url").val();
       $.ajax({
            url:series_color_url,
            method:"get",
            dataType:"json",
            async:false,
            data:{
                "series_id":series_id
            },
            success:function (data) {
                if(data.length != 0){
                    var edit_data = new Object();
                    for(var j=0; j<data.length; j++){
                        var year = data[j]["year"];
                        if(!edit_data[year]){
                            edit_data[year] = new Object();
                        }
                        var id = data[j]['id'];
                        var name = data[j]['name'];
                        edit_data[year][id] = name
                    }
                    $("#color_json").text(JSON.stringify(edit_data))
                }
            }
        })

}


function sale_detail_submit(){
    var _url = $("#edit_action_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "sale_id":$("#sale_id").val(),
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val(),
            "headline":$("#news_title").val(),
            "introduction":$("#news_introduction").val(),
            "big_pic_id":$("#big_pic_val").val(),
            "small_pic_str":$("#small_pic_val").val(),
            "is_upkeep":$("#is_upkeep").prop("checked"),
            "is_addr":$("#is_addr").prop("checked"),
            "is_map":$("#is_map").prop("checked"),
            "is_tel":$("#is_tel").prop("checked")
        },
        success:function (data) {
            if(data['res'] == "success"){
                console.log($("#edit_action_href").val());
                window.location.href= $("#edit_action_href").val() + "?producer_id";
            }
        }
    })

}





// 促销页面---批量写入颜色展示浮动窗
function batch_write_color() {
    console.log("aaaa");
    var data_str = $("#color_json").text();
    console.log(data_str);
    if (data_str) {
        var data = JSON.parse(data_str);
        $(".edit-car-color").each(function () {
            // single_car_color_popover(this, data);
            $(this).attr("data-content", change_popover_html(this, data))
        })
    }
}


// 促销页面---改变显示颜色的弹窗显示内容 方法内部直接修改
function single_car_color_popover(obj, data) {
    console.log($(obj).attr("year"));
    var table_td = $(obj).parent().parent();
    var color_obj = table_td.find(".show-color");
    // console.log(color_obj);
    var color = $(obj).attr("chosen_color");
    var year = $(obj).attr("year");
    var color_num = 0;
    var text = "<ul style='width: 100%'>";
    if (color) {
        var color_list = color.split(",");
        console.log(color);
        color_num = color_list.length;
        console.log("color_num" + color_num);
        for (var i = 0; i < color_num; i++) {
            var name = data[year][color_list[i]];
            // console.log(name);
            var txt = "<li style='list-style-type:none; width: 50%;display: run-in'><span>" + name + "</span></li>";
            text += txt;
        }
    }
    color_obj.attr("data-content", text + "</ul>");
    var title = "<div style='width: 80px;text-align: center'><span>";
    if (color_num > 0) {
        color_obj.attr("data-original-title", title +color_num + "色</span></div>");
    }else{
        color_obj.attr("data-original-title", title + "无颜色</span></div>");
    }
}



// 促销页面---改变修改颜色弹窗显示内容 返回txt
function change_popover_html(obj, color_json) {
    // console.log("popover_html");
    // console.log(obj);
    var year = $(obj).attr("year");
    var chosen_color_str = $(obj).attr("chosen_color");
    var a_id = $(obj).prop("id");
    var chosen_color_list = chosen_color_str.split(",");
    var color_obj = color_json[year];
    var text = "";
    var all_color_list = [];
    console.log(a_id+ "a_id");
    for (var id in color_obj){
        var name = color_obj[id];
        all_color_list.push(id);
        if($.inArray(id, chosen_color_list)>= 0){
            var txt = "<div style='padding-right: 10px;display: inline; padding-bottom: 5px' >" +
                "<input type='checkbox' value='"+ id
                +"' checked='checked' class='single_color' onclick='single_car_ati_color_control(this)'><span>"+ name
                +"</span></div>"
        }else{
            var txt = "<div style='padding-right: 10px;display: inline; padding-bottom: 5px'>" +
                "<input type='checkbox' value='"+ id
                +"' class='single_color' onclick='single_car_ati_color_control(this)'><span>"+ name
                +"</span></div>"
        }
        text += txt;
    }
    text += "</div>";
    var html = "";

    if(all_color_list.length>chosen_color_list.length){
        html = "<div style='width: 150px;'>" +
            "<div style='text-align: center'>" +
            "<input type='checkbox' onclick='single_car_color_control(this)' class='all_color'>" +
            "<span>全部颜色</span></div>"+
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" + text +
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" +
            "<div style='text-align: center; padding-top:15px'>" +
            "<a class='btn btn-sm btn-warning' bind_id='"+ a_id +
            "' onclick='ensure_car_color(this)'>确定</a></div>";
        ;
    }else{
        html = "<div style='width: 150px;'>" +
            "<div style='text-align: center'>" +
            "<input type='checkbox' checked='checked' onclick='single_car_color_control(this)' class='all_color'>" +
            "<span>全部颜色</span></div>"+
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" + text +
            "<div style='height:1px;width: 90%;background-color: #e5e5e5'></div>" +
            "<div style='text-align: center; padding-top:15px'>" +
            "<a class='btn btn-sm btn-warning' bind_id='"+ a_id +
            "' onclick='ensure_car_color(this)'>确定</a></div>";
    }
    // console.log(html);
    return html
}



function detail_article_modal_show() {
    var _url = $("#preview_url").val();
    $.ajax({
        url:_url,
        method:"post",
        dataType:"json",
        data:{
            "is_simple":0,
            "series_id":series_id,
            "producer_id":$("#producer_id").val(),
            "car_str":make_up_car_string(),
            "sale_start":$("#datetimeStart").val(),
            "sale_end":$("#datetimeEnd").val(),
            "headline":$("#news_title").val(),
            "introduction":$("#news_introduction").val(),
            "big_pic_id":$("#big_pic_val").val(),
            "small_pic_str":$("#small_pic_val").val(),
            "is_upkeep":$("#is_upkeep").prop("checked"),
            "is_addr":$("#is_addr").prop("checked"),
            "is_map":$("#is_map").prop("checked"),
            "is_tel":$("#is_tel").prop("checked")
        },
        success:function (data) {
            $("#article_body").html(data['html']);
            $("#article_model").modal("show");
        }
    })
}




