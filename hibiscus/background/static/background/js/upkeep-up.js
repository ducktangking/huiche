$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var type_id = $("#typeId").val();
    var pkg_id = $("#pkgId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"pkg_id": pkg_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 4,
            "render": function (data, type, row) {
                var txt = "";
                if(row["type_id"] == 1){
                    txt = txt + "<a href='/background/upkeep/material/?up_id="+row["DT_RowId"]+"&type_id="+type_id+"&pkg_id="+pkg_id+"&SecondMenu=upMaterialManage'>" +
                    "材料管理</a>";
                    txt = txt + "<br/>";
                }

                txt = txt + "<a href='/background/upkeep/proj/edit/?up_id="+row["DT_RowId"]+"&type_id="+type_id+"&pkg_id="+pkg_id+"'>" +
                "&nbsp;&nbsp;&nbsp;编辑&nbsp;&nbsp;&nbsp;</a>";

                return txt;
            }
        },
        {
            "targets": 2,
            "ordering": false,
            "orderable": false,
            "render": function (data, type, row) {
                return row["details"];
            }
        },{
            "targets": 1,
            "ordering": false,
            "orderable": false}],
        "columns": [
            { "data": "name" },
            { "data": "packages" },
            { "data": "details" },
            { "data": "created_at" }
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });

    });

    $("#clearCon").click(function () {
        var url = $("#prjIndex").val();
        location.href = url;
    });
})