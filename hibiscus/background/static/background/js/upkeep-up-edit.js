$(document).ready(function () {

 
 
 
    $('#projName').css({display:'inline-block'}).attr({maxlength:'16',placeholder:"输入16位以内的项目名称"});
    $('#lowPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入低端品牌报价"});
    $('#middlePrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入中端品牌报价"});
    $('#highPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入高端品牌报价"});
    $('#minPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入项目最低价格"});
    $('#maxPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入项目最高价格"});
    $('#origPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入原厂工时费指导价格"});
    $('#genPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入普通工时费指导价格"});
    $('#labMinPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入工时费最低价格"});
    $('#labMaxPrice').css({display:'inline-block',imeMode:"disabled"}).attr({maxlength:'8',placeholder:"输入工时费最高价格"});

     function in_array(search,array){
        for(var i in array){
            if(array[i]==search){
                return true;
            }
        }
        return false;
    }

    function get_up_type() {
        var _url = $("#pkgUrl").val();
        var type_id = $("#typeId").val();
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"type_id": type_id},
            success: function (data) {
                var pkg_data = data;

                selectedPackagesArray = $("#selectedPackages").val().split(',');
                console.log(selectedPackagesArray);
                for(var j=0; j<pkg_data.length; j++){
                    if (in_array(pkg_data[j]["id"], selectedPackagesArray)){
                        var t1 = "<input type='checkbox' name='upPkg' checked='checked' value='"+pkg_data[j]["id"]+"'>" + pkg_data[j]["name"];
                    }else{
                        var t1 = "<input type='checkbox' name='upPkg' value='"+pkg_data[j]["id"]+"'>" + pkg_data[j]["name"];
                    }

                    $("#upPkg").append(t1);
                }
            }
        });

        if($("#typeId").val() == 1){
            $("#upParams1").hide();
            $("#upParams2").show();
        }else{
            $("#upParams2").hide();
            $("#upParams1").show();
        }

    }
    get_up_type();
    
    $("#UpkeepEditForm").submit(function(event){
        event.preventDefault();
        //return false;

        var all = "";
        $("#upPkg").find(":checkbox:checked").each(function () {
            console.log($(this).attr("value"));
            all = $(this).attr("value") + "," + all;
        });
        if(all == ""){
            alert("请至少选择一个参会的套餐！");
            return false;
        }
        $("#selectedPackages").val(all.substr(0, all.length-1));

    });


    $('#UpkeepEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            projName: {
                validators: {
                    notEmpty: {
                        message: '项目名称不得为空'
                    }
                }
            },
            lowPrice: {
                validators: {
                    notEmpty: {
                        message: '低端品牌报价不得为空'
                    }
                }
            },
            middlePrice: {
                validators: {
                    notEmpty: {
                        message: '中端品牌报价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                },
            },
            highPrice: {
                validators: {
                    notEmpty: {
                        message: '高端品牌报价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            minPrice: {
                validators: {
                    notEmpty: {
                        message: '项目最低价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            maxPrice: {
                validators: {
                    notEmpty: {
                        message: '项目最高价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            origPrice: {
                validators: {
                    notEmpty: {
                        message: '原厂工时费指导价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            genPrice: {
                validators: {
                    notEmpty: {
                        message: '普通工时费指导价不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            labMinPrice: {
                validators: {
                    notEmpty: {
                        message: '工时费最低价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
            labMaxPrice: {
                validators: {
                    notEmpty: {
                        message: '工时费最高价格不得为空'
                    },
                    regexp: {
                        regexp:/^[0-9]+\.+([0-9]{2})$|^([1-9])([0-9]{0,7})$/,
                        message: '价格格式错误',
                    },
                }
            },
        }
    }).on('success.form.bv',function () {
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#UpkeepEditForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });

    });
});