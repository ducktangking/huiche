    var conditionList;
    var api;

$(document).ready(function () {
    var selected = [];
    var _url = $("#DataUrl").val();
    var table = $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            data: function (d) {//添加额外的参数传给服务器
                d.filter = conditionList;
               },
            type: 'GET'
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 1,
            "ordering": true,
            "orderable": true,
            "render": function (data, type, row) {
                if(row["created_type"] == "o"){
                    return "原创";
                }else{
                    return "转载";
                }
            }
        },{
            "targets": 6,
            "ordering":false,
            "orderable": false,
            "render": function (data, type, row) {
                if(row['status']){
                    var status = "已通过";
                }else{
                    var status = "未通过";
                }
                if(row['is_new']){
                    status += "&nbsp;|&nbsp;";
                    status += "最新";
                }else{
                    status += "&nbsp;|&nbsp;";
                    status += "非最新";
                }
                return status;
            }
        },{
            "targets": 7,
            "ordering": false,
            "orderable": false,
            "render": function (data, type, row) {
                var action = "<div class='btn-group'>" +
                    "<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' onclick='popover_active(this)'>"+
                    "&nbsp操作&nbsp<span class='caret'></span></button><ul class='dropdown-menu' role='menu'>";
                if(row['status']){
                    action += "<li><a href='/background/article/switch/status/?arId="+row["DT_RowId"]+"&status="+row["status"]+"' arId='"+row["DT_RowId"]+"' status='"+row["status"]+"'>取消通过</a></li>";

                }else{
                    action += "<li><a href='/background/article/switch/status?arId="+row["DT_RowId"]+"&status="+row["status"]+"' arId='"+row["DT_RowId"]+"' status='"+row["status"]+"'>设置通过</a></li>";
                }

                if(row['is_new']){
                    action += "<li><a href='/background/article/switch/new/?arId="+row["DT_RowId"]+"&is_new="+row["is_new"]+"' arId='"+row["DT_RowId"]+"' isNew='"+row["is_new"]+"'>取消最新</a></li>";

                }else{
                    action += "<li><a href='/background/article/switch/new?arId="+row["DT_RowId"]+"&is_new="+row["is_new"]+"' arId='"+row["DT_RowId"]+"' isNew='"+row["is_new"]+"'>设置最新</a></li>";
                }
                action += "<li><a href='javascript:void(0)' class='article_popover'  id='show_position_"+row["DT_RowId"]+"' articleId='"+row["DT_RowId"]+"'>展示</a></li>";
                return action + "</ul><a href='/background/article/preview?arId=" +
                    row["DT_RowId"]+ "' style='cursor: pointer' class='btn' onclick='return article_preview(this)'>" +
                    "预览</a></div> ";
            }
        }],
        "columns": [
            { "data": "title" },
            { "data": "created_type"},
            { "data": "author"},
            { "data": "created_at" },
            { "data": "classfy"},
            { "data": "label"},
            { "data": "status"},
            { "data": "action"}
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }

        }
        });

    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();

/*   $('#test001').change(function () {
       api.ajax.reload();
       var args = api.ajax.params();
       console.log(args.test);
       // table.ajax.reload()
   });*/


    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
    event.preventDefault();
    //return false;
    var _url = $("#ActionNewUrl").val();
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#NewForm").serialize(),
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#IndexUrl").val();
                window.location.href = index_url;
                }
            }
        });

    });
    $('#carouselPage_2, #carouselPage_3, #carouselPage_4').hide();
    $('#pageNums').children().each(function () {
        var page_id = $(this).attr('pageId');
        $(this).mousemove(function () {
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().parent().find('div[class="carouselPage"]').each(function () {
                $(this).hide();
            });
            $('#carouselPage_'+ page_id).show();
        });
    });
    //$('#areaArticleSet').modal("show");
    
//    以下方法用于筛选功能
//    将选择的条件放入筛选栏

    // $("#list_condition input[type='checkbox']");
/*    var choosen_condition = $("input[id^='checkbox-filter-']");
    choosen_condition.change(function () {
        console.log(this);
        var name = this.getAttribute("text");//分类名称
        var class_id = this.getAttribute("classId")||1;//类型ID
        var id_key = this.getAttribute("idKey") || 6;//前后台交互使用，识别属于那种类型
    });*/

    // $("#list_condition").find("label").click(function () {
    //     console.log(this);
    //
    // });
    var con_list = $("#list_condition input.addfilter");
    if(con_list.length == 0){
        console.log("hide");
        setTimeout(function () {
            $("#clearAllBtn").hide();
        },400)
    }else{
        $("#clearAllBtn").show();
    }
    





});


function popover_active(obj_temp) {
    // console.log($(obj).parent());
    var type_json_str = $("#showArgs").val();
    if(type_json_str != undefined && type_json_str != ""){
        var args_data = JSON.parse(type_json_str);
        var obj = $(obj_temp);
        var obj_parent = obj.parent();
        var a_popover = obj_parent.find(".article_popover");
        var a_id = a_popover.prop("id");
        var article_id = a_popover.attr("articleId");
        var all_text = "<div style='width: 200px'>";
        var pc_show = "</div><div style='height: 1px; width: 85%; background-color: #e5e5e5'></div>" +
            "<div style='width: 100%'>";
        var app_show = "<div style='width: 100%'>";
        for(var i=0; i<args_data.length; i++){
            var type_data = args_data[i];
            var is_app = type_data['is_app'];
            var type = type_data['type'];
            var name = type_data['name'];
            var str = "<a href='javascript:void(0)' class='btn' onclick='show_article_modal(this)' type='"
                +type+ "' articleId='"+ article_id +"'>"
                + name + "</a>";
            if(is_app){
                app_show += str;
            }else{
                pc_show += str;
            }
        }
        all_text += app_show + pc_show + "</div></div>";
        a_popover.popover({
                                    trigger:'manual',//manual 触发方式
                                    placement : 'left',
                                    title:'<div style="text-align:left; color:gray; font-size:13px;">文章展示</div>',
                                    html: 'true',
                                    content : all_text,  //这里可以直接写字符串，也可以 是一个函数，该函数返回一个字符串；
                                    animation: false
                                }) .on("mouseenter", function () {
                                            var _this = this;
                                            $(this).popover("show");
                                            $(this).siblings(".popover").on("mouseleave", function () {
                                                $(_this).popover('hide');
                                            });
                                        }).on("mouseleave", function () {
                                            var _this = this;
                                            setTimeout(function () {
                                                if (!$(".popover:hover").length) {
                                                    $(_this).popover("hide")
                                                }
                                            }, 100);
                                            });

    }
}

    //查询异步回调
    function search_ajax_callback() {
        $.ajax({
            url:$("#ajaxDataUrl").val(),
            data:{"filter":conditionList},
            dataType:"json",
            method:"GET",
            success:function (data) {
                console.log(data);
                var disable_line = "<li role='separator' class='divider'></li>"
                for(var classfy in data){
                    if(data[classfy]){
                        var enable_text = "";
                        var disable_text = "";
                        var all_text = "";
                        for (var i=0;i<data[classfy].length; i++){
                            var class_id = data[classfy][i]['class_id'];
                            var id_key = data[classfy][i]['id_key'];
                            var name = data[classfy][i]['name'];
                            var li_id = "li-" +id_key+ "-" + class_id;
                            if(data[classfy][i]['able']){
                                var option = "<li class='enabled' text='"+ name +"' idKey='"+
                                    id_key+"' classId='"+ class_id +"' onclick='add_this(this)'><a href='#'>"+name+"</a></li>";
                                enable_text += option;
                            }else{
                                var option = "<li class='disabled' text='"+ name +"' idKey='"+
                                    id_key+"' classId='"+ class_id +"' id='"+ li_id + "'><a href='#'>"+name+"</a></li>";
                                disable_text += option
                            }
                        }
                    all_text = enable_text + disable_line + disable_text;
                    $('#filter_condition_'+ data[classfy][0]['id_key']).html(all_text);
                    }
                }
            }

        })
    }




//    添加过滤条件
    function add_this(obj) {
        var this_obj = $(obj);
        console.log(this_obj);

        var name = this_obj.attr("text");//分类名称
        var class_id = this_obj.attr("classId")||1;//在各类别中的ID
        var id_key = this_obj.attr("idKey") || 6;//前后台交互使用，识别属于那种类型
        var li_id = "li-" +id_key+ "-" + class_id;
        var txt = "<input type='checkbox' id='checkbox-filter-"+
            id_key+"-"+class_id+"' style='display: none' classId='"+
            class_id+"'  idKey='"+id_key+"' text='"+name+"'/><label for='checkbox-filter-"+
            id_key+"-"+class_id+"' onclick='remove_this(this)'><span>"+name+"</span></label>";
        console.log('#checkbox-filter-'+ id_key +"-"+ class_id);
        // alert('#checkbox-filter-'+ class_id+"-"+ id_key);
        $("#list_condition").append(txt);
        $('#checkbox-filter-'+ id_key+'-'+class_id).addClass("addfilter");
        conditionList = list_all_condition();
        setTimeout(function () {
        $('#checkbox-filter-'+ id_key+'-'+class_id).attr("checked", "checked");
        }, 300);
        api.ajax.reload();
        $("#clearAllBtn").show();
        var option = "<li class='disabled' text='"+ name +"' idKey='"+
            id_key+"' classId='"+ class_id +"' id='"+ li_id + "'><a href='#'>"+name+"</a></li>";
        $("#filter_condition_" + id_key).append(option);
        this_obj.remove();
        search_ajax_callback()

    }


//拼接查询条件
function list_all_condition() {
    var con_list = $("#list_condition input.addfilter");
    if(con_list.length == 0){
        console.log("hide");
        setTimeout(function () {
            $("#clearAllBtn").hide();
        },400)
    }
    console.log("查询条件" + con_list.length);
    var args = "";
    for(var i = 0;i<con_list.length;i++){
        var condition = con_list.eq(i);
        var name = condition.attr("text");
        var class_id = condition.attr("classId");
        var id_key = condition.attr("idKey");
        var one = class_id + "," + id_key;
        args += one + "#";
    }
    return args
}

//点击已选择内容，等同删除
function remove_this(obj) {
    var this_obj = $(obj);
    var checkbox_id = this_obj.attr("for");
    console.log(this_obj);
    var name = $("#"+checkbox_id).attr("text");
    var class_id = $("#"+checkbox_id).attr("classId");
    var id_key = $("#"+checkbox_id).attr("idKey");
    var li_content = name +","+id_key + "," + class_id;
    $("#"+ checkbox_id).attr("readonly", true);
    $("#"+ checkbox_id).removeClass("addfilter");
    // 处理被删除的条件
    handle_delete_condition(li_content);

    conditionList = list_all_condition();
    search_ajax_callback();
    api.ajax.reload();
}

function clearAllfilter(obj) {
    $("#list_condition input").attr("readonly", true);
    $("#list_condition input").attr("checked", false);
    setTimeout(function () {
        $("#list_condition input").remove();
        $("#list_condition label").remove();
        $(obj).hide();
    }, 500);
    conditionList = "";
    search_ajax_callback();
    api.ajax.reload();

}


function handle_delete_condition(li_id_list) {
    var li_id_arr = li_id_list.split("#");
    for(var i=0;i<li_id_arr.length;i++){
        var li_content = li_id_arr[i];
        var li_arr = li_content.split(",");
        var name = li_arr[0];
        var id_key = li_arr[1];
        var class_id = li_arr[2];
        var li_id = id_key +"-"+ class_id;
        var checkbox_id = "checkbox-filter-"+li_id;
        console.log(checkbox_id);
        setTimeout(function () {
            $("#"+ checkbox_id).remove();
            $('label[for="'+checkbox_id+'"]').remove();
        }, 400);

        $("#li-" + li_id).remove();
        var option = "<li class='enabled' text='"+ name +"' idKey='"+
            id_key+"' classId='"+ class_id +"' onclick='add_this(this)'><a href='#'>"+name+"</a></li>";
        $("#filter_condition_" + id_key).prepend(option);
    }
}


function show_article_modal(obj_temp){
    var obj = $(obj_temp);
    var article_id = obj.attr("articleId");
    var type = obj.attr("type");
    // $('#articleModal').modal("show");
    $.ajax({
        url : $("#articleModalShow").val(),
        method: "get",
        dataType:"html",
        data:{
            "type" : type,
            "article_id": article_id
        },
        success:function (data) {
            $("#articleModal").html(data);
            $("#articleModal").modal("show");
            if($("#area_province").val() != undefined){
                area_city_change()
            };
        }
    })
}

function area_city_change() {
    var province_id = $("#area_province").val();
    var area_str = $("#area_json").val();
    var area_json = JSON.parse(area_str);
    for(var i=0; i<area_json.length;i++){
        var pro_info = area_json[i];
        if(pro_info['index'] = province_id){
            var city_arr = pro_info['city'];
            var txt = "";
            for(var j=0; j<city_arr.length; j++){
                var city_info = city_arr[j];
                var str = "";
                if(city_info['is_checked']){
                    str = "<option value='"+ city_info['id'] +"' selected='selected'>"+ city_info['city']+"</option>";
                }else {
                    str = "<option value='"+ city_info['id'] +"'>"+ city_info['city']+"</option>";
                }
                txt += str;
            }
            $("#area_city").html(txt);
            return;
        }
    }
}

function sub_city_html_change(obj_temp) {
    var obj = $(obj_temp);
    var area_id = obj.val();
    var type = $("#modal_submit").attr("typeValue");
    var article_id = $("#modal_submit").attr("articleId");
    $.ajax({
        url: $("#subsiteModalBody").val(),
        method: "get",
        dataType:"html",
        data:{
            "area_id": area_id,
            "type": type,
            "article_id": article_id
        },
        success:function (data) {
            $("#modal_submit").attr("areaId", area_id);
            $("#modal_body").html(data);
        }
    })
}


function switch_modal_page(obj_temp) {
    var obj = $(obj_temp);
    var page = obj.attr("page");
    obj.addClass("active");
    obj.siblings().removeClass("active");
    $("*[id^=show_page_]").hide();
    $("#show_page_" + page).show();
}

function choose_position_ornot(obj_temp){
    var obj = $(obj_temp);
    var this_check = obj.find("input");
    if(this_check.prop("checked")){
        this_check.prop("checked", false);
        obj.removeClass("active")
    }else{
        var input_arr = $("#articleModal").find("input");
        for(var i=0;i<input_arr.length; i++){
            var input_check_obj = input_arr[i];
            var input_check = $(input_check_obj);
            if(input_check.prop("checked")){
                input_check.prop("checked", false);
                input_check.parent().removeClass("active")
            }
        }
        this_check.prop("checked", true);
        obj.addClass("active");
    }
}

function submit_article_position(obj_temp) {
    var obj = $(obj_temp);
    var article_id = obj.attr("articleId");
    var area_id = obj.attr("areaId") || -1;
    var type = obj.attr("typeValue");
    var show_id = -1;
    var input_arr = $("#articleModal").find("input");
    for(var i=0;i<input_arr.length; i++){
        var input_check_obj = input_arr[i];
        var input_check = $(input_check_obj);
        if(input_check.prop("checked")){
            show_id = input_check.val();
        }
    }
    $.ajax({
        url: $("#modalSubmitAction").val(),
        method:"get",
        data:{
            "show_id":show_id,
            "type":type,
            "article_id":article_id,
            "area_id":area_id
        },
        dataType:"json",
        success:function (data) {
            if(data['res'] == "success"){
                $("#articleModal").modal("hide");
            }else{
                $("#articleModal").modal("hide");
                alert("设置展示失败")
            }
        }
    })

}