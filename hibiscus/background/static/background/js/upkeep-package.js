$(document).ready(function () {
    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var type_id = $("#typeId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"type_id": type_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [
            {
                "targets": 3,
                "render": function (data, type, row) {
                    // var txt = "<a href='/background/upkeep/proj/new/from/pkg/?" +
                    //     "pkg_id="+row["DT_RowId"]+"'>创建项目</a>";
                    var txt = "<a href='/background/upkeep/proj/?pkg_id="+row["DT_RowId"]+"&type_id="+row["type_id"]+"&SecondMenu=upProjManage'>项目管理</a>"
                    return txt;
                }
            }
        ],
        "columns": [
            { "data": "name" },
            { "data": "type" },
            { "data": "created_at" }
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });


    $.ajax({
        url: $("#typeUrl").val(),
        method: "GET",
        dataType: "json",
        success: function (data) {
            for(var i=0; i<data.length; i++){
                var txt = "<option value='"+data[i]["id"]+"'>"+data[i]["type"]+"</option>";
                console.debug(txt);
                $("select[name=upkeepT]").append(txt);
            }
        }
    });

    $("#clearCon").click(function () {
        var url = $("#pkgIndex").val();
        location.href = url;
    });




    $('#NewForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            upkeepPkg: {
                validators: {
                    notEmpty: {
                        message: '套餐名称不得为空'
                    }
                }
            },
        }
    }).on('success.form.bv',function(event){
        event.preventDefault();
        //return false;
        var _url = $("#ActionNewUrl").val();
        $.ajax({
            url: _url,
            dataType: "json",
            method: "POST",
            data: $("#NewForm").serialize(),
            success: function(data){
                if(data["res"] == "success"){
                    var index_url = $("#IndexUrl").val();
                    window.location.href = index_url;
                    }
                }
            });
    });
});