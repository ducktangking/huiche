/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var type_url = $("#brTyAction").attr("action");
    $.ajax({
        url: type_url,
        dataType: "json",
        method: "get",
        success: function(data){
            var type_data = data["type_l"];
            var level_data = data["level"];
            for(var i=0; i<type_data.length; i++){
                var txt = "<option value='"+type_data[i]["id"]+"'>"+type_data[i]["name"]+"</option>";
                $("#brandType").append(txt);
            }
            for(var j=0; j<level_data.length; j++){
                var txt = "<option value='"+level_data[j]["id"]+"'>"+level_data[j]["name"]+"</option>";
                $("#brandLevel").append(txt);
            }

        }
    });
    var options = {
        beforeSubmit: validate_brand_data,
        url: $("#brandAction").attr("action"),
        type: "post",
        success: function (data) {
            if(data == "success"){
                location.href = $("#bIndexAction").attr("action");
            }
        }
    };
    // ajaxForm
    // ajaxSubmit

    function validate_brand_data(){
        var unique_url = $("#uniqueUrl").attr("action");
        var img = $("#brandImg").val();
        // var br_type = $("#brandType").find("option:selected").val();
        var name = $("#brandName").val();
        // if(img == ""){
        //     var msg = "Please select image.";
        //     alert(msg);
        //     return false;
        // }
        // if(br_type == ""){
        //     var msg = "Please select brand type";
        //     alert(msg);
        //     return false;
        // }
        /*if(name == ""){
            var msg = "Please input brand name";
            alert(msg);
            return false;
        }*/

        var exists = "";
        $.ajax({
            url: unique_url,
            method: "GET",
            dataType: "json",
            async: false,
            data: {"name": name, "level": "one"},
            success: function(data){
                exists = data["res"];
            }
        });
        if(exists == "no"){
            console.log("not exists");
            return true;
        }else{
            var msg = "系统中已存在相同名称的一级品牌";
            alert(msg);
            return false;
        }
    }


    $('#brandName').css({display: "inline-block"}).attr('placeholder',"请输入16位以内的品牌名称");
    $('#brandStor').css({display: "inline-block"}).attr('placeholder',"输入...");
    $('#logoStor').css({display: "inline-block"}).attr('placeholder',"输入...");

    
    $('#brandDataForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            brandName: {
                validators: {
                    notEmpty: {
                        message: '品牌名称不得为空'
                    }
                }
            },
            logo: {
                validators: {
                    notEmpty: {
                        message: '请上传图片'
                    },
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            },
            brandStor: {
                validators: {
                    notEmpty: {
                        message: '品牌历史不得为空',
                    },
                }
            },
            logoStor: {
                validators: {
                    notEmpty: {
                        message: 'Logo历史不得为空'
                    },

                }

            }
        }
    }).on('success.form.bv',function (event) {
        event.defaultPrevented;
        $("#brandDataForm").ajaxForm(options);
        //return false;
    });

});