$(document).ready(function () {
    function get_new_data() {
        var brandId = $("#upSeriesBrand").val();
        var _url = $("#newUpSeriesDataUrl").val();
        $.ajax({
            url: _url,
            method: 'get',
            dataType: 'json',
            data: {"brandId": brandId, "brand_initinal": $("#brand_initinal").val(), "topbrand_id":$('#upSeriesTopBrand').val()},
            success: function (data) {
                if ($.isEmptyObject(data)) {
                    return false;
                }
                var series_obj = data['series'];
                var series_param_obj = data['series_one_param'];
                var brand_obj = data['brand'] || null;
                var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";
                if(brand_obj){
                        $('#upSeriesBrand').empty();
                    for (var i=0;i<brand_obj.length; i++){
                        var tx ="<option value='" + brand_obj[i]["br_id"] + "'>" + brand_obj[i]["name"] + "</option>";
                        $('#upSeriesBrand').append(tx);
                    }
                };
                for (var i = 0; i < series_obj.length; i++) {
                    var t = "<option value='" + series_obj[i]["s_id"] + "'>" + series_obj[i]["name"] + "</option>";
                    $("#upSeries").append(t);
                }
                //year
                var car_obj = series_param_obj["year_data"];
                for (var j = 0; j < car_obj.length; j++) {
                    var txt = "<input type='checkbox' name='upseriesYear' value='"+car_obj[j]+"'>" + car_obj[j];
                   $("#carYear").append(txt);
                }
                $("#carYear").prepend(select_all_year);
                var cc_obj = series_param_obj["cc_data"];
                for (var k = 0; k < cc_obj.length; k++) {
                    var tx = "<option value='" + cc_obj[k] + "'>" + cc_obj[k] + "</option>";
                    $("#carCc").append(tx);
                }
            }
        });
    }

    get_new_data();

    $("#upSeriesForm").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: $("#UpSeriesNewUrl").val(),
            method: "post",
            dataType: "json",
            data: $("#upSeriesForm").serialize(),
            success: function (data) {
                if(data['res'] == "success"){
                    location.href = $("#UpSeriesIndexUrl").val();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log(XMLHttpRequest.readyState);
                console.log(textStatus);
                console.log(errorThrown);
            }

        });
    });

    $(".char_select").each(function () {
        $(this).click(function (event) {
            event.preventDefault();
            $("#upSeriesBrand").empty();
            $("#upSeries").empty();
            $("#carYear").empty();
            $("#carCc").empty();
            var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

            var _url = $(this).attr("href");
            $.ajax({
               url: _url,
               method: 'get',
               dataType: 'json',
               success: function (data) {
                   if($.isEmptyObject(data)){
                       return false;
                   }
                   var series_obj = data['series'];
                   var series_param_obj = data['series_one_param'];


                   for(var i=0; i<series_obj.length; i++){
                       var t = "<option value='"+series_obj[i]["s_id"]+"'>"+series_obj[i]["name"]+"</option>";
                       $("#upSeries").append(t);
                   }
                    //year
                   var car_obj =series_param_obj["year_data"];
                   for(var j=0; j<car_obj.length; j++){
                       var txt = "<input type='checkbox' name='upseriesYear' value='"+car_obj[j]+"'>" + car_obj[j];
                       $("#carYear").append(txt);
                   }
                   $("#carYear").prepend(select_all_year);
                   var cc_obj = series_param_obj["cc_data"];
                   for (var k=0; k < cc_obj.length; k++){
                       var tx = "<option value='"+cc_obj[k]+"'>"+cc_obj[k]+"</option>";
                       $("#carCc").append(tx);
                   }
               }  //end of success
            });

        });
    });

});