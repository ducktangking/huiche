/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    var addr_url = $("#AddrUrl").val();
    $("#showAddr").click(function () {
        $("#addrList").empty();
        $.ajax({
            url: addr_url,
            method: "GET",
            dataType: "json",
            success: function (data) {
                for(var i=0; i<data.length; i++){
                    var txt = "<input type='checkbox' name='addrList' " +
                        "value='"+data[i]["ad_id"]+"'>" + data[i]["address"] + "&nbsp;&nbsp;&nbsp;";
                    $("#addrList").css("display", "block");
                    $("#addrList").append(txt);
                }
            }
        });
    });

    var options = {
        // beforeSubmit: validate_username,
        url: $("#editUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            if(data["res"] == "success"){
                location.href = $("#adminDetail").val();
            }
        }
    };


        $('#adminName').css({display: "inline-block"}).attr({placeholder:"请输入16位以内的名称",maxLength:'16'});
        $('#email').css({display: "inline-block"}).attr({placeholder:"请输入正确格式的邮箱",maxLength:'32'});
        $('#adminPhone').css({display: "inline-block",imeMode:'disabled'}).attr({placeholder:"请输入正确格式的手机号",maxLength:'11'});



    $('#adminPhone').keypress(function (event) {
        var eventObj = event || e;
        var keyCode = eventObj.keyCode || eventObj.which;
            if (eventObj.ctrlKey == true||(keyCode >= 48 && keyCode <= 57)||keyCode == 8||keyCode == 46||keyCode == 9|| keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
                return true;
            else
                return false;
    });
       

    $('#adminEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            adminName: {
                validators: {
                    notEmpty: {
                        message: '用户名不得为空'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: '手机号码不得为空',
                    },
                    regexp: {
                        regexp: /^1(3|4|5|7|8)\d{9}$/,
                        message: '手机号码格式错误',
                    },
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '邮箱不得为空',
                    },
                    regexp: {
                        regexp: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)?([a-zA-Z0-9]{2,4})$/,
                        message: '邮箱格式错误',
                    },
                },

            },
            adminPic:{

                    validators: {
                        regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },

                    }
                }
        }
    }).on('success.form.bv',function (event) {
            event.defaultPrevented;
            $("#adminEditForm").ajaxForm(options);
        });
    birthday_date();

});