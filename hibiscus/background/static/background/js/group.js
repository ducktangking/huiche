function operator(o) {
    var _url = $(o).attr("href");
    var _action = $(o).attr("action");
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: {"action": _action},
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#IndexUrl").val();
                window.location.href = index_url;
                }
            }
    });
}


$(document).ready(function () {

    var api;
    var selected = [];
    var _url = $("#DataUrl").val();
    var parent_id = $("#articleParentId").val();
    $('#DataTable').dataTable( {
        serverSide: true,
        ajax: {
            url: _url,
            type: 'GET',
            data: {"parent_id": parent_id}
        },
        "language": {
                "url": $("#lanFile").val()
            },
        "columnDefs": [{
            "targets": 7,
            
            "render": function (data, type, row) {
                if(row["is_timeout"]){
                    return "已过期";
                }else{
                    return "未过期";
                }
            }
        }, {
            "targets": 8,
            
            "render": function (data, type, row) {
                if (row["status"]) {
                    return "已启动";
                } else {
                    return "未启动";
                }
            }
        },{
            "targets": 10,
            'ordering':false,
            'orderable':false,
            "render": function (data, type, row) {
                var txt = "<a href='/background/group/pics/?gId="+row['DT_RowId']+"'>图片详情</a>";
                if(row["article"] != "-1"){
                    txt += "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='/background/group/article/?gId="+row['DT_RowId']+"&aId="+row["article"]+"'>团购详情</a>";
                }else{
                    txt += "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='/background/group/articles/?gId="+row['DT_RowId']+"'>绑定详情</a>";
                }
                txt += "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='/background/group/users/?gId="+row['DT_RowId']+"'>参与人数</a>";
                if(row["status"]){
                    if(!row['is_left']){
                        txt += "&nbsp;&nbsp;|&nbsp;&nbsp;" +
                        "<a href='/background/group/left/show/?gId="+row['DT_RowId']+"&is_left="+row['is_left']+"'>大图展示</a>";
                    }else{
                        txt += "&nbsp;&nbsp;|&nbsp;&nbsp;" +
                        "<a href='/background/group/left/show/?gId="+row['DT_RowId']+"&is_left="+row['is_left']+"'>集体展示</a>";
                    }
                }
                if(row["status"]){
                    txt += "&nbsp;&nbsp;|&nbsp;&nbsp;" +
                        "<button onclick='return operator(this);' action='stop' href='/background/group/action/?gId="+row['DT_RowId']+"'>停止</button>";
                }else{
                    txt += "&nbsp;&nbsp;|&nbsp;&nbsp;" +
                        "<button onclick='return operator(this);' class='group_operation_action' action='start' href='/background/group/action/?gId="+row['DT_RowId']+"'>启动</button>";
                }

                return txt;
            }
        }],
        "columns": [
            { "data": "series" },
            { "data": "name" },
            { "data": "brief"},
            { "data": "area" },
            { "data": "time_begin" },
            { "data": "time_end" },
            { "data": "days_el" },
            { "data": "is_timeout" },
            { "data": "status" },
            { "data": "created_at"},
            { "data": "article"},
        ],
        "pagingType": "full_numbers",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        }
        });
    $('#DataTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);

        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }

        $(this).toggleClass('selected');
    } );

    api = $('#DataTable').dataTable().api();
    $("#DelAll").click(function(){
        if(selected.length == 0){
            $("#ConfirmModal").modal('show');
        }else{
            $("#DeleteModal").modal('show');
        }
    });

    $("#DeleteForm").submit(function (event) {
        event.defaultPrevented;
        var ids = "";
        var ids_l = selected.length;
        if(ids_l != 0){
            for(var i=0; i<selected.length; i++){
                ids += selected[i] + "_";
            }
            $("#DeleteItems").val(ids);
            var _url = $("#ActionDeleteUrl").val();
            $.ajax({
                url: _url,
                method: "POST",
                dataType: "json",
                data: $("#DeleteForm").serialize(),
                success: function(data){
                    console.log(data);
                    if(data["res"] == "success"){
                        $("#DeleteModal").modal('hide');
                        api.ajax.reload(null, false);

                    }
                }
            });
            return false;
        }

    });

    $("#NewForm").submit(function(event){
    event.preventDefault();
    //return false;
    var _url = $("#ActionNewUrl").val();
    $.ajax({
        url: _url,
        dataType: "json",
        method: "POST",
        data: $("#NewForm").serialize(),
        success: function(data){
            if(data["res"] == "success"){
                var index_url = $("#IndexUrl").val();
                window.location.href = index_url;
                }
            }
        });

    });

});