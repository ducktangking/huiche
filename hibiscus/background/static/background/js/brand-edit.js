/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    console.log($("#brandEditUrl").val());
    var options = {
        url: $("#brandEditUrl").val(),
        type: "post",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if(data["res"] == "success"){
                console.log($("#bIndexAction").attr("action"));
                location.href = $("#bIndexAction").attr("action");
            }
        }
    };
    // ajaxForm
    // ajaxSubmit

    
    
    $('#brandName').css({display: "inline-block"}).attr('placeholder',"请输入16位以内的品牌名称");
    $('#brandStor').css({display: "inline-block"}).attr('placeholder',"输入...");
    $('#logoStor').css({display: "inline-block"}).attr('placeholder',"输入...");


    $('#brandEditForm').bootstrapValidator({
        message: '输入值不可用',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok ',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            brandName: {
                validators: {
                    notEmpty: {
                        message: '品牌名称不得为空'
                    }
                }
            },
            logo: {
                validators:{
                    regexp:{
                        regexp:/\.(j|J)(p|P)(g|G)$|\.(g|G)(i|I)(f|F)$|\.(b|B)(m|M)(p|P)$|\.(b|B)(n|N)(p|P)$|\.(p|P)(n|N)(g|G)$/,
                        message:'图片格式错误'
                    },
                }
            },
            brandStor: {
                validators: {
                    notEmpty: {
                        message: '品牌历史不得为空',
                    },
                }
            },
            logoStor: {
                validators: {
                    notEmpty: {
                        message: 'Logo历史不得为空'
                    },

                }

            }
        }
    }).on('success.form.bv',function (event) {
        event.defaultPrevented;
        $("#brandEditForm").ajaxForm(options);
    });

});