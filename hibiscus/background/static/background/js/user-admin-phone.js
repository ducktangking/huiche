/**
 * Created by server on 4/11/16.
 */

$(document).ready(function(){
    $("#changePhoneNum").click(function () {
        $("#phone1").hide();
        $("#phone2").show();
    });



    $('#newPhone').css({display: "inline-block"}).attr({placeholder:"请输入正确格式的手机号",maxLength:'11'});
    $('#newPhone').keypress(function (event) {
            var eventObj = event || e;
            var keyCode = eventObj.keyCode || eventObj.which;
                if (eventObj.ctrlKey == true||(keyCode >= 48 && keyCode <= 57)||keyCode == 8||keyCode == 46||keyCode == 9|| keyCode == 37 || keyCode == 39 || keyCode == 36 || keyCode == 35 || keyCode == 108)
                    return true;
                else
                    return false;
        });
    $('#newPhone').bind('blur input propertychange',function () {
        var reg1 = /^1(3|4|5|7|8)\d{9}$/;
        var phnum = $('#newPhone').val();
        if (phnum == "") {
            $('#newPhone_remind').text("手机号码不得为空").css({color: "#ff643d", fontSize: 'small'});
            $('#validateCode').attr('disabled','true');
        } else {
            var phnumad = reg1.test(phnum);
            if (!phnumad) {
                $('#newPhone_remind').text("请输入正确的手机号").css({color: "#ff643d", fontSize: 'small'});
                       $('#validateCode').attr('disabled','true');
            }
            else {
                $('#newPhone_remind').text("");
                $('#validateCode').removeAttr('disabled');
      }
        }
    });
});