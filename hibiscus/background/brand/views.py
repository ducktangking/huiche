__author__ = 'gmj'
import subprocess
import simplejson
import logging

from django.http import HttpResponse
from django.db.models import Q
from django.shortcuts import render, redirect

from hibiscus.models import BrandType, Brand, Picture, Logo, BrandLevel, Car, BrandCountry, BrandCallOut
from hibiscus.background.public import deal_with_upload_file
from hibiscus.decorators import data_table
from hibiscus.background.upkeep.upkeepseries.views import car_params2

LOG = logging.getLogger(__name__)
BRAND_CLOUMNS = ["initial", "name", "brand_type", "level_name", "name", "name", "is_app", "is_pc"]
BRAND_TYPE_CLOUMNS = ["name"]
TOP_PANEL = "selectCar"
LEFT_PANEL = "brandLeft"


def index(request):
    initial = request.GET.get('initial', "")
    return render(request, 'background/brand/index.html', {
        "chars": [chr(i) for i in range(97,97+26,1)],
        "initial": initial
    })


@data_table(columns=BRAND_CLOUMNS, model=Brand)
def data(request):
    try:
        initial = request.GET.get('initial', "")
        order_clounm = request.table_data["order_cloumn"]
        order_cl = request.table_data['order_cl']
        # if order_clounm == 2:
        #     if request.table_data["order_dir"] == "desc":
        #         order_cl = "-type__name"
        #     else:
        #         order_cl = "type__name"
        # elif order_clounm == 3:
        #     if request.table_data["order_dir"] == "desc":
        #         order_cl = "-level__name"
        #     else:
        #         order_cl = "level__name"
        # else:
        #     order_cl = request.table_data["order_cl"]
        if initial:
            sts = Brand.objects.filter(
                                       Q(name__contains=request.table_data["key_word"]) |
                                       Q(type__name__contains=request.table_data["key_word"]) |
                                       Q(level__name__contains=request.table_data["key_word"]),
                                       Q(parent__isnull=True), Q(initial=initial)
                                       ).order_by(order_cl) \
                [request.table_data["start"]:request.table_data["end"]]
        else:
            sts = Brand.objects.filter(Q(parent__isnull=True),
                    Q(name__contains=request.table_data["key_word"]) |
                    Q(type__name__contains=request.table_data["key_word"]) |
                    Q(level__name__contains=request.table_data["key_word"])
                    ).order_by(order_cl)\
                [request.table_data["start"]:request.table_data["end"]]
        brand_parents = Brand.objects.filter(Q(parent__isnull=True))
        st_list = []
        # st_dict = {}
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["initial"] = st.initial
            st_params["name"] = st.name
            # st_params["type"] = [bt.name for bt in st.type.all()]
            st_params["pic_path"] = st.logo.picture.path
            st_params["brand_level"] = st.level.name
            if st.parent:
                st_params["parent"] = st.parent.name
            else:
                st_params["parent"] = "no"
            st_list.append(st_params)
        return st_list, len(brand_parents)
    except Exception, e:
        LOG.error('Can not get brand data. %s' % e)



def new(request):
    brandtype = BrandType.objects.all()
    brandcountry = BrandCountry.objects.all()
    brand_call_out = BrandCallOut.objects.all()
    return render(request, 'background/brand/new.html',  {"chars": [chr(i) for i in range(97,97+26,1)],
                      "brandtype": brandtype, "brandcountry": brandcountry, "brandcallout": brand_call_out})



def new_action(request):
    res = "success"
    try:
        logo = request.FILES["logo"]
        name = request.POST["brandName"]
        b_story = request.POST["brandStor"]
        logo_story = request.POST["logoStor"]
        b_init = request.POST["brandInit"]
        brand_country = request.POST['brandCountry']
        brand_call_out = request.POST.getlist('isHot')
        b_country_obj = BrandCountry.objects.get(pk=brand_country)

        img_path = deal_with_upload_file(logo)
        # brandLevel obj
        bl_id = request.POST["brandLevel"]
        bl_obj = BrandLevel.objects.get(pk=bl_id)

        p_obj = Picture.objects.create(name=logo.name, path=img_path)
        l_obj = Logo.objects.create(picture=p_obj)

        is_pc = request.POST.get('isPC', "")
        is_app = request.POST.get('isAPP', "")
        pc = True if str(is_pc) == "on" else False
        app = True if str(is_app) == "on" else False

        br = Brand.objects.create(logo=l_obj, level=bl_obj, name=name, initial=b_init, story=b_story,
              logo_story=logo_story, country=b_country_obj, is_app=app, is_pc=pc)
        bc_obj = BrandCallOut.objects.filter(id__in=brand_call_out)
        for bc in bc_obj:
            br.is_hot.add(bc)

    except Exception, e:
        res = "fail"
        LOG.error('Error happened when create brand. %s' % e)
    return HttpResponse(res)


def second_brand_index(request):
    brandId = request.GET['brandId']
    return render(request, 'background/brand/second_index.html', {"brandId": brandId})


@data_table(columns=BRAND_CLOUMNS, model=Brand)
def level_two_data(request):
    try:
        brandId = request.GET['brandId']
        brand_obj = Brand.objects.get(pk=brandId)
        order_clounm = request.table_data["order_cloumn"]
        if order_clounm == 2:
            if request.table_data["order_dir"] == "desc":
                order_cl = "-type__name"
            else:
                order_cl = "type__name"
        elif order_clounm == 3:
            if request.table_data["order_dir"] == "desc":
                order_cl = "-level__name"
            else:
                order_cl = "level__name"
        else:
            order_cl = request.table_data["order_cl"]
        sts = Brand.objects.filter(Q(parent=brand_obj),
                Q(name__contains=request.table_data["key_word"]) |
                Q(initial__contains=request.table_data["key_word"]) |
                Q(level__name__contains=request.table_data["key_word"])
                ).order_by(order_cl)\
            [request.table_data["start"]:request.table_data["end"]]
        brand_second_level = Brand.objects.filter(Q(parent=brand_obj))
        st_list = []
        # st_dict = {}
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["initial"] = st.initial
            st_params["name"] = st.name
            st_params["type"] = [bt.name for bt in st.type.all()]
            st_params["pic_path"] = st.logo.picture.path
            st_params["brand_level"] = st.level.name
            st_params['is_app'] = st.is_app
            st_params['is_pc'] = st.is_pc
            if st.parent:
                st_params["parent"] = st.parent.name
            else:
                st_params["parent"] = "no"
            st_list.append(st_params)
        return st_list, len(brand_second_level)
    except Exception, e:
        LOG.error('Can not get brand data. %s' % e)


def second_brand_new(request):
    brandId = request.GET["brandId"]
    brandtype = BrandType.objects.all()
    brandcountry = BrandCountry.objects.all()
    brand_call_out = BrandCallOut.objects.all()
    return render(request, 'background/brand/brand-two-new.html',
                  {"chars": [chr(i) for i in range(97,97+26,1)],
                   "brandId": brandId,
                   "brandtype": brandtype,
                   "brandcountry": brandcountry,
                   "brand_call_out": brand_call_out
                   })


def second_brand_action(request):
    res = "success"
    try:
        name = request.POST["tbrandName"]
        b_story = request.POST["brandStor"]
        logo_story = request.POST["logoStor"]
        b_init = request.POST["brandInit"]
        parent_id = request.POST["tbrandId"]
        brand_country = request.POST['brandCountry']
        country_obj = BrandCountry.objects.get(pk=brand_country)
        brand_call_out = request.POST.getlist('isHot')
        parent_obj = Brand.objects.get(pk=parent_id)
        is_pc = request.POST.get('isPC', "")
        is_app = request.POST.get('isAPP', "")
        pc = True if str(is_pc) == "on" else False
        app = True if str(is_app) == "on" else False
        p_logo = request.POST.get('parentLogo', None)
        if p_logo and p_logo == "on":
            l_obj = parent_obj.logo
        else:
            logo = request.FILES["tlogo"]
            img_path = deal_with_upload_file(logo)

            p_obj = Picture.objects.create(name=logo.name, path=img_path)
            l_obj = Logo.objects.create(picture=p_obj)

        bt_ids = request.POST.getlist("tbrandType")
        # brandLevel obj
        bl_id = request.POST["tbrandLevel"]
        bl_obj = BrandLevel.objects.get(pk=bl_id)
        #brandType obj
        bt_obj = get_brand_t_obj(request, bt_ids)
        b = Brand.objects.create(parent=parent_obj, logo=l_obj, level=bl_obj, name=name, initial=b_init,
                                 story=b_story, logo_story=logo_story, country=country_obj,
                                 is_app=app, is_pc=pc)
        for t_obj in bt_obj:
            b.type.add(t_obj)
            b.save()
        bc_obj = BrandCallOut.objects.filter(id__in=brand_call_out)
        for bc in bc_obj:
            b.is_hot.add(bc)
    except Exception, e:
        res = "fail"
        LOG.error('Error happened when create two brand. %s' % e)
    return HttpResponse(res)


def _run_command(command):
    try:
        data = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if data:
                std_out, std_err = data.communicate()
                return std_out, std_err

    except Exception, e:
        LOG.error("run command error. %s" % e)
    return None


def get_brand_type(request):
    type_l = []
    brand_level = []
    try:
        bts = BrandType.objects.all()
        for b in bts:
            bt_params = {}
            bt_params["id"] = b.id
            bt_params["name"] = b.name
            type_l.append(bt_params)
        bls = BrandLevel.objects.all()
        for l in bls:
            l_params = {}
            l_params["id"] = l.id
            l_params["name"] = l.name
            brand_level.append(l_params)
    except Exception, e:
        LOG.error('Error happened when get brand type. %s' % e)
    return HttpResponse(simplejson.dumps({"type_l": type_l, "level": brand_level}))


def get_brand_t_obj(request, ids):
    t_obj = None
    try:
        t_obj = BrandType.objects.filter(id__in=ids)
    except Exception, e:
        LOG.error('Error happened when get brand obj. %s' % e)
    return t_obj


def get_unique_data(request):
    try:
        res = "no"
        name = request.GET["name"]
        level = request.GET["level"]
        # type_id = request.GET["type_id"]

        # t_obj = BrandType.objects.get(id=type_id)
        if level == "one":
            brand = Brand.objects.filter(name=name, parent__isnull=True)
        else:
            brand = Brand.objects.filter(name=name, parent__isnull=False)
        if brand:
            res = "exists"
    except Exception, e:
        res = "error"
        LOG.error('Error happened when get unique brand. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def delete_action(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Brand.objects.filter(pk=id_v).delete()
    except Exception, e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def brand_car(brand_obj):
    car_data = []
    try:
        series = brand_obj.series_set.all()
        if series:
            for s in series:
                cars = Car.objects.filter(series=s)
                for c in cars:
                    c_p = {}
                    c_p['car_id'] = c.id
                    c_p['car_name'] = c.name
                    car_data.append(c_p)
    except Exception as e:
        LOG.error('Get brand car error. %s' % e)
    return car_data


def ajax_second_brand_by_top_brand(request):
    brand_data = []
    series_list = []
    cc_year_data = []
    top_brands = []
    try:
        upseries = request.GET.get("upseries", "")
        brand_id = request.GET.get('brand_id', "")
        initial = request.GET.get("initial", "")
        if brand_id:
            brand_data = [{"id": brand.id, "name": brand.name} for brand in Brand.objects.filter(parent_id=brand_id)]
        if upseries and not initial:
            brobj = Brand.objects.filter(parent__id=brand_id)
            if brobj:
                series = brobj[0].series_set.all()
                for s in series:
                    sp = {}
                    sp['id'] = s.id
                    sp['name'] = s.name
                    series_list.append(sp)
                if series:
                    cc_year_data = car_params2(series[0])
            all_data = {"brand_data": brand_data, "series_data": series_list, "cc_year_data": cc_year_data}
            return HttpResponse(simplejson.dumps(all_data))
        if upseries and initial:
            top_brands_data = Brand.objects.filter(parent__isnull=True, initial=initial)
            if top_brands_data:
                second_brand_data = Brand.objects.filter(parent__id=top_brands_data[0].id)
                if second_brand_data:
                    series = second_brand_data[0].series_set.all()
                    if series:
                        cc_year_data = car_params2(series[0])
            for t in top_brands_data:
                tp = {}
                tp['id'] = t.id
                tp['name'] = t.name
                top_brands.append(tp)
            for b in second_brand_data:
                bp = {}
                bp['id'] = b.id
                bp['name'] = b.name
                brand_data.append(bp)
            for s in series:
                sp = {}
                sp['id'] = s.id
                sp['name'] = s.name
                series_list.append(sp)
            all_data = {"top_brands": top_brands, "brand_data": brand_data,
                        "series_data": series_list, "cc_year_data": cc_year_data, "top_brands": top_brands}
            return HttpResponse(simplejson.dumps(all_data))
    except Exception as e:
        LOG.error('Can not get brand car by ajax. %s' % e)
    return HttpResponse(simplejson.dumps(brand_data))


def ajax_top_brand_by_initial(request):
    brand_data = []
    try:
        initial = request.GET.get('initial', "a")
        brand_data = [{"id": brand.id, "name": brand.name} for brand in Brand.objects.filter(Q(initial=initial), Q(parent__isnull=True))]
    except Exception as e:
        LOG.error('Can not get brand car by ajax. %s' % e)
    return HttpResponse(simplejson.dumps(brand_data))


def ajax_brand_series(request):
    car_data = []
    try:
        brand_id = request.GET['brand_id']
        br_obj = Brand.objects.get(pk=brand_id)
        series = br_obj.series_set.all()
        for s in series:
            sp = {}
            sp['id'] = s.id
            sp['name'] = s.name
            car_data.append(sp)
    except Exception as e:
        LOG.error('Can not get brand car by ajax. %s' % e)
    return HttpResponse(simplejson.dumps(car_data))


def edit(request):
    br_data = {}
    bt_data = []
    bl_data = []
    chr_data = []
    call_out_list = []
    try:
        brandId = request.GET['brandId']
        br_obj = Brand.objects.get(pk=brandId)
        br_data['name'] = br_obj.name
        br_data['id'] = br_obj.id
        br_data['logo_story'] = br_obj.logo_story
        br_data['story'] = br_obj.story
        br_data['pic_img'] = br_obj.logo.picture.path
        br_data['logo_id'] = br_obj.logo.id
        br_data['level'] = br_obj.level.name
        br_data['type'] = br_obj.type.name
        br_data['is_hot'] = br_obj.is_hot.all()
        br_data['is_app'] = br_obj.is_app
        br_data['is_pc'] = br_obj.is_pc
        brand_types = BrandType.objects.all()
        brand_level = BrandLevel.objects.all()
        has_type = br_obj.type.all()
        is_parent = br_obj.parent
        parent_id = Brand.objects.values_list('parent_id').filter(id=br_obj.id)[0][0]
        for bt in brand_types:
            bt_p = {}
            bt_p["id"] = bt.id
            bt_p["name"] = bt.name
            if bt in has_type:
                bt_p['is_checked'] = True
            else:
                bt_p['is_checked'] = False
            bt_data.append(bt_p)
        for bl in brand_level:
            bl_p = {}
            bl_p['id'] = bl.id
            bl_p['name'] = bl.name
            if bl.name == br_obj.level.name:
                bl_p['is_checked'] = True
            else:
                bl_p['is_checked'] = False
            bl_data.append(bl_p)

        for i in range(97, 97 + 26, 1):
            chr_p = {}
            chr_p['chars'] = chr(i)
            if chr(i) == br_obj.initial:
                chr_p['is_checked'] = True
            else:
                chr_p['is_checked'] = False
            chr_data.append(chr_p)

        countries = BrandCountry.objects.all()
        country_list = []
        for c in countries:
            cp = {}
            cp['id'] = c.id
            cp['name'] = c.name
            cp['is_checked'] = False
            if br_obj.country:
                if c.name == br_obj.country.name:
                    cp['is_checked'] = True
            country_list.append(cp)
        call_out = BrandCallOut.objects.all()

        for c in call_out:
            cp = {}
            cp['id'] = c.id
            cp['name'] = c.name
            if c in br_obj.is_hot.all():
                cp['is_checked'] = True
            else:
                cp['is_checked'] = False
            call_out_list.append(cp)
    except Exception as e:
        LOG.error('Can not get edit brand data. %s' % e)
    return render(request, 'background/brand/edit.html',
                  {"br_data": br_data, "bl_data": bl_data,
                   'bt_data': bt_data, 'chr_data': chr_data,
                   "is_parent": is_parent,
                   "country_list": country_list,
                   "call_out_list": call_out_list,
                   "parent_id": parent_id,
                  })


def edit_action(request):
    res = "success"
    try:
        brandId = request.POST['brandId']
        name = request.POST['brandName']
        bl_id = request.POST['brandLevel']
        bt_ids = request.POST.getlist('tbrandType', None)
        initinal = request.POST['brandInit']
        story = request.POST['brandStor']
        brand_country = request.POST['brandCountry']
        country = BrandCountry.objects.get(pk=brand_country)
        logo_story = request.POST['logoStor']
        is_pc = request.POST.get('isPC', "")
        is_app = request.POST.get('isAPP', "")
        pc = True if str(is_pc) == "on" else False
        app = True if str(is_app) == "on" else False
        brand_call_out = request.POST.getlist('isHot')
        if "logo" in request.FILES:
            img = request.FILES['logo']
            pic_path = deal_with_upload_file(img)
            pic = Picture.objects.create(name=img.name, path=pic_path)
            logo_obj = Logo.objects.create(picture=pic)
        else:
            logo_id = request.POST['originalLogoId']
            logo_obj = Logo.objects.get(pk=logo_id)

        bl_obj = BrandLevel.objects.get(pk=bl_id)
        bobj = Brand.objects.get(pk=brandId)
        bobj.name = name
        bobj.initial = initinal
        bobj.level = bl_obj
        bobj.story = story
        bobj.logo_story = logo_story
        bobj.logo = logo_obj
        # bobj.is_hot = hot
        bobj.country = country
        bobj.is_pc = pc
        bobj.is_app = app
        bobj.save()

        bobj.type.clear()
        if bt_ids:
            bt_objs = BrandType.objects.filter(pk__in=bt_ids)
            bobj.type.add(*[t for t in bt_objs])
            bobj.save()
        bobj.is_hot.clear()
        if brand_call_out:
            bc_objs = BrandCallOut.objects.filter(pk__in=brand_call_out)
            bobj.is_hot.add(*[c for c in bc_objs])
            bobj.save()

    except Exception as e:
        LOG.error('Error happened when edit brand info. %s' % e)
        res = 'fail'
    return HttpResponse(simplejson.dumps({"res": res}))


def brand_app(request):
    res = "success"
    try:
        brand_id = request.GET['brandId']
        brobj = Brand.objects.filter(pk=brand_id)
        if brobj:
            brobj[0].is_app = not brobj[0].is_app
            brobj[0].save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to app. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def brand_pc(request):
    res = "success"
    try:
        brand_id = request.GET['brandId']
        brobj = Brand.objects.filter(pk=brand_id)
        if brobj:
            brobj[0].is_pc = not brobj[0].is_pc
            brobj[0].save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


