__author__ = 'gmj'

from django.conf.urls import url
from hibiscus.background.brand import views
urlpatterns = [
    url(r'^$', views.index, name="background-brand-index"),
    url(r'^new/$', views.new, name='background-brand-new'),
    url(r'^new/action/$', views.new_action, name='background-brand-new-action'),
    url(r'^type/$', views.get_brand_type, name='get_brand_type'),
    url(r'^data/$', views.data, name='background-brand-data'),
    url(r'^validata/unique/$', views.get_unique_data, name='get_unique_data'),
    url(r'^del/action/$', views.delete_action, name="background-brand-delete-action"),
    url(r'^two/new/$', views.second_brand_new, name="background-second-brand-new"),
    url(r'^two/new/action/$', views.second_brand_action, name="background-second-brand-new-action"),
    url(r'^ajax/car/$', views.ajax_brand_series, name="background-brand-series-data"),
    url(r'^edit/$', views.edit, name="background-brand-edit"),
    url(r'^edit/action/$', views.edit_action, name="background-brand-edit-action"),
    url(r'^two/index/$', views.second_brand_index, name="background-brand-two-index"),
    url(r'^two/data/$', views.level_two_data, name="background-brand-two-data"),
    url(r'^set/app/$', views.brand_app, name="background-brand-set-app"),
    url(r'^set/pc/$', views.brand_pc, name="background-brand-set-pc"),
    url(r'^ajax/second/brand/$', views.ajax_second_brand_by_top_brand, name="background-second-brand-data"),
    url(r'^ajax/top/brand/$', views.ajax_top_brand_by_initial, name="background-top-brand-data"),
]