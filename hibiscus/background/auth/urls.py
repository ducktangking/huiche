# -*- coding:utf-8 -*-

from django.conf.urls import url

from hibiscus.background.auth import views

urlpatterns = [
    url(r'^login/$', views.login, name="background-login"),
    url(r'^logout/$', views.logout, name="background-logout"),
    url(r'^password_change/$', views.password_change2, name='background_password_change'),
    url(r'^password_change/done/$', views.password_change_action, name='background_password_change_done'),
    url(r'^producer/$', views.producer_login, name="producer-login"),
    url(r'^adviser/$', views.adviser_login, name="adviser_login"),
    url(r'^adviser/login/test/$', views.adviser_login_test, name="adviser_login_test"),
    url(r'^validate/pwssword/$', views.user_pwd, name='background-password-validate'),

]