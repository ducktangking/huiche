import logging
import simplejson
import inspect

# Create your views here.

from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied

from django.views.generic.base import View
from django.contrib.auth import (
    login as auth_login,
    logout as auth_logout,
    authenticate,
    load_backend,
    _clean_credentials
)
from django.contrib.auth.signals import user_login_failed

from hibiscus.models import HcAdmin, Producer
from hibiscus.background.public import pwd_hash
from hibiscus.background.user.admin.views import admin_detail_data

LOG = logging.getLogger(__name__)
PRODUCER_BACKEND_PATH = "hibiscus.background.background_backends.ProducerBackend"
ADMIN_BACKEND_PATH = "hibiscus.background.background_admin_backends.AdminBackend"


def manual_authenticate(backend_path, **credentials):
    """
    If the given credentials are valid, return a User object.
    """
    backend = load_backend(backend_path)

    try:
        inspect.getcallargs(backend.authenticate, **credentials)
        user = backend.authenticate(**credentials)
        if user:
            user.backend = backend_path
            return user
        else:
            return None
    except PermissionDenied:
        user_login_failed.send(sender=__name__,
                               credentials=_clean_credentials(credentials))
        # This backend says to stop in our tracks - this user should not be allowed in at all.
        return None


class IndexView(View):

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        if request.META.get(settings.PHONE_INDICATOR, None):
            return HttpResponse('Hello, Phone!')
        else:
            return HttpResponse('Hello, PC!')


@csrf_protect
@never_cache
def login(request):
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
            kwargs = {"user_role": "admin"}
            user = manual_authenticate(username=request.POST['username'],
                       password=request.POST['password'],
                       backend_path=ADMIN_BACKEND_PATH, **kwargs)

            if user and user.is_active:
                auth_login(request, user)
                if user.type == "m":
                    return redirect('background-user-admin-detail')
                elif user.type == "o":
                    return redirect('background-user-operator-detail')
                elif user.type == "a":
                    return redirect('background-user-auditor-index')
    request.session.set_test_cookie()
    return render(request, 'background/auth/login.html')


def logout(request):
    auth_logout(request)
    if request.GET['role'] == "admin":
        return redirect("background-login")
    else:
        return redirect("producer-login")


@csrf_protect
@never_cache
def producer_login(request):
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
            kwargs = {"user_role": "producer"}
            user = manual_authenticate(username=request.POST['username'],
                                       password=request.POST['password'],
                                       backend_path=PRODUCER_BACKEND_PATH, **kwargs)
            if user and user.is_active:
                auth_login(request, user)
                return redirect('background-user-adviser-index')
    request.session.set_test_cookie()
    return render(request, 'background/auth/producer-login.html')


@csrf_protect
@never_cache
def adviser_login(request):
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
            kwargs = {"user_role": "adviser"}
            user = authenticate(username=request.POST['username'], password=request.POST['password'], **kwargs)
            if user and user.is_active:
                auth_login(request, user)
                return redirect('adviser_login_test')
    request.session.set_test_cookie()
    return render(request, 'background/auth/adviser-login.html')


def adviser_login_test(request):
    return render(request, 'background/auth/adviser-test.html')


def password_change2(request):
    try:
        role = request.GET.get('role', "")
        admin_id = request.GET.get('adminId', "")
        admin_data = admin_detail_data(admin_id)
    except Exception as e:
        LOG.error('Error happened when change pwd. %s' % e)
    return render(request, "background/auth/password_change.html", {"role": role, "adminId": admin_id, "admin_data": admin_data})


def password_change_action(request):
    res = "fail"
    try:
        role = request.POST['adminRole']
        adminId = request.POST['adminId']
        original_pwd = request.POST['originalPwd']
        new_pwd = request.POST['NewPwd']
        confirm_pwd = request.POST['confirmPwd']
        if role == "admin":
            user = HcAdmin.objects.get(pk=adminId)
        else:
            user = Producer.objects.get(pk=adminId)
        if pwd_hash(original_pwd) != user.password:
            msg = "original password is wrong"
        else:
            if new_pwd != confirm_pwd:
                msg = "New password does not equal confirm password"
            else:
                user.password = pwd_hash(new_pwd)
                user.save()
                res = 'success'
                msg = "change password success"
    except Exception as e:
        res = "error"
        LOG.error('Change password error. %s' % e)
        msg = "error happened"
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def user_pwd(request):
    try:
        role = request.GET['role']
        userId = request.GET['userId']
        pwd = request.GET['pwd']
        if role == "admin":
            user = HcAdmin.objects.get(pk=userId)
        if role == "producer":
            user = Producer.objects.get(pk=userId)
        if user.password != pwd_hash(pwd):
            return HttpResponse('not equal')
        else:
            return HttpResponse('equal')
    except Exception as e:
        LOG.error('Can not compare user passord. %s ' % e)
    return HttpResponse('0')
