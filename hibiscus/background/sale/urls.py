from django.conf.urls import url
from hibiscus.background.sale import views

urlpatterns = [
    url(r'^$', views.index, name="background-sale-index"),
    url(r'^index/data$', views.index_data, name="background-sale-index-data"),
    url(r'^new$', views.create_sale, name='background-sale-new'),
    url(r'^car$', views.table_car_info, name='background-sale-car-list'),
    url(r'^series/color$',views.sale_series_color, name='background-sale-series-color'),
    url(r'^new/detail$', views.detail_sale_new, name='background-sale-new-detail'),
    url(r'^new/pic/bank$', views.pic_bank, name='background-sale-pic-bank'),
    url(r'^preview$', views.preview, name='background-sale-preview'),
    url(r'^new/action$', views.new_action, name='background-sale-new-action'),
    url(r'^delete/action$', views.delete_action, name='background-sale-delete-action'),
    url(r'^edit$', views.edit, name='background-sale-edit'),
    url(r'^edit/action$', views.edit_action, name='background-sale-edit-action'),
]