# -*-coding:utf8-*-
import logging
from django.shortcuts import render, redirect
import simplejson
from django.http import HttpResponse
from django.db.models import Q
from hibiscus.models import ProducerPreSale, Article, Producer, ProducerCarSeries,CarQuote, Series,\
    Car, SeriesYearColor, CarPicture, Picture, SaleSeriesAndColor, SaleCarAndColor, CarTypeColor,\
    ArticleType
from django.utils import timezone
from hibiscus.decorators import data_table


LOG = logging.getLogger(__name__)

SALE_COLUMNS = ["article__title", "article__title", "article__title", "article__title", "created_at", "article__clicks"]
NEWS_INFO = {
    "title" :u"<producer_name><series_name>优惠高达<price>万元",
    "introduction" :u"<producer_name><series_name>优惠高达<price>万元，感兴趣的朋友可以到店咨询购买，具体优惠信息如下："
}
PIC_TYPE = {1: u"外观", 2: u"内饰", 3: u"空间", 4: u"图解", 5: u"官方", 6: u"车展"}


def index(request):
    if "producer_id" in request.GET and "_auth_user_id" in request.session:
        producer_id = request.session['_auth_user_id']
        return render(request, "background/sale/producer-index.html", {"producer_id":producer_id})
    else:
        return redirect('producer-login')


@data_table(columns=SALE_COLUMNS, model=ProducerPreSale)
def index_data(request):
    data = list()
    data_sum = 0
    try:
        producer_id = request.GET.get("producer_id")
        data_sum = ProducerPreSale.objects.filter(
                    Q(producer_id=producer_id),
                    Q(article__title__contains=request.table_data["key_word"])).count()
        sale_obj_list = ProducerPreSale.objects.filter(
            Q(producer_id=producer_id),
            Q(article__title__contains=request.table_data["key_word"]))\
                            .order_by(request.table_data["order_cl"])\
            [request.table_data["start"]: request.table_data["end"]]
        now = timezone.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        for sale_obj in sale_obj_list:
            # sale_start_time = timezone.datetime.strptime(sale_obj.sale_start, "%Y-%m-%d %H:%M:%S")
            # sale_end_time = timezone.datetime.strptime(sale_obj.sale_end, "%Y-%m-%d %H:%M:%S")
            # sale_publish_time = timezone.datetime.strptime(sale_obj.created_at, "%Y-%m-%d %H:%M:%S")
            sale_start_time = sale_obj.sale_start.strftime("%Y-%m-%d %H:%M:%S")
            sale_end_time = sale_obj.sale_end.strftime("%Y-%m-%d %H:%M:%S")
            sale_publish_time = sale_obj.created_at.strftime("%Y-%m-%d %H:%M:%S")
            data_param = dict()
            data_param['DT_RowId'] = sale_obj.id
            data_param['title'] = sale_obj.article.title
            # 0为降价促销，1为礼包促销
            data_param['type'] = 0 if sale_obj.gift is None else 1
            # 0 未开始， 1 进行中， -1 已结束
            if sale_start_time > now:
                data_param['sale_status'] = 0
            elif sale_start_time < now < sale_end_time:
                data_param['sale_status'] = 1
            else:
                data_param['sale_status'] = -1
            data_param['sale_time'] = sale_start_time.split(" ")[0] \
                                    + " -- " + sale_end_time.split(" ")[0]\
                                    if data_param['sale_status'] != -1 else " -- "
            data_param['publish_time'] = sale_publish_time.split(" ")[0]
            data_param['view_count'] = sale_obj.article.clicks
            data.append(data_param)
    except Exception as e:
        LOG.error('Error happened while get sale index data .%s' % e)
    return data,data_sum


def create_sale(request):
    try:
        if "producer_id" in request.GET and "_auth_user_id" in request.session:
            producer_id = request.session['_auth_user_id']
            producer_obj = Producer.objects.get(pk=producer_id)
            # car_quote_list = CarQuote.objects.filter(producer=producer_obj)
            brand_obj_list = ProducerCarSeries.objects.filter(producer=producer_obj)
            now = timezone.datetime.now()
            sale_obj_list = ProducerPreSale.objects.filter(Q(sale_end__gte=now), Q(sale_start__lte=now))
            # 促销车型括号中的数字
            sale_series_dict = dict()
            for sale_obj in sale_obj_list:
                sale_series_id = sale_obj.bind_series.series_id
                car_num = sale_obj.bind_series.car_and_color.all().count()
                if not sale_series_dict.has_key(sale_series_id):
                    sale_series_dict[sale_series_id] = car_num
                else:
                    sale_series_dict[sale_series_id] = sale_series_dict[sale_series_id] + car_num
            #获得促销车型
            brand_dict = dict()
            series_id_list = list()
            # for car_obj in car_quote_list:
            #     series = car_obj.car.series
            #     if series.id not in series_id_list:
            #         brand = series.brand
            #         if not brand_dict.has_key(brand.name):
            #             brand_dict[brand.name] = dict()
            #             brand_dict[brand.name]['id'] = brand.id
            #             brand_dict[brand.name]['series'] = list()
            #         series_param = dict()
            #         series_param['id'] = series.id
            #         series_param['name'] = series.name
            #         series_param['num'] = 0 if not sale_series_dict.has_key(series.id) else sale_series_dict[series.id]
            #         brand_dict[brand.name]['series'].append(series_param)
            #         series_id_list.append(series.id)
            # 修改 根据用户代理品牌获得车型
            for brand_obj in brand_obj_list:
                if brand_obj.brand.parent is not None:
                    series_list = Series.objects.filter(brand_id=brand_obj.brand_id)
                else:
                    series_list = Series.objects.filter(brand__parent_id=brand_obj.brand_id)
                for series_obj in series_list:
                    if series_obj.id not in series_id_list:
                        if not brand_dict.has_key(series_obj.brand.name):
                            brand_dict[series_obj.brand.name] = dict()
                            brand_dict[series_obj.brand.name]['id'] = series_obj.brand_id
                            brand_dict[series_obj.brand.name]['series'] = list()
                        series_param = dict()
                        series_param['id'] = series_obj.id
                        series_param['name'] = series_obj.name
                        series_param['num'] = 0 if not sale_series_dict.has_key(series_obj.id) else sale_series_dict[series_obj.id]
                        brand_dict[series_obj.brand.name]['series'].append(series_param)
                        series_id_list.append(series_obj.id)
            today = timezone.datetime.now().strftime("%Y-%m-%d")
            month_later =(timezone.datetime.now() + timezone.timedelta(days=30)).strftime("%Y-%m-%d")
            return render(request, "background/sale/producer-new.html",
                          {"producer_id":producer_id, "brand_dict":brand_dict,
                           "sale_start":today, "sale_end":month_later})
        else:
            return redirect('producer-login')
    except Exception as e:
        LOG.error('Error happened when create new sale .%s' % e)


def table_car_info(request):
    data_list = list()
    year_list = list()
    try:
        producer_id = request.GET.get("producer_id")
        series_id = request.GET.get("series_id")
        producer_obj = Producer.objects.get(pk=producer_id)
        # car_quote_list = CarQuote.objects.filter(producer=producer_obj)
        brand_obj_list = ProducerCarSeries.objects.filter(producer=producer_obj)
        now = timezone.datetime.now()
        sale_obj_list = ProducerPreSale.objects.filter(
            Q(sale_end__gte=now), Q(sale_start__lte=now),
            Q(bind_series__series_id=series_id))
        sale_car_dict = dict()
        # 统计已发促销次数
        for sale_series_obj in sale_obj_list:
            car_list = sale_series_obj.bind_series.car_and_color.all()
            for sale_car in car_list:
                car_id = sale_car.car_id
                if not sale_car_dict.has_key(car_id):
                    sale_car_dict[car_id] = 1
                else:
                    sale_car_dict[car_id] = sale_car_dict[car_id] + 1
        # 所有代理车款
        car_list = Car.objects.filter(series_id = series_id)
        for car_obj in car_list:
            car_param = dict()
            car_quote_list = CarQuote.objects.filter(Q(car = car_obj), Q(producer=producer_obj))
            if len(car_quote_list) != 0:
                car_param['price'] = car_quote_list[0].sale_price
            else:
                car_param['price'] = car_obj.price
            car_param['year'] = car_obj.produce_year
            car_param['id'] = car_obj.id
            car_param['name'] = car_obj.name
            if sale_car_dict.has_key(car_obj.id):
                car_param['num'] = sale_car_dict[car_obj.id]
            else:
                car_param['num'] = 0
            data_list.append(car_param)
            if car_obj.produce_year not in year_list:
                year_list.append(car_obj.produce_year)
        return render(request, "background/sale/sale_car_table.html", {"data": data_list, "year_list":year_list})
    except Exception as e:
        LOG.error('Error happened when add new sale car .%s' % e)


def sale_series_color(request):
    color_list = list()
    try:
        series_id = request.GET.get("series_id")
        series_color_list = SeriesYearColor.objects.filter(series_id=series_id)
        for series_color in series_color_list:
            year = series_color.year
            all_color = series_color.color.filter(side=0)
            for color_obj in all_color:
                color_param = dict()
                color_param['id'] = color_obj.id
                color_param['name'] = color_obj.name
                color_param['year'] = year
                color_list.append(color_param)
    except Exception as e:
        LOG.error('Error happened when get sale series color .%s' % e)
    return HttpResponse(simplejson.dumps(color_list))


def detail_sale_new(request):
    big_pic_dict = dict()
    small_pic_list = list()
    title = ""
    introduction = ""
    try:
        producer_id = request.GET.get("producer_id")
        series_id = request.GET.get("series_id")
        car_pic_list = CarPicture.objects.filter(car__series_id=series_id)
        car_big_pic_list = car_pic_list.filter(type__name=PIC_TYPE[1])
        car_small_pic_list = car_pic_list.filter(type__name=PIC_TYPE[2])
        if car_big_pic_list:
            big_pic_obj = car_big_pic_list[0]
            big_pic_dict['id'] = big_pic_obj.picture_id
            big_pic_dict['name'] = big_pic_obj.picture.name
            big_pic_dict['path'] = big_pic_obj.picture.path
        if car_small_pic_list:
            for small_pic_obj in car_small_pic_list:
                if len(small_pic_list) <4:
                    pic_param = dict()
                    pic_param['id'] = small_pic_obj.picture_id
                    pic_param['name'] = small_pic_obj.picture.name
                    pic_param['path'] = small_pic_obj.picture.path
                    small_pic_list.append(pic_param)
        producer_obj = Producer.objects.get(pk=producer_id)
        producer_name = producer_obj.information.name if producer_obj.information \
            else producer_obj.short_name
        series_obj = Series.objects.get(pk=series_id)
        series_name = series_obj.name
        title_ori = NEWS_INFO['title']
        introduction_ori = NEWS_INFO['introduction']
        title = title_ori.replace("<producer_name>",producer_name).replace("<series_name>", series_name)
        introduction = introduction_ori.replace("<producer_name>",producer_name).replace("<series_name>", series_name)
    except Exception as e:
        LOG.error('Error happened when get sale news detail info .%s' % e)
    return render(request, 'background/sale/detail_sale_new.html',
                  {"big_pic_dict":big_pic_dict, "small_pic_list":small_pic_list,
                   "title":title, "introduction":introduction})


def pic_bank(request):
    data = dict()
    data['args'] = dict()
    data["pic"] = list()
    data["type"] = 1
    data['args']['total'] = 0
    data['args']['page'] = 1
    data['args']['last_page'] = 1
    data['args']['page_size'] = 12
    try:
        pic_list = list()
        pic_len = int(request.GET.get("pic_len", 0))
        big_pic_id = request.GET.get("big_pic")
        small_pic_str = request.GET.get("small_pic")
        series_id = request.GET.get("series_id")
        pic_type = request.GET.get("pic_type")
        page = int(request.GET.get("page", 1))
        page_size = int(request.GET.get("page_size", 12))
        small_pic_id = small_pic_str.split(",")[:-1] if small_pic_str else []
        car_pic_list = Picture.objects.filter(
            Q(carpicture__car__series_id=series_id),~Q(id=big_pic_id),
            ~Q(id__in=small_pic_id),
            Q(carpicture__type__name=PIC_TYPE[int(pic_type)])).distinct().order_by("-name")[(page-1)*page_size: page*page_size]
        if not pic_len:
            car_pic_list_len = Picture.objects.filter(
                                Q(carpicture__car__series_id=series_id),~Q(id=big_pic_id),
                                ~Q(id__in=small_pic_id),
                                Q(carpicture__type__name=PIC_TYPE[int(pic_type)])).distinct()
            pic_len = len(car_pic_list_len)
            print pic_len
        for car_pic in car_pic_list:
            pic_param = dict()
            pic_param['id'] = car_pic.id
            pic_param['name'] = car_pic.name
            pic_param['path'] = car_pic.path
            pic_list.append(pic_param)

        data['args']['total'] = pic_len
        data['args']['page'] = page
        data['args']['page_size'] = page_size
        data['args']['last_page'] = (pic_len-1)/page_size + 1
        data['pic']= pic_list
        data['type'] = int(pic_type)
    except Exception as e:
        LOG.error('Error happened when get picture bank .%s' % e)
    return HttpResponse(simplejson.dumps(data))


# def sale_new_gift_bag(request):
#     return render(request, "background/sale/sale_new_gift_bag.html")

def preview(request):
    try:
        is_simple = request.POST.get("is_simple")
        if int(is_simple) == 1:
            return simple_preview(request)
        else:
            return detail_preview(request)
    except Exception as e:
        LOG.error('Error happened when show aticle preview: .%s' % e)


def simple_preview(request):
    series_id = request.POST.get("series_id")
    producer_id = request.POST.get("producer_id")
    car_str = request.POST.get("car_str")
    sale_start = request.POST.get("sale_start")
    sale_end = request.POST.get("sale_end")
    headline_str = NEWS_INFO['title']
    intro_str = NEWS_INFO['introduction']
    car_pic_list = Picture.objects.filter(carpicture__car__series_id=series_id)
    car_big_pic_list = car_pic_list.filter(carpicture__type__name=PIC_TYPE[1])[0:1]
    car_small_pic_list = car_pic_list.filter(carpicture__type__name=PIC_TYPE[2]).distinct()[0:4]
    big_pic_id = car_big_pic_list[0].id if car_big_pic_list else -1
    small_pic_id_list = list()
    car_list = parse_car_str(car_str)
    if car_small_pic_list:
        for small_pic_obj in car_small_pic_list:
            if len(small_pic_id_list) < 4:
                small_pic_id_list.append(small_pic_obj.id)
    producer_obj = Producer.objects.get(pk=producer_id)
    series_obj = Series.objects.get(pk=series_id)
    price = max(list(data['sale_price'] for data in car_list))
    # print producer_obj.full_name
    producer_name = producer_obj.information.name if producer_obj.information \
            else producer_obj.short_name
    headline = headline_str.replace("<producer_name>", producer_name)\
        .replace("<series_name>", series_obj.name)\
        .replace("<price>", str(price))
    introduction = intro_str.replace("<producer_name>", producer_name)\
        .replace("<series_name>", series_obj.name)\
        .replace("<price>", str(price))
    article_html = make_up_article(producer_obj, headline, introduction, sale_start, sale_end, series_obj, big_pic_id, small_pic_id_list, car_list)
    return HttpResponse(simplejson.dumps({"html":article_html}))


def detail_preview(request):
    series_id = request.POST.get("series_id")
    producer_id = request.POST.get("producer_id")
    car_str = request.POST.get("car_str")
    sale_start = request.POST.get("sale_start")
    sale_end = request.POST.get("sale_end")
    headline = request.POST.get("headline")
    introduction = request.POST.get("introduction")
    big_pic_id = request.POST.get("big_pic_id")
    small_pic_list_str = request.POST.get("small_pic_str")
    is_upkeep =True if str(request.POST.get("is_upkeep")) == 'true' else False
    is_addr = True if str(request.POST.get("is_addr")) == 'true'  else False
    is_map = True if str(request.POST.get("is_map")) == 'true' else False
    is_tel = True if str(request.POST.get("is_tel")) == 'true' else False
    small_pic_id_list = small_pic_list_str.split(",")[:-1]
    producer_obj = Producer.objects.get(pk=producer_id)
    series_obj = Series.objects.get(pk=series_id)
    car_list = parse_car_str(car_str)
    ps_info = {"is_upkeep":is_upkeep, "is_addr":is_addr, "is_map":is_map, "is_tel":is_tel}
    # price = max(list(data['sale_price'] for data in car_list))
    article_html = make_up_article(producer_obj, headline, introduction, sale_start, sale_end, series_obj, big_pic_id, small_pic_id_list, car_list, ps_info)
    return HttpResponse(simplejson.dumps({"html":article_html}))


def make_up_article(producer_obj, headline,introduction, sale_start, sale_end,series_obj, big_pic_id, small_pic_id_list, car_data, ps_info=dict(), gift_bag=dict()):
    """
    根据相应的擦参数, 生成促销新闻
    :param producer_obj: 经销商对象
    :param headline: 新闻标题
    :param sale_start: 促销开始时间
    :param sale_end: 促销结束时间
    :param series_obj: 车型对象
    :param big_pic_id: 车型对应大图id
    :param small_pic_id_list: 车型对应小图的id list
    :param car_data: 车款的促销信息,其中包括车款颜色
    :return: 返回一个String类型的article文本, 可放在html中生成文章
    """
    producer_name = producer_obj.information.name if producer_obj.information \
            else producer_obj.short_name
    producer_sale_tel = producer_obj.phone
    producer_addr_all = producer_obj.information.addresses if producer_obj.information \
            else None
    producer_addr = None
    addr_dict = dict()
    if producer_addr_all:
        producer_addr = producer_addr_all.all()[0] if producer_addr_all.all() else None
    if producer_addr:
        addr_dict['longitude'] = producer_addr.longitude if producer_addr.longitude else 31.964282
        addr_dict['latitude'] = producer_addr.latitude if producer_addr.latitude else 118.828778
        addr_dict['address'] = producer_addr.address
    outlook = u"<div stryle='padding: 0 50px 40px;border-top: 3px solid #e9e9e9'>"
    end_div = u"</div>"
    headline = u"<h3 style='text-align:center; color:#1f70db'>{0}</h3>".format(headline)
    time_string =u"{0} - {1}".format(sale_start, sale_end)
    subhead = u"<div style='height: 40px;line-height: 40px;	background: #fbfbfb;font-weight: bold;text-align: center;	margin-bottom: 23px;'>"+\
              u"促销时间：" + time_string + \
              u"</div>"
    main_body = u"<p>&nbsp;&nbsp;&nbsp;&nbsp;" + introduction +u" </p>"
    table_head = u"<div style='margin-bottom: 20px;border-top: 3px solid #e9e9e9;width:80%;margin:0 auto;"+\
                 u"border-left: 1px solid #e9e9e9;border-right: 1px solid #e9e9e9;border-spacing: 0;'>" + \
                 u'<table width="100%" cellspacing="0" cellpadding="0" border="0"><thead>' + \
                 u'<tr><th style="text-align:center; padding:10px">车型</th>'+\
                 u'<th style="text-align:center; padding:10px">厂商指导价</th>'+\
                 u'<th style="text-align:center; padding:10px">优惠幅度</th>'+\
                 u'<th style="text-align:center; padding:10px">优惠价</th>'+\
                 u'<th style="text-align:center; padding:10px">库存情况</th>'+\
                 u'</tr></thead><tbody>'
    table_end = u"</tbody></table></div>"
    table_body = ""
    for car_info in car_data:
        color_status = u"颜色齐全" if not car_data and not car_info else car_info['color_list'][0]['status']
        tr_str = u'<tr><td style="padding:8px">{0}</td>'.format(car_info['name'])+\
                 u'<td style="padding:8px">{0}</td>'.format(str(car_info['origin_price']))+\
                 u'<td style="padding:8px; color:red">{0}</td>'.format(str(car_info['sale_price']))+\
                 u'<td style="padding:8px; color:green">{0}</td>'.format(str(car_info['finaly_price']))+\
                 u'<td style="padding:8px; text-align:center">'+\
                 u'<div>{0}</div><span style="font-size:12px">{1}<span></td></tr>'.format(car_info['inventory'],color_status)
        table_body += tr_str

    # 礼包模块
    gift_bag_div = u""
    if gift_bag:
        gift_bag_intro = u"<p>此外购车还赠送{0}元礼包，具体内容:</p>".format(gift_bag['total'])
        gift_bag_outline = u"<div style='position: relative;padding: 20px 15px 0;overflow: hidden;zoom: 1;border: 1px solid #e9e9e9;margin: 20px 0;	font-size: 12px;'>" +\
            u"<div style='padding-bottom: 10px'>"
        gift_bag_content = ""
        for content in gift_bag['content']:
            gift_str = u"<span>{0}<text>{1}</text></span>".format(content["value"], content["label"])
            gift_bag_content += gift_str
        gift_bag_mark = u"<i style='position: absolute;	top: 0;left: 0;width: 25px;height: 25px;display: block;	overflow: hidden;color: #fff;font-size: 11px;line-height: 16px;text-indent: 2px;'>礼</i>"
        gift_bag_div = gift_bag_intro + gift_bag_outline + gift_bag_content +  end_div + end_div + gift_bag_mark
    big_pic_obj = Picture.objects.get(pk=big_pic_id) if big_pic_id != -1 else None
    big_pic_path = big_pic_obj.path if big_pic_obj else ""
    big_pic_modal = u'<div style="margin-bottom: 10px; width:75%; margin:0 auto;padding:15px">'+\
                    u'<img src="{0}" width="100%"></div>'.format(big_pic_path)
    small_pic_id_list_int = list(int(aa) for aa in small_pic_id_list)
    small_pic_list = Picture.objects.filter(id__in=small_pic_id_list_int)
    small_pic_head = u'<div style="margin: 0 auto 20px;width: 600px;">'
    small_pic_str = ""
    temp_pic_str = ["", "", "", ""]
    for small_pic_obj in small_pic_list:
        pic_id = small_pic_obj.id
        pic_index =small_pic_id_list_int.index(pic_id)
        small_str = u"<div style='width:50%;display:inline-block'><img width='295px' src='{0}'></div>".format(small_pic_obj.path)
        temp_pic_str[pic_index] = small_str
    for temp_str in temp_pic_str:
        small_pic_str += temp_str
    small_pic_modal = small_pic_head + small_pic_str + u"</div>" if small_pic_str else ""
    # ps_info_str = ""
    tel_str = u"<p>&nbsp;&nbsp;&nbsp;&nbsp;电话:<span style='font-weight:600;color:red; font-size:15px'>"+\
              u"{0}</span></p>".format(producer_sale_tel) if not ps_info or ps_info['is_tel'] else u""
    addr_str = u""
    addr_map = u""
    if addr_dict:
        addr_str = u"<p>&nbsp;&nbsp;&nbsp;&nbsp;地址:"+\
                   u"<span>{0}</span></p>".format(addr_dict["address"]) if not ps_info or ps_info['is_addr'] else u""
        addr_map = u'<div style="width:600px;height:300px;border:#ccc solid 1px;margin:0 auto" id="dituContent"></div>'+\
        u'<script type="text/javascript">\n' +\
        u'//创建和初始化地图函数：\n' +\
        u'function initMap(){\n' +\
        u'   createMap();//创建地图\n'+\
        u'   setMapEvent();//设置地图事件\n'+\
        u'   addMapControl();//向地图添加控件\n'+\
        u'}\n'+\
        u'//创建地图函数：\n'+\
        u'function createMap(){\n'+\
        u'   var map = new BMap.Map("dituContent");//在百度地图容器中创建一个地图\n'+\
        u'   var point = new BMap.Point({0},{1});//定义一个中心点坐标\n'.format(addr_dict['latitude'], addr_dict['longitude'])+\
        u'   map.centerAndZoom(point,15);//设定地图的中心点和坐标并将地图显示在地图容器中\n'+\
        u'   var marker = new BMap.Marker(point);\n' +\
        u'   map.addOverlay(marker);\n' +\
        u'   marker.setAnimation(BMAP_ANIMATION_BOUNCE);' +\
        u'   window.map = map;//将map变量存储在全局\n'+\
        u'}\n'+\
        u'//地图事件设置函数：\n'+\
        u'function setMapEvent(){\n'+\
        u'   map.enableDragging();//启用地图拖拽事件，默认启用(可不写)\n'+\
        u'   map.enableScrollWheelZoom();//启用地图滚轮放大缩小\n'+\
        u'   map.enableDoubleClickZoom();//启用鼠标双击放大，默认启用(可不写)\n'+\
        u'   map.enableKeyboard();//启用键盘上下左右键移动地图\n'+\
        u'}\n'+\
        u'//地图控件添加函数：\n'+\
        u'function addMapControl(){\n'+\
        u'   //向地图中添加缩放控件\n'+\
        u'//var ctrl_nav = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_SMALL});\n'+\
        u'//map.addControl(ctrl_nav);\n'+\
        u'   //向地图中添加缩略图控件\n'+\
        u'//var ctrl_ove = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:1});\n'+\
        u'//map.addControl(ctrl_ove);\n'+\
        u'    //向地图中添加比例尺控件\n'+\
        u'//var ctrl_sca = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});\n'+\
        u'//map.addControl(ctrl_sca);\n'+\
        u'}\n'+\
        u'initMap();//创建和初始化地图\n'+\
        u'</script>' if not ps_info or ps_info['is_map'] else u""
    return outlook + headline + subhead + main_body + table_head +\
           table_body + table_end + gift_bag_div + big_pic_modal + small_pic_modal +\
            tel_str + addr_str + addr_map + end_div


def parse_car_str(car_str):
    INVANTORY_DICT = {0:u"需提前预定", 1:u"少量现车", 2:u"现车充足"}
    if not car_str:
        return list()
    one_car_list = car_str.split("@")[:-1]
    data = list()
    for one_car_info_str in one_car_list:
        one_car_info = one_car_info_str.split("#")
        car_id = one_car_info[0]
        car_origin_price = one_car_info[1]
        car_sale_price = one_car_info[2]
        car_finaly_price = one_car_info[3]
        car_inven_state = one_car_info[4]
        color_str = one_car_info[5]
        # color_list = color_str.split(",")
        car_param = dict()
        car_obj = Car.objects.get(pk=car_id)
        car_param["id"] = car_obj.id
        car_param['name'] = car_obj.name
        car_param['year'] = car_obj.produce_year
        car_param['origin_price'] = float(car_origin_price)
        car_param["sale_price"] = float(car_sale_price)
        car_param['finaly_price'] = float(car_finaly_price)
        car_param['inven_state'] = int(car_inven_state)
        car_param['inventory'] = INVANTORY_DICT[int(car_inven_state)]
        car_param['color_list'] = parse_color_str(color_str, car_id)
        data.append(car_param)
    # print simplejson.dumps(data)
    return data


def parse_color_str(color_str, car_id):
    data = list()
    color_id_list_temp = color_str.split(",")
    color_id_list = list(int(aa) for aa in color_id_list_temp)
    car_obj = Car.objects.get(pk=car_id)
    series_obj = car_obj.series
    series_year_color_obj = SeriesYearColor.objects.filter(Q(series=series_obj),
                                                    Q(year=car_obj.produce_year))
    forbid_color_obj_list = car_obj.car_color.all()
    forbid_color_list = list(obj for obj in forbid_color_obj_list)
    if series_year_color_obj:
        color_obj_list = series_year_color_obj[0].color.all().filter(Q(side=0), ~Q(color__in=forbid_color_list))
        color_status = u"{0}色".format(len(color_id_list))\
        if color_obj_list.count()> len(color_id_list) else u"颜色齐全"
        for color_obj in color_obj_list:
            color_param = dict()
            if color_obj.id in color_id_list:
                color_param['id'] = color_obj.id
                color_param['status'] = color_status
                color_param['name'] = color_obj.name
                data.append(color_param)
    return data


def new_action(request):
    is_simple = request.POST.get("is_simple")
    if int(is_simple) == 1:
        return simple_new_action(request)
    else:
        return detail_new_action(request)


def simple_new_action(request):
    res = "fail"
    try:
        series_id = request.POST.get("series_id")
        producer_id = request.POST.get("producer_id")
        car_str = request.POST.get("car_str")
        sale_start = request.POST.get("sale_start")
        sale_end = request.POST.get("sale_end")
        headline_str = NEWS_INFO['title']
        intro_str = NEWS_INFO['introduction']
        car_pic_list = Picture.objects.filter(carpicture__car__series_id=series_id)
        car_big_pic_list = car_pic_list.filter(carpicture__type__name=PIC_TYPE[1]).distinct()[0:1]
        car_small_pic_list = car_pic_list.filter(carpicture__type__name=PIC_TYPE[2]).distinct()[0:4]
        big_pic_id = car_big_pic_list[0].id if car_big_pic_list else -1
        big_pic_obj = car_big_pic_list[0] if car_big_pic_list else None
        small_pic_id_list = list()
        small_pic_obj_list = list()
        car_list = parse_car_str(car_str)
        if car_small_pic_list:
            for small_pic_obj in car_small_pic_list:
                if len(small_pic_id_list) < 4:
                    small_pic_id_list.append(small_pic_obj.id)
                    small_pic_obj_list.append(small_pic_obj)
        producer_obj = Producer.objects.get(pk=producer_id)
        series_obj = Series.objects.get(pk=series_id)
        price = max(list(data['sale_price'] for data in car_list))
        # print producer_obj.full_name
        producer_name = producer_obj.information.name if producer_obj.information \
                else producer_obj.short_name
        headline = headline_str.replace("<producer_name>", producer_name)\
            .replace("<series_name>", series_obj.name)\
            .replace("<price>", str(price))
        introduction = intro_str.replace("<producer_name>", producer_obj.full_name)\
            .replace("<series_name>", series_obj.name)\
            .replace("<price>", str(price))
        article_html = make_up_article(producer_obj, headline, introduction, sale_start, sale_end, series_obj, big_pic_id, small_pic_id_list, car_list)
        art_type_list = ArticleType.objects.filter(Q(parent_id__isnull=False), Q(name=u'销售促销'))
        art_type_obj = art_type_list[0] if art_type_list else None
        ar = Article(type=art_type_obj, series=series_obj, area=producer_obj.area,
                     title=headline, content=article_html, producer=producer_obj)
        ar.save()
        real_sale_end = str(sale_end) + " 23:59:59"
        real_sale_start = str(sale_start) + " 00:00:00"
        se = SaleSeriesAndColor(series=series_obj)
        se.save()
        for car_info in car_list:
            car_id = car_info['id']
            car_obj = Car.objects.get(pk=car_id)
            sa = SaleCarAndColor(car=car_obj, origin_price=car_info['origin_price'],
                                 discount_price=car_info['sale_price'], finaly_price=car_info['finaly_price'],
                                 inventory=car_info['inven_state'])
            sa.save()
            for color_info in car_info['color_list']:
                cc = CarTypeColor.objects.get(pk=color_info['id'])
                sa.color.add(cc)
            se.car_and_color.add(sa)
        pr = ProducerPreSale(producer=producer_obj, bind_series=se,
                             sale_start=real_sale_start, sale_end=real_sale_end,
                             gift=None, article=ar, hd_pic= big_pic_obj, title=headline,
                             introduction=introduction)
        pr.save()
        for small in small_pic_obj_list:
            pr.small_pic.add(small)
        res = 'success'
    except Exception as e:
        LOG.error('Error happened when execute simple sumbit action .%s' % e)
    return HttpResponse(simplejson.dumps({"res":res}))


def detail_new_action(request):
    res = "fail"
    try:
        series_id = request.POST.get("series_id")
        producer_id = request.POST.get("producer_id")
        car_str = request.POST.get("car_str")
        sale_start = request.POST.get("sale_start")
        sale_end = request.POST.get("sale_end")
        headline = request.POST.get("headline")
        introduction = request.POST.get("introduction")
        big_pic_id = request.POST.get("big_pic_id")
        big_pic_obj = Picture.objects.get(pk=big_pic_id)
        small_pic_list_str = request.POST.get("small_pic_str")
        is_upkeep =True if str(request.POST.get("is_upkeep")) == 'true' else False
        is_addr = True if str(request.POST.get("is_addr")) == 'true'  else False
        is_map = True if str(request.POST.get("is_map")) == 'true' else False
        is_tel = True if str(request.POST.get("is_tel")) == 'true' else False
        small_pic_id_list = small_pic_list_str.split(",")[:-1]
        small_pic_obj_list = list()
        for small_pic_id in small_pic_id_list:
            aa = Picture.objects.get(pk=small_pic_id)
            small_pic_obj_list.append(aa)
        producer_obj = Producer.objects.get(pk=producer_id)
        series_obj = Series.objects.get(pk=series_id)
        car_list = parse_car_str(car_str)
        ps_info = {"is_upkeep":is_upkeep, "is_addr":is_addr, "is_map":is_map, "is_tel":is_tel}
        # price = max(list(data['sale_price'] for data in car_list))
        article_html = make_up_article(producer_obj, headline, introduction, sale_start, sale_end, series_obj, big_pic_id, small_pic_id_list, car_list, ps_info)
        art_type_list = ArticleType.objects.filter(Q(parent_id__isnull=False), Q(name=u'销售促销'))
        art_type_obj = art_type_list[0] if art_type_list else None
        ar = Article(type=art_type_obj, series=series_obj, area=producer_obj.area,
                     title=headline, content=article_html, producer=producer_obj)
        ar.save()
        real_sale_start = str(sale_start) + " 00:00:00"
        real_sale_end = str(sale_end) + " 23:59:59"
        se = SaleSeriesAndColor(series=series_obj)
        se.save()
        for car_info in car_list:
            car_id = car_info['id']
            car_obj = Car.objects.get(pk=car_id)
            sa = SaleCarAndColor(car=car_obj, origin_price=car_info['origin_price'],
                                 discount_price=car_info['sale_price'], finaly_price=car_info['finaly_price'],
                                 inventory=car_info['inven_state'])
            sa.save()
            for color_info in car_info['color_list']:
                cc = CarTypeColor.objects.get(pk=color_info['id'])
                sa.color.add(cc)
            se.car_and_color.add(sa)
        pr = ProducerPreSale(producer=producer_obj, bind_series=se,
                             sale_start=real_sale_start, sale_end=real_sale_end,
                             gift=None, article=ar, hd_pic= big_pic_obj, title=headline,
                             introduction=introduction)
        pr.save()
        for small in small_pic_obj_list:
            pr.small_pic.add(small)
        res = 'success'
    except Exception as e:
        LOG.error('Error happened when execute detail sumbit action .%s' % e)
    return HttpResponse(simplejson.dumps({"res":res}))



def delete_action(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                pr = ProducerPreSale.objects.filter(pk=id_v)
                if pr:
                    pr_obj = pr[0]
                    # art_obj = pr_obj.article
                    # art_obj.delete()
                    # se_color = pr_obj.bind_series
                    # se_color.car_and_color.all().delete()
                    # se_color.delete()
                    pr_obj.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete logo. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    data_list = list()
    year_dict = dict()
    try:
        if "producer_id" in request.GET and "_auth_user_id" in request.session:
            producer_id = request.session['_auth_user_id']
            sale_id = request.GET.get("sale_id")
            Presale_obj = ProducerPreSale.objects.get(pk=sale_id)
            checked_series_obj = Presale_obj.bind_series.series
            producer_obj = Producer.objects.get(pk=producer_id)
            # brand_obj_list = ProducerCarSeries.objects.filter(producer=producer_obj)
            now = timezone.datetime.now()
            sale_obj_list = ProducerPreSale.objects.filter(Q(sale_end__gte=now),
                                                           Q(sale_start__lte=now),
                                                           Q(bind_series__series=checked_series_obj))
            brand_name = checked_series_obj.brand.name
            # series_name = checked_series_obj.name
            color_base_list = CarTypeColor.objects.filter(Q(seriesyearcolor__series=checked_series_obj),
                                                          Q(side=0))
            series_year_color_list = SeriesYearColor.objects.filter(Q(series=checked_series_obj),
                                                                Q(color__side=0))
            base_year_color = dict()
            for series_year in series_year_color_list:
                year = series_year.year
                all_color_list = series_year.color.filter(side=0).distinct()
                for color in all_color_list:
                    c_param = dict()
                    c_param['id'] = color.id
                    c_param['name'] = color.name
                    if not base_year_color.has_key(year):
                        base_year_color[year] = list()
                    if c_param not in base_year_color[year]:
                        base_year_color[year].append(c_param)
            # color_origin_list = list()
            # for base_obj in color_base_list:
            #     base_param = dict()
            #     base_param['id'] = base_obj.id
            #     base_param['name'] = base_obj.name
            # 促销车型括号中的数字
            car_sum = 0
            for sale_obj in sale_obj_list:
                car_num = sale_obj.bind_series.car_and_color.all().count()
                car_sum += car_num
            sale_start = Presale_obj.sale_start.strftime("%Y-%m-%d")
            sale_end = Presale_obj.sale_end.strftime("%Y-%m-%d")

            sale_obj_list = ProducerPreSale.objects.filter(
                Q(sale_end__gte=now), Q(sale_start__lte=now),
                Q(bind_series__series=checked_series_obj))
            sale_car_dict = dict()
            # 首先记录本次促销车款促销相关信息
            car_color_list = Presale_obj.bind_series.car_and_color.all()
            car_pub_dict = dict()
            # car_pub_id_list = list()
            for car_info in car_color_list:
                # car_obj = car_info.car
                car_param = dict()
                car_param['car_id'] = car_info.car_id
                car_param['origin_price'] = car_info.origin_price
                car_param['discount_price'] = car_info.discount_price
                car_param['finaly_price'] = car_info.finaly_price
                car_param['inventory'] = car_info.inventory
                car_param['year'] = car_info.car.produce_year
                car_param['name'] = car_info.car.name
                # 颜色模块
                car_color_obj_list = car_info.color.all()
                car_param['color'] = dict()
                chosen_color_num = car_color_obj_list.count()
                car_param['color']['num'] = chosen_color_num
                car_param['color']['is_all'] = True \
                    if chosen_color_num == len(base_year_color[car_info.car.produce_year]) else False
                co_str = ""
                co_name_list = list()
                for car_color in car_color_obj_list:
                    co_str += "{0},".format(car_color.id)
                    co_name_list.append(car_color.name)
                car_param['color']['str'] = co_str[:-1] if co_str else co_str
                car_param['color']['name'] = co_name_list
                car_pub_dict[car_info.car_id] = car_param
            # 统计已发促销次数
            for sale_series_obj in sale_obj_list:
                car_list = sale_series_obj.bind_series.car_and_color.all()
                for sale_car in car_list:
                    car_id = sale_car.car_id
                    if not sale_car_dict.has_key(car_id):
                        sale_car_dict[car_id] = 1
                    else:
                        sale_car_dict[car_id] = sale_car_dict[car_id] + 1
            # 所有代理车款
            car_list = Car.objects.filter(series=checked_series_obj)
            for car_obj in car_list:
                car_param = dict()
                if car_pub_dict.has_key(car_obj.id):
                    car_param = car_pub_dict[car_obj.id]
                    car_param['is_checked'] = True
                else:
                    car_quote_list = CarQuote.objects.filter(Q(car=car_obj), Q(producer=producer_obj))
                    if len(car_quote_list) != 0:
                        car_param['origin_price'] = car_quote_list[0].sale_price
                        car_param['finaly_price'] = car_quote_list[0].sale_price
                    else:
                        car_param['origin_price'] = car_obj.price
                        car_param['finaly_price'] = car_obj.price
                    car_param['year'] = car_obj.produce_year
                    car_param['discount_price'] = 0
                    car_param['inventory'] = 0
                    car_param['car_id'] = car_obj.id
                    car_param['name'] = car_obj.name
                    car_param['is_checked'] = False
                    car_param['color'] = dict()
                    color_list = base_year_color[car_obj.produce_year]
                    car_param['color']['num'] = len(color_list)
                    # year_color = series_year_color_list.filter(year=car_info.car.produce_year)
                    car_param['color']['is_all'] = True
                    car_param['color']['name'] =list()
                    color_str = ""
                    for cc in color_list:
                        color_str += "{0},".format(cc['id'])
                    car_param['color']['str'] = color_str[:-1] if color_str else ""
                if sale_car_dict.has_key(car_obj.id):
                    car_param['num'] = sale_car_dict[car_obj.id]
                else:
                    car_param['num'] = 0
                data_list.append(car_param)
                year = str(car_obj.produce_year)
                if year_dict.has_key(year):
                    if not car_param['is_checked']:
                        year_dict[year]['is_checked'] = False
                else:
                    year_dict[year] = dict()
                    year_dict[year]['year'] = year
                    if not car_param['is_checked']:
                        year_dict[year]['is_checked'] = False
                    else:
                        year_dict[year]['is_checked'] = True
            # 文字部分
            headline = Presale_obj.title
            introduction = Presale_obj.introduction
            # 图片部分
            big_pic_dict = dict()
            big_pic_obj = Presale_obj.hd_pic
            if big_pic_obj:
                big_pic_dict['id'] = big_pic_obj.id
                big_pic_dict["path"] = big_pic_obj.path
            small_pic_list = list()
            small_pic_obj_list = Presale_obj.small_pic.all()
            for small_obj in small_pic_obj_list:
                pic_param = dict()
                pic_param['id'] = small_obj.id
                pic_param['path'] = small_obj.path
                small_pic_list.append(pic_param)

            # 额外信息模块
            add_info_dict = dict()
            add_info_dict['is_addr'] = Presale_obj.company_adr_in_buttom
            add_info_dict['is_map'] = Presale_obj.map_in_buttom
            add_info_dict['is_tel'] = Presale_obj.sale_tel_in_buttom
            add_info_dict['is_upkeep'] = Presale_obj.keep_up_in_buttom
            return render(request, "background/sale/producer-edit.html",
                          {"producer_id": producer_id, "series": checked_series_obj,
                           "brand_name": brand_name, "year_dict": year_dict,
                           "sale_start": sale_start, "sale_end": sale_end, "data":data_list,
                           "title":headline,"introduction":introduction,
                           "big_pic_dict": big_pic_dict, "small_pic_list":small_pic_list,
                           "add_info_dict": add_info_dict, "sale_id":Presale_obj.id}
                          )
        else:
            return redirect('producer-login')
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete logo. %s' % e)


def edit_action(request):
    res = "fail"
    try:
        sale_id = request.POST.get("sale_id")
        series_id = request.POST.get("series_id")
        producer_id = request.POST.get("producer_id")
        car_str = request.POST.get("car_str")
        sale_start = request.POST.get("sale_start")
        sale_end = request.POST.get("sale_end")
        headline = request.POST.get("headline")
        introduction = request.POST.get("introduction")
        big_pic_id = request.POST.get("big_pic_id")
        big_pic_obj = Picture.objects.get(pk=big_pic_id)
        small_pic_list_str = request.POST.get("small_pic_str")
        is_upkeep =True if str(request.POST.get("is_upkeep")) == 'true' else False
        is_addr = True if str(request.POST.get("is_addr")) == 'true'  else False
        is_map = True if str(request.POST.get("is_map")) == 'true' else False
        is_tel = True if str(request.POST.get("is_tel")) == 'true' else False
        small_pic_id_list = small_pic_list_str.split(",")[:-1]
        small_pic_obj_list = list()
        for small_pic_id in small_pic_id_list:
            aa = Picture.objects.get(pk=small_pic_id)
            small_pic_obj_list.append(aa)
        producer_obj = Producer.objects.get(pk=producer_id)
        series_obj = Series.objects.get(pk=series_id)
        car_list = parse_car_str(car_str)
        ps_info = {"is_upkeep":is_upkeep, "is_addr":is_addr, "is_map":is_map, "is_tel":is_tel}
        # price = max(list(data['sale_price'] for data in car_list))
        article_html = make_up_article(producer_obj, headline, introduction, sale_start, sale_end, series_obj, big_pic_id, small_pic_id_list, car_list, ps_info)
        # 删除旧的
        pr_obj = ProducerPreSale.objects.get(pk=sale_id)
        se_color = pr_obj.bind_series
        s_car_solor = se_color.car_and_color
        for ss in s_car_solor.all():
            ss.color.clear()
            ss.delete()
        # s_car_solor.clear()
        se_color.delete()
        pr_obj.small_pic.clear()
        # 文章部分不能直接删除,使用更新
        # ar = Article.objects.filter(pk=pr_obj.article_id)
        # ar.update(title=headline, content=article_html)
        pr_obj.article.title=headline
        pr_obj.article.content=article_html
        pr_obj.article.save()
        real_sale_start = str(sale_start) + " 00:00:00"
        real_sale_end = str(sale_end) + " 23:59:59"
        se = SaleSeriesAndColor(series=series_obj)
        se.save()
        for car_info in car_list:
            car_id = car_info['id']
            car_obj = Car.objects.get(pk=car_id)
            sa = SaleCarAndColor(car=car_obj, origin_price=car_info['origin_price'],
                                 discount_price=car_info['sale_price'], finaly_price=car_info['finaly_price'],
                                 inventory=car_info['inven_state'])
            sa.save()
            for color_info in car_info['color_list']:
                cc = CarTypeColor.objects.get(pk=color_info['id'])
                sa.color.add(cc)
            se.car_and_color.add(sa)
        # pr = ProducerPreSale(producer=producer_obj, bind_series=se,
        #                      sale_start=real_sale_start, sale_end=real_sale_end,
        #                      gift=None, article=ar, hd_pic= big_pic_obj, title=headline,
        #                      introduction=introduction)
        # pr.save()
        pr = ProducerPreSale.objects.filter(pk=sale_id)
        pr.update(producer=producer_obj, bind_series=se,
                             sale_start=real_sale_start, sale_end=real_sale_end,
                             gift=None, hd_pic= big_pic_obj, title=headline,
                             introduction=introduction)
        for small in small_pic_obj_list:
            pr_obj.small_pic.add(small)
        res = 'success'
    except Exception as e:
        LOG.error('Error happened when execute detail sumbit action .%s' % e)
    return HttpResponse(simplejson.dumps({"res":res}))
