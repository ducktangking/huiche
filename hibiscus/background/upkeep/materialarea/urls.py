from django.conf.urls import url

from hibiscus.background.upkeep.materialarea import views

urlpatterns = [
    url(r'^$', views.index, name="background-material-area-index"),
    url(r'^data/$', views.data, name="background-material-area-data"),
    url(r'^delete/action/$', views.action_delete, name="background-material-area-action-delete"),
    url(r'^new/$', views.new, name="background-material-area-new"),
    url(r'^new/action/$', views.action_new, name="background-material-area-action-new"),
    url(r'^edit/$', views.edit, name="background-material-area-edit"),
    url(r'^edit/action/$', views.action_edit, name="background-material-area-action-edit"),
    url(r'^ajax/data/$', views.ajax_data, name="background-material-area-ajax-data"),
    url(r'^config/$', views.config, name="background-material-area-config"),
]
