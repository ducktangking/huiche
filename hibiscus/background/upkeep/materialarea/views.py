import simplejson
import logging
import random
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q, Count
from django.contrib import messages
from django.utils.translation import ugettext as _

from hibiscus.models import UpkeepPackage, UpkeepType, Upkeep, SupportedArea, Picture, UpkeepMaterial, Series
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
UPKEEP_MATERIAL_COLUMNS = ["name", "brand", "model", "price", "price_min", "price_max", "created_at"]


def index(request):
    type_id = request.GET["type_id"] if "type_id" in request.GET else ""
    pkg_id = request.GET["pkg_id"] if "pkg_id" in request.GET else ""
    up_id = request.GET["up_id"] if "up_id" in request.GET else ""
    upkeep_pkg = None
    upkeep = None
    try:
        if pkg_id != "":
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
        if up_id != "":
            upkeep = Upkeep.objects.get(id=up_id)

    except Exception as e:
        upkeep_pkg = None
        upkeep = None
        messages.add_message(request, messages.ERROR, _("Upkeep or Upkeep Package is not exist!"))
        LOG.debug("Get Upkeep Package or Upkeep error, error is %s", e)

    return render(request, 'background/upkeep/material/index.html', {"up_id": up_id, "type_id": type_id, "pkg_id": pkg_id,
                                                                     "upkeep_pkg": upkeep_pkg, "upkeep": upkeep})


@data_table(columns=UPKEEP_MATERIAL_COLUMNS, model=UpkeepMaterial)
def data(request):
    try:
        up_id = request.GET["up_id"] if "up_id" in request.GET else ""
        if up_id != "":
            sts = UpkeepMaterial.objects.filter(
                Q(upkeep__id=up_id), Q(name__contains=request.table_data["key_word"])). \
                order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepMaterial.objects.filter(upkeep__id=up_id).count()
        else:
            sts = UpkeepMaterial.objects.filter(
                Q(name__contains=request.table_data["key_word"])). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepMaterial.objects.all().count()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["model"] = st.model
            st_params["name"] = st.name
            st_params["brand"] = st.brand
            st_params["price"] = st.price
            st_params["price_min"] = st.price_min
            st_params["price_max"] = st.price_max
            st_params["upkeep"] = st.upkeep.name
            # st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list, total_num
    except Exception as e:
        LOG.error('Can not get cart upkeep package data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                pkg = UpkeepMaterial.objects.filter(pk=id_v)
                pkg.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep material. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def random_code():
    code = random.randint(10000, 99999)
    has_exist = UpkeepMaterial.objects.filter(code=code).count()
    if has_exist > 0:
        return random_code()
    else:
        return code


def new(request):
    code = random_code()
    type_id = request.GET["type_id"] if "type_id" in request.GET else ""
    pkg_id = request.GET["pkg_id"] if "pkg_id" in request.GET else ""
    up_id = request.GET["up_id"] if "up_id" in request.GET else ""
    upkeep_pkg = None
    upkeep = None
    try:
        if pkg_id != "":
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
        if up_id != "":
            upkeep = Upkeep.objects.get(id=up_id)
    except Exception as e:
        upkeep_pkg = None
        upkeep = None
        messages.add_message(request, messages.ERROR, _("Upkeep or Upkeep Package is not exist!"))
        LOG.debug("Get Upkeep Package or Upkeep error, error is %s", e)
    return render(request, 'background/upkeep/material/new.html', {"up_id": up_id, "type_id": type_id, "pkg_id": pkg_id,
                                                                     "upkeep_pkg": upkeep_pkg, "upkeep": upkeep,
                                                                   "code": code})


def action_new(request):
    res = "fail"
    try:
        up_id = request.POST.get("upkeep", "")
        if up_id != "":

            code = request.POST.get("code")
            up_obj = Upkeep.objects.get(pk=up_id)
            area_ids = request.POST.getlist("area")
            areas = SupportedArea.objects.filter(id__in=area_ids)

            model = request.POST.get("modal", "")
            brand = request.POST.get("materialBrand", "")
            name = request.POST.get("name", "")
            content = request.POST.get("content", "")
            price = request.POST.get("price", 0)
            price_min = request.POST.get("priceMin", 0)
            price_max = request.POST.get("priceMax", 0)
            ma_type = request.POST.get("materialT", UpkeepMaterial.TYPE_CHOICES[0][0])
            materialPic = request.FILES["materialPic"]
            img_path = deal_with_upload_file(materialPic)
            p_obj = Picture.objects.create(name=materialPic.name, path=img_path)
            m = UpkeepMaterial.objects.create(upkeep=up_obj, picture=p_obj, model=model, name=name, brand=brand, type=ma_type,
                                              content=content, price=price, price_min=price_min, price_max=price_max, code=code)
            for a in areas:
                m.area.add(a)
            res = "success"
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create upkeey package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    up_id = request.GET.get("up_id", "")
    pkg_id =  request.GET.get("pkg_id", "")
    type_id = request.GET.get("type_id", "")
    material_id = request.GET.get("material_id", "")
    if material_id != "":
        upkeep_material = None
        upkeep_obj = None
        upkeep_package = None
        try:
            upkeep_material = UpkeepMaterial.objects.get(id=material_id)
            upkeep_obj = upkeep_material.upkeep
            selectedAreas = [area.id for area in upkeep_material.area.all()]
            if pkg_id != "":

                upkeep_package =  upkeep_obj.package.first()
        except Exception as e:
            LOG.error("Material edit, get upkeep material error! error is %s", e)
            upkeep_material = None
            upkeep_obj = None
            upkeep_package = None
        return render(request, "background/upkeep/material/edit.html", {"upkeep_material": upkeep_material, "upkeep":upkeep_obj,
                                                                        "upkeep_package": upkeep_package, "selectedAreas":selectedAreas,
                                                                        "up_id":up_id,"type_id":type_id, "pkg_id":pkg_id})


def action_edit(request):
    res = "fail"
    try:
        material_id = request.POST.get("materialId", "")
        if material_id != "":

            material_obj = UpkeepMaterial.objects.get(pk=material_id)
            upkeep_id = request.POST.get("upkeep", "")
            if material_obj.upkeep.id != upkeep_id:
                material_obj.upkeep =  Upkeep.objects.get(pk=upkeep_id)

            material_obj.model = request.POST.get("modal", "")
            material_obj.brand = request.POST.get("materialBrand", "")
            material_obj.name = request.POST.get("name", "")
            material_obj.content = request.POST.get("content", "")
            material_obj.price = request.POST.get("price", 0)
            material_obj.price_min = request.POST.get("priceMin", 0)
            material_obj.price_max = request.POST.get("priceMax", 0)
            material_obj.type = request.POST.get("materialT", UpkeepMaterial.TYPE_CHOICES[0][0])



            materialPic = request.FILES.get("materialPic", None)
            if materialPic:
                img_path = deal_with_upload_file(materialPic)
                p_obj = Picture.objects.create(name=materialPic.name, path=img_path)
                material_obj.picture = p_obj
            material_obj.save()
            selected_area_ids = request.POST.getlist("area")
            selected_areas = SupportedArea.objects.filter(id__in=selected_area_ids)

            material_obj.area.clear()
            for selected_area in selected_areas:
                material_obj.area.add(selected_area)

            res = "success"
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create upkeey package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def ajax_data(request):
    material_data = []
    upkeeps = []
    all_data = []
    t_list = []
    try:
        type_id = request.GET.get("type_id", None)
        if type_id and int(type_id) == 1:
            upkeep_type = UpkeepType.objects.get(pk=type_id)
            for upkeep_package in upkeep_type.upkeeppackage_set.all():
                __pkg = upkeep_package.upkeep_set.all()
                up_m_params = {}
                for __up in __pkg:
                    material_data = []
                    # up_m_params = {}
                    up_m_params['up_id'] = __up.id
                    up_m_params['up_name'] = __up.name
                    __material = __up.upkeepmaterial_set.all().values("brand").annotate(Count("brand", distinct=True))
                    for __m in __material:
                        m_params = {}
                        m_params['m_brand'] = __m['brand']
                        # get one brand's model
                        ms = UpkeepMaterial.objects.filter(brand=__m['brand'])
                        ms_model = [m.model for m in ms]
                        m_params['m_model'] = ms_model
                        material_data.append(m_params)
                    up_m_params['m_data'] = material_data
                    all_data.append(up_m_params)
        if type_id and int(type_id) == 2:
            upkeep_type = UpkeepType.objects.get(pk=type_id)
            for upkeep_package in upkeep_type.upkeeppackage_set.all():
                __pkg = upkeep_package.upkeep_set.all()
                for __up in __pkg:
                    material_data = []
                    up_m_params = {}
                    up_m_params['up_id'] = __up.id
                    up_m_params['up_name'] = __up.name
                    upkeeps.append(up_m_params)
            return HttpResponse(simplejson.dumps(upkeeps))
    except Exception as e:
        LOG.error('Can not get material data. %s' % e)
    return HttpResponse(simplejson.dumps(all_data))


def material_model(brand_name):
    ms_model = []
    try:
        ms = UpkeepMaterial.objects.filter(brand=brand_name)
        ms_model = [m.model for m in ms]
    except Exception as e:
        LOG.error('Can not get material model from brand name. %s' % e)
    return HttpResponse(simplejson.dumps(ms_model))


def config(request):
    provinces = {}
    try:
        materialId = request.GET['materialId']
        provinces = SupportedArea.objects.values("province").annotate(Count("province"))
        series = Series.objects.all()
    except Exception as e:
        LOG.error('Can not get config data.%s' % e)
    return render(request, 'background/upkeep/materialarea/config.html',
                  {"materialId": materialId, "provinces": provinces, "series": series})
