from django.conf.urls import url, include

urlpatterns = [
    url(r'^type/', include('hibiscus.background.upkeep.type.urls')),
    url(r'^package/', include('hibiscus.background.upkeep.package.urls')),
    url(r'^proj/', include('hibiscus.background.upkeep.upkeep.urls')),
    url(r'^material/', include('hibiscus.background.upkeep.material.urls')),
    url(r'^oil/', include('hibiscus.background.upkeep.oil.urls')),
    url(r'^series/upkeep/', include('hibiscus.background.upkeep.upkeepseries.urls')),
    url(r'^material/area/', include('hibiscus.background.upkeep.materialarea.urls')),
    url(r'^year/cc/', include('hibiscus.background.upkeep.yearcc.urls')),
]
