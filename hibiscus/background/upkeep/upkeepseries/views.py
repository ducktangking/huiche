# -*- coding: utf-8 -*-
import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q, Count
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils import timezone

from hibiscus.models import Upkeep, UpkeepPackage, UpkeepType, SeriesUpkeepInfo, Series, Car, Brand, \
    UpkeepMaterial, UpkeepSeries, UpkeepMaterialModel, SeriesUpkeepQuote, Producer, CarYear
from hibiscus.decorators import data_table
from hibiscus.background.user.producer.views import pkg_upkeep_quote_detail

LOG = logging.getLogger(__name__)
SERIES_UP_INFO_COLUMNS = ["cc", "year"]
SERIES_UP_MANAGE_COLUMNS = ["series__brand__name", "series__name", "cc", "series__brand__country", "series__brand__level", "produce_year"]

PARAMS_ID1 = [1]
PARAMS_ID2 = [2]

UP_SERIES_COLUMNS = ["brand__parent__initial", "brand__parent__name", "brand__name", "name", "brand__country__name", "brand__level"]


def reuqst_get(item_dict, name, default):
    __value = item_dict.get(name, default)
    if __value == "":
        return default
    else:
        return __value


def index(request):
    upkeep_s = UpkeepSeries.objects.all()

    all_data = []
    for up in upkeep_s:
        ups = up.seriesupkeepinfo_set.all().filter(is_quote=False)
        upkeep_series_params = {}
        upkeep_series_params['cc'] = up.cc
        if up.produce_year:
            upkeep_series_params['produce_year'] = up.produce_year.year
        else:
            upkeep_series_params['produce_year'] = ""
        upkeep_series_params['series_name'] = up.series.name
        upkeep_detail = []
        for u in ups:
            params = {}
            if u.upkeep_material_model or u.upkeep:
                if u.upkeep_material_model:
                    params['upkeep_material'] = u.upkeep_material_model
                else:
                    params['upkeep_material'] = ""
                if u.upkeep:
                    params['upkeep'] = u.upkeep
                else:
                    params['upkeep'] = ""
                params['id'] = u.id
                upkeep_detail.append(params)
            else:
                continue
        if upkeep_detail:
            upkeep_series_params['upkeep_detail'] = upkeep_detail

            all_data.append(upkeep_series_params)

    return render(request, 'background/upkeep/upkeepseries/index.html', {"all_data": all_data})


def index_new(request):
    all_data = []
    try:
        sec_brand = Brand.objects.filter(parent_id__isnull=True).order_by('initial')
        len = 0
        ini = ''
        ini_len = dict()
        for se in sec_brand:
            ini_len[se.initial] = 0
        for se in sec_brand:
            ini_len[se.initial] += 1
        for se in sec_brand:
            se_params = dict()
            se_params['id'] = se.id
            se_params['len'] = ini_len[se.initial]
            if ini == se.initial:
                se_params['initial'] = ''
            else:
                se_params['initial'] = se.initial
            se_params['name'] = se.name
            se_params['country'] = se.country.name
            se_params['level'] = se.level.name
            ini = se.initial
            all_data.append(se_params)
    except Exception as e:
        LOG.error('Can not get cart upkeep data.%s' % e)
    return render(request, 'background/upkeep/upkeepseries/index_new.html', {"all_data": all_data})


def show_material_info(request):
    """
        # 分析：根据一级页面的车型二级品牌来筛选材料，可得出该材料的方式有：
        # 1、品牌绑定、车型对应品牌绑定
        # 2、级别绑定，材料对应级别绑定，且如果材料级别中有指定车型，车型中存在该二级品牌车辆
        # 3、all_series = True

        修改，根据一级品牌来筛选材料，得出材料的方式有
        1、二级品牌绑定，二级品牌车型绑定，相应的材料出现在一级品牌下
        2、级别绑定，材料对应级别绑定，且如果材料级别中有指定车型，车型中存在该一级品牌车辆
        3、all_series = True
    """
    edit_data = []
    try:
        all_data = []
        top_brand_id = request.GET['brandId']
        top_brand = Brand.objects.get(pk=top_brand_id)
        level_name = top_brand.level.name
        s_upkeep_1 = SeriesUpkeepInfo.objects.filter(Q(brands__parent_id=top_brand_id) |
                                                     Q(series__brand__parent_id=top_brand_id))
        s_upkeep_2 = SeriesUpkeepInfo.objects.filter(Q(brands__isnull=True) | Q(series__isnull=True),
                                                     Q(upkeep_material_model__level__name=level_name))
        s_upkeep_3 = SeriesUpkeepInfo.objects.filter(all_series=True)
        # 取到所有会出现的材料的id
        model_id_list = []
        if s_upkeep_1:
            for su in s_upkeep_1:
                # su_params = dict()
                # su_params['id'] = su.upkeep_material_model_id
                # su_params['m_name'] = su.upkeep_material_model.name
                # su_params['label_price'] = su.upkeep_material_model.time_labor_price
                # su_params['material_price'] = su.upkeep_material_model.material_price
                # su_params['code'] = su.upkeep_material_model.code
                # su_params['type'] = u"通用" if su.upkeep_material_model.type == 'G' else u"原厂"
                model_id_list.append(su.upkeep_material_model_id)
        if s_upkeep_2:
            for su in s_upkeep_2:
                # su_params = dict()
                # su_params['id'] = su.upkeep_material_model_id
                # su_params['m_name'] = su.upkeep_material_model.name
                # su_params['label_price'] = su.upkeep_material_model.time_labor_price
                # su_params['material_price'] = su.upkeep_material_model.material_price
                # su_params['code'] = su.upkeep_material_model.code
                # su_params['type'] = u"通用" if su.upkeep_material_model.type == 'G' else u"原厂"
                if su.upkeep_material_model_id not in model_id_list:
                    model_id_list.append(su.upkeep_material_model_id)
        if s_upkeep_3:
            for su in s_upkeep_3:
                # su_params = dict()
                # su_params['id'] = su.upkeep_material_model_id
                # su_params['m_name'] = su.upkeep_material_model.name
                # su_params['label_price'] = su.upkeep_material_model.time_labor_price
                # su_params['material_price'] = su.upkeep_material_model.material_price
                # su_params['code'] = su.upkeep_material_model.code
                # su_params['type'] = u"通用" if su.upkeep_material_model.type == 'G' else u"原厂"
                if su.upkeep_material_model_id not in model_id_list:
                    model_id_list.append(su.upkeep_material_model_id)
        # if material_data:
        #     num_dict = dict()
        #     # 添加套餐和品牌
        #     for ma in material_data:
        #         model_obj = UpkeepMaterialModel.objects.get(pk=ma['id'])
        #         ma['upkeep_name'] = model_obj.material.upkeep.name
        #         ma['upkeep_id'] = model_obj.material.upkeep_id
        #         ma['material_brand'] = model_obj.material.brand
        #         ma['material_id'] = model_obj.material_id
        #         num_dict['u_' + model_obj.material.upkeep_id] = 0
        #         num_dict['m_' + model_obj.material_id] = 0
        #     for ma in material_data:
        #         num_dict['u_' + ma['upkeep_id']] += 1
        #         num_dict['m_' + ma['material_id']] += 1
        #     for ma in material_data:
        upkeep_obj = Upkeep.objects.filter(package__type_id=1)
        up_id_list = []
        for up in upkeep_obj:
            up_params = dict()
            up_params['name'] = up.name
            if up.id in up_id_list:
                continue
            up_id_list.append(up.id)
            up_params['material_list'] = []
            up_params['up_len'] = 0
            material_obj = up.upkeepmaterial_set.all()
            if material_obj:
                for mt in material_obj:
                    mt_params = dict()
                    mt_params['brand'] = mt.brand
                    mt_params['model_list'] = []
                    mt_params['brand_len'] = 0
                    model_obj = UpkeepMaterialModel.objects.filter(material=mt)
                    if model_obj:
                        for mo in model_obj:
                            mo_params = dict()
                            if mo.id not in model_id_list:
                                continue
                            mt_params['brand_len'] += 1
                            mo_params['id'] = mo.id
                            mo_params['m_name'] = mo.name + " " + mo.model
                            mo_params['label_price'] = mo.time_labor_price
                            mo_params['material_price'] = mo.material_price
                            mo_params['code'] = mo.code
                            mo_params['type'] = u"通用" if mo.type == 'G' else u"原厂"
                            mt_params['model_list'].append(mo_params)
                    if not mt_params['model_list']:
                        continue
                    up_params['material_list'].append(mt_params)
                    up_params['up_len'] += mt_params['brand_len']
            if not up_params['material_list']:
                continue
            all_data.append(up_params)
        if all_data:
            upkeep_len = 0
            material_len = 0
            for all_dat in all_data:
                for all_d in all_dat['material_list']:
                    for al in all_d['model_list']:
                        al_params = dict()
                        if upkeep_len == 0:
                            al_params['up_name'] = all_dat['name']
                        else:
                            al_params['up_name'] = ''
                        al_params['up_len'] = all_dat['up_len']
                        upkeep_len += 1
                        if upkeep_len == al_params['up_len']:
                            upkeep_len = 0
                        if material_len == 0:
                            al_params['m_brand'] = all_d['brand']
                        else:
                            al_params['m_brand'] = ''
                        al_params['brand_len'] = all_d['brand_len']
                        material_len += 1
                        if material_len == al_params['brand_len']:
                            material_len = 0
                        al_params['model_id'] = al['id']
                        al_params['m_name'] = al['m_name']
                        al_params['label_price'] = al['label_price']
                        al_params['material_price'] = al['material_price']
                        al_params['code'] = al['code']
                        al_params['type'] = al['type']
                        edit_data.append(al_params)
    except Exception as e:
        LOG.error('Error happened when get bind-material info.%s' % e)
    return render(request, 'background/upkeep/upkeepseries/material_bind_series.html', {"edit_data": edit_data, "brandId":top_brand_id})


def series_match(request):
    """
        分析：车型匹配根据材料id，找出对应的适用车型，再根据车型分成各种年份、排量,详细:
        1、指定车型，出现相应的车型
        2、指定品牌，所有车型
        3、高低端，无指定品牌车型，出现全部车型
        4、所有车型。所有车型
        及，如果为指定车型，可能有对应车型的可能，其他情况都是全部车型
        然后通过series_id 找upkeepseriesinfo中的数据，所有存在的车型
    """
    all_data = []
    try:
        top_brand_id = request.GET.get("brandId", '')
        brand_list = Brand.objects.filter(parent_id=top_brand_id)
        for brand in brand_list:
            j = 0
            ca_data = []
            sec_brand_id = brand.id
            model_id = request.GET.get("modelId", '')
            sui_obj_list = SeriesUpkeepInfo.objects.filter(upkeep_material_model_id=model_id)
            sui_obj = sui_obj_list[0]
            series_obj = sui_obj.series.all()
            # 所有取消绑定的车型
            # un_bind_list = []
            # un_bind_series = sui_obj.exclude_series.all()
            # if un_bind_series:
            #     for unbs in un_bind_series:
            #         un_bind_list.append(unbs.id)
            series_list = []
            # 判断是否有指定车型，且是否指定车型中有属于该二级品牌
            if series_obj:
                for se in series_obj:
                    if se.brand_id == sec_brand_id:
                        series_list.append(se.id)
            if series_list:
                up_series_obj = UpkeepSeries.objects.filter(series_id__in=series_list)
                print len(up_series_obj)
                up_series_id_list = []
                # 找出所有已创建保养年份排量的车型，并分类
                if up_series_obj:
                    for up in up_series_obj:
                        if up.series_id in up_series_id_list:
                            continue
                        up_series_id_list.append(up.series_id)
                    for up_se_id in up_series_id_list:
                        upkeep_se_obj = UpkeepSeries.objects.filter(series_id=up_se_id)
                        binded_up_se_obj = SeriesUpkeepInfo.objects.filter(Q(upkeep_series__series_id=up_se_id),
                                                                           Q(upkeep_material_model_id=model_id) )
                        binded_list = []
                        if binded_up_se_obj:
                            for buso in binded_up_se_obj:
                                binded_list.append(buso.upkeep_series_id)
                        i = 0
                        for us in upkeep_se_obj:
                            us_params = dict()
                            us_params['id'] = us.id
                            if j == 0:
                                us_params['sec_brand'] = brand.name
                            else:
                                us_params['sec_brand'] = ''
                            j += 1
                            if i == 0:
                                us_params['series'] = us.series.name
                            else:
                                us_params['series'] = ''
                            i += 1
                            us_params['cc'] = us.cc
                            us_params['brand_len'] = len(up_series_obj)
                            us_params['se_len'] = len(upkeep_se_obj)
                            us_params['produce_year'] = us.produce_year.year
                            if us.id in binded_list:
                                us_params['status'] = True
                            else:
                                us_params['status'] = False
                            ca_data.append(us_params)
            else:
                up_series_obj = UpkeepSeries.objects.filter(series__brand_id=sec_brand_id)
                print len(up_series_obj)
                up_series_list = []
                # 找到所有保养车型
                if up_series_obj:
                    for up_s in up_series_obj:
                        if up_s.series_id in up_series_list:
                            continue
                        up_series_list.append(up_s.series_id)
                        #按照车型给保养车型分类
                    for up_se_id in up_series_list:
                        upkeep_se_obj = UpkeepSeries.objects.filter(series_id=up_se_id)
                        binded_up_se_obj = SeriesUpkeepInfo.objects.filter(Q(upkeep_series__series_id=up_se_id),
                                                                           Q(upkeep_material_model_id=model_id))
                        binded_list = []
                        # 找到所有已绑定的车型
                        if binded_up_se_obj:
                            for buso in binded_up_se_obj:
                                binded_list.append(buso.upkeep_series_id)
                        i = 0
                        for us in upkeep_se_obj:
                            us_params = dict()
                            us_params['id'] = us.id
                            if j == 0:
                                us_params['sec_brand'] = brand.name
                            else:
                                us_params['sec_brand'] = ''
                            j += 1
                            if i == 0:
                                us_params['series'] = us.series.name
                            else:
                                us_params['series'] = ''
                            i += 1
                            us_params['cc'] = us.cc
                            us_params['se_len'] = len(upkeep_se_obj)
                            us_params['brand_len'] = len(up_series_obj)
                            us_params['produce_year'] = us.produce_year.year
                            if us.id in binded_list:
                                us_params['status'] = True
                            else:
                                us_params['status'] = False
                            ca_data.append(us_params)
            all_data += ca_data
    except Exception as e:
        LOG.error('Error happened when get bind-material info.%s' % e)
    return render(request, 'background/upkeep/upkeepseries/series_bind_upkeep.html',
                                  {"all_data": all_data, "modelId": model_id})


def bind_action(request):
    res = 'fail'
    try:
        status = request.GET.get('status', '')
        model_id = request.GET.get('modelId', '')
        upkeepseries_id = request.GET.get('upkeepSeriesId', '')
        se_up_obj = SeriesUpkeepInfo.objects.filter(upkeep_material_model_id=model_id)[0]
        up_series_obj = UpkeepSeries.objects.get(pk=upkeepseries_id)
        if status:
            aa = SeriesUpkeepInfo.objects.create(all_series=se_up_obj.all_series, upkeep_material_model=se_up_obj.upkeep_material_model,
                                                 upkeep=se_up_obj.upkeep, is_quote=se_up_obj.is_quote, created_at=se_up_obj.created_at,
                                                 upkeep_series=up_series_obj)
            if se_up_obj.series.all():
                for se in se_up_obj.series.all():
                    aa.series.add(se)
            if se_up_obj.brands.all():
                for br in se_up_obj.brands.all():
                    aa.brands.add(br)
            res = 'success'
        else:
            del_obj = SeriesUpkeepInfo.objects.filter(Q(upkeep_material_model_id=model_id),
                                                      Q(upkeep_series=up_series_obj))
            if del_obj:
                del_obj[0].delete()
            res = 'success'
    except Exception as e:
        LOG.error('Error happened when get bind-material info.%s' % e)
    return HttpResponse(simplejson.dumps(res))


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                SeriesUpkeepInfo.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    try:
        upkeep_types = UpkeepType.objects.all()
        top_brands = Brand.objects.filter(parent__isnull=True)
        brands = Brand.objects.filter(parent_id=top_brands[0].id).filter(parent__isnull=False)
        brand_initinal = request.GET.get("brand_initinal", "")
    except Exception as e:
        LOG.error('Can not get upkeep series info. when create upkeep series. %s') % e
    return render(request, 'background/upkeep/upkeepseries/new.html', {"upkeep_types": upkeep_types,
                                                                       "brands": brands,
                                                                       "brand_initinal": brand_initinal,
                                                                       "top_brands": top_brands,
                                                                       "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def new_series_data(request):
    try:
        all_data = {}
        s_data = []
        br_data = []
        brand_id = request.GET.get("brandId", None)
        seriesId = request.GET.get('seriesId', None)
        topbrand_id = request.GET.get('topbrand_id', None)
        year = request.GET.get("year", None)
        cc = request.GET.get("cc", None)
        brand_initinal = request.GET.get("brand_initinal", None)
        if brand_initinal and not brand_id:
            all_data = brand_data_by_initial(brand_initinal)
            return HttpResponse(simplejson.dumps(all_data))
        if topbrand_id:
            brand=Brand.objects.filter(parent_id=topbrand_id)
            for br in brand:
                b_param = {}
                b_param['br_id'] = br.id
                b_param['name'] = br.name
                br_data.append(b_param)
            series = Series.objects.filter(brand__id=brand[0].id)
            # series data
            if series.count() != 0:
                for s in series:
                    s_param = {}
                    s_param["s_id"] = s.id
                    s_param["name"] = s.name
                    s_data.append(s_param)

                series_data = car_params2(series[0])
                all_data['brand'] = br_data
                all_data['series'] = s_data
                all_data['series_one_param'] = series_data
            return HttpResponse(simplejson.dumps(all_data))
        if brand_id and not topbrand_id:
            series = Series.objects.filter(brand__id=brand_id)
            # series data
            if series.count() != 0:
                for s in series:
                    s_param = {}
                    s_param["s_id"] = s.id
                    s_param["name"] = s.name
                    s_data.append(s_param)

                series_data = car_params2(series[0])
                if br_data:
                    all_data['brand'] = br_data
                all_data['series'] = s_data
                all_data['series_one_param'] = series_data
            return HttpResponse(simplejson.dumps(all_data))
        if seriesId and not cc:
            series_obj = Series.objects.get(pk=seriesId)
            series_car_data = car_params2(series_obj)
            return HttpResponse(simplejson.dumps(series_car_data))

        if seriesId and cc:
            series_obj = Series.objects.get(pk=seriesId)
            years = UpkeepSeries.objects.get(series=series_obj, cc=cc)
            series_cc_data = years.produce_year.split(",")
            return HttpResponse(simplejson.dumps({"cc_data": series_cc_data}))
    except Exception as e:
        LOG.error('Can not get new series data. %s' % e)
    return HttpResponse(simplejson.dumps(all_data))


def brand_data_by_initial(ini):
    all_data = {}
    s_data = []
    try:
        # ini = request.GET['initial']
        bs = Brand.objects.filter(parent__isnull=False).filter(initial=ini)
        if bs:
            brand_list = []
            for b in bs:
                bp = {}
                bp['id'] = b.id
                bp['name'] = b.name
                brand_list.append(bp)
            brand_id = bs[0].id
            series = Series.objects.filter(brand__id=brand_id)
            # series data
            if series.count() != 0:
                for s in series:
                    s_param = {}
                    s_param["s_id"] = s.id
                    s_param["name"] = s.name
                    s_data.append(s_param)

                series_data = car_params2(series[0])

                all_data['series'] = s_data
                all_data['series_one_param'] = series_data
                all_data['brand_list'] = brand_list

    except Exception as e:
        LOG.error('Can not get brand data by initial. %s' % e)
    return all_data


def brand_by_initial(request):
    all_data = []
    try:
        ini = request.GET['brand_initial']
        all_data = brand_data_by_initial(ini)
    except Exception as e:
        LOG.error('Can not get brand data by initial. %s' % e)
    return HttpResponse(simplejson.dumps(all_data))

def car_params(series):
    c_param = {}

    try:
        cars_year = Car.objects.filter(series=series).values("produce_year").annotate(pcount=Count('produce_year'))
        year_data = []
        if cars_year.count() != 0:
            for c in cars_year:
                year_data.append(c['produce_year'])
            cc_data = car_cc(series, cars_year[0]['produce_year'])
            c_param["year_data"] = year_data
            c_param["cc_data"] = cc_data
    except Exception as e:
        LOG.error('Can not get car year and cc. %s' % e)
    return c_param


def car_params2(series):
    c_param = {}
    try:
        sump = UpkeepSeries.objects.filter(series=series).values("cc").annotate(pcount=Count("cc"))
        if sump:
            cc_data = []
            for s in sump:
                cc_data.append(s['cc'])
            years = UpkeepSeries.objects.filter(series=series, cc=cc_data[0]).values("produce_year__year").annotate(pcount=Count('produce_year__year'))
            c_param['year_data'] = [y['produce_year__year'] for y in years]
            c_param['cc_data'] = cc_data
        else:
            c_param['year_data'] = []
            c_param['cc_data'] = []
    except Exception as e:
        LOG.error('can not ge series upkeep data. %s' % e)
    return c_param


def car_cc(series, year):
    cc_data = []
    try:
        cars_cc = Car.objects.filter(series=series).filter(produce_year=year)

        for c_c in cars_cc:
            if c_c.cc.name in cc_data:
                continue
            else:
                cc_data.append(c_c.cc.name)
    except Exception, e:
        LOG.error('can not get series year car cc.%s' % e)
    return cc_data


def action_new(request):
    res = "success"
    try:
        series_id = request.POST["upSeries"]
        year = request.POST.getlist("upseriesYear")
        cc = request.POST["carCc"]

        series_obj = Series.objects.get(pk=series_id)
        upkeep_series = UpkeepSeries.objects.filter(series=series_obj, cc=cc, produce_year__year__in=year)
        if "upkeepModel" in request.POST:
            model_ids = request.POST.getlist("upkeepModel")
            for mid in model_ids:
                model_obj = UpkeepMaterialModel.objects.filter(pk=mid)
                if model_obj:
                    for y in upkeep_series:
                        SeriesUpkeepInfo.objects.create(upkeep_series=y, upkeep_material_model=model_obj[0], is_quote=False)

        if "banjinUpkeep" in request.POST:
            banjin_proj_ids = request.POST.getlist('banjinUpkeep')
            for banjin_proj_id in banjin_proj_ids:
                m_obj = Upkeep.objects.get(pk=banjin_proj_id)
                for y in upkeep_series:
                    SeriesUpkeepInfo.objects.create(upkeep_series=y, upkeep=m_obj, is_quote=False)
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create Upkeep. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    try:
        # "chars": [chr(i) for i in range(97, 97 + 26, 1)]
        upseries_id = request.GET.get('upseries_id', "")
        upobj = UpkeepSeries.objects.filter(pk=upseries_id)
        if upobj:
            series_obj = upobj[0].series
            initial = upobj[0].series.brand.initial
            cc = upobj[0].cc
            ups = UpkeepSeries.objects.filter(series=series_obj).filter(cc=cc)

            has_year = []
            for u in ups:
                has_year.append(u.produce_year.year if u.produce_year else "")
            years = CarYear.objects.all()
            year_list = []
            for y in years:
                yp = {}
                yp['year'] = y.year
                yp['id'] = y.id
                if y.year in has_year:
                    yp['is_checked'] = True
                else:
                    yp['is_checked'] = False
                year_list.append(yp)

    except Exception as e:
        LOG.error('Can not get upkeep series data. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/edit_manage.html',
                  {"years": year_list,
                   "cc": cc,
                   "upseries_id": upobj[0].id,
                   "series_name": upobj[0].series.name,
                   "series_id": upobj[0].series.id})


def action_edit(request):
    res = "success"
    try:
        upseries_id = request.POST.get("upseries_id", "")
        upobj = UpkeepSeries.objects.get(pk=upseries_id)
        UpkeepSeries.objects.filter(series=upobj.series).filter(cc=upobj.cc).delete()
        seriesId = request.POST['maSeries']
        series_obj = Series.objects.get(pk=seriesId)
        produce_year = request.POST.getlist('manageProduceYear')
        cc = request.POST['manageCC']
        for yid in produce_year:
            yobj = CarYear.objects.get(pk=yid)
            UpkeepSeries.objects.create(series=series_obj, produce_year=yobj, cc=cc)
    except Exception as e:
        res = "fail"
        LOG.error('Can not add new upkeep series manage obj')
    return HttpResponse(simplejson.dumps({"res": res}))


def ajax_data(request):
    upkeep_data = []
    type_id = request.GET.get("type_id", "")
    upkeeps = []
    try:
        if type_id != "":
            try:
                upkeep_type = UpkeepType.objects.get(pk=type_id)
                for upkeep_package in upkeep_type.upkeeppackage_set.all():
                    __pkg = upkeep_package.upkeep_set.all()
                    for __up in __pkg:
                        upkeeps.append(__up)
                upkeeps = list(set(upkeeps))
            except Exception as e:
                pass
        else:
            upkeeps = Upkeep.objects.all()
        for upkeep in upkeeps:
            params = {}
            params["name"] = upkeep.name
            params["id"] = upkeep.id
            upkeep_data.append(params)
    except Exception as e:
        LOG.error('Can not get upkeep. %s' % e)
    return HttpResponse(simplejson.dumps(upkeep_data))


def upkeep_series_detail(request):
    up_series_params = {}
    try:
        upkeep_series_id = request.GET['up_series_id']
        up_series_obj = SeriesUpkeepInfo.objects.get(pk=upkeep_series_id)
        up_series_params['series_name'] = up_series_obj.series.name
        up_series_params['cc'] = up_series_obj.cc
        up_series_params['produce_year'] = up_series_obj.produce_year

        upkeep_data = up_series_obj.upkeep
        upkeep_material_data = up_series_obj.upkeep_material
        up_series_params['upkeep_info'] = ""
        up_series_params['upkeep_material_info'] = ""
        if upkeep_data:
            up_series_params['upkeep_info'] = upkeep_data
        if upkeep_material_data:
            up_series_params['upkeep_material_info'] = upkeep_material_data

    except Exception as e:
        LOG.error('Can not get producer detail. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/detail.html', {"usp": up_series_params})


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                SeriesUpkeepInfo.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def manage_action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                obj = UpkeepSeries.objects.filter(pk=id_v)
                if obj:
                    UpkeepSeries.objects.filter(series=obj[0].series, cc=obj[0].cc).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def upkeep_series_index(request):
    initial = request.GET.get("initial", "all")
    return render(request, 'background/upkeep/upkeepseries/manage_index_new.html', {"chars": [chr(i) for i in range(97, 97 + 26, 1)], "initial": initial})


@data_table(columns=SERIES_UP_MANAGE_COLUMNS, model=UpkeepSeries)
def upkeep_series_data(request):
    st_list = []
    order_cl = request.table_data["order_cl"]
    try:
        sts = UpkeepSeries.objects.order_by(order_cl).values("series", "cc").filter(
             Q(series__name__contains=request.table_data["key_word"]) |
             Q(series__brand__name__contains=request.table_data["key_word"])).annotate(Count("produce_year"))
        for up in sts:
            st_params = {}
            series_data = UpkeepSeries.objects.filter(series__id=up['series']).filter(cc=up['cc'])
            if series_data:
                st_params['DT_RowId'] = series_data[0].id
                st_params['series'] = series_data[0].series.name
                st_params['brand'] = series_data[0].series.brand.name
                st_params['cc'] = series_data[0].cc
            year_list = []
            for s in series_data:
                year_list.append(s.produce_year.year if s.produce_year else 0)
            st_params['produce_year'] = str(min(year_list)) + "--" + str(max(year_list))
            st_params['brand_level'] = series_data[0].series.brand.level.name
            if series_data[0].series.brand.country:
                st_params['brand_country'] = series_data[0].series.brand.country.name
            else:
                st_params['brand_country'] = ""
            st_list.append(st_params)
    except Exception as e:
        LOG.error('Can not get upkeep series manage data. %s' % e)
    return st_list[request.table_data["start"]: request.table_data["end"]], len(st_list)


@data_table(columns=UP_SERIES_COLUMNS, model=Series)
def upkeep_series_data_new(request, filter_msg):
    st_list = []
    try:
        if filter_msg == "" or filter_msg == "all":
            series_data = Series.objects.filter(
                Q(name__contains=request.table_data["key_word"])
                    ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
        else:
            series_data = Series.objects.filter(
                Q(name__contains=request.table_data["key_word"])
                    ).filter(brand__parent__initial=filter_msg).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
        total_num = Series.objects.all().count()
        for s in series_data:
            sp = {}
            sp['DT_RowId'] = s.id
            sp['first_brand'] = ""
            if s.brand.parent:
                sp['first_brand'] = s.brand.parent.name
            sp['second_brand'] = s.brand.name
            # print s.brand.parent
            sp['initial'] = s.brand.parent.initial
            sp['series_name'] = s.name
            if s.brand.country:
                sp['country'] = s.brand.country.name
            else:
                sp['country'] = ""
            if s.brand.level:
                sp['level'] = s.brand.level.name
            else:
                sp['level'] = ''
            st_list.append(sp)
    except Exception as e:
        LOG.error('Can not get upkeep series manage data. %s' % e)
    return st_list, total_num



def upseries_has_quote_pkg(request):
    try:
        upseries_id = request.GET['upseries_id']
        upseries= UpkeepSeries.objects.get(pk=upseries_id)
        pkg_quotes = SeriesUpkeepQuote.objects.filter(series_upkeep_info__upkeep_series__id=upseries_id).\
                                            values("package__id", "package__name",
                                             "package__type__id").annotate(pcount=Count("package__id"))
    except Exception as e:
        LOG.error('Can not get pkg quote info. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/quote_pkg.html',
                  {"quote_type": "pkgs", "quotes": pkg_quotes, "upseries_id": upseries_id, "upseries": upseries})


def upseries_pkg_quote_producer(request):
    try:
        pkg_id = request.GET['pkg_id']
        upseries_id = request.GET['upseries_id']
        upseries = UpkeepSeries.objects.get(pk=upseries_id)
        pkgobj = UpkeepPackage.objects.get(pk=pkg_id)
        producers_info = SeriesUpkeepQuote.objects.filter(package__id=pkg_id).\
            filter(series_upkeep_info__upkeep_series_id=upseries_id).values("producer__id", "producer__username").\
            annotate(pcount=Count("producer__id"))

    except Exception as e:
        LOG.error('Can not get pkg quote producer. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/quote_pkg.html',
                  {"quote_type": "pkgs_producer", "producers": producers_info,
                   "pkg_id": pkg_id, "upseries_id": upseries_id, "pkobj": pkgobj, "upseries": upseries})


def get_upquote_by_producer(request):
    all_data = []
    try:
        producer_id = request.GET['producer_id']
        pkg_id = request.GET['pkg_id']
        pkobj = UpkeepPackage.objects.get(pk=pkg_id)
        upseries_id = request.GET['upseries_id']
        upseries = UpkeepSeries.objects.get(pk=upseries_id)
        producer_obj = Producer.objects.get(pk=producer_id)
        pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
        all_data = pkg_upkeep_quote_detail(producer_id, pkg_id, upseries_id)
    except Exception as e:
        LOG.error('Can not get producer quote for upkeep. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/quote_pkg.html',
                  {"quote_type": "detail", "all_data": all_data,
                   "probj": producer_obj, "pkg_obj": pkg_obj,
                   "producer_name": producer_obj.username,
                   "pkobj": pkobj,
                   "upseries": upseries})


def upkeep_series_new(request):
    series_id = request.GET.get("series_id", "")
    years = CarYear.objects.all()
    series = {"name": ""}
    if series_id:
        series = Series.objects.get(pk=series_id)
    return render(request, "background/yearcc/new.html",
                  {"years": years, "series": series})


def new_upkeepseries_data(request, ):
    try:
        all_data = {}
        s_data = []
        topbrandId = request.GET.get('topbrandId', None)
        brand_id = request.GET.get("brandId", None)
#        brand_initial = Brand.objects.values_list('initial').filter(id=topbrandId)[0][0]
        if not brand_id:
            bs = Brand.objects.filter(parent__isnull=False).filter(parent_id=topbrandId)
            if bs:
                brand_list = []
                for b in bs:
                    bp = {}
                    bp['id'] = b.id
                    bp['name'] = b.name
                    brand_list.append(bp)
                brand_id = bs[0].id
                series = Series.objects.filter(brand__id=brand_id)
                # series data
                if series.count() != 0:
                    for s in series:
                        s_param = {}
                        s_param["s_id"] = s.id
                        s_param["name"] = s.name
                        s_data.append(s_param)

                    series_data = car_params2(series[0])

                    all_data['series'] = s_data
                    all_data['series_one_param'] = series_data
                    all_data['brand_list'] = brand_list
                return HttpResponse(simplejson.dumps(all_data))
        if brand_id:
            print brand_id
            series = Series.objects.filter(brand__id=brand_id)
            # series data
            if series.count() != 0:
                for s in series:
                    s_param = {}
                    s_param["s_id"] = s.id
                    s_param["name"] = s.name
                    s_data.append(s_param)

                series_data = car_params2(series[0])

                all_data['series'] = s_data
                all_data['series_one_param'] = series_data
            return HttpResponse(simplejson.dumps(all_data))
    except Exception as e:
        LOG.error('Can not get new series data. %s' % e)
    return HttpResponse(simplejson.dumps(all_data))


def upkeep_series_action(request):
    res = "success"
    try:
        seriesId = request.POST['maSeries']
        series_obj = Series.objects.get(pk=seriesId)
        produce_year = request.POST.getlist('manageProduceYear')
        cc = request.POST['manageCC']
        for yid in produce_year:
            yobj = CarYear.objects.get(pk=yid)
            UpkeepSeries.objects.create(series=series_obj, produce_year=yobj, cc=cc)
    except Exception as e:
        res = "fail"
        LOG.error('Can not add new upkeep series manage obj')
    return HttpResponse(simplejson.dumps({"res": res}))


def year_index(request):
    return render(request, 'background/upkeep/upkeepseries/year_index.html')


@data_table(columns=['year'], model=CarYear)
def year_data(request):
    st_list = []
    try:
        sts = CarYear.objects.filter(year__contains=request.table_data["key_word"]).\
            order_by(request.table_data["order_cl"])[request.table_data['start']:request.table_data["end"]]
        for st in sts:
            st_params = {}
            st_params['DT_RowId'] = st.id
            st_params['year'] = st.year
            # st_params['created_at'] = st.created_at
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_list.append(st_params)
    except Exception as e:
        LOG.error('Error happened when get year data. %s' % e)
    return st_list


def year_new(request):
    return render(request, 'background/upkeep/upkeepseries/new_year.html')


def year_new_action(request):
    res = "success"
    try:
        year = request.POST['car_year']
        CarYear.objects.create(year=year)
    except Exception as e:
        res = "fail"
        LOG.error('Can not create new year item. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def year_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                CarYear.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeepseries year. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))

def ajax_second_brand_manage(request):
    brand_data = []
    series_list = []
    top_brands = []
    series = []
    try:
        initial = request.GET.get("initial", "")
        if initial:
            top_brands_data = Brand.objects.filter(parent__isnull=True, initial=initial)
            if top_brands_data:
                second_brand_data = Brand.objects.filter(parent__id=top_brands_data[0].id)
                if second_brand_data:
                    series = second_brand_data[0].series_set.all()
            for t in top_brands_data:
                tp = {}
                tp['id'] = t.id
                tp['name'] = t.name
                top_brands.append(tp)
            if second_brand_data:
                for b in second_brand_data:
                    bp = {}
                    bp['id'] = b.id
                    bp['name'] = b.name
                    brand_data.append(bp)
            if series:
                for s in series:
                    sp = {}
                    sp['id'] = s.id
                    sp['name'] = s.name
                    series_list.append(sp)
            all_data = {"top_brands": top_brands, "brand_data": brand_data,
                        "series_data": series_list}
            return HttpResponse(simplejson.dumps(all_data))
    except Exception as e:
        LOG.error('Can not get brand car by ajax. %s' % e)
    return HttpResponse(simplejson.dumps(brand_data))

