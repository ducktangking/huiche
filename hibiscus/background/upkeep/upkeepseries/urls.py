from django.conf.urls import url
from hibiscus.background.upkeep.upkeepseries import views

urlpatterns = [
    url(r'^$', views.index_new, name="background-upkeepseries-index"),
    url(r'^material/bind/series/$', views.show_material_info, name="background-upkeepseries-show-material-info"),
    url('^series/match/$', views.series_match, name='background-upkeepseries-series-match'),
    url('^bind/action/$', views.bind_action, name='background-upkeepseries-bind-action'),
    # url(r'^data/$', views.data, name="background-upkeepseries-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeepseries-action-delete"),
    url(r'^new/$', views.new, name="background-upkeepseries-new"),
    url(r'^new/data/$', views.new_series_data, name="background-upkeepseries-new-series-data"),
    url(r'^new/action/$', views.action_new, name="background-upkeepseries-action-new"),
    url(r'^edit/$', views.edit, name="background-upkeepseries-edit"),
    url(r'^edit/action/$', views.action_edit, name="background-upkeepseries-action-edit"),

    # data
    url(r'^upkeeps/$', views.ajax_data, name="background-upkeep-upkeepseries-ajax-data"),
    url(r'^detail/$', views.upkeep_series_detail, name="background-upkeep-upkeepseries-detail"),
    url(r'^delete/action/manage/$', views.manage_action_delete, name="background-upkeep-upkeepseries-action-delete"),
    url(r'^manage/$', views.upkeep_series_index, name="background-upkeep-upkeepseries-manage"),
    url(r'^manage/data/(\w+)/$', views.upkeep_series_data_new, name="background-upkeep-upkeepseries-data"),
    url(r'^manage/new/$', views.upkeep_series_new, name="background-upkeep-upkeepseries-manage-new"),
    url(r'^manage/new/data$', views.new_upkeepseries_data, name="background-upkeep-upkeepseries-manage-new-data"),
    url(r'^mange/new/action/$', views.upkeep_series_action, name="background-upkeepseries-manage-action-new"),
    url(r'^quote/pkg/$', views.upseries_has_quote_pkg, name="background-upkeepseries-quote-pkg"),
    url(r'^quote/pkg/producer/$', views.upseries_pkg_quote_producer, name="background-upkeepseries-quote-pkg-producer"),
    url(r'^quote/detail/by/producer/$', views.get_upquote_by_producer, name="background-upkeepseries-quote-detail-by-producer"),
    url(r'^year/$', views.year_index, name='background-upkeepseries-year-index'),
    url(r'^year/data/$', views.year_data, name='background-upkeepseries-year-data'),
    url(r'^year/new/$', views.year_new, name='background-upkeepseries-year-new'),
    url(r'^year/new/action/$', views.year_new_action, name='background-upkeepseries-year-new-action'),
    url(r'^year/delete/$', views.year_delete, name='background-upkeepseries-year-delete'),
    url(r'^initial/$', views.brand_by_initial, name='background-brand-list-by-initial'),
    url(r'^ajax/second/brand/$', views.ajax_second_brand_manage, name="manage-second-brand-data"),

]
