# -*- coding: utf-8 -*-
import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.db.models import Q, Count
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.utils import timezone

from hibiscus.models import UpkeepSeries
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
SERIES_UP_MANAGE_COLUMNS = ["series__name", "cc", "produce_year"]


def index(request):
    series_id = request.GET.get("series_id", "")
    return render(request, 'background/yearcc/index.html', {"series_id": series_id})


@data_table(columns=SERIES_UP_MANAGE_COLUMNS, model=UpkeepSeries)
def data(request):
    st_list = []
    order_cl = request.table_data["order_cl"]
    series_id = request.GET.get("series_id", "")
    try:
        if series_id:
            sts = UpkeepSeries.objects.order_by(order_cl).values("series", "cc").filter(series__id=series_id).filter(
                 Q(series__name__contains=request.table_data["key_word"]) |
                 Q(series__brand__name__contains=request.table_data["key_word"])).annotate(Count("produce_year"))
            for up in sts:
                st_params = {}
                series_data = UpkeepSeries.objects.filter(series__id=up['series']).filter(cc=up['cc'])
                if series_data:
                    st_params['DT_RowId'] = series_data[0].id
                    st_params['name'] = series_data[0].series.name
                    st_params['cc'] = series_data[0].cc
                year_list = []
                for s in series_data:
                    year_list.append(s.produce_year.year if s.produce_year else 0)
                st_params['produce_year'] = str(min(year_list)) + "--" + str(max(year_list))
                st_list.append(st_params)
            return st_list[request.table_data["start"]: request.table_data["end"]], len(st_list)
    except Exception as e:
        LOG.error('Can not get upkeep series manage data. %s' % e)
    return st_list, 0
