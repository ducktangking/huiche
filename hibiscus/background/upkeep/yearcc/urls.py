from django.conf.urls import url, include
from hibiscus.background.upkeep.yearcc import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-yearcc-index"),
    url(r'^data/$', views.data, name="background-upkeep-yearcc-data"),
]
