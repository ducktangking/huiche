from django.conf.urls import url

from hibiscus.background.upkeep.oil import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-oil-index"),
    url(r'^data/$', views.data, name="background-upkeep-oil-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeep-oil-action-delete"),
    url(r'^new/$', views.new, name="background-upkeep-oil-new"),
    url(r'^new/action/$', views.action_new, name="background-upkeep-oil-action-new"),

]