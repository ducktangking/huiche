import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Oil
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
OIL_COLUMNS = ["cc", "capacity", "created_at"]


def index(request):
    return render(request, 'background/upkeep/oil/index.html')


@data_table(columns=OIL_COLUMNS, model=Oil)
def data(request):
    try:
        sts = Oil.objects.filter(
            Q(cc__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["cc"] = st.cc
            st_params["capacity"] = st.capacity
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart oil.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Oil.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete oil. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/upkeep/oil/new.html')


def action_new(request):
    res = "success"
    try:
        cc = request.POST["cc"]
        capacity = request.POST["capacity"]
        Oil(cc=cc, capacity=capacity).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create oil. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


