# -*-coding: utf8-*-
import simplejson
import logging
import random
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q, Count
from django.contrib import messages
from django.utils.translation import ugettext as _

from hibiscus.models import UpkeepPackage, SeriesUpkeepInfo, Upkeep, SupportedArea, Picture, UpkeepMaterial, \
    UpkeepMaterialModel, UpkeepMaterialLevel, Brand, Series, BrandUpkeepArea
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
UPKEEP_MATERIAL_COLUMNS = ["upkeep__name", "brand"]

MATERIAL_AREA_COLUMNS = ["brand", "code", "model", "price"]
UPKEEP_MATERIAL_MODEL_COLUMNS = ["model", "name", "model", "time_labor_price", "material_price", "type", "code"]

MODEL_SPECIAL_LEVEL = '5'
MODEL_BRAND_LEVE = '6'


def index(request):
    type_id = request.GET["type_id"] if "type_id" in request.GET else ""
    pkg_id = request.GET["pkg_id"] if "pkg_id" in request.GET else ""
    up_id = request.GET["up_id"] if "up_id" in request.GET else ""
    upkeep_pkg = None
    upkeep = None
    try:
        if pkg_id != "":
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
    except Exception as e:
        upkeep_pkg = None
        upkeep = None
        messages.add_message(request, messages.ERROR, _("Upkeep or Upkeep Package is not exist!"))
        LOG.debug("Get Upkeep Package or Upkeep error, error is %s", e)

    return render(request, 'background/upkeep/material/index.html', {"up_id": up_id, "type_id": type_id, "pkg_id": pkg_id,})


@data_table(columns=UPKEEP_MATERIAL_COLUMNS, model=UpkeepMaterial)
def data(request):
    try:
        up_id = request.GET["up_id"] if "up_id" in request.GET else ""
        if up_id != "all" and up_id != '':
            sts = UpkeepMaterial.objects.filter(
                Q(upkeep__id=up_id), Q(brand__contains=request.table_data["key_word"])). \
                order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepMaterial.objects.filter(upkeep__id=up_id).count()
        else:
            sts = UpkeepMaterial.objects.filter(
                Q(brand__contains=request.table_data["key_word"])). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepMaterial.objects.all().count()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["brand"] = st.brand
            st_params["upkeep"] = st.upkeep.name
            st_params['pic'] = st.picture.path
            # st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list, total_num
    except Exception as e:
        LOG.error('Can not get cart upkeep package data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                pkg = UpkeepMaterial.objects.filter(pk=id_v)
                pkg.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep material. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def random_code():
    code = random.randint(10000, 99999)
    has_exist = UpkeepMaterial.objects.filter(code=code).count()
    if has_exist > 0:
        return random_code()
    else:
        return code


def new(request):
    type_id = request.GET["type_id"] if "type_id" in request.GET else ""
    pkg_id = request.GET["pkg_id"] if "pkg_id" in request.GET else ""
    up_id = request.GET["up_id"] if "up_id" in request.GET else ""
    upkeep_pkg = None
    upkeep = None
    try:
        if pkg_id != "":
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
        if up_id != "all" and up_id !='':
            upkeep = Upkeep.objects.get(id=up_id)
    except Exception as e:
        upkeep_pkg = None
        upkeep = None
        messages.add_message(request, messages.ERROR, _("Upkeep or Upkeep Package is not exist!"))
        LOG.debug("Get Upkeep Package or Upkeep error, error is %s", e)
    return render(request, 'background/upkeep/material/new.html', {"up_id": up_id, "type_id": type_id, "pkg_id": pkg_id,
                                                                     "upkeep_pkg": upkeep_pkg, "upkeep": upkeep
                                                                   })


def action_new(request):
    res = "fail"
    try:
        up_id = request.POST.get("upkeep", "")
        if up_id != "":

            # code = request.POST.get("code")
            up_obj = Upkeep.objects.get(pk=up_id)
            # area_ids = request.POST.getlist("area")
            # areas = SupportedArea.objects.filter(id__in=area_ids)

            # model = request.POST.get("modal", "")
            brand = request.POST.get("materialBrand", "")
            # name = request.POST.get("name", "")
            # content = request.POST.get("content", "")
            # price = request.POST.get("price", 0)
            # price_min = request.POST.get("priceMin", 0)
            # price_max = request.POST.get("priceMax", 0)
            # ma_type = request.POST.get("materialT", UpkeepMaterial.TYPE_CHOICES[0][0])
            materialPic = request.FILES["materialPic"]
            img_path = deal_with_upload_file(materialPic)
            p_obj = Picture.objects.create(name=materialPic.name, path=img_path)
            m = UpkeepMaterial.objects.create(upkeep=up_obj, picture=p_obj, brand=brand)
            # for a in areas:
            #     m.area.add(a)
            res = "success"
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create upkeey package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    material_id = request.GET.get("material_id", "")
    if material_id != "":
        upkeep_material = None
        upkeep_list = []
        upkeep_package = None
        try:
            upkeep_material = UpkeepMaterial.objects.get(id=material_id)
            upkeeps = Upkeep.objects.all().filter(package__type__id=1)
            for u in upkeeps:
                up = {}
                up['id'] = u.id
                up['name'] = u.name
                if u == upkeep_material.upkeep:
                    up['is_checked'] = True
                else:
                    up['is_checked'] = False
                if up in upkeep_list:
                    continue
                upkeep_list.append(up)
        except Exception as e:
            LOG.error("Material edit, get upkeep material error! error is %s", e)
        return render(request, "background/upkeep/material/edit.html",
                      {"upkeep_material": upkeep_material, "upkeep_list":upkeep_list})


def action_edit(request):
    res = "fail"
    try:
        material_id = request.POST.get("materialId", "")
        if material_id != "":
            material_obj = UpkeepMaterial.objects.get(pk=material_id)
            material_obj.upkeep_id = request.POST.get('upkeep', "")
            material_obj.brand = request.POST.get("materialBrand", "")
            materialPic = request.FILES.get("materialPic", None)
            if materialPic:
                img_path = deal_with_upload_file(materialPic)
                p_obj = Picture.objects.create(name=materialPic.name, path=img_path)
                material_obj.picture = p_obj
            material_obj.save()
            res = "success"
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create upkeey package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def ajax_data(request):
    all_data = []
    try:
        type_id = request.GET.get("type_id", None)
        upkeeps = Upkeep.objects.all()
        for u in upkeeps:
            up_params = {}
            __material = u.upkeepmaterial_set.all().values("brand").annotate(Count("brand", distinct=True))
            if __material and int(type_id) == 1:
                material_data = []
                for _m in __material:
                    m_params = {}
                    m_params['m_brand'] = _m['brand']
                    # get one brand's model
                    ms = UpkeepMaterial.objects.filter(brand=_m['brand'])
                    ms_model = []
                    for m in ms:
                        for mo in m.upkeepmaterialmodel_set.all():
                            mop = {}
                            mop['model_name'] = mo.model
                            mop['model_id'] = mo.id
                            ms_model.append(mop)
                    m_params['m_model'] = ms_model
                    material_data.append(m_params)
                up_params['m_data'] = material_data
                up_params['up_id'] = u.id
                up_params['up_name'] = u.name
                all_data.append(up_params)
            if not __material and int(type_id) == 2:
                up_params['up_id'] = u.id
                up_params['up_name'] = u.name
                all_data.append(up_params)
    except Exception as e:
        LOG.error('Can not get test data. %s' % e)
    return render(request, 'background/upkeep/upkeepseries/bind_info.html',
                  {"all_data": all_data, "pkg_type_id": type_id})


def bind_area(request):

    return render(request, 'background/upkeep/material/bind-area.html')


AREA_COLUMNS = ["province", "city"]


@data_table(columns=AREA_COLUMNS, model=SupportedArea)
def area_data(request):
    try:
        sts = SupportedArea.objects.filter(
            Q(province__contains=request.table_data["key_word"]) |
            Q(city__contains=request.table_data["key_word"])).\
            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["province"] = st.province
            if st.province == u'-1':
                continue
            st_params["city"] = st.city
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception, e:
        LOG.error('Get area index data error. %s' % e)


@data_table(columns=MATERIAL_AREA_COLUMNS, model=UpkeepMaterial)
def material_by_area(request):
    material_data = []
    is_default = False
    try:
        province = request.GET.get("province", '-1')
        city = request.GET.get("city", '-1')
        if province == '-1' or city == '-1':
            area_obj = SupportedArea.objects.filter(province=province, city=city)
            ms = UpkeepMaterial.objects.filter( Q(brand__contains=request.table_data["key_word"]) |
                   Q(code__contains=request.table_data['key_word']) |
                   Q(model__contains=request.table_data['key_word']) |
                   Q(price__contains=request.table_data['key_word'])).order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        else:
            area_obj = SupportedArea.objects.filter(province=province, city=city)
            if area_obj:
                ms = UpkeepMaterial.objects.filter(Q(area=area_obj[0]),
                       Q(brand__contains=request.table_data["key_word"]) |
                       Q(code__contains=request.table_data['key_word']) |
                       Q(model__contains=request.table_data['key_word']) |
                       Q(price__contains=request.table_data['key_word'])). \
                        order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        for m in ms:
            m_params = {}
            m_params['DT_RowId'] = m.id
            m_params['brand'] = m.brand
            m_params['code'] = m.code
            m_params['model'] = m.model
            m_params['price'] = m.price
            if m.default_area:
                if m.default_area.province == "-1" or m.default_area.city == "-1":
                    m_params['default_area'] = "All Areas"
                else:
                    m_params['default_area'] = m.default_area.province + "/" + m.default_area.city
            else:
                m_params['default_area'] = None
            if area_obj:
                if m.default_area == area_obj[0]:
                    is_default = True
            m_params['is_default'] = is_default
            material_data.append(m_params)
    except Exception as e:
        LOG.error('Can not get material by area. %s' % e)
    return material_data


def ajax_material_area(request):
    all_data = []
    is_default = False
    try:
        province = request.GET["province"]
        city = request.GET["city"]
        if province == '-1' or city == '-1':
            ms = UpkeepMaterial.objects.all()
            area_obj = SupportedArea.objects.filter(Q(province="-1") | Q(city="-1"))
        else:
            area_obj = SupportedArea.objects.filter(province=province, city=city)
            if area_obj:
                ms = UpkeepMaterial.objects.filter(area=area_obj[0])


        for m in ms:
            m_params = {}
            m_params['DT_RowId'] = m.id
            m_params['brand'] = m.brand
            m_params['code'] = m.code
            m_params['model'] = m.model
            m_params['price'] = m.price
            if m.default_area:
                m_params['default_area'] = m.default_area.province + "/" + m.default_area.city
            else:
                m_params['default_area'] = None
            if m.default_area == area_obj[0]:
                is_default = True
            m_params['is_default'] = is_default

            all_data.append(m_params)
    except Exception as e:
        LOG.error('Can not get area material from ajax. %s' % e)
    return HttpResponse(simplejson.dumps(all_data))


def set_default_area(request):
    res = "success"
    try:
        province = request.GET.get("province", None)
        city = request.GET.get("city", None)
        materialId = request.GET.get("materialId", None)

        area_obj = SupportedArea.objects.filter(province=province, city=city)
        if area_obj:
            material_obj = UpkeepMaterial.objects.filter(pk=materialId)
            material_obj.update(default_area=area_obj[0])
    except Exception as e:
        res = 'fail'
        LOG.error('Can not set material default area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def model_index(request):
    material_id = request.GET.get('material_id', '')
    if material_id:
        material = UpkeepMaterial.objects.filter(pk=material_id)
        upkeep = Upkeep.objects.filter(pk=material[0].upkeep_id)
        return render(request, 'background/upkeep/material/model_index.html', {"material_id": material_id, 'brand':material[0].brand, 'upkeep':upkeep[0].name})
    else:
        return render(request, 'background/upkeep/material/model_index.html', {"material_id": material_id})


@data_table(columns=UPKEEP_MATERIAL_MODEL_COLUMNS, model=UpkeepMaterialModel)
def model_data(request):
    sts = []
    try:
        mId = request.GET.get('material_id', "")
        if mId:
            mobj = UpkeepMaterial.objects.get(pk=mId)
            sts = mobj.upkeepmaterialmodel_set.all().filter(Q(code__contains=request.table_data["key_word"]) |
                              Q(model__contains=request.table_data['key_word'])).\
                            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        else:
            sts = UpkeepMaterialModel.objects.filter(Q(code__contains=request.table_data["key_word"]) |
                              Q(model__contains=request.table_data['key_word'])).\
                            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for s in sts:
            s_params = {}
            s_params['DT_RowId'] = s.id
            s_params['pic_path'] = s.picture.path
            s_params['name'] = s.name
            s_params['code'] = s.code
            s_params['model'] = s.model
            s_params['type'] = s.type
            s_params['time_labor_price'] = s.time_labor_price
            s_params['material_price'] = s.material_price
            st_list.append(s_params)
    except Exception as e:
        LOG.error('Can not get model data. %s' % e)
    return st_list, len(sts)


def model_new(request):
    material_id = request.GET['material_id']
    material_levels = UpkeepMaterialLevel.objects.all()
    topbrand = Brand.objects.filter(parent__isnull=True, initial='a')
    secondbrand = []
    if topbrand:
        secondbrand = Brand.objects.filter(parent__id=topbrand[0].id)
    return render(request, 'background/upkeep/material/model_new.html',
                  {"material_id": material_id,
                   "levels": material_levels,
                   "topbrand": topbrand,
                   "secondbrand": secondbrand,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def model_new_action(request):
    res = "success"
    try:
        material_id = request.POST.get("materialId", "")
        if material_id != "":
            mobj = UpkeepMaterial.objects.get(pk=material_id)
            levels = request.POST.getlist('avaLevel')
            level_objs = UpkeepMaterialLevel.objects.filter(pk__in=levels)

            code = request.POST.get("code")
            # area_ids = request.POST.getlist("area")
            #area default to all the country
            areas = SupportedArea.objects.filter(id__in=[2, ])

            model = request.POST.get("modal", "")
            # brand = request.POST.get("materialBrand", "")
            name = request.POST.get("name", "")
            content = request.POST.get("content", "")
            # price = request.POST.get("price", 0)
            # original_price = request.POST.get("origPrice", 0)
            # gen_price = request.POST.get("genPrice", 0)
            # price_min = request.POST.get("priceMin", 0)
            # price_max = request.POST.get("priceMax", 0)

            time_labor_price = request.POST.get('time_labor_price', 0)
            time_labor_min_price = request.POST.get('time_labor_min_price', 0)
            time_labor_max_price = request.POST.get('time_labor_max_price', 0)
            material_price = request.POST.get('material_price', 0)
            material_min_price = request.POST.get('material_min_price', 0)
            material_max_price = request.POST.get('material_max_price', 0)
            ma_type = request.POST.get("materialT", UpkeepMaterialModel.TYPE_CHOICES[0][0])
            materialPic = request.FILES["materialModelPic"]
            img_path = deal_with_upload_file(materialPic)
            p_obj = Picture.objects.create(name=materialPic.name, path=img_path)

            m = UpkeepMaterialModel.objects.create(picture=p_obj, material=mobj, code=code, model=model, name=name,
                                                   content=content, type=ma_type, time_labor_price=time_labor_price, time_labor_min_price=time_labor_min_price,
                                                   time_labor_max_price=time_labor_max_price, material_price=material_price, material_min_price=material_min_price,
                                                   material_max_price=material_max_price)
            for a in areas:
                m.area.add(a)
            for l in level_objs:
                m.level.add(l)
            m.save()

            all_series = False
            if '1' in levels:
                all_series = True
            series = ""
            if MODEL_SPECIAL_LEVEL in levels:
                top_brand_id = request.POST.get("upseriesTopBrand", "")
                second_brand_id = request.POST.get("upSeriesBrand", "")
                is_special = request.POST.get("upSeries", "")
                if top_brand_id != '-1' and second_brand_id != '-1' and is_special != '-1':
                    series_ids_list = request.POST["modelSeriesList"]
                    series_ids = series_ids_list.split(',')[:-1]
                    if series_ids:
                        series = Series.objects.filter(pk__in=series_ids)
            if MODEL_BRAND_LEVE in levels:
                brandIds = request.POST.getlist("bindBrands")
                secondBrands = Brand.objects.filter(id__in=brandIds)
                all_series = True
            upseries_info = SeriesUpkeepInfo.objects.create(upkeep_material_model=m, is_quote=False)
            upseries_info.all_series = all_series
            if series:
                for s in series:
                    upseries_info.series.add(s)
            if secondBrands:
                for sb in secondBrands:
                    upseries_info.brands.add(sb)
            upseries_info.save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not create new model. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def model_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                pkg = UpkeepMaterialModel.objects.filter(pk=id_v)
                pkg.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep material. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def model_edit(request):
    model_id = request.GET.get("model_id", "")
    material_id = request.GET.get("material_id", "")
    model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
    levels = UpkeepMaterialLevel.objects.all()
    l_list = []
    # 判断 is_chosen_series:是否打开隐藏页面 is_topbrand:二级品牌是否为全部 is_all_and_chosen:车型是否为全部
    # is_chosen_brand 是否为绑定品牌
    is_chosen = ''
    is_chosen_series = ''
    is_topbrand = ''
    is_chosen_brand = ''
    is_all_and_chosen = ''
    topbrand_list = []
    secbrand_list = []
    left_series_list = []
    right_series_list = []
    data = ''
    chars = [chr(i) for i in range(97, 97 + 26, 1)]
    for l in levels:
        lp = {}
        lp['id'] = l.id
        lp['name'] = l.name
        if l in model_obj.level.all():
            lp['is_checked'] = True
            if l.name == u'指定车型':
                is_chosen = True
            elif l.name == u'指定品牌':
                is_chosen_brand = True
        else:
            lp['is_checked'] = False
        l_list.append(lp)
    topbrand = Brand.objects.filter(parent__isnull=True)
    for top in topbrand:
        top_params = {}
        top_params['id'] = top.id
        top_params['name'] = top.name
        topbrand_list.append(top_params)
    secbrand = Brand.objects.filter(parent__isnull=False).filter(parent_id=topbrand_list[0]['id'])
    for sec in secbrand:
        sec_params = {}
        sec_params['id'] = sec.id
        sec_params['name'] = sec.name
        secbrand_list.append(sec_params)
    if is_chosen:
        info_obj = SeriesUpkeepInfo.objects.filter(upkeep_material_model_id=model_obj.id)
        if info_obj:
            a = info_obj[0]
            is_chosen_series = True
            all_series = ''
            if info_obj[0].all_series:
                all_series = all_series
                l_list = []
                for l in levels:
                    lp = {}
                    lp['id'] = l.id
                    lp['name'] = l.name
                    if l.name == u'全部':
                        lp['is_checked'] = True
                    else:
                        lp['is_checked'] = False
                    l_list.append(lp)
                return render(request, 'background/upkeep/material/model_edit.html', locals())
            elif info_obj[0].series.all():
                is_all_and_chosen = True
                right_series_list = []
                series_id_list = []
                data = ''
                for s in info_obj[0].series.all():
                    s_params = {}
                    s_params['id'] = s.id
                    s_params['name'] = s.name
                    right_series_list.append(s_params)
                    series_id_list.append(s.id)
                    data += str(s.id)+','
                the_series = Series.objects.get(pk=series_id_list[0])
                topbrand = Brand.objects.filter(parent__isnull=True)
                topbrand_list = []
                for top in topbrand:
                    top_params = {}
                    top_params['id'] = top.id
                    top_params['name'] = top.name
                    if top.id == the_series.brand.parent_id:
                        top_params['is_checked'] = True
                    else:
                        top_params['is_checked'] = False
                    topbrand_list.append(top_params)
                secbrand = Brand.objects.filter(parent__isnull=False).filter(parent_id=the_series.brand.parent_id)
                secbrand_list = []
                for sec in secbrand:
                    sec_params = {}
                    sec_params['id'] = sec.id
                    sec_params['name'] = sec.name
                    if sec.id == the_series.brand_id:
                        sec_params['is_checked'] = True
                    else:
                        sec_params['is_checked'] = False
                    secbrand_list.append(sec_params)
                left_series_list = []
                left_series = Series.objects.filter(brand=the_series.brand)
                for left in left_series:
                    left_params = {}
                    left_params['id'] = left.id
                    left_params['name'] = left.name
                    if left.id in series_id_list:
                        continue
                    left_series_list.append(left_params)
                return render(request, 'background/upkeep/material/model_edit.html', locals())
            else:
                return render(request, 'background/upkeep/material/model_edit.html', locals())
        else:
            return render(request, 'background/upkeep/material/model_edit.html', locals())
    elif is_chosen_brand:
        info_obj = SeriesUpkeepInfo.objects.filter(upkeep_material_model_id=model_obj.id)
        if info_obj:
            if info_obj[0].brands:
                bind_brands = info_obj[0].brands.all()
                topbrand = Brand.objects.filter(parent__isnull=True, initial='a')
                if topbrand:
                    top_one_second_brands = Brand.objects.filter(parent=topbrand[0])
                return render(request, 'background/upkeep/material/model_edit.html', locals())
    else:
        return render(request, 'background/upkeep/material/model_edit.html', locals())


def model_edit_action(request):
    res = "success"
    try:
        model_id = request.POST['model_id']
        model_obj = UpkeepMaterialModel.objects.filter(pk=model_id)
        material_id = request.POST.get("materialId", "")
        if material_id != "":
            mobj = UpkeepMaterial.objects.get(pk=material_id)
            time_labor_price = request.POST.get("timeLaborPrice", 0)
            time_labor_min_price = request.POST.get("timeLaborMinPrice", 0)
            time_labor_max_price = request.POST.get('timeLaborMaxPrice', 0)
            material_price = request.POST.get('materialPrice', 0)
            material_min_price = request.POST.get('materialMinPrice', 0)
            material_max_price = request.POST.get('materialMaxPrice', 0)
            levels = request.POST.getlist('avaLevel')
            level_objs = UpkeepMaterialLevel.objects.filter(pk__in=levels)
            code = request.POST.get("code")

            model = request.POST.get("modal", "")
            # brand = request.POST.get("materialBrand", "")
            name = request.POST.get("name", "")
            content = request.POST.get("content", "")
            ma_type = request.POST.get("materialT", UpkeepMaterialModel.TYPE_CHOICES[0][0])
            if "materialModelPic" in request.FILES:
                materialPic = request.FILES["materialModelPic"]
                img_path = deal_with_upload_file(materialPic)
                p_obj = Picture.objects.create(name=materialPic.name, path=img_path)

                model_obj.update(picture=p_obj, material=mobj, code=code, model=model, name=name,
                                   content=content, type=ma_type, time_labor_price=time_labor_price, time_labor_min_price=time_labor_min_price,
                                 time_labor_max_price=time_labor_max_price, material_price=material_price, material_min_price=material_min_price,
                                 material_max_price=material_max_price)
            else:
                model_obj.update(material=mobj, code=code, model=model, name=name,
                                 content=content, type=ma_type, time_labor_price=time_labor_price, time_labor_min_price=time_labor_min_price,
                                 time_labor_max_price=time_labor_max_price, material_price=material_price, material_min_price=material_min_price,
                                 material_max_price=material_max_price)
            model_obj[0].level.clear()
            for l in level_objs:
                model_obj[0].level.add(l)

            all_series = False
            if '1' in levels:
                all_series = True
            if '2' in levels and '3' in levels and '4' in levels:
                all_series = True
            brand_obj = ""
            series = ""
            if MODEL_SPECIAL_LEVEL in levels:
                top_brand_id = request.POST.get("upseriesTopBrand", "")
                second_brand_id = request.POST.get("upSeriesBrand", "")
                is_special = request.POST.get("upSeries", "")
                if top_brand_id != '-1' and second_brand_id == '-1':
                    brand_obj = Brand.objects.filter(pk=top_brand_id)
                if top_brand_id != '-1' and second_brand_id != '-1' and is_special == '-1':
                    brand_obj = Brand.objects.filter(pk=second_brand_id)
                if top_brand_id != '-1' and second_brand_id != '-1' and is_special != '-1':
                    # brand_obj = Brand.objects.filter(pk=second_brand_id)
                    series_ids_list = request.POST["modelSeriesList"]
                    series_ids = series_ids_list.split(',')[:-1]
                    if series_ids:
                        series = Series.objects.filter(pk__in=series_ids)
                if top_brand_id == '-1':
                    all_series = True
            if MODEL_BRAND_LEVE in levels:
                brandIds = request.POST.getlist("bindBrands")
                secondBrands = Brand.objects.filter(id__in=brandIds)
                all_series = True

            upseries_info = SeriesUpkeepInfo.objects.filter(upkeep_material_model=model_obj[0])
            if upseries_info:
                upseries_info = upseries_info[0]
                upseries_info.all_series = all_series
                if brand_obj:
                    upseries_info.brands = brand_obj[0]
                if series:
                    upseries_info.series.clear()
                    for s in series:
                        upseries_info.series.add(s)
                if secondBrands:
                    upseries_info.brands.clear()
                    for sb in secondBrands:
                        upseries_info.brands.add(sb)
                upseries_info.save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not create new model. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def get_priority_area_data(request):
    area_params = {"province": [], "city": [], "default_province": "", "default_city": ""}
    province_list = []
    city_list = []
    try:
        model_id = request.GET.get("model_id", '')
        model_obj = UpkeepMaterialModel.objects.filter(pk=model_id)
        areas = SupportedArea.objects.values("province").annotate(Count("province"))

        if model_obj:
            for a in areas:
                province_list.append(a['province'])
            if province_list:
                province_areas = SupportedArea.objects.filter(province=province_list[0])
                for pa in province_areas:
                    pap = {}
                    pap['id'] = pa.id
                    pap['city'] = pa.city
                    city_list.append(pap)
        area_params['province'] = province_list
        area_params['city'] = city_list
        if model_obj[0].default_area:
            area_params['default_province'] = model_obj[0].default_area.province
            area_params['default_city'] = model_obj[0].default_area.city
            area_params['default_id'] = model_obj[0].default_area.id
        area_params['model_id'] = model_id
    except Exception as e:
        LOG.error('Error happened when set_material_priority_area. %s' % e)
    return HttpResponse(simplejson.dumps(area_params))


def get_province_city(request):
    city_data = []
    try:
        province_name = request.GET.get("province_name", "")
        areas = SupportedArea.objects.filter(province=province_name)
        for a in areas:
            ac = {}
            ac['id'] = a.id
            ac['city'] = a.city
            city_data.append(ac)
    except Exception as e:
        LOG.error('Can not get_province_city. %s' % e)
    return HttpResponse(simplejson.dumps(city_data))


def set_model_default_area_action(request):
    res = "fail"
    try:
        city_id = request.POST.get("materialModelDefaultCity", "")
        model_id = request.POST.get("materialModelId", "")
        if city_id:
            area = SupportedArea.objects.filter(pk=city_id)
            if area:
                model_obj = UpkeepMaterialModel.objects.filter(pk=model_id)
                if model_obj:
                    model_obj[0].default_area = area[0]
                    model_obj[0].save()
                    res = "success"
    except Exception as e:
        LOG.error('Error happened when set_model_default_area_action. %s' % e)
    return HttpResponse(res)


def get_special_series(request):
    brands = []
    top_brands = []
    series = []
    try:
        top_brands = Brand.objects.filter(parent__isnull=True)
        if top_brands:
            brands = Brand.objects.filter(parent_id=top_brands[0].id).filter(parent__isnull=False)
            if brands:
                series = Series.objects.filter(brand__id=brands[0].id)
    except Exception as e:
        LOG.error('Can not get special series. %s' % e)
    return render(request, 'background/upkeep/material/special_series.html',
                  {"series": series,
                   "brands": brands,
                   "top_brands": top_brands,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def get_edit_special_series(request):
    try:
        model_data = {"top_brands": [], "second_brand_name": "", "second_brand_id": "", "series": []}
        model_id = request.GET.get("model_id")
        upseries_info = SeriesUpkeepInfo.objects.filter(upkeep_material_model__id=model_id)
        top_brands = Brand.objects.filter(parent__isnull=True)
        if upseries_info:
            upso = upseries_info[0]
            if upso.all_series:
                top_brand_list = []
                for t in top_brands:
                    tp = {}
                    tp['id'] = t.id
                    tp['name'] = t.name
                    top_brand_list.append(tp)
                return render(request, 'background/upkeep/material/special_series_edit.html',
                              {"all_series": True, 'top_brands': top_brand_list,
                               "chars": [chr(i) for i in range(97, 97 + 26, 1)]})
            else:
                if upso.brands:
                    if upso.brands.parent: #level two
                        for t in top_brands:
                            tp = {}
                            tp['id'] = t.id
                            tp['name'] = t.name
                            tp['is_checked'] = False
                            if upso.brands.parent == t:
                                tp['is_checked'] = True
                            else:
                                tp['is_checked'] = False

                            model_data['top_brands'].append(tp)
                        model_data['second_brand_name'] = upso.brands.name
                        model_data['second_brand_id'] = upso.brands.id
                        if upso.series.all():
                            series = upso.brands.series_set.all()
                            for s in series:
                                sp = {}
                                sp['id'] = s.id
                                sp['name'] = s.name
                                if s in upso.series.all():
                                    sp['is_checked'] = True
                                else:
                                    sp['is_checked'] = False
                                model_data['series'].append(sp)
                        else:
                            model_data['series'] = '-1'

                    else:  #level one
                        for t in top_brands:
                            tp = {}
                            tp['id'] = t.id
                            tp['name'] = t.name
                            tp['is_checked'] = False
                            if upso.brands.parent == t:
                                tp['is_checked'] = True
                            else:
                                tp['is_checked'] = False
                            model_data['top_brands'].append(tp)

                        model_data['second_brand_name'] = "All Brands"
                        model_data['second_brand_id'] = '-1'
                        model_data['series'] = '-1'

    except Exception as e:
        LOG.error('Error happened when get_edit_special_series. %s' % e)
    return render(request, 'background/upkeep/material/special_series_edit.html',
                  {"all_series": False, "data": model_data,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def bind_series_choice(request):
    data = {}
    try:
        initial = request.GET.get('initial', '')
        topbrandid = request.GET.get('topbrand_id', '')
        secbrandid = request.GET.get('secbrand_id', '')
        existseries_id_list = request.GET.get('existseries_id','')
        secbrand = None
        topbrand = None
        existseries_id = existseries_id_list.split(',')
        try:
            i = 0
            if existseries_id:
                for ei in existseries_id:
                    existseries_id[i] = int(ei)
                    i+=1
        except:
            existseries_id = []
        if initial:
            topbrand = Brand.objects.filter(Q(initial=initial), Q(parent__isnull=True))
            if topbrand:
                secbrand = Brand.objects.filter(parent_id=topbrand[0].id)
                series = Series.objects.filter(brand_id=secbrand[0].id)
        if topbrandid:
            secbrand = Brand.objects.filter(parent_id=topbrandid)
            series = Series.objects.filter(brand_id=secbrand[0].id)
        if secbrandid:
            series = Series.objects.filter(brand_id=secbrandid)
        series_list = []
        for se in series:
            se_params = {}
            se_params['id'] = se.id
            if se_params['id'] in existseries_id:
                continue
            se_params['name'] = se.name
            series_list.append(se_params)
        if topbrand:
            topbrand_list =[]
            for tp in topbrand:
                tp_params = {}
                tp_params['id'] = tp.id
                tp_params['name'] = tp.name
                topbrand_list.append(tp_params)
            secbrand_list =[]
            for sc in secbrand:
                sc_params = {}
                sc_params['id'] = sc.id
                sc_params['name'] = sc.name
                secbrand_list.append(sc_params)
            data['topbrand'] = topbrand_list
            data['secbrand'] = secbrand_list
            data['series'] = series_list
        if secbrand and not topbrand:
            secbrand_list = []
            for sc in secbrand:
                sc_params = {}
                sc_params['id'] = sc.id
                sc_params['name'] = sc.name
                secbrand_list.append(sc_params)
            data['topbrand'] = ''
            data['secbrand'] = secbrand_list
            data['series'] = series_list
        if secbrandid:
            data['topbrand'] = ''
            data['secbrand'] = ''
            data['series'] = series_list
    except Exception, e:
        LOG.error('Error happened when search related series. %s' % e)
    return HttpResponse(simplejson.dumps(data), content_type="application/json")


def material_brand_bind(request):
    all_data = []
    try:
        area_id = request.GET.get("areaId", '')
        sec_brand = Brand.objects.filter(parent_id__isnull=True).order_by('initial')
        len = 0
        ini = ''
        ini_len = dict()
        for se in sec_brand:
            ini_len[se.initial] = 0
        for se in sec_brand:
            ini_len[se.initial] += 1
        for se in sec_brand:
            se_params = dict()
            se_params['id'] = se.id
            se_params['len'] = ini_len[se.initial]
            if ini == se.initial:
                se_params['initial'] = ''
            else:
                se_params['initial'] = se.initial
            se_params['name'] = se.name
            se_params['country'] = se.country.name
            se_params['level'] = se.level.name
            ini = se.initial
            all_data.append(se_params)
    except Exception as e:
        LOG.error('Can not get cart upkeep data.%s' % e)
    return render(request, 'background/upkeep/material/brand_bind.html', {"all_data": all_data,
                                                                          "areaId": area_id})


def area_stick(request):
    edit_data = []
    try:
        all_data = []
        sec_brand_id = request.GET['brandId']
        area_id = request.GET['areaId']
        sec_brand = Brand.objects.get(pk=sec_brand_id)
        level_name = sec_brand.level.name
        s_upkeep_1 = SeriesUpkeepInfo.objects.filter(Q(brands__parent_id=sec_brand_id) |
                                                     Q(series__brand__parent_id=sec_brand_id))
        s_upkeep_2 = SeriesUpkeepInfo.objects.filter(Q(brands__isnull=True) | Q(series__isnull=True),
                                                     Q(upkeep_material_model__level__name=level_name))
        s_upkeep_3 = SeriesUpkeepInfo.objects.filter(all_series=True)
        # 取到所有会出现的材料的id
        model_id_list = []
        if s_upkeep_1:
            for su in s_upkeep_1:
                model_id_list.append(su.upkeep_material_model_id)
        if s_upkeep_2:
            for su in s_upkeep_2:
                if su.upkeep_material_model_id not in model_id_list:
                    model_id_list.append(su.upkeep_material_model_id)
        if s_upkeep_3:
            for su in s_upkeep_3:
                if su.upkeep_material_model_id not in model_id_list:
                    model_id_list.append(su.upkeep_material_model_id)

        upkeep_obj = Upkeep.objects.filter(package__type_id=1)
        up_id_list = []
        for up in upkeep_obj:
            up_params = dict()
            up_params['name'] = up.name
            if up.id in up_id_list:
                continue
            up_id_list.append(up.id)
            up_params['material_list'] = []
            up_params['up_len'] = 0
            material_obj = up.upkeepmaterial_set.all()
            if material_obj:
                for mt in material_obj:
                    mt_params = dict()
                    mt_params['brand'] = mt.brand
                    mt_params['model_list'] = []
                    mt_params['brand_len'] = 0
                    model_obj = UpkeepMaterialModel.objects.filter(material=mt)
                    if model_obj:
                        for mo in model_obj:
                            mo_params = dict()
                            if mo.id not in model_id_list:
                                continue
                            mt_params['brand_len'] += 1
                            mo_params['id'] = mo.id
                            mo_params['m_name'] = mo.name + " " + mo.model
                            mo_params['label_price'] = mo.time_labor_price
                            mo_params['material_price'] = mo.material_price
                            mo_params['code'] = mo.code
                            mo_params['type'] = u"通用" if mo.type == 'G' else u"原厂"
                            mt_params['model_list'].append(mo_params)
                    if not mt_params['model_list']:
                        continue
                    up_params['material_list'].append(mt_params)
                    up_params['up_len'] += mt_params['brand_len']
            if not up_params['material_list']:
                continue
            all_data.append(up_params)
        if all_data:
            upkeep_len = 0
            material_len = 0
            for all_dat in all_data:
                for all_d in all_dat['material_list']:
                    for al in all_d['model_list']:
                        al_params = dict()
                        if upkeep_len == 0:
                            al_params['up_name'] = all_dat['name']
                        else:
                            al_params['up_name'] = ''
                        al_params['up_len'] = all_dat['up_len']
                        upkeep_len += 1
                        if upkeep_len == al_params['up_len']:
                            upkeep_len = 0
                        if material_len == 0:
                            al_params['m_brand'] = all_d['brand']
                        else:
                            al_params['m_brand'] = ''
                        al_params['brand_len'] = all_d['brand_len']
                        material_len += 1
                        if material_len == al_params['brand_len']:
                            material_len = 0
                        al_params['model_id'] = al['id']
                        al_params['m_name'] = al['m_name']
                        al_params['label_price'] = al['label_price']
                        al_params['material_price'] = al['material_price']
                        al_params['code'] = al['code']
                        al_params['type'] = al['type']
                        area_stick_obj = BrandUpkeepArea.objects.filter(Q(brand_id=sec_brand_id),
                                                                        Q(area_id=area_id), Q(upkeep_material_model_id=al['id']))
                        if area_stick_obj:
                            al_params['status'] = True
                        else:
                            al_params['status'] = False
                        edit_data.append(al_params)
    except Exception as e:
        LOG.error('Error happened when get bind-material info.%s' % e)
    return render(request, 'background/upkeep/material/material_area_stick.html',
                  {"edit_data": edit_data, "brandId": sec_brand_id, "areaId": area_id})


def stick_action(request):
    res = 'fail'
    try:
        brand_id = request.GET['brand_id']
        area_id = request.GET['area_id']
        model_id = request.GET['model_id']
        status = request.GET['status']
        if status:
            brand = Brand.objects.get(pk=brand_id)
            area = SupportedArea.objects.get(pk=area_id)
            model = UpkeepMaterialModel.objects.get(pk=model_id)
            BrandUpkeepArea.objects.create(brand=brand, area=area, upkeep_material_model=model)
            res = "success"
        else:
            aa = BrandUpkeepArea.objects.filter(Q(brand_id=brand_id),
                                            Q(area_id=area_id), Q(upkeep_material_model_id=model_id))
            if aa:
                for a in aa:
                    a.delete()
            res = "success"
    except Exception as e:
        LOG.error('Error happen when doing area stick. %s' % e)
    return HttpResponse(simplejson.dumps(res))