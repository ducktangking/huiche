from django.conf.urls import url

from hibiscus.background.upkeep.material import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-material-index"),
    url(r'^data/$', views.data, name="background-upkeep-material-data"),
    url(r'^area/data/$', views.area_data, name="background-upkeep-material-area-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeep-material-action-delete"),
    url(r'^new/$', views.new, name="background-upkeep-material-new"),
    url(r'^new/action/$', views.action_new, name="background-upkeep-material-action-new"),
    url(r'^edit/$', views.edit, name="background-upkeep-material-edit"),
    url(r'^edit/action/$', views.action_edit, name="background-upkeep-material-action-edit"),
    url(r'^ajax/data/$', views.ajax_data, name="background-upkeep-material-ajax-data"),
    url(r'^bind/area/$', views.bind_area, name="background-upkeep-material-bind-area"),
    url(r'^list/area/$', views.material_by_area, name="background-upkeep-material-by-area"),
    url(r'^ajax/area/material/$', views.ajax_material_area, name="background-upkeep-material-ajax-area"),
    url(r'^defatult/area/action/$', views.set_default_area, name="background-upkeep-material-defautl-area"),
    url(r'^model/manage/$', views.model_index, name="background-upkeep-material-model-index"),
    url(r'^model/data/$', views.model_data, name="background-upkeep-material-model-data"),
    url(r'^model/new/$', views.model_new, name="background-upkeep-material-model-new"),
    url(r'^model/new/action/$', views.model_new_action, name="background-upkeep-material-model-action-new"),
    url(r'^model/delete/$', views.model_delete, name="background-upkeep-material-model-delete"),
    url(r'^model/edit/$', views.model_edit, name="background-upkeep-material-model-edit"),
    url(r'^model/edit/action/$', views.model_edit_action, name="background-upkeep-material-model-edit-action"),
    url(r'^model/priority/area/$', views.get_priority_area_data, name='background-upkeep-material-model-default-area'),
    url(r'^model/province/city/$', views.get_province_city, name='background-upkeep-material-model-province-city'),
    url(r'^model/defa/area/action/$', views.set_model_default_area_action, name="background-upkeep-material-default-area-action"),
    url(r'^model/special/series/$', views.get_special_series, name='background-upkeep-material-model-special-series'),
    url(r'model/special/series/edit/$', views.get_edit_special_series, name='background-upkeep-material-model-edit-special-data'),
    url(r'^model/bind/series/choice/$', views.bind_series_choice, name='background-upkeep-bind-series-choice'),
    url(r'^brand/bind/$', views.material_brand_bind, name="background-upkeep-material-brand-bind"),
    url(r'^area/stick/$', views.area_stick, name="background-upkeep-material-area-stick"),
    url(r'^stick/action/$', views.stick_action, name="background-upkeep-material-area-stick-action")
]
