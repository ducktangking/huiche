import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.contrib import messages
from django.utils.translation import ugettext as _

from hibiscus.models import UpkeepPackage, UpkeepType, Upkeep
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
UPKEEP_PKG_COLUMNS = ["name", "type", "created_at"]


def index(request):
    upkeep_type = None
    type_id = request.GET.get("type_id", "")
    if type_id != "":
        try:
            upkeep_type = UpkeepType.objects.get(id=type_id)
        except Exception as e:
            upkeep_type = None
            messages.add_message(request, messages.ERROR, _("Upkeep Type is not exist!"))
            LOG.debug("UpkeepType DoesNotExist, error is %s", e)
    return render(request, 'background/upkeep/package/index.html', {"type_id": type_id,
                                                                    "upkeep_type": upkeep_type,
                                                                    "upkeep_types": UpkeepType.objects.all()})


@data_table(columns=UPKEEP_PKG_COLUMNS, model=UpkeepPackage)
def data(request):
    try:
        type_id = request.GET.get("type_id", "")
        if type_id != "":
            sts = UpkeepPackage.objects.filter(
                Q(type__id=type_id), Q(name__contains=request.table_data["key_word"])). \
                order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepPackage.objects.filter(type__id=type_id).count()
        else:
            sts = UpkeepPackage.objects.filter(
                Q(name__contains=request.table_data["key_word"])). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = UpkeepPackage.objects.all().count()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["type"] = st.type.name
            st_params["name"] = st.name
            st_params["type_id"] = st.type.id
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list, total_num
    except Exception as e:
        LOG.error('Can not get cart upkeep package data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                pkg = UpkeepPackage.objects.filter(pk=id_v)
                if pkg:
                    for upkeep in pkg[0].upkeep_set.all():
                        if upkeep.package.count() <= 1:
                            upkeep.delete()
                    pkg.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    upkeep_type = None
    type_id = request.GET.get("type_id", "")
    if type_id != "":
        try:
            upkeep_type = UpkeepType.objects.get(id=type_id)
        except UpkeepType.DoesNotExist as e:
            upkeep_type = None
            messages.add_message(request, messages.ERROR, _("Upkeep Type is not exist!"))
            LOG.debug("UpkeepType DoesNotExist, error is %s", e)
    return render(request, 'background/upkeep/package/new.html', {"type_id": type_id, "upkeep_type": upkeep_type})


def action_new(request):
    res = "fail"
    type_id = request.POST.get("upkeepT", "")
    name = request.POST.get("upkeepPkg", "")
    if type_id != "" and name != "":
        try:
            t_obj = UpkeepType.objects.get(pk=type_id)
            UpkeepPackage(type=t_obj, name=name).save()
            res = "success"
        except Exception as e:
            res = "fail"
            LOG.error('Error happened when create upkeey package. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def get_upkeep_type(request):
    type_data = []
    try:
        upkeep_types = UpkeepType.objects.all()
        for obj in upkeep_types:
            params = {}
            params["id"] = obj.id
            params["type"] = obj.name
            type_data.append(params)
    except Exception as e:
        LOG.error('Can not get upkeep type data when create upkeep package. %s' % e)
    return HttpResponse(simplejson.dumps(type_data))


def get_pkg_detail(request):
    detail_data = {}
    try:
        pkg_id = request.GET["pkg_id"]
        pkg_obj = UpkeepPackage.objects.filter(id=pkg_id)
        detail_data["id"] = pkg_obj.id
        detail_data["type_id"] = pkg_obj.type.id
        detail_data["name"] = pkg_obj.name
    except Exception as e:
        LOG.error('Can not package detail. %s' % e)
    return HttpResponse(simplejson.dumps(detail_data))


def ajax_data(request):
    pkg_data = []
    type_id = request.GET.get("type_id", 1)
    if type_id == "":
        type_id = 1
    try:
        pkgs = UpkeepPackage.objects.filter(type__id__exact=type_id)
        for pkg in pkgs:
            params = {}
            params["name"] = pkg.name
            params["id"] = pkg.id
            params["type_name"] = pkg.type.name
            params["type_id"] = pkg.type.id
            pkg_data.append(params)
    except Exception as e:
        LOG.error('Can not get upkeep package. %s' % e)
    return HttpResponse(simplejson.dumps(pkg_data))