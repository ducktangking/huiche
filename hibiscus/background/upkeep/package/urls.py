from django.conf.urls import url

from hibiscus.background.upkeep.package import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-package-index"),
    url(r'^data/$', views.data, name="background-upkeep-package-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeep-package-action-delete"),
    url(r'^new/$', views.new, name="background-upkeep-package-new"),
    url(r'^new/action/$', views.action_new, name="background-upkeep-package-action-new"),
    url(r'^new/type/data/$', views.get_upkeep_type, name="background-upkeep-pkg-type"),
    url(r'^detail/$', views.get_pkg_detail, name="background-upkeep-pkg-detail"),

    #data
    url(r'^packages/$', views.ajax_data, name="background-upkeep-package-ajax-data"),
]
