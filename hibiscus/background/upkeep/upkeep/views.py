import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.contrib import messages
from django.utils.translation import ugettext as _

from hibiscus.models import Upkeep, UpkeepPackage, UpkeepType
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
UPKEEP_COLUMNS = ["name", "package", "created_at", "created_at"]

PARAMS_ID1 = [1]
PARAMS_ID2 = [2]


def reuqst_get(item_dict, name, default):
    __value = item_dict.get(name, default)
    if __value == "":
        return default
    else:
        return __value


def index(request):
    type_id = request.GET.get("type_id", "")
    pkg_id = request.GET.get("pkg_id", "")
    upkeep_pkg = None
    if pkg_id != "":
        try:
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
        except Exception as e:
            upkeep_pkg = None
            messages.add_message(request, messages.ERROR, _("Upkeep Type is not exist!"))
            LOG.debug("Get Upkeep Package error, error is %s", e)
    return render(request, 'background/upkeep/upkeep/index.html', {"upkeep_pkg": upkeep_pkg,
                                                                   "pkg_id": pkg_id,
                                                                   "type_id": type_id})


@data_table(columns=UPKEEP_COLUMNS, model=Upkeep)
def data(request):
    try:
        pkg_id = request.GET.get("pkg_id", "")
        pkg_obj = None
        try:
            pkg_obj = UpkeepPackage.objects.get(id=pkg_id)
        except:
            pass

        if pkg_obj:
            sts = pkg_obj.upkeep_set.all().filter(
                Q(name__contains=request.table_data["key_word"])
            ).order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            # sts = Upkeep.objects.filter(
            #     Q(package__upkeep__id=pkg_id), Q(name__contains=request.table_data["key_word"])). \
            #           order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = pkg_obj.upkeep_set.all().count()
        else:
            sts = Upkeep.objects.filter(
                Q(name__contains=request.table_data["key_word"])). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            total_num = Upkeep.objects.all().count()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["packages"] = ",".join([package.name for package in st.package.all()])
            type_id = st.package.first().type.id if st.package.first() else None
            if type_id == 2:
                st_params["details"] = "<br/>".join([":".join([_("Low Price"), str(st.price_low)]),
                                               ":".join([_("Middle Price"), str(st.price_middle)]),
                                               ":".join([_("High Price"), str(st.price_high)]),
                                               ":".join([_("Mini Price"), str(st.price_min)]),
                                               ":".join([_("Max Price"), str(st.price_max)])])
            else:
                st_params["details"] = "<br/>".join([":".join([_("Original Price"), str(st.labor_price_original)]),
                                               ":".join([_("General Price"), str(st.labor_price_general)]),
                                               ":".join([_("Labor Min Price"), str(st.labor_price_min)]),
                                               ":".join([_("Labor Max Price"), str(st.labor_price_max)])])
            st_params["type_id"] = type_id
            # st_params["package"] = st.package.name
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list, total_num
    except Exception as e:
        LOG.error('Can not get cart upkeep data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Upkeep.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    type_id = request.GET.get("type_id", "")
    pkg_id = request.GET.get("pkg_id", "")
    upkeep_pkg = None
    upkeep_type = None
    if pkg_id != "" and type_id != "":
        try:
            upkeep_pkg = UpkeepPackage.objects.get(id=pkg_id)
            upkeep_type = UpkeepType.objects.get(id=type_id)
        except Exception as e:
            upkeep_pkg = None
            messages.add_message(request, messages.ERROR, _("Upkeep Type is not exist!"))
            LOG.debug("Get Upkeep Package error, error is %s", e)
    return render(request, 'background/upkeep/upkeep/new.html', {"pkg_id": pkg_id, "type_id": type_id,
                                                                 "upkeep_pkg": upkeep_pkg,
                                                                 "upkeep_type": upkeep_type})


def action_new(request):
    res = "fail"
    try:
        name = request.POST.get("projName", "")
        selectedPackages = request.POST["selectedPackages"].split(',')
        if name != "" and len(selectedPackages) > 0:
            original_price = reuqst_get(request.POST, "origPrice", 0)
            gen_price = reuqst_get(request.POST, "genPrice",0)
            lab_min_price = reuqst_get(request.POST, "labMinPrice",0)
            lab_max_price = reuqst_get(request.POST, "labMaxPrice",0)
            low_price = reuqst_get(request.POST, "lowPrice", 0)
            middle_price = reuqst_get(request.POST, "middlePrice",0)
            high_price = reuqst_get(request.POST, "highPrice",0)
            min_price = reuqst_get(request.POST, "minPrice", 0)
            max_price = reuqst_get(request.POST, "maxPrice", 0)

            is_oil = False
            isOil = reuqst_get(request.POST, "isOil", "off")
            if isOil == "on":
                is_oil = True
            up_obj = Upkeep.objects.create(name=name, price_low=low_price, price_middle=middle_price,
                        price_high=high_price, price_min=min_price, price_max=max_price,
                        labor_price_general=gen_price, labor_price_original=original_price,
                        labor_price_min=lab_min_price, labor_price_max=lab_max_price,
                        is_oil=is_oil)
            for upPackage in selectedPackages:
                up_obj.package.add(UpkeepPackage.objects.get(id=upPackage))
            res = "success"
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create Upkeep. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    up_id = request.GET.get("up_id", "")
    pkg_id = request.GET.get("pkg_id", "")
    if up_id != "":
        upkeep = Upkeep.objects.get(id=up_id)
        selectedPackages = [package.id for package in upkeep.package.all()]
        return render(request, "background/upkeep/upkeep/edit.html", {"upkeep": upkeep, "upkeep_package": upkeep.package.first(),
                                                                      "selectedPackages":selectedPackages,
                                                                      "pkg_id":pkg_id})


def action_edit(request):
    res = "fail"
    up_id = request.POST.get("up_id", "")
    if up_id != "":
        try:
            upkeep = Upkeep.objects.get(id=up_id)
            original_price = reuqst_get(request.POST, "origPrice", 0)
            gen_price = reuqst_get(request.POST, "genPrice", 0)
            lab_min_price = reuqst_get(request.POST, "labMinPrice", 0)
            lab_max_price = reuqst_get(request.POST, "labMaxPrice", 0)
            low_price = reuqst_get(request.POST, "lowPrice", 0)
            middle_price = reuqst_get(request.POST, "middlePrice", 0)
            high_price = reuqst_get(request.POST, "highPrice", 0)
            min_price = reuqst_get(request.POST, "minPrice", 0)
            max_price = reuqst_get(request.POST, "maxPrice", 0)

            is_oil = False
            isOil = reuqst_get(request.POST, "isOil", "off")
            if isOil == "on":
                is_oil = True
            upkeep.price_low = low_price
            upkeep.price_middle = middle_price
            upkeep.price_high = high_price
            upkeep.price_min = min_price
            upkeep.price_max = max_price
            upkeep.labor_price_original = original_price
            upkeep.labor_price_general = gen_price
            upkeep.labor_price_min = lab_min_price
            upkeep.labor_price_max = lab_max_price
            upkeep.is_oil = is_oil
            upkeep.save()

            upkeep.package.clear()
            selectedPackages = request.POST["selectedPackages"].split(',')
            for upPackage in selectedPackages:
                upkeep.package.add(UpkeepPackage.objects.get(id=int(upPackage)))
            res = "success"
        except Exception as e:
            LOG.error('Error happened when edit Upkeep. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


def ajax_data(request):
    upkeep_data = []
    type_id = request.GET.get("type_id", "")
    upkeeps = []
    try:
        if type_id != "":
            try:
                upkeep_type = UpkeepType.objects.get(pk=type_id)
                for upkeep_package in upkeep_type.upkeeppackage_set.all():
                    __pkg = upkeep_package.upkeep_set.all()
                    for __up in __pkg:
                        upkeeps.append(__up)
                upkeeps = list(set(upkeeps))
            except Exception as e:
                pass
        else:
            upkeeps = Upkeep.objects.all()
        for upkeep in upkeeps:
            params = {}
            params["name"] = upkeep.name
            params["id"] = upkeep.id
            upkeep_data.append(params)
    except Exception as e:
        LOG.error('Can not get upkeep. %s' % e)
    return HttpResponse(simplejson.dumps(upkeep_data))


def upkeep_data_for_material(request):
    up_list = []
    try:
        up_id = request.GET.get("up_id", None)
        ups = Upkeep.objects.filter(package__type__id=1)
        if up_id and up_id != 'all':
            upobj = Upkeep.objects.get(pk=up_id)
        else:
            upobj = None
        for u in ups:
            up = {}
            up['id'] = u.id
            up['name'] = u.name
            if u == upobj:
                up['is_checked'] = True
            else:
                up['is_checked'] = False
            if up in up_list:
                continue
            up_list.append(up)
    except Exception as e:
        LOG.error('Can not get upkeep data for material. %s' % e)
    return HttpResponse(simplejson.dumps(up_list))