from django.conf.urls import url
from hibiscus.background.upkeep.upkeep import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-up-index"),
    url(r'^data/$', views.data, name="background-upkeep-up-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeep-up-action-delete"),
    url(r'^new/$', views.new, name="background-upkeep-up-new"),
    url(r'^new/action/$', views.action_new, name="background-upkeep-up-action-new"),
    url(r'^edit/$', views.edit, name="background-upkeep-up-edit"),
    url(r'^edit/action/$', views.action_edit, name="background-upkeep-up-action-edit"),

    # data
    url(r'^upkeeps/$', views.ajax_data, name="background-upkeep-up-ajax-data"),
    url(r'^upkeeps/material/$', views.upkeep_data_for_material, name='background-upkeep-up-data-for-material'),
]