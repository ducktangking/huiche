import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import UpkeepType
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
UPKEEP_TYPE_COLUMNS = ["name", "created_at"]


def index(request):
    return render(request, 'background/upkeep/type/index.html')


@data_table(columns=UPKEEP_TYPE_COLUMNS, model=UpkeepType)
def data(request):
    try:
        sts = UpkeepType.objects.filter(
            Q(name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart upkeep type data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                UpkeepType.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/upkeep/type/new.html')


def action_new(request):
    res = "success"
    try:
        name = request.POST["upkeepType"]
        UpkeepType(name=name).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create upkeey type. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def get_type_by_id(request):
    type_data = {}
    try:
        type_id = request.GET.get("type_id", "")
        if type_id != "":
            up_type = UpkeepType.objects.get(pk=type_id)
            type_data["name"] = up_type.name
            type_data["id"] = up_type.id
    except Exception as e:
        LOG.error('Can not get upkeep type data by id. %s' % e)
    return HttpResponse(simplejson.dumps(type_data))


def ajax_data(request):
    type_data = []
    try:
        ts = UpkeepType.objects.all()
        for t in ts:
            t_p = {}
            t_p['id'] = t.id
            t_p['name'] = t.name
            type_data.append(t_p)
    except Exception as e:
        LOG.error('Can not get upkeep type. %s' % e)
    return HttpResponse(simplejson.dumps(type_data))