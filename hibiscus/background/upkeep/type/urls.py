from django.conf.urls import url

from hibiscus.background.upkeep.type import views

urlpatterns = [
    url(r'^$', views.index, name="background-upkeep-type-index"),
    url(r'^data/$', views.data, name="background-upkeep-type-data"),
    url(r'^delete/action/$', views.action_delete, name="background-upkeep-type-action-delete"),
    url(r'^new/$', views.new, name="background-upkeep-type-new"),
    url(r'^new/action/$', views.action_new, name="background-upkeep-type-action-new"),
    url(r'^detail/$', views.get_type_by_id, name="background-upkeep-type-detail"),

    # data
    url(r'^types/$', views.ajax_data, name="background-upkeep-type-ajax-data"),

]