from django.conf.urls import url
from hibiscus.background.article import views

urlpatterns = [
    url(r'^$', views.index, name="background-article-index"),
    url(r'^data/$', views.data, name="background-article-data"),
    url(r'^delete/action/$', views.action_delete, name="background-article-action-delete"),
    url(r'^new/$', views.new, name="background-article-new"),
    url(r'^new/action/$', views.action_new, name="background-article-action-new"),
    url(r'^pic/new/action/$', views.action_new_pic, name="background-article-pic-action-new"),
    url(r'^preview/$', views.preview, name="background-article-preview"),
    url(r'^detail/$', views.detail, name="background-article-detail"),
    url(r'^audit/$', views.audit_index, name="background-article-audit-index"),
    url(r'^audit/data/$', views.audit_data, name="background-article-audit-data"),
    url(r'^audit/ajax/filter/data/$', views.filter_ajax_data, name="background-article-filter_ajax_data"),
    url(r'^detail/data/$', views.detail_data, name="background-article-detail-data"),
    url(r'^switch/status/$', views.switch_status, name="background-article-switch_status"),
    url(r'^switch/new/$', views.switch_new, name="background-article-switch_new"),
    url(r'^type/level/two/data/$', views.article_type_two, name="background-article-level-two-data"),
    url(r'^image/upload/file/$', views.image_upload_file, name="background-article-image-upload-file"),
    url(r'^edit/action/$', views.action_edit, name="background-article-edit-action"),
    url(r'^assess/show/$', views.assess_show, name="background-article-assess-show"),
    # url(r'^news/headline/$', views.news_headline, name="background-article-news-headline"),
    url(r'carousel/position/$', views.carousel_position, name='background-article-carousel-position'),
    url(r'^article/modal/show/$', views.article_modal_show, name='background-article-modal-show'),
    url(r'^article/modal/subsite/body/$', views.sub_site_modal_body, name='background-article-subsite-modal-body'),
    url(r'^article/modal/action$', views.article_show_action, name='background-article-modal-action'),

]