# -*- coding:utf-8 -*-
import simplejson
import logging
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Logo, Picture, Article, Brand, SupportedArea, Car, ArticleType, Producer, HcAdmin, Series, \
    ArticleShow
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file
from hibiscus.background.brand.views import brand_car



LOG = logging.getLogger(__name__)
ARTICLE_COLUMNS = ["title", "created_type", "producer", "created_at"]
ID_KEY = {"classify": 1, "label": 2, "province": 3, "city": 4, "men": 5, "status": 6}


def index(request):
    parent_id = request.GET.get('parent_id', None)
    producer_id = request.GET.get("producer_id", "")
    if producer_id:
        return render(request, 'background/article/producer-index.html',
                      {"parent_id": parent_id, "producer_id": producer_id})
    else:
        return render(request, 'background/article/index.html',
                  {"parent_id": parent_id, "producer_id": producer_id})


@data_table(columns=ARTICLE_COLUMNS, model=Article)
def data(request):
    parent_id = request.GET.get('parent_id', None)
    producer_id = request.GET.get('producer_id', "")

    try:
        if producer_id:
            # producer_obj = Producer.objects.get(pk=producer_id)
            sts = Article.objects.filter(
                Q(title__contains=request.table_data["key_word"]), Q(producer__id=producer_id)). \
                  order_by(request.table_data["order_cl"]).order_by("-created_at")[request.table_data["start"]: request.table_data["end"]]
        else:
            # operator_obj = HcAdmin.objects.get(pk=request.session['_auth_user_id'])
            operator_id = request.session['_auth_user_id']
            if parent_id and parent_id != "None":
                sts = Article.objects.filter(
                    Q(title__contains=request.table_data["key_word"]), Q(admin__id=operator_id), (Q(type__parent__id=parent_id)|Q(type__id=parent_id))). \
                          order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
            else:
                sts = Article.objects.filter(
                    Q(title__contains=request.table_data["key_word"]), Q(admin__id=operator_id)). \
                          order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["title"] = st.title
            st_params["created_type"] = st.created_type
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params['producer_id'] = st.producer.id if st.producer else ""
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Article.objects.filter(pk=id_v).delete()
                # for art_obj in art_list:
                #     art_obj.delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete logo. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    brand_data = {}
    top_brands = []
    all_brands = []
    producer_id = request.GET.get("producer_id", None)
    try:
        if producer_id:
            probj = Producer.objects.get(pk=producer_id)
            brands = Brand.objects.filter(id__in=[p.brand.id for p in probj.producercarseries_set.all()])
        else:
            top_brands = Brand.objects.filter(parent__isnull=True)
            brands = Brand.objects.filter(parent__isnull=False)

        if brands:
            brand_data['brand_name'] = brands[0].name
            brand_data['brand_id'] = brands[0].id
            brand_data['brand_car_data'] = []
            series = brands[0].series_set.all()
            for s in series:
                sp = {}
                sp['id'] = s.id
                sp['name'] = s.name
                brand_data['brand_car_data'].append(sp)

        for b in brands:
            b_p = {}
            b_p["brand_id"] = b.id
            b_p["brand_name"] = b.name
            all_brands.append(b_p)
        # areas = SupportedArea.objects.all()
        # for a in areas:
        #     ap = {}
        #     ap['province'] = a.province
        #     ap['city'] = a.city
        #     ap['id'] = a.id
        #     area_data.append(ap)
        # # article type
        if not producer_id:
            area_data = HcAdmin.objects.get(pk=request.user.id).area
        else:
            area_data = probj.area
        # area = {"id": area_data.id, "city": area_data.city, "province": area_data.province}
        level_one_types = ArticleType.objects.filter(parent=None)
        one_obj = level_one_types[0]
        level_two = ArticleType.objects.filter(parent=one_obj)

        if producer_id:
            return render(request, 'background/article/producer-new.html',
                          {"brand_data": brand_data, "all_brands": all_brands,
                           "area": area_data,
                           "producer_id": producer_id,
                           "level_one_types": level_one_types,
                           "level_two": level_two,
                           "original_id": one_obj,
                           "chars": [chr(i) for i in range(97, 97 + 26, 1)]})
    except Exception as e:
        LOG.error('Can not get brand data when create article.%s' % e)
    return render(request, 'background/article/new.html',
                  {"brand_data": brand_data,
                   "top_brands": top_brands,
                   "all_brands": all_brands, "area": area_data,
                   "level_one_types": level_one_types,
                   "level_two": level_two,
                   "original_id": one_obj,
                   "operator_id": request.session['_auth_user_id'],
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def detail(request):
    try:
        arId = request.GET['arId']
        producer_id = request.GET.get('producer_id', "")
        arobj = Article.objects.get(pk=arId)

        brand_data = {}
        all_brands = []
        area_data = []
        top_brands = []
        article_type_list = []
        producer_id = request.GET.get("producer_id", None)
        if producer_id:
            probj = Producer.objects.get(pk=producer_id)
            brands = Brand.objects.filter(id__in=[p.brand.id for p in probj.producercarseries_set.all()])
        else:
            top_brands = Brand.objects.filter(parent__isnull=True)
            brands = Brand.objects.filter(parent__isnull=False)

        ar_brand = arobj.brand

        for b in brands:
            b_p = {}
            b_p["brand_id"] = b.id
            b_p["brand_name"] = b.name
            if ar_brand:
                if b.id == ar_brand.id:
                    b_p['is_checked'] = True
                else:
                    b_p['is_checked'] = False
            else:
                b_p['is_checked'] = False
            all_brands.append(b_p)

        car_data = []
        if ar_brand:
            series = ar_brand.series_set.all()
            if series:
                for s in series:
                    sp = {}
                    sp['id'] = s.id
                    sp['name'] = s.name
                    if arobj.series:
                        if s == arobj.series:
                            sp['is_checked'] = True
                        else:
                            sp['is_checked'] = False
                    else:
                        sp['is_checked'] = False
                    car_data.append(sp)

        areas = SupportedArea.objects.all()
        for a in areas:
            ap = {}
            ap['province'] = a.province
            ap['city'] = a.city
            ap['id'] = a.id
            if a.id == arobj.area.id:
                ap['is_checked'] = True
            else:
                ap['is_checked'] = False
            area_data.append(ap)

        level_one_list = []
        level_two_list = []
        level_one_types = ArticleType.objects.filter(parent__isnull=True)
        if not arobj.type.parent:
            parent_id = arobj.type.id
            current_id = parent_id
            parent = arobj.type.parent
        else:
            parent_id = arobj.type.parent.id
            current_id = arobj.type.id
            parent = arobj.type.parent

        for lo in level_one_types:
            lop = {}
            lop['id'] = lo.id
            lop['name'] = lo.name
            if lo.id == parent_id:
                lop['is_checked'] = True
            else:
                lop['is_checked'] = False
            level_one_list.append(lop)

        level_two_types = ArticleType.objects.filter(parent_id=parent_id)
        level_two_types = list(level_two_types)
        if parent:
            level_two_types.append(parent)
        for lt in level_two_types:
            ltp = {}
            ltp['id'] = lt.id
            ltp['name'] = lt.name
            if lt.id == current_id:
                ltp['is_checked'] = True
            else:
                ltp['is_checked'] = False

            level_two_list.append(ltp)
        if arobj.type.parent:
            original = False
        else:
            original = True
        # if producer_id:
        #     return render(request, 'background/article/edithtml',
        #                   {"brand_data": brand_data, "all_brands": all_brands,
        #                    "area_data": area_data,
        #                    "producer_id": producer_id})
    except Exception as e:
        LOG.error('Can not get article detail. %s' % e)
    return render(request, 'background/article/edit.html', {
                    'arobj': arobj,
                    'producer_id': producer_id,
                    'top_brands': top_brands,
                    'brand_data': all_brands,
                    'brand_cars': car_data,
                    'area_data': area_data,
                    'level_one_list': level_one_list,
                    'level_two_list': level_two_list,
                    'original': original,
                    "chars": [chr(i) for i in range(97, 97 + 26, 1)]
                    })


def action_new(request):
    res = "success"
    try:
        content = request.POST.get("content", "")
        cr_type = request.POST['articleType']
        title = request.POST["title"]
        article_style = request.POST['articleStyle']
        if int(article_style) == 2:
            content = request.POST.get("videoLink", "")
        pobj = None
        if request.FILES:
            f = request.FILES['default_picture']
            f_path = deal_with_upload_file(f)
            pobj = Picture.objects.create(name=f.name, path=f_path)
        article_type_id = request.POST['articleTwo']
        article_type_obj = ArticleType.objects.get(pk=article_type_id)
        producer_id = request.POST.get('producerId', "")
        operator_id = request.POST.get('operatorId', "")
        if producer_id:
            probj = Producer.objects.get(pk=producer_id)
            article_obj = Article.objects.create(title=title, created_type=cr_type, content=content, article_style_type=int(article_style),
                                                 type=article_type_obj, producer=probj, default_picture=pobj)
        if operator_id:
            operator_obj = HcAdmin.objects.get(pk=operator_id)
            article_obj = Article.objects.create(title=title, created_type=cr_type, content=content,  article_style_type=int(article_style),
                                                 type=article_type_obj, admin=operator_obj, default_picture=pobj)

        brand_id = request.POST['articleBrand11']
        car_id = request.POST['articleCar']
        area_ids = request.POST.getlist('articleArea')

        if brand_id != "-1":
            br_obj = Brand.objects.get(pk=brand_id)
            # manytomay field use add
            # article_obj.brand.add(br_obj)
            article_obj.brand = br_obj
            article_obj.save()
        if car_id != "-1":
            car_obj = Series.objects.get(pk=car_id)
            # article_obj.car.add(car_obj)
            article_obj.series = car_obj
            article_obj.save()
        for area_id in area_ids:
            area_obj = SupportedArea.objects.get(pk=area_id)
            # article_obj.area.add(area_obj)
            article_obj.area = area_obj
            article_obj.save()

    except Exception as e:
        LOG.error('Error happened when create logo. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def action_edit(request):
    res = "success"
    try:
        arobj_id = request.POST.get('arId', "")
        if not arobj_id:
            return HttpResponse(simplejson.dumps({"res": "fail", "reason": "No article id found."}))
        arobj = Article.objects.get(pk=arobj_id)
        content = request.POST.get("content", "")
        cr_type = request.POST['articleType']
        title = request.POST["title"]
        article_type_id = request.POST['articleTwo']
        default_picture = request.FILES.get("default_picture", '')
        if default_picture:
            new_path = deal_with_upload_file(default_picture)
            pobj = Picture.objects.create(name=default_picture.name, path=new_path)
            arobj.default_picture = pobj
            arobj.save()
        article_type_obj = ArticleType.objects.get(pk=article_type_id)
        arobj.title = title
        arobj.content = content
        arobj.type = article_type_obj
        arobj.created_type = cr_type
        arobj.save()

        brand_id = request.POST['articleBrand11']
        car_id = request.POST['articleCar']
        area_ids = request.POST.getlist('articleArea')

        if brand_id != "-1":
            br_obj = Brand.objects.get(pk=brand_id)
            # manytomay field use add
            # article_obj.brand.add(br_obj)
            arobj.brand = br_obj
            arobj.save()
        if car_id != "-1":
            car_obj = Series.objects.get(pk=car_id)
            # article_obj.car.add(car_obj)
            arobj.series = car_obj
            arobj.save()
        for area_id in area_ids:
            area_obj = SupportedArea.objects.get(pk=area_id)
            # article_obj.area.add(area_obj)
            arobj.area = area_obj
            arobj.save()

    except Exception as e:
        LOG.error('Error happened when create logo. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def action_new_pic(request):
    img_data = {"res": "success", "pic_str": "", "pic_name": "", "id": "", "finish": ""}
    try:
        if "img" in request.FILES:
            img = request.FILES['img']
            img_path = deal_with_upload_file(img)
            Picture(name=img.name, path=img_path).save()
            img_data["pic_str"] = "<img src='" + img_path + "'>"
            img_data["pic_name"] = img.name
            img_data["res"] = "success"
            return HttpResponse(simplejson.dumps(img_data))
        else:
            article_style = request.POST['articleStyle']
            title = request.POST["title"]
            cr_type = request.POST['articleType']
            reprint_addr = request.POST.get("reprint_addr", "")
            if int(article_style) == 1:
                content = request.POST["allData"]
            elif int(article_style) == 0:
                content = request.POST.get("content", "")
            elif int(article_style) == 2:
                content = request.POST.get("videoLink", "")
            pobj = None
            if request.FILES:
                f = request.FILES['default_picture']
                f_path = deal_with_upload_file(f)
                pobj = Picture.objects.create(name=f.name, path=f_path)
            article_type_id = request.POST['articleTwo']
            article_type_obj = ArticleType.objects.get(pk=article_type_id)
            producer_id = request.POST.get('producerId', "")
            operator_id = request.POST.get('operatorId', "")
            if producer_id:
                probj = Producer.objects.get(pk=producer_id)
                article_obj = Article.objects.create(title=title, created_type=cr_type, content=content,
                                                     article_style_type=int(article_style), reprint_addr=reprint_addr,
                                                     type=article_type_obj, producer=probj, default_picture=pobj)
            if operator_id:
                operator_obj = HcAdmin.objects.get(pk=operator_id)
                article_obj = Article.objects.create(title=title, created_type=cr_type, content=content,
                                                     article_style_type=int(article_style), reprint_addr=reprint_addr,
                                                     type=article_type_obj, admin=operator_obj,
                                                     default_picture=pobj)
            # article_obj = Article.objects.create(title=title, created_type=cr_type, content=content)
            img_data["id"] = article_obj.id

            brand_id = request.POST['articleBrand11']
            car_id = request.POST['articleCar']
            area_ids = request.POST.getlist('articleArea')

            if brand_id != "-1":
                br_obj = Brand.objects.get(pk=brand_id)
                # manytomay field use add
                # article_obj.brand.add(br_obj)
                article_obj.brand = br_obj
                article_obj.save()
            if car_id != "-1":
                car_obj = Series.objects.get(pk=car_id)
                # article_obj.car.add(car_obj)
                article_obj.series = car_obj
                article_obj.save()
            for area_id in area_ids:
                area_obj = SupportedArea.objects.get(pk=area_id)
                # article_obj.area.add(area_obj)
                article_obj.area = area_obj
                article_obj.save()
            img_data["finish"] = "done"

    except Exception as e:
        img_data["res"] = "fail"
        LOG.error('Error happened when create logo. %s' % e)
    return HttpResponse(simplejson.dumps(img_data))


def preview(request):
    pre_data = {}
    try:
        ar_id = request.GET.get("arId", None)
        if ar_id:
            ar_obj = Article.objects.get(pk=ar_id)
            pre_data["res"] = "success"
            pre_data["title"] = ar_obj.title
            pre_data["content"] = ar_obj.content
            pre_data["created_type"] = ar_obj.created_type
            pre_data['author'] = get_article_auth(ar_obj)
            pre_data['reprint_addr'] = ar_obj.reprint_addr
            pre_data['created_at'] = timezone.localtime(ar_obj.created_at).strftime('%Y-%m-%d %H:%M:%S')
    except Exception, e:
        pre_data["res"] = "fail"
        LOG.error('Can not get preview data. %s' % e)
    return HttpResponse(simplejson.dumps(pre_data))


def detail_data(request):
    ar_detail = {}
    try:
        arId = request.GET['arId']
        ar_obj = Article.objects.get(pk=arId)
        ar_detail['title'] = ar_obj.title
        ar_detail['producer'] = ""
        ar_detail['admin'] = ""
        if ar_obj.producer:
            ar_detail['producer'] = ar_obj.producer.username
        if ar_obj.admin:
            ar_detail['admin'] = ar_obj.admin.username
        ar_detail['created_at'] = timezone.localtime(ar_obj.created_at).strftime('%Y-%m-%d %H:%M:%S')
        ar_detail['status'] = ar_obj.status
        ar_detail['content'] = ar_obj.content
        ar_detail['type'] = ar_obj.type.name
    except Exception as e:
        LOG.error('Can not get article detail data. %s' % e)
    return HttpResponse(simplejson.dumps(ar_detail))


def handle_with_filter_string(filter_string):
    data = dict()
    data['classify'] = list()
    data['label'] = list()
    data['province'] = list()
    data['city'] = list()
    data['men'] = list()
    data['status'] = list()
    re_key_dict = dict((value,key) for key,value in ID_KEY.iteritems())
    try:
        if filter_string:
            filter_one = filter_string.split("#")[:-1]
            if filter_one:
                for filter_t in filter_one:
                    filter_condition = filter_t.split(",")
                    # name = filter_condition[0]
                    class_id = filter_condition[0]
                    id_key = int(filter_condition[1])
                    con_param = dict()
                    # con_param['name'] = name
                    con_param['id_key'] = int(id_key)
                    con_param['class_id'] = long(class_id) if int(id_key) in [1, 2, 4, 6] else str(class_id)
                    data[re_key_dict[id_key]].append(con_param)
    except Exception as e:
        LOG.error('Error happened while turn a string to dict. %s' % e)
    return data


def article_show_type():
    data = list()
    show_list = ArticleShow.objects.filter()
    used_type = list()
    for show_obj in show_list:
        if show_obj.type not in used_type:
            show_param = dict()
            show_param['id'] = show_obj.id
            show_param['name'] = show_obj.name
            show_param['type'] = show_obj.type
            show_param['is_app'] = show_obj.is_app
            data.append(show_param)
            used_type.append(show_obj.type)
    return data


def audit_index(request):
    filter_string = request.GET.get("filter", "")
    filter_data = handle_with_filter_string(filter_string)
    edit_data = callback_data(filter_data,True)
    show_args = article_show_type()
    return render(request, 'background/article/audit-index.html', {"data": edit_data, "show":simplejson.dumps(show_args)})


def filter_article_objects(filter_condition, ignore):
    """
    该函数只用于处理article对象的过滤，并且返回一个过滤之后的对象
    当ignore为true的时候，会选择性忽视label，province，city，men这四项
    :param filter_condition:
    :param ignore:
    :return:
    """
    article = Article.objects.filter()
    for key in filter_condition:
        if key == "classify":
            if filter_condition[key]:
                type_list = list(val["class_id"] for val in filter_condition[key])
                article = article.filter(type__parent_id__in=type_list)
        elif key == "status":
            if filter_condition[key]:
                status_list = list(val["class_id"] for val in filter_condition[key])
                article = article.filter(status__in=status_list)
        elif key == "label" and not ignore:
            if filter_condition[key]:
                type_list = list(val["class_id"] for val in filter_condition[key])
                article = article.filter(type_id__in=type_list)
        # bug exist
        elif key == "province" and not ignore:
            if filter_condition[key]:
                province_list = list(val["class_id"].decode("hex").decode("utf8") for val in filter_condition[key])
                article = article.filter(area__province__in=province_list)
        elif key == "city" and not ignore:
            if filter_condition[key]:
                city_list = list(val["class_id"] for val in filter_condition[key])
                article = article.filter(area_id__in=city_list)
        elif key == "men" and not ignore:
            if filter_condition[key]:
                men_list = list(val["class_id"] for val in filter_condition[key])
                article = filter_article_from_men(men_list, article)
    return article


def filter_article_from_men(men_list, article):
    admin_list = list()
    pro_list = list()
    for men in men_list:
        if "a-" in men:
            admin_list.append(men.replace("a-", ""))
        else:
            pro_list.append(men.replace("p-", ""))
    art = article.filter(Q(producer_id__in=pro_list) | Q(admin_id__in=admin_list))
    return art


def callback_data(filter_condition, ignore):
    edit_data = dict()
    edit_data['classify'] = list()
    edit_data['label'] = list()
    edit_data['province'] = list()
    edit_data['city'] = list()
    edit_data['men'] = list()
    edit_data['status'] = list()
    try:
        article_list = filter_article_objects(filter_condition, ignore)
        # print len(article_list)
        article_type = ArticleType.objects.filter(parent_id__isnull=True)
        for art in [0, 1]:
            sta_param = dict()
            sta_param['id_key'] = ID_KEY['status']
            sta_param['class_id'] = art
            if sta_param in filter_condition['status']:
                sta_param['able'] = False
            else:
                sta_param['able'] = True
            if art:
                sta_param['name'] = "通过"
            else:
                sta_param['name'] = "未通过"
            edit_data['status'].append(sta_param)
        for arti in article_type:
            art_param = dict()
            art_param['id_key'] = ID_KEY['classify']
            art_param['class_id'] = arti.id
            if art_param in filter_condition['classify']:
                art_param['able'] = False
            else:
                art_param['able'] = True
            art_param['name'] = arti.name
            edit_data['classify'].append(art_param)
        if article_list:
            for article in article_list:
                label_param = dict()
                label_param['id_key'] = ID_KEY['label']
                label_param['class_id'] = article.type_id
                if label_param in filter_condition['label']:
                    label_param['able'] = False
                else:
                    label_param['able'] = True
                label_param['name'] = article.type.name
                if label_param not in edit_data['label'] and len(edit_data['label']) < 16:
                    edit_data['label'].append(label_param)

                men_param = dict()
                men_param['id_key'] = ID_KEY['men']
                # men_param['class_id'] = article.auditor_id
                if article.admin:
                    men_param['class_id'] = "a-" + str(article.admin_id)
                elif article.producer:
                    men_param['class_id'] = "p-" + str(article.producer_id)
                else:
                    men_param['class_id'] = "a-" + str(article.auditor_id)
                if men_param in filter_condition['men']:
                    men_param['able'] = False
                else:
                    men_param['able'] = True
                if article.admin:
                    men_param['name'] = article.admin.information.name \
                        if article.admin.information else article.admin.username
                elif article.producer:
                    men_param['name'] = article.producer.information.name \
                        if article.producer.information else article.producer.username
                else:
                    men_param['name'] = article.auditor.information.name \
                        if article.auditor.information else article.auditor.username

                # men_param['name'] = article.auditor.username
                if men_param not in edit_data['men']:
                    edit_data['men'].append(men_param)

                pro_param = dict()
                pro_param['id_key'] = ID_KEY['province']
                if article.area:
                    pro_param['class_id'] = article.area.province.encode("utf8").encode("hex")
                else:
                    continue
                if pro_param in filter_condition['province']:
                    pro_param['able'] = False
                else:
                    pro_param['able'] = True
                if article.area:
                    pro_param['name'] = article.area.province
                else:
                    continue
                if pro_param not in edit_data['province']:
                    edit_data['province'].append(pro_param)

                city_param = dict()
                city_param['id_key'] = ID_KEY['city']
                if article.area:
                    city_param['class_id'] = article.area_id if article.area else "-1"
                else:
                    continue
                if city_param in filter_condition['city']:
                    city_param['able'] = False
                else:
                    city_param['able'] = True
                if article.area:
                    city_param['name'] = article.area.city if article.area else "-1"
                else:
                    continue
                if city_param not in edit_data['city']:
                    edit_data['city'].append(city_param)
    except Exception as e:
        LOG.error('Error happened while edit ajax data.%s' % e)
    return edit_data


def filter_ajax_data(request):
    data = dict()
    try:
        filter_string = request.GET.get("filter", "")
        filter_data = handle_with_filter_string(filter_string)
        data = callback_data(filter_data, True)
        print simplejson.dumps(data)
    except Exception as e:
        LOG.error('Error happened while return ajax data.%s' % e)
    return HttpResponse(simplejson.dumps(data))


@data_table(columns=ARTICLE_COLUMNS, model=Article)
def audit_data(request):
    try:
        # test = request.GET.get("filter", "")
        filter_string = request.GET.get("filter", "")
        filter_data = handle_with_filter_string(filter_string)
        # print test
        sts = filter_article_objects(filter_data, False).filter(
              Q(title__contains=request.table_data["key_word"])). \
            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["title"] = st.title
            st_params["created_type"] = st.created_type
            if st.admin:
                st_params['author'] = st.admin.information.name if st.admin.information else st.admin.username
            elif st.producer:
                st_params['author'] = st.producer.information.name if st.producer.information else st.producer.username
            else:
                st_params['author'] = "admin"
            # st_params['is_show'] = st.is_show
            st_params['is_headline'] = st.is_headline
            # st_params['position'] = st.position if st.position else 0
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params['classfy'] = st.type.parent.name if st.type.parent_id else st.type.name
            st_params['label'] = st.type.name
            st_params['status'] = st.status
            st_params['is_new'] = st.is_new
            # if st.type.id == 1 or st.type.id == 6 or st.type.parent_id == 1 or \
            #         st.type.parent_id == 6 or st.type.id == 45 or st.type.parent_id == 45 and st.type.name != u'销售促销' and st.type.name !=u'售后促销':
            #     st_params['is_news'] = True
            # else:
            #     st_params['is_news'] = False
            province = "" if not st.area else st.area.province
            if st.area_id == 2 or province == u"全国":
                st_params['is_area'] = False
            else:
                st_params['is_area'] = True
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)


def switch_status(request):
    try:
        arId = request.GET["arId"]
        status = request.GET['status']
        if status == "true":
            st = False
        else:
            st = True
        if st == True:
            Article.objects.filter(pk=arId).update(status=st, audited_at=timezone.now())
        else:
            Article.objects.filter(pk=arId).update(status=st)
    except Exception as e:
        LOG.error('Switch status error. %s' % e)
    return redirect('background-article-audit-index')


def switch_new(request):
    try:
        arId = request.GET["arId"]
        is_new = request.GET['is_new']
        if is_new == "true":
            st = False
        else:
            st = True
        if st is True:
            Article.objects.filter(pk=arId).update(is_new=st, updated_at=timezone.now())
        else:
            Article.objects.filter(pk=arId).update(is_new=st)
    except Exception as e:
        LOG.error('Switch new error. %s' % e)
    return redirect('background-article-audit-index')


def assess_show(request):
    try:
        arId = request.GET["arId"]
        is_show = request.GET['is_show']
        if is_show == "true":
            st = False
        else:
            st = True
        Article.objects.filter(pk=arId).update(is_show=st)
    except Exception as e:
        LOG.error('Switch assess error. %s' % e)
    return redirect('background-article-audit-index')

#
# def news_headline(request):
#     try:
#         arId = request.GET["arId"]
#         is_headline = request.GET['is_headline']
#         if is_headline == "true":
#             st = False
#         else:
#             st = True
#         if st == True:
#             Article.objects.filter(pk=arId).update(is_headline=st, updated_at=timezone.now(), status=st, audited_at=timezone.now())
#             area_id = Article.objects.filter(pk=arId)[0].area_id
#             art = Article.objects.filter(Q(is_headline=True), Q(area__ad=area_id))
#             if area_id == 2:
#                 if len(art) > 2:
#                     overart = art.order_by('updated_at')[0]
#                     overart.is_headline = False
#                     overart.save()
#             else:
#                 if len(art) > 5:
#                     overart = art.order_by('updated_at')[0]
#                     overart.is_headline = False
#                     overart.save()
#         else:
#             Article.objects.filter(pk=arId).update(is_headline=st)
#     except Exception as e:
#         LOG.error('Switch headline error. %s' % e)
#     return redirect('background-article-audit-index')


def article_type_two(request):
    two_level_data = {"two_level_data": [], "one_level_name": ""}
    try:
        type_id = request.GET['type_id']
        parent_obj = ArticleType.objects.get(pk=type_id)
        two_levels = ArticleType.objects.filter(parent=parent_obj)
        for t in two_levels:
            t_p = {}
            t_p['id'] = t.id
            t_p['name'] = t.name
            two_level_data['two_level_data'].append(t_p)
        two_level_data['one_level_name'] = parent_obj.name
    except Exception as e:
        LOG.error('Can not get article level two type. %s' % e)
    return HttpResponse(simplejson.dumps(two_level_data))


def image_upload_file(request):
    img_server_path = {"link": ""}
    try:
        img = request.FILES['file']
        img_path = deal_with_upload_file(img)
        Picture(name=img.name, path=img_path).save()
        img_server_path = {"link": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], img_path)}
    except Exception as e:
        LOG.error('Error happened when deal with upload file. %s' % e)
    return HttpResponse(simplejson.dumps(img_server_path))


def get_article_auth(article):
    if article.admin:
        return article.admin.information.name if article.admin.information else ""
    if article.producer:
        return article.producer.information.name if article.producer.information else ""
    if article.auditor:
        return article.auditor.information.name if article.auditor.information else ""


def carousel_position(request):
    res = 'fail'
    try:
        article_id = request.GET.get('article_id', '')
        position = request.GET.get('position', '')
        art = Article.objects.filter(pk=article_id).update(position=position)
        if art:
            res = 'success'
    except Exception as e:
        LOG.error('Error happened when change the article position. %s' % e)
    return HttpResponse(simplejson.dumps(res))

SHOW_TYPE = {1:u"APP轮播",2:u"首页轮播", 3:u"焦点轮播",4:u"首页文字推送",
             5:u"首页说客",6:u"首页视频",7:u"资讯头条",8:u"资讯首页",9:u"视频首页",
             10:u"分站头条",11:u"分站轮播",12:u"分站文字推送",13:u"人物访谈"}
#1,3 单图轮播1*4
#2 5图轮播
#4 文字推送, 非图
#5,6 两排十个
#7 资讯头条 5 图
#8,13 两排八个
#9 视频首页 两排六个
# 10 1+4*2 分站
# 11 分站 单图轮播 1*4
#12 6, 非图
def article_modal_show(request):
    try:
        type = request.GET.get("type")
        article_id = request.GET.get("article_id")
        article_obj = Article.objects.get(pk=article_id)
        article_show_obj = ArticleShow.objects.filter(type=type)[0]
        type_temp = int(type)
        if type_temp in [1, 3]:
            return one_pic_carousel(request, article_obj, article_show_obj)
        if type_temp == 2:
            return five_pic_carousel(request, article_obj, article_show_obj)
        if type_temp == 4:
            return four_character(request, article_obj, article_show_obj)
        if type_temp in [5, 6]:
            return ten_pic_in_two_row(request, article_obj, article_show_obj)
        if type_temp == 7:
            return five_pic(request, article_obj, article_show_obj)
        if type_temp in [8, 13]:
            return eight_pic_in_two_row(request, article_obj, article_show_obj)
        if type_temp == 9:
            return six_pic_in_two_row(request, article_obj, article_show_obj)
        # if type_temp == 12:
        #     return six_character(request, article_obj, article_show_obj)
        # 分站部分 文章展示, 涉及到地区单独处理
        if type_temp in [10, 11, 12]:
            return sub_site_show(request, article_obj, article_show_obj)
    except Exception as e:
        LOG.error('Error happened when show article show modal. %s' % e)


def one_pic_carousel(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/one-pic-carousel.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def five_pic_carousel(request, article_obj, article_show_obj):
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    last_page = show_list.last().page
    data = list(dict(data=list()) for i in range(0, last_page)) if show_list else list()
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['position'] = show_obj.position
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data[show_obj.page-1]['data'].append(show_param)
        data[show_obj.page-1]['page'] = show_obj.page
    return render(request, "background/article/index-carousel.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})



def four_character(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/index-headline.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def ten_pic_in_two_row(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/headline-and-video.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def five_pic(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/news-headline.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def eight_pic_in_two_row(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/news-and-video-index.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def six_pic_in_two_row(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/video-index.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def six_character(request, article_obj, article_show_obj):
    data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    is_checked_list = article_obj.article_show.filter(type=type)
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for show_obj in show_list:
        show_param = dict()
        show_param['id'] = show_obj.id
        show_param['page'] = show_obj.page
        show_param['position'] = show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if show_obj.id == checked_position else False
        data.append(show_param)
    return render(request, "background/article/six-character.html",
                  {"data":data, "type_name":type_name,"article_id":article_id,
                   "desc":desc, "type":type})


def sub_site_show(request, article_obj, article_show_obj):
    data = list()
    area_data = list()
    type = article_show_obj.type
    type_name = article_show_obj.name
    desc = article_show_obj.desc
    # show_list = ArticleShow.objects.filter(type=type).order_by("page", "position")
    article_id = article_obj.id
    art_area = article_obj.area if article_obj.area else SupportedArea.objects.filter(province=u"全国")[0]
    is_disabled = True

    if art_area.province == u"全国":
        art_area = SupportedArea.objects.filter(~Q(province=u'全国'))[0]
        is_disabled = False

    province_list = SupportedArea.objects.filter(~Q(province=u'全国')).values("province").distinct().order_by("province")
    for i in range(0, len(province_list)):
        area_param = dict()
        area_param["index"] = i
        pro_obj = province_list[i]
        area_param['province'] = pro_obj['province']
        area_param['is_checked'] = False
        area_param['city'] = list()
        city_list = SupportedArea.objects.filter(province=pro_obj['province'])

        for city_obj in city_list:
            city_param = dict()
            city_param['id'] = city_obj.id
            city_param['is_checked'] = False
            if city_obj.id == art_area.id:
                area_param['is_checked'] = True
                city_param['is_checked'] = True
            city_param['city'] = city_obj.city
            area_param['city'].append(city_param)
        area_data.append(area_param)

    area_show_list = article_show_obj_list(type, art_area)
    is_checked_list = article_obj.article_show.filter(Q(type=type), Q(area=art_area))
    checked_position = is_checked_list[0].id if is_checked_list else -1
    for area_show_obj in area_show_list:
        show_param = dict()
        show_param['id'] = area_show_obj.id
        show_param['page'] = area_show_obj.page
        show_param['position'] = area_show_obj.position
        show_param['type'] = type
        show_param['is_checked'] = True if area_show_obj.id == checked_position else False
        data.append(show_param)
    if type == 10:
        return render(request, "background/article/subsite-headline.html",
                      {"data": data, "type_name": type_name, "article_id": article_id,
                       "desc": desc, "type": type, "area_json":simplejson.dumps(area_data),
                       "area":area_data, "is_disabled":is_disabled, "area_id":art_area.id})

    if type == 11:
        return render(request, "background/article/subsite-carousel.html",
                      {"data": data, "type_name": type_name, "article_id": article_id,
                       "desc": desc, "type": type, "area_json":simplejson.dumps(area_data),
                       "area":area_data, "is_disabled":is_disabled, "area_id":art_area.id})

    if type==12:
        return render(request, "background/article/six-character.html",
                      {"data": data, "type_name": type_name, "article_id": article_id,
                       "desc": desc, "type": type, "area_json":simplejson.dumps(area_data),
                       "area":area_data, "is_disabled":is_disabled, "area_id":art_area.id})


def sub_site_modal_body(request):
    data = list()
    # area_data = list()
    try:
        type = request.GET.get("type")
        article_id = request.GET.get("article_id")
        area_id = request.GET.get("area_id")
        article_obj = Article.objects.get(pk=article_id)
        # article_show_obj = ArticleShow.objects.filter(type=type)[0]
        area_obj = SupportedArea.objects.get(pk=area_id)
        # type = article_show_obj.type
        # type_name = article_show_obj.name
        # desc = article_show_obj.desc
        area_show_list = article_show_obj_list(type, area_obj)
        is_checked_list = article_obj.article_show.filter(Q(type=type), Q(area=area_obj))
        checked_position = is_checked_list[0].id if is_checked_list else -1
        for area_show_obj in area_show_list:
            show_param = dict()
            show_param['id'] = area_show_obj.id
            show_param['page'] = area_show_obj.page
            show_param['position'] = area_show_obj.position
            show_param['type'] = type
            show_param['is_checked'] = True if area_show_obj.id == checked_position else False
            data.append(show_param)
        if int(type)==10:
            return render(request, "background/article/subsite-headline-modal-body.html",
                          {"data": data})
        if int(type)==11:
            return render(request, "background/article/subsite-carousel-modal-body.html",
                          {"data": data})
        if int(type)==12:
            return render(request, "background/article/six-character-modal-body.html",
                          {"data": data})

    except Exception as e:
        LOG.error('Error happened when change sub website modal body. %s' % e)


        # else:
    #     is_exist_list = ArticleShow.objects.filter(Q(type=type), Q(area=art_area))
    #     if not is_exist_list:
    #         for show_obj in show_list:
    #             s = ArticleShow(type=type, area=art_area, is_app=show_obj.is_app,
    #                             position=show_obj.position, page=show_obj.page,
    #                             name=show_obj.name, desc=show_obj.desc)
    #             s.save()
    # is_checked_list = article_obj.article_show.filter(type=type)
    # checked_position = is_checked_list[0].id if is_checked_list else -1


def article_show_obj_list(type, area):
    show_list = ArticleShow.objects.filter(Q(type=type), Q(area=None)).order_by("page", "position")
    is_exist_list = ArticleShow.objects.filter(Q(type=type), Q(area=area))
    data_list = list()
    if not is_exist_list:
        for show_obj in show_list:
            s = ArticleShow(type=type, area=area, is_app=show_obj.is_app,
                            position=show_obj.position, page=show_obj.page,
                            name=show_obj.name, desc=show_obj.desc)
            s.save()
            data_list.append(s)
    else:
        return is_exist_list
    return data_list
    # is_checked_list = article_obj.article_show.filter(type=type)


def article_show_action(request):
    res = "fail"
    try:
        article_id = request.GET.get("article_id")
        type = request.GET.get("type")
        show_id = request.GET.get("show_id", "-1")
        area_id = request.GET.get("area_id", "-1")
        article_obj = Article.objects.get(pk=article_id)
        type_list = article_obj.article_show.all().filter(~Q(type=type)) \
            if int(type) not in [10, 11, 12] else \
            article_obj.article_show.all().filter(~Q(type=type),~Q(area_id=area_id))
        show_obj = ArticleShow.objects.get(pk=show_id) if int(show_id) != -1 else None
        type_obj_list =list(obj for obj in type_list)
        # 替换之前的选项
        if show_obj:
            on_article_obj_list = Article.objects.filter(article_show = show_obj)
            if on_article_obj_list:
                for on_article_obj in on_article_obj_list:
                    an_show_list = on_article_obj.article_show.all().filter(~Q(pk=show_id))
                    copy_show_list = list(co for co in an_show_list)
                    on_article_obj.article_show.clear()
                    for an_show_obj in copy_show_list:
                        on_article_obj.article_show.add(an_show_obj)
            type_obj_list.append(show_obj)
        article_obj.article_show.clear()
        for obj in type_obj_list:
            article_obj.article_show.add(obj)
        res = "success"
    except Exception as e:
        LOG.error('Error happened when submit article show position. %s' % e)
    return HttpResponse(simplejson.dumps({"res":res}))