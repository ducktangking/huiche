import simplejson
import logging

from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone

from hibiscus.models import UserAddress
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
ADDRESS_COLUMNS = ["address"]


def index(request):
    return render(request, 'background/user/address/index.html')


def new(request):
    return render(request, 'background/user/address/new.html')


def action_new(request):
    res = "success"
    try:
        addresss = request.POST["address"]
        is_default = 0
        if "def_addr" in request.POST:
            if request.POST["def_addr"] == "on":
                is_default = 1
                ua = UserAddress.objects.filter(is_default=1)
                if ua:
                    ua.update(is_default=0)
        UserAddress(address=addresss, is_default=is_default).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


@data_table(columns=ADDRESS_COLUMNS, model=UserAddress)
def data(request):
    try:
        sts = UserAddress.objects.filter(
            Q(address__contains=request.table_data["key_word"])
        ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["address"] = st.address
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params["is_default"] = st.is_default
            # st_params["pic_path"] = st.logo.picture.path
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get address index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                UserAddress.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete user address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def validate(request):
    res = "no"
    try:
        address = request.GET['userAddr']
        ua = UserAddress.objects.filter(address=address)
        if ua:
            res = "yes"
    except Exception as e:
        res = "error"
        LOG.error('Can not validate user address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def address(request):
    ad_list = []
    try:
        ads = UserAddress.objects.all()
        for ad in ads:
            ad_params = {}
            ad_params["address"] = ad.address
            ad_params["ad_id"] = ad.id
            ad_list.append(ad_params)
    except Exception as e:
        LOG.error('Get addresses error.%s' % e)
    return HttpResponse(simplejson.dumps(ad_list))
