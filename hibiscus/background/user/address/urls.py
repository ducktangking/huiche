# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background.user.address import views

urlpatterns = [
    url(r'^$', views.index, name='background-user-address-index'),
    url(r'^data/$', views.data, name='background-user-address-data'),
    url(r'^new/$', views.new, name='background-user-address-new'),
    url(r'^delete/action/$', views.action_delete, name="background-user-address-action-delete"),
    url(r'^new/action/$', views.action_new, name="background-user-address-action-new"),
    url(r'^validate/$', views.validate, name="background-user-address-validate"),
    url(r'^info/ad/$', views.address, name="background-user-address-info"),
]
