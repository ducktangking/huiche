# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background.user.users import views

urlpatterns = [
    url(r'^$', views.index, name='background-user-users-index'),
    url(r'^data/$', views.data, name='background-user-users-data'),
    url(r'^new/$', views.new, name='background-user-users-new'),
    url(r'^delete/action/$', views.action_delete, name="background-user-users-action-delete"),
    url(r'^new/action/$', views.action_new, name="background-user-users-action-new"),
    url(r'^update/$', views.edit, name="background-user-users-edit"),
    url(r'^update/action/$', views.edit_action, name="background-user-users-edit-action"),
    url(r'^validate/name/$', views.name_validate, name="background-user-name-validate"),
    url(r'^active/$', views.active_set, name="background-user-active"),
    url(r'^pwd/reset/$', views.password_reset, name="background-user-pwd-reset"),
]
