import simplejson
import logging
import hashlib

from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone

from hibiscus.models import HcAdmin, Users, UserInformation, UserAddress, Picture, Portrait
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file, DEFAULT_PASSWORD, pwd_hash

LOG = logging.getLogger(__name__)
ADMIN_COLUMNS = ["username", "name", "phone", "last_login", "created_at", "is_active"]


def index(request):
    return render(request, 'background/user/users/index.html')


def new(request):
    return render(request, 'background/user/users/new.html')


def action_new(request):
    res = "success"
    try:
        info = action_info_new(request)
        username = request.POST['username']
        phone = request.POST["phone"]
        password = request.POST['password']

        pwd = hashlib.sha256(password).hexdigest()
        Users(information=info, username=username, phone=phone,
               password=pwd).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


@data_table(columns=ADMIN_COLUMNS, model=Users)
def data(request):
    st_list = []
    key_word = request.table_data["key_word"]
    try:
        if request.table_data["order_cloumn"] != 1:
            sts = Users.objects.filter(
                Q(username__contains=key_word) | Q(phone__contains=key_word)
            ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
        else:
            if request.table_data["order_dir"] == "desc":
                sts = Users.objects.filter(
                    Q(username__contains=key_word) | Q(phone__contains=key_word)
                ).order_by("-information__name")[request.table_data["start"]:request.table_data["end"]]
            else:
                sts = Users.objects.filter(
                    Q(username__contains=key_word) | Q(phone__contains=key_word)
                ).order_by("information__name")[request.table_data["start"]:request.table_data["end"]]

        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["username"] = st.username
            st_params["realname"] = st.information.name if st.information else ""
            st_params["phone"] = st.phone
            st_params["is_active"] = st.is_active
            st_params['register_time'] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params['login_time'] = st.last_login
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get users index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Users.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete users address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def action_info_new(request):
    info1 = None
    try:
        por = request.FILES["portrait"]
        por_path = deal_with_upload_file(por)
        pic_obj = Picture.objects.create(name=por.name, path=por_path)
        # portrait obj
        por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST["infoName"]
        sex = request.POST["sex"]
        birthday = request.POST["birthday"]
        tel = request.POST["tel"]
        qq = request.POST['qq']
        weibo = request.POST["weibo"]
        weixin = request.POST["weixin"]

        info1 = UserInformation(name=name, portrait=por_obj, sex=sex, birthday=birthday,
                                tel=tel, qq=qq, weibo=weibo, weixin=weixin)
        info1.save()
        if "InfoAddr" in request.POST:
            addr = request.POST["InfoAddr"]
            a1 = UserAddress.objects.create(address=addr)
            info1.addresses.add(a1)
        if "addrList" in request.POST:
            ids = request.POST.getlist("addrList")
            uads = UserAddress.objects.filter(id__in=ids)
            for uad in uads:
                info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Error happened when create user info. %s' % e)

    return info1


def edit(request):
    user_id = request.GET.get('user_id', "")
    user_obj = Users.objects.get(pk=user_id)
    user_data = user_detail_data(user_id)
    return render(request, 'background/user/users/edit.html',
                  {"user_obj": user_obj, 'user_data': user_data})


def user_detail_data(admin_id):
    admin_data = {}
    try:
        # admin_id = request.session["_auth_user_id"]
        # admin_id = request.GET['admin_id'] if 'admin_id' in request.GET else 1
        admin_obj = Users.objects.get(pk=admin_id)
        admin_data["sex"] = admin_obj.information.sex if admin_obj.information else ""
        if admin_obj.information and admin_obj.information.portrait:
            admin_data["portrait"] = admin_obj.information.portrait.picture.path
        else:
            admin_data["portrait"] = ""
        admin_data['realname'] = admin_obj.information.name if admin_obj.information else ""
        admin_data["email"] = admin_obj.information.email if admin_obj.information else ""
        # admin_data['province'] = admin_obj.area.province if admin_obj.area else ''
        # admin_data['city'] = admin_obj.area.city if admin_obj.area else ''
        admin_data['birthday'] = admin_obj.information.birthday if admin_obj.information else ""
        admin_data['qq'] = admin_obj.information.qq if admin_obj.information else ""
        admin_data['weibo'] = admin_obj.information.weibo if admin_obj.information else ""
        admin_data['weixin'] = admin_obj.information.weixin if admin_obj.information else ""
        admin_data['tel'] = admin_obj.information.tel if admin_obj.information else ""
    except Exception as e:
        LOG.error('Can not get admin detail.%s' % e)
    return admin_data


def edit_action(request):
    res = "success"
    try:
        user_id = request.POST.get('user_id', "")
        user_obj = Users.objects.get(pk=user_id)
        info = edit_user_information(request, user_obj)
        username = request.POST['username']
        phone = request.POST["phone"]
        # password = request.POST['password']

        # pwd = hashlib.sha256(password).hexdigest()
        # user_obj.update(username=username, phone=phone)
        user_obj.username = username
        user_obj.phone = phone
        user_obj.save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


def edit_user_information(request, user_obj):
    res = "success"
    try:
        por_obj = ""
        if "portrait" in request.FILES:
            por = request.FILES["portrait"]
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST.get("infoName", "")
        email = request.POST.get("email", "")
        sex = request.POST.get("sex", "")
        birthday = request.POST.get("birthday", None)
        tel = request.POST.get("tel", "")
        qq = request.POST.get('qq', "")
        weibo = request.POST.get("weibo", "")
        weixin = request.POST.get("weixin", "")
        if user_obj.information:
            if por_obj:

                user_obj.information.name = name
                user_obj.information.sex = sex
                user_obj.information.portrait = por_obj
                user_obj.information.tel = tel
                user_obj.information.qq = qq
                user_obj.information.weibo = weibo
                user_obj.information.weixin = weixin
                user_obj.information.email = email


            else:
                user_obj.information.name = name
                user_obj.information.sex = sex
                user_obj.information.tel = tel
                user_obj.information.qq = qq
                user_obj.information.weibo = weibo
                user_obj.information.weixin = weixin
                user_obj.information.email = email

            if birthday and birthday != "None":
                user_obj.information.birthday = birthday
            user_obj.information.save()
        else:
            if por_obj:
                info1 = UserInformation(name=name, portrait=por_obj, sex=sex,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            else:
                info1 = UserInformation(name=name,  sex=sex,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            if birthday and birthday != "None":
                info1.birthday = birthday
            info1.save()
            user_obj.information = info1
            user_obj.save()


        # user_obj.information.addr
        # if "InfoAddr" in request.POST:
        #     addr = request.POST["InfoAddr"]
        #     a1 = UserAddress.objects.create(address=addr)
        #     info1.addresses.add(a1)
        # if "addrList" in request.POST:
        #     ids = request.POST.getlist("addrList")
        #     uads = UserAddress.objects.filter(id__in=ids)
        #     for uad in uads:
        #         info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Can not edit user information. %s' % e)
        res = "fail"
    return res


def name_validate(request):
    res = "no"
    try:
        user_name = request.GET['user_name']
        action = request.GET.get('action')

        if action == "edit":
            pid = request.GET['id']
            producer = Users.objects.filter(username=user_name).exclude(pk=pid)
        else:
            producer = Users.objects.filter(username=user_name)
        if producer:
            res = "exists"
            return HttpResponse(res)
    except Exception as e:
        res = "error"
        LOG.error('validate producer name error. %s' % e)
    return HttpResponse(res)


def active_set(request):
    try:
        ac = request.GET['is_active']
        user_id = request.GET['user_id']
        pobj = Users.objects.get(pk=user_id)
        if ac == "true":
            pobj.is_active = False
            pobj.save()
        else:
            pobj.is_active = True
            pobj.save()
    except Exception as e:
        LOG.error('Can not change producer active status. %s' % e)
    return redirect('background-user-users-index')


def password_reset(request):
    res = "success"
    try:
        producer_id = request.GET['admin_id']
        p = Users.objects.get(pk=producer_id)
        p.password = pwd_hash(DEFAULT_PASSWORD)
        p.save()
    except Exception as e:
        res = "fail"
        LOG.error('Reset producer password error. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
