# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background.user.information import views

urlpatterns = [
    url(r'^$', views.index, name='background-user-info-index'),
    url(r'^data/$', views.data, name='background-user-info-data'),
    url(r'^new/$', views.new, name='background-user-info-new'),
    url(r'^delete/action/$', views.action_delete, name="background-user-info-action-delete"),
    url(r'^new/action/$', views.action_new, name="background-user-info-action-new"),
    url(r'^validate/$', views.validate, name="background-user-info-validate"),
]
