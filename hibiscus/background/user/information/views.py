import simplejson
import logging

from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone

from hibiscus.models import UserAddress, Picture, UserInformation, Portrait
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
INFO_COLUMNS = ["name", "sex", "birthday", "tel", "qq", "weibo", "weixin"]


def index(request):
    return render(request, 'background/user/info/index.html')


def new(request):
    return render(request, 'background/user/info/new.html')


def action_new(request):
    res = "success"
    try:
        por = request.FILES["portrait"]
        por_path = deal_with_upload_file(por)
        pic_obj = Picture.objects.create(name=por.name, path=por_path)
        # portrait obj
        por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST["infoName"]
        sex = request.POST["sex"]
        birthday = request.POST["birthday"]
        tel = request.POST["tel"]
        qq = request.POST['qq']
        weibo = request.POST["weibo"]
        weixin = request.POST["weixin"]

        info1 = UserInformation(name=name, portrait=por_obj, sex=sex, birthday=birthday,
                                tel=tel, qq=qq, weibo=weibo, weixin=weixin)
        info1.save()
        if "InfoAddr" in request.POST:
            addr = request.POST["InfoAddr"]
            a1 = UserAddress.objects.create(address=addr)
            info1.addresses.add(a1)
        if "addrList" in request.POST:
            ids = request.POST.getlist("addrList")
            uads = UserAddress.objects.filter(id__in=ids)
            for uad in uads:
                info1.addresses.add(uad)

    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user info. %s' % e)

    return HttpResponse(res)


@data_table(columns=INFO_COLUMNS, model=UserInformation)
def data(request):
    try:
        key_word = request.table_data["key_word"]
        sts = UserInformation.objects.filter(
            Q(name__contains=key_word) | Q(sex__contains=key_word) | Q(tel__contains=key_word) |
            Q(qq__contains=key_word) | Q(weibo__contains=key_word) | Q(weixin__contains=key_word)
        ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            # st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params["sex"] = st.sex
            st_params["birthday"] = st.birthday.strftime('%Y-%m-%d %H:%M:%S')
            st_params["tel"] = st.tel
            st_params["weibo"] = st.weibo
            st_params["weixin"] = st.weixin
            st_params["pro_path"] = st.portrait.picture.path
            st_params["qq"] = st.qq
            # st_params["pic_path"] = st.logo.picture.path
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get info index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                UserInformation.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete user info. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def validate(request):
    res = "no"
    try:
        address = request.GET['userAddr']
        ua = UserAddress.objects.filter(address=address)
        if ua:
            res = "yes"
    except Exception as e:
        res = "error"
        LOG.error('Can not validate user info. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
