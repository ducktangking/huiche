# -*- coding:utf-8 -*-
from django.conf.urls import url, include
# from hibiscus.background import views

urlpatterns = [
    url(r'^address/', include("hibiscus.background.user.address.urls")),
    url(r'^admin/', include("hibiscus.background.user.admin.urls")),
    url(r'^adviser/', include("hibiscus.background.user.adviser.urls")),
    url(r'^information/', include("hibiscus.background.user.information.urls")),
    url(r'^producer/', include("hibiscus.background.user.producer.urls")),
    url(r'^users/', include("hibiscus.background.user.users.urls")),
]
