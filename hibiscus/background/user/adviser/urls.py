# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background.user.adviser import views

urlpatterns = [
    url(r'^$', views.index, name='background-user-adviser-index'),
    url(r'^new/$', views.new, name='background-user-adviser-new'),
    url(r'^delete/action/$', views.action_delete, name="background-user-adviser-action-delete"),
    url(r'^new/action/$', views.action_new, name="background-user-adviser-action-new"),
    url(r'^data/$', views.data, name='background-user-adviser-data'),
    url(r'^info/detail/$', views.adviser_info, name="background-user-adviser-info"),
    url(r'^info/data/$', views.info_data, name="background-user-adviser-info-data"),
    url(r'^name/validate/$', views.name_validate, name="background-adviser-name-validate"),
    url(r'^active/$', views.active_set, name="background-adviser-active"),
    url(r'producer/$', views.producer_advisers, name="background-producer-advisers"),
]
