import simplejson
import logging
import hashlib

from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponse

from hibiscus.background.public import action_info_new
from hibiscus.models import Producer, Adviser
from hibiscus.decorators import data_table
from hibiscus.background.public import pwd_hash

LOG = logging.getLogger(__name__)
ADVISER_COLUMNS = ["type", "username", "phone", "weight"]


def index(request):
    if "type" in request.GET:
        adviser_type = request.GET['type']
    else:
        adviser_type = 0
    return render(request, 'background/user/adviser/index.html', {"adviser_type": adviser_type})


def new(request):
    # producers = [{"id": item.id, "name": item.username} for item in Producer.objects.all()]
    producer_id = request.session['_auth_user_id']
    return render(request, 'background/user/adviser/new.html', {"producer_id": producer_id})


def action_new(request):
    res = "success"
    try:
        info = action_info_new(request)
        username = request.POST['username']
        phone = request.POST["phone"]
        password = request.POST['password']
        producer_id = request.POST["advProducer"]
        ad_type = request.POST["adType"]
        producer = Producer.objects.get(id=producer_id)
        weight = request.POST["weight"]
        loop = True
        if "loop" in request.POST:
            is_loop = request.POST["loop"]
            if is_loop == "off":
                loop = False

        pwd = pwd_hash(password)
        is_active = True
        if "isActive" in request.POST:
            ac = request.POST['isActive']
            if ac == "off":
                is_active = False
        Adviser(producer=producer, information=info, username=username, phone=phone,
                password=pwd, weight=weight, is_loop=loop, is_active=is_active, type=ad_type).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


def name_validate(request):
    res = "no"
    try:
        adviser_name = request.GET['user_name']
        advisers = Adviser.objects.filter(username=adviser_name)
        if advisers:
            res = "exists"
            return HttpResponse(res)

    except Exception as e:
        res = "error"
        LOG.error('Can not validate advieser name. %s' % e)
    return HttpResponse(res)


@data_table(columns=ADVISER_COLUMNS, model=Adviser)
def data(request):
    key_word = request.table_data["key_word"]
    adviser_type = request.GET['adviser_type']
    try:
        producerId = request.session['_auth_user_id']
        pr_obj = Producer.objects.get(pk=producerId)
        sts = pr_obj.adviser_set.all().filter(Q(type__exact=adviser_type),
            Q(username__contains=key_word) | Q(phone__contains=key_word) |
            Q(password__contains=key_word) | Q(type__contains=key_word)
        ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        # sts = Adviser.objects.filter(Q(type__exact=adviser_type),
        #     Q(username__contains=key_word) | Q(phone__contains=key_word) |
        #     Q(password__contains=key_word) | Q(type__contains=key_word)
        # ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["username"] = st.username
            st_params["phone"] = st.phone
            st_params["password"] = st.password
            st_params["is_active"] = st.is_active
            st_params["type"] = st.type
            st_params["producer"] = st.producer.username
            st_params["weight"] = st.weight
            st_params["loop"] = st.is_loop

            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get admin index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Producer.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def adviser_info(request):
    user_id = request.GET["user_id"]
    return render(request, 'background/user/adviser/info-detail.html', {"user_id": user_id})


def info_data(request):
    info_params = {}
    try:
        user_id = request.GET["user_id"]
        ad = Adviser.objects.get(id=user_id)
        info_params["name"] = ""
        info_params["sex"] = ""
        info_params["birthday"] = ""
        info_params["tel"] = ""
        info_params["qq"] = ""
        info_params["weibo"] = ""
        info_params["weixin"] = ""
        info_params["email"] = ""
        info_params["address"] = []
        info_params['username'] = ad.username
        info_params['type'] = ad.type
        info_params['phone'] = ad.phone

        info = ad.information
        info_params["address"] = []
        if info:
            if info.addresses:
                info_params["address"] = [ad.address for ad in info.addresses.all()]

            if info.portrait:
                info_params["por_path"] = info.portrait.picture.path
            else:
                info_params["por_path"] = None
            info_params["name"] = info.name if info.name else ""
            info_params["sex"] = info.sex if info.sex else ""
            info_params["birthday"] = info.birthday.strftime('%Y-%m-%d %H:%M:%S') if info.birthday else ""
            info_params["tel"] = info.tel if info.tel else ""
            info_params["qq"] = info.qq if info.qq else ""
            info_params["weibo"] = info.weibo if info.weibo else ""
            info_params["weixin"] = info.weixin if info.weixin else ""
            info_params["email"] = info.email if info.email else ""
    except Exception as e:
        LOG.error('Get admin error. %s' % e)
    return HttpResponse(simplejson.dumps(info_params))


def active_set(request):
    try:
        ac = request.GET['is_active']
        user_id = request.GET['user_id']
        pobj = Adviser.objects.get(pk=user_id)
        if ac == "true":
            pobj.is_active = False
            pobj.save()
        else:
            pobj.is_active = True
            pobj.save()
    except Exception as e:
        LOG.error('Can not change producer active status. %s' % e)
    return redirect('background-user-adviser-index')


def producer_advisers(request):
    adviser_list = []
    try:
        producer_id = request.session['_auth_user_id']
        advisers = Adviser.objects.filter(producer__id=producer_id)
        for ad in advisers:
            adp = {}
            adp['id'] = ad.id
            adp['name'] = ad.username
            adviser_list.append(adp)
    except Exception as e:
        LOG.error('Error happened when get producer_advisers. %s' % e)
    return HttpResponse(simplejson.dumps(adviser_list))
