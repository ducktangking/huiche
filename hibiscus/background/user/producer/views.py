#-*- coding:utf-8 -*-
import simplejson
import logging
import hashlib
import datetime

from django.db.models import Q, Count
from django.utils import timezone
from django.shortcuts import render, redirect
from django.http import HttpResponse

from hibiscus.background.public import action_info_new
from hibiscus.models import Producer, SupportedArea, ProducerCarSeries, UpkeepMaterialModel, \
    CarQuote, Car, UpkeepPackage, Upkeep, SeriesUpkeepQuote, Brand, SeriesUpkeepInfo, UpkeepSeries, \
    UpkeepType, Picture, Portrait, UserInformation, UserAddress, HcAdmin
from hibiscus.decorators import data_table
from hibiscus.background.public import pwd_hash, deal_with_upload_file, DEFAULT_PASSWORD

LOG = logging.getLogger(__name__)
PRODUCER_COLUMNS = ["id", "information", "type", "username", "phone", "repair_phone", "is_recommend", "is_active"]
UPKEEP_SERIES_COLUMNS = ["series__name", "produce_year", "cc"]
LEVEL_ONE = 1
LEVEL_TWO = 2
LEVEL_THREE = 3


def index(request):
    return render(request, 'background/user/producer/index.html')


def new(request):
    areas = [{"id": item.id, "province": item.province, "city": item.city}
             for item in SupportedArea.objects.all()]
    admin_id = ''
    admin_obj = None
    if '_auth_user_id' in request.session:
       admin_id = request.session['_auth_user_id']
    if admin_id:
        admin_obj = HcAdmin.objects.get(pk=admin_id)
    return render(request, 'background/user/producer/new.html', {"areas": areas,
                                                                 "admin_obj": admin_obj, "admin_id": admin_id})


def action_new(request):
    """
        20170116经销商信息大改，添加公司简称、全称字段，暂定数据库不更改，简称归入
        userinformation中name字段，方便其他接口读取，全称取producer中full_name字段
        如果发现该字段被使用,再进行替换，另：QQ，tel，weixin，weibo等联系方式删除
    """
    res = "success"
    try:
        admin_id = ''
        admin_obj = ''
        if '_auth_user_id' in request.session:
            admin_id = request.session['_auth_user_id']
        if admin_id:
            admin_obj = HcAdmin.objects.get(pk=admin_id)
        area_id = request.POST["supArea"]
        username = request.POST['username']
        producer_rank = request.POST.get("producerRank", '4')
        info = producer_action_info_new(request)
        full_name = request.POST.get('fullName', "")
        sale_phone = request.POST['salePhone']
        repair_phone = request.POST['repairPhone']
        area = SupportedArea.objects.get(id=area_id)
        ac = request.POST.get("isActive", "")
        if ac == "on":
            is_active = True
        else:
            is_active = False
        recommend = request.POST.get("isRecommend", "")
        if recommend == "on":
            is_recommend = True
        else:
            is_recommend = False
        password = DEFAULT_PASSWORD
        pwd = pwd_hash(password)
        Producer(area=area, is_recommend=is_recommend, repair_phone=repair_phone, information=info, username=username,
                 password=pwd, is_active=is_active, type=producer_rank,
                 full_name=full_name, phone=sale_phone, creator=admin_obj).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


@data_table(columns=PRODUCER_COLUMNS, model=Producer)
def data(request):
    st_list = []
    key_word = request.table_data["key_word"]
    try:
        sts = Producer.objects.filter(
            Q(username__contains=key_word) | Q(phone__contains=key_word) | Q(password__contains=key_word) |
            Q(repair_phone__contains=key_word)
        ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params['pic'] = st.information.portrait.picture.path if st.information and st.information.portrait else ""
            st_params["username"] = st.username
            st_params["phone"] = st.phone
            st_params["repair_phone"] = st.repair_phone
            st_params["password"] = st.password
            st_params['realname'] = st.information.name if st.information else ""
            st_params['is_active'] = st.is_active
            st_params["is_recommend"] = st.is_recommend
            st_params['rank'] = st.type
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get admin index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Producer.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_info(request):
    user_id = request.GET["user_id"]
    return render(request, 'background/user/producer/info-detail.html', {"user_id": user_id})


def info_data(request):
    info_params = {}
    try:
        user_id = request.GET["user_id"]
        ad = Producer.objects.get(id=user_id)
        info = ad.information
        info_params["address"] = []
        info_params["por_path"] = ""
        info_params["name"] = ""
        info_params["sex"] = ""
        info_params["birthday"] = ""
        info_params["tel"] = ""
        info_params["qq"] = ""
        info_params["weibo"] = ""
        info_params["weixin"] = ""
        info_params['email'] = ""
        if info:
            info_params["address"] = [ad.address for ad in info.addresses.all()]
            info_params["por_path"] = info.portrait.picture.path
            info_params["name"] = info.name
            info_params["sex"] = info.sex
            info_params["birthday"] = info.birthday.strftime('%Y-%m-%d %H:%M:%S')
            info_params["tel"] = info.tel
            info_params["qq"] = info.qq
            info_params["weibo"] = info.weibo
            info_params["weixin"] = info.weixin
            info_params['email'] = info.email
    except Exception as e:
        LOG.error('Get admin error. %s' % e)
    return HttpResponse(simplejson.dumps(info_params))


def producer_car_series_index(request):
    producer_id = request.GET.get("producer_id", None)
    producer_name = ""
    if producer_id:
        producer = Producer.objects.get(pk=producer_id)
        producer_name = producer.username

    return render(request, "background/user/producer/car-series.html", {"producer_id": producer_id,
                                                                        "producer_name": producer_name,
                                                                        "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


@data_table(columns=["producer"], model=ProducerCarSeries)
def producer_car_series_data(request):
    st_list = []
    producer_id = request.GET.get("producer_id", "")
    try:
        sts = ProducerCarSeries.objects.filter(Q(producer__id=producer_id),
                                               Q(brand__name__contains=request.table_data['key_word']) |
                                               Q(producer__username__contains=request.table_data['key_word'])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
        for st in sts:
            st_params = {}
            st_params['DT_RowId'] = st.id
            st_params['producer_name'] = st.producer.username
            st_params['brand_name'] = st.brand.name
            st_params['type'] = st.type
            st_list.append(st_params)
    except Exception as e:
        LOG.error('Can not get producer proxy car data. %s' % e)
    return st_list


def producer_car_series_delete(request):
    res = "success"
    try:
        producer_id = request.POST['producer_id']
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                ProducerCarSeries.objects.filter(producer__id=producer_id).filter(pk=id_v).delete()
    except Exception as e:
        LOG.error('Delete car series error. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_car_series(request):
    car_series = []
    exists_series = []
    other_series = []
    try:
        producer_id = request.GET.get("producer_id", None)
        pro_obj = Producer.objects.get(pk=producer_id)
        producer_series_type = request.GET["producer_series_type"]

        exists_car_series = pro_obj.producercarseries_set.all(). \
            filter(Q(type=int(producer_series_type)))
        all_series = list(Brand.objects.filter(parent__isnull=False))
        all_exists_cars = pro_obj.producercarseries_set.all()
        for s3 in all_exists_cars:
            s1_params = {}
            s1_params['series_name'] = s3.brand.name
            s1_params['series_id'] = s3.brand.id
            s1_params['producer_type'] = s3.type
            # s1_params['material_type'] = s3.material_type
            exists_series.append(s1_params)

        for s1 in exists_car_series:
            if s1.brand in all_series:
                all_series.remove(s1.brand)

        for s2 in all_series:
            s2_params = {}
            s2_params['series_id'] = s2.id
            s2_params['series_name'] = s2.name
            s2_params['initial'] = s2.initial
            other_series.append(s2_params)
        car_series.append(exists_series)
        car_series.append(other_series)
        # exsts_series = ProducerCarSeries.
    except Exception as e:
        LOG.error('Can not get car series for producer. %s' % e)
    return HttpResponse(simplejson.dumps(car_series))


def set_producer_series_car_action(request):
    res = "success"
    try:
        producer_id = request.POST['producer_id']
        pro_obj = Producer.objects.get(pk=producer_id)
        se_ids = request.POST.getlist("other_series")
        series = Brand.objects.filter(id__in=se_ids)
        producer_type = request.POST['adviserType']
        for s in series:
            p1 = ProducerCarSeries.objects.create(producer=pro_obj, brand=s, type=producer_type)

    except Exception as e:
        LOG.error('Set producer car series error. %s' % e)
        res = 'fail'
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_car_quote(request):
    try:
        producer_id = ""
        if '_auth_user_id' in request.session:
            producer_id = request.session['_auth_user_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        all_cars = car_quote_real_data(producer_obj)
    except Exception as e:
        LOG.error('Can not get car quote index. %s' % e)
    return render(request, 'background/user/producer/car-quote.html',
                  {"producer_id": producer_id, "all_cars": all_cars})


def car_quote_real_data(producer_obj):
    all_cars = []
    try:
        # cars = producer_obj.car_quote.all()
        car_series = ProducerCarSeries.objects.filter(producer__id=producer_obj.id, type=0)
        for cs in car_series:
            brand = cs.brand
            cars = Car.objects.filter(series__brand=brand)
            for car in cars:
                car_params = {}
                car_params['quote_price'] = car.price
                car_params['sale_price'] = ''
                car_params['mail_price'] = ''
                car_params['sale_time'] = ''
                car_params['mail_time'] = ''
                already_quote = car.carquote_set.all().filter(producer__id=producer_obj.id)
                if already_quote:
                    car_params['sale_price'] = already_quote[0].sale_price
                    car_params['mail_price'] = already_quote[0].mail_price
                    if already_quote[0].sale_time:
                        car_params['sale_time'] = already_quote[0].sale_time
                    else:
                        car_params['sale_time'] = '长效'
                    if already_quote[0].mail_time:
                        car_params['mail_time'] = already_quote[0].mail_time
                    else:
                        car_params['mail_time'] = '长效'
                if car_params['mail_time'] == '长效':
                    car_params['is_mail_time'] = True
                else:
                    car_params['is_mail_time'] = False
                if car_params['sale_time'] == '长效':
                    car_params['is_sale_time'] = True
                else:
                    car_params['is_sale_time'] = False
                car_params["car_name"] = car.name
                car_params["producer_id"] = producer_obj.id
                # car_params['price'] = car.price
                car_params["car_id"] = car.id
                car_params['car_series'] = car.series.name
                car_params['producer_type'] = cs.type
                car_params['material_type'] = cs.material_type
                all_cars.append(car_params)
    except Exception as e:
        LOG.error('Can not get producer car quote data. %s') % e
    return all_cars


def car_quote_data(request):
    try:
        producer_id = request.GET['producer_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        all_cars = car_quote_real_data(producer_obj)
    except Exception as e:
        LOG.error('Can not get car quote data. %s' % e)
    return HttpResponse(simplejson.dumps(all_cars))


def car_quote_action(request):
    res = "success"
    try:
        car_id = request.POST['carId']
        car_obj = Car.objects.get(pk=car_id)
        quote_price = car_obj.price
        sale_price = request.POST['salePrice']
        mail_price = request.POST['mailPrice']
        mail_time = request.POST['mailTime']
        producer_id = request.POST['producer_id']
        sale_time = request.POST['saleTime']
        if mail_time == u'长效' or mail_time == '':
            mail_time = None
        if sale_time == u'长效' or sale_time == '':
            sale_time = None
        if not sale_price:
            sale_price = quote_price
        if not mail_price:
            mail_price = quote_price
        pro_obj = Producer.objects.get(pk=producer_id)
        quote_obj = CarQuote.objects.filter(car__id=car_id, producer__id=producer_id)
        if quote_obj:
            quote_obj.update(price=quote_price, sale_price=sale_price, mail_price=mail_price, sale_time=sale_time, mail_time=mail_time)
        else:
            CarQuote.objects.create(car=car_obj, producer=pro_obj, price=quote_price, sale_price=sale_price, mail_price=mail_price, sale_time=sale_time, mail_time=mail_time)
    except Exception as e:
        res = "fail"
        LOG.error('Can not publish car quote. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def car_quote_delete(request):
    res = 'success'
    try:
        carId = request.GET['carId']
        producerId = request.GET['producerId']
        CarQuote.objects.filter(car__id=carId, producer__id=producerId).delete()
    except Exception as e:
        LOG.error('Delete car quote error.%s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def batch_car_quote_delete(request):
    res = 'success'
    try:
        data_list = request.GET['data_list']
        producer_id = request.GET['producer_id']
        car_id_list = data_list.split(',')[:-1]
        if car_id_list:
            for car_id in car_id_list:
                CarQuote.objects.filter(car__id=car_id, producer__id=producer_id).delete()
    except Exception as e:
        LOG.error('Delete batch car quote error.%s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_upkeep_quote(request):
    producer_id = ""
    try:
        producer_id = request.GET["producer_id"]
    except Exception as e:
        LOG.error('Can not get upkeep quote. %s' % e)
    return render(request, 'background/user/producer/upkeep-quote.html', {"producer_id": producer_id})


def producer_upkeep_quote_data(request):
    all_data = []
    try:
        if '_auth_user_id' in request.session:
            producer_id = request.session['_auth_user_id']
        else:
            producer_id = request.GET.get("producer_id", None)
        pcars = ProducerCarSeries.objects.filter(producer__id=producer_id, type__in=[1, ])
        ss = SeriesUpkeepInfo.objects.filter(upkeep_series__series__brand__in=[p.brand for p in pcars])
        # car_series = ProducerCarSeries.objects.filter(Q(producer__id=producer_id), Q(type__in=[1, ]))
        # for cs in car_series:
        # series_upkeep = cs.series.seriesupkeepinfo_set.all() #seriesUpkeepInfo object
        series_upkeep = ss
        for su in series_upkeep:
            su_params = {}
            upkeeps = su.upkeep
            upkeep_materials = su.upkeep_material_model
            if not upkeeps and not upkeep_materials:
                continue

            already_quote = su.seriesupkeepquote_set.all()

            su_params["series_name"] = su.upkeep_series.series.name
            su_params["series_id"] = su.upkeep_series.id

            su_params['upkeep_role'] = ""
            if upkeeps:
                su_params['upkeep_id'] = upkeeps.id
                su_params['upkeep_name'] = upkeeps.name
                su_params['upkeep_role'] = "banjin"
                su_params['quote_price'] = ""
                su_params['quote_labor'] = ""
                if already_quote:
                    su_params['quote_price'] = already_quote[0].price
                    su_params['quote_labor'] = already_quote[0].labor
                    su_params['id'] = already_quote[0].id

            if upkeep_materials:
                su_params['material_id'] = upkeep_materials.id
                su_params['material_name'] = upkeep_materials.name
                su_params['material_type'] = upkeep_materials.type
                su_params['upkeep_role'] = "weixiu"
                su_params['quote_price'] = ""
                su_params['quote_labor'] = ""
                if already_quote:
                    su_params['quote_price'] = already_quote[0].price
                    su_params['quote_labor'] = already_quote[0].labor
                    su_params['id'] = already_quote[0].id

            su_params["producer_id"] = producer_id
            all_data.append(su_params)

    except Exception as e:
        LOG.error('Error happened when get upkeep quote data. %s' % e)
    # return HttpResponse(simplejson.dumps(all_data))
    return render(request, 'background/user/producer/upkeep-quote.html',
                  {"all_data": all_data, "producer_id": producer_id})


def producer_upkeep_quote_action(request):
    res = "success"
    try:
        producer_id = request.POST['producer_id']
        series_id = request.POST['seriesId']
        pro_obj = Producer.objects.get(pk=producer_id)
        series_obj = UpkeepSeries.objects.get(pk=series_id)
        action_type = request.POST['actionType']
        if "upkeepId" in request.POST:
            upkeepId = request.POST['upkeepId']
            up_quote_price = request.POST['upkeepQuotePrice']
            labor = request.POST['labor']
            upkeep_obj = Upkeep.objects.get(pk=upkeepId)
            up_series_info = upkeep_obj.seriesupkeepinfo_set.get(upkeep_series=series_obj)
            if action_type == 'new':
                SeriesUpkeepQuote.objects.create(price=up_quote_price, labor=labor, producer=pro_obj,
                                                 series_upkeep_info=up_series_info)
            else:
                SeriesUpkeepQuote.objects.filter(series_upkeep_info=up_series_info,
                                                 producer=pro_obj).update(price=up_quote_price, labor=labor)

        if "upkeepMaterialId" in request.POST:
            upkeepMaterialId = request.POST['upkeepMaterialId']
            up_material_price = request.POST['upkeepMaterialQuotePrice']
            labor = request.POST['labor']
            material_obj = UpkeepMaterialModel.objects.get(pk=upkeepMaterialId)
            material_series_info = material_obj.seriesupkeepinfo_set.filter(upkeep_series=series_obj)[0]
            if action_type == 'new':
                SeriesUpkeepQuote.objects.create(price=up_material_price, labor=labor, producer=pro_obj,
                                                 series_upkeep_info=material_series_info)
            else:
                SeriesUpkeepQuote.objects.filter(series_upkeep_info=material_series_info,
                                                 producer=pro_obj).update(price=up_material_price, labor=labor)

    except Exception as e:
        res = "fail"
        LOG.error('Can not config upkeep quote. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def upkeep_quote_delete(request):
    res = "success"
    try:
        suq_id = request.GET['up_series_quote_id']
        SeriesUpkeepQuote.objects.get(pk=suq_id).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Delete upkeep quote error. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def admin_car_quote_index(request):
    return render(request, 'background/user/producer/admin-car-quote.html')


@data_table(
    columns=["car__series__brand__name", "car__series__name", "car__name", "producer__username", "price", "weight"],
    model=CarQuote)
def admin_car_quote_data(request):
    st_list = []
    try:
        sts = CarQuote.objects.filter(Q(car__name__contains=request.table_data['key_word']) |
                                      Q(producer__username__contains=request.table_data['key_word'])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        for st in sts:
            st_params = {}
            st_params['brand_name'] = st.car.series.brand.name
            st_params['series_name'] = st.car.series.name
            st_params['car_name'] = st.car.name
            st_params['producer_username'] = st.producer.username
            st_params['DT_RowId'] = st.id
            st_params['price'] = st.price
            st_params['weight'] = st.weight
            st_list.append(st_params)
    except Exception as e:
        LOG.error('Can not get admin upkeep data. %s' % e)
    return st_list


def password_reset(request):
    res = "success"
    try:
        producer_id = request.GET['producer_id']
        p = Producer.objects.get(pk=producer_id)
        p.password = pwd_hash(DEFAULT_PASSWORD)
        p.save()
    except Exception as e:
        res = "fail"
        LOG.error('Reset producer password error. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_upkeep_quote_index(request):
    upkeep_types = UpkeepType.objects.all()
    type_id = request.GET.get('type_id', "")
    if '_auth_user_id' in request.session:
        producer_id = request.session['_auth_user_id']
    else:
        producer_id = ""
    return render(request, 'background/user/producer/upkeep-quote-index.html',
                  {"upkeep_types": upkeep_types, "type_id": type_id, "producer_id": producer_id})


@data_table(columns=UPKEEP_SERIES_COLUMNS, model=UpkeepSeries)
def producer_upkeep_quote_data2(request):
    try:
        st_list = []
        producer_id = request.GET['producer_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        pcars = ProducerCarSeries.objects.filter(producer=producer_obj).filter(type__in=[2, 1])

        sts = UpkeepSeries.objects.filter(series__brand__in=[p.brand for p in pcars]). \
                  filter(Q(produce_year__year__contains=request.table_data['key_word']) |
                         Q(cc__contains=request.table_data['key_word']) |
                         Q(series__name__contains=request.table_data['key_word'])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        for st in sts:
            st_params = {}
            st_params['brand_name'] = st.series.brand.name
            st_params['series_name'] = st.series.name
            st_params['produce_year'] = st.produce_year.year
            bs = st.series.brand.producercarseries_set.all().filter(producer=producer_obj).filter(type__in=[2, 1])
            type_list = []
            # Judge proxy brand type weixiu or banjin
            for b in bs:
                if b.type == 1 and b.type != 2:
                    upkeep_type_id = 1
                if b.type == 2 and b.type != 1:
                    upkeep_type_id = 2
                type_list.append(upkeep_type_id)
            if 1 in type_list and 2 in type_list:
                st_params['upkeep_type_id'] = 'all'
            elif 1 in type_list and 2 not in type_list:
                st_params['upkeep_type_id'] = 1
            else:
                st_params['upkeep_type_id'] = 2

            st_params['cc'] = st.cc
            st_params['DT_RowId'] = st.id
            st_list.append(st_params)
    except Exception as e:
        LOG.error('Can not get upkeep series info. %s' % e)
    return st_list


def upkeep_quote_list(request):
    try:
        upkeep_series_id = request.GET['upkeep_series_id']
        upkeep_type_id = request.GET['type_id']
        if upkeep_type_id == "2":
            packages = UpkeepPackage.objects.filter(type__id=2)
        else:
            packages = UpkeepPackage.objects.filter(type__id=1)
    except Exception as e:
        LOG.error('Can not get quote detail(package). %s' % e)
    return render(request, 'background/user/producer/upkeep-quote-detail.html',
                  {"packages": packages, "upkeep_series_id": upkeep_series_id,
                   "type_id": upkeep_type_id})


def pkg_upkeep_detail(request):
    try:
        all_data = []
        # pkg_id = request.GET['pkg_id']
        type_id = request.GET['type_id']
        producer_id = request.session['_auth_user_id']
        upkeep_series_id = request.GET['upkeep_series_id']
        pkg_list = UpkeepPackage.objects.filter(type_id=type_id)
        for pkg in pkg_list:
            if pkg.type.id == 1:
                pkg_params = {}
                pkg_params['upkeep_series_id'] = upkeep_series_id
                pkg_params['type'] = 1
                pkg_params['pkg_id'] = pkg.id
                pkg_params['pkg_name'] = pkg.name
                pkg_params['upkeep_data'] = []
                upkeeps = pkg.upkeep_set.all()
                for up in upkeeps:
                    up_data = {}
                    up_data['upkeep_id'] = up.id
                    up_data['upkeep_name'] = up.name
                    up_data['material_data'] = []
                    material = up.upkeepmaterial_set.all()
                    for m in material:
                        material_params = {}
                        material_params['ma_id'] = m.id
                        material_params['ma_brand'] = m.brand
                        material_params['model_data'] = []

                        model = m.upkeepmaterialmodel_set.all()
                        for md in model:
                            m_data = {}
                            m_data['model_id'] = md.id
                            # m_data['m_brand'] = md.material.brand
                            m_data['code'] = md.code
                            m_data['model'] = md.model
                            m_data['material_type'] = md.type
                            m_data['labor'] = md.time_labor_price
                            m_data['material_price'] = md.material_price
                            quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id). \
                                filter(series_upkeep_info__upkeep_material_model=md). \
                                filter(series_upkeep_info__upkeep_series__id=upkeep_series_id)
                            if quote:
                                m_data['sale_labor'] = quote[0].sale_labor
                                m_data['sale_price'] = quote[0].sale_price
                            else:
                                m_data['sale_labor'] = ''
                                m_data['sale_price'] = ''
                            material_params['model_data'].append(m_data)
                        up_data['material_data'].append(material_params)
                    pkg_params['upkeep_data'].append(up_data)
                all_data.append(pkg_params)
            else:
                pkg_p = {}
                pkg_p['pkg_id'] = pkg.id
                pkg_p['pkg_name'] = pkg.name
                pkg_p['upkeep_data'] = []
                upkeeps = pkg.upkeep_set.all()
                for up in upkeeps:
                    up_params = {}
                    up_params['upkeep_id'] = up.id
                    up_params['upkeep_name'] = up.name
                    quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id).filter(series_upkeep_info__upkeep=up)
                    up_params['price_low'] = up.price_low
                    up_params['price_middle'] = up.price_middle
                    up_params['price_high'] = up.price_high
                    print quote.values()
                    if quote:
                        up_params['sale_price_low'] = quote[0].sale_price_low
                        up_params['sale_price_middle'] = quote[0].sale_price_middle
                        up_params['sale_price_high'] = quote[0].sale_price_high
                    else:
                        up_params['sale_price_low'] = ''
                        up_params['sale_price_middle'] = ''
                        up_params['sale_price_high'] = ''
                    pkg_p['upkeep_data'].append(up_params)
                all_data.append(pkg_p)
    except Exception as e:
        LOG.error('Can not get pkg upkeeps .%s' % e)
    # return HttpResponse(simplejson.dumps(all_data))
    return render(request, 'background/user/producer/upkeep-quote-pkg-detail.html',
                  {"all_data": all_data})


def pkg_upkeep_quote_detail(producer_id, pkg_id, upkeep_series_id):
    try:
        all_data = []
        pkg = UpkeepPackage.objects.get(pk=pkg_id)
        if pkg.type.id == 1:
            pkg_params = {}
            pkg_params['upkeep_series_id'] = upkeep_series_id
            pkg_params['type'] = 1
            pkg_params['pkg_id'] = pkg.id
            pkg_params['pkg_name'] = pkg.name
            pkg_params['upkeep_data'] = []
            upkeeps = pkg.upkeep_set.all()
            for up in upkeeps:
                up_data = {}
                up_data['upkeep_id'] = up.id
                up_data['upkeep_name'] = up.name
                up_data['material_data'] = []
                material = up.upkeepmaterial_set.all()
                for m in material:
                    material_params = {}
                    material_params['ma_id'] = m.id
                    material_params['ma_brand'] = m.brand
                    material_params['model_data'] = []

                    model = m.upkeepmaterialmodel_set.all()
                    for md in model:
                        m_data = {}
                        m_data['model_id'] = md.id
                        # m_data['m_brand'] = md.material.brand
                        m_data['code'] = md.code
                        m_data['model'] = md.model
                        m_data['material_type'] = md.type
                        quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id). \
                            filter(series_upkeep_info__upkeep_material_model=md). \
                            filter(series_upkeep_info__upkeep_series__id=upkeep_series_id)
                        if quote:
                            m_data['labor'] = quote[0].labor
                            m_data['material_price'] = quote[0].price
                        else:
                            m_data['labor'] = ""
                            m_data['material_price'] = ""
                        material_params['model_data'].append(m_data)
                    up_data['material_data'].append(material_params)
                pkg_params['upkeep_data'].append(up_data)
            all_data.append(pkg_params)
        else:
            pkg_p = {}
            pkg_p['pkg_id'] = pkg.id
            pkg_p['pkg_name'] = pkg.name
            pkg_p['upkeep_data'] = []
            upkeeps = pkg.upkeep_set.all()
            for up in upkeeps:
                up_params = {}
                up_params['upkeep_id'] = up.id
                up_params['upkeep_name'] = up.name
                quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id).filter(series_upkeep_info__upkeep=up)
                if quote:
                    up_params['labor'] = quote[0].price
                else:
                    up_params['labor'] = ""
                pkg_p['upkeep_data'].append(up_params)
            all_data.append(pkg_p)
    except Exception as e:
        LOG.error('Can not get pkg upkeeps .%s' % e)
    return all_data


def upkeep_quote_action(request):
    res = "success"
    msg = res
    try:
        pkg_id = request.POST['pkgId']
        pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
        producer_id = request.session['_auth_user_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        # items = request.POST['items']
        model_data = request.POST['model_data']
        model_list = model_data.split("#")[:-1]
        upkeep_series_id = request.POST['upkeepSeriesId']
        upsobj = UpkeepSeries.objects.get(pk=upkeep_series_id)

        for m in model_list:
            ns = m.split(",")
            labor = ns[0]
            material_price = ns[1]
            model_id = ns[2]
            upkeep_id = ns[3]
            material_type = ns[4]
            sale_labor = ns[5]
            sale_price = ns[6]
            if labor == '':
                upobj = Upkeep.objects.get(pk=upkeep_id)
                if material_type == 'G':
                    labor = upobj.labor_price_general
                else:
                    labor = upobj.labor_price_original
            if material_price == "":
                material_model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
                material_price = material_model_obj.price
            if sale_labor =='':
                sale_labor = labor
            if sale_price == '':
                sale_price == material_price
            upkeep_material_model = UpkeepMaterialModel.objects.get(pk=model_id)
            seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_series=upsobj,
                                                           upkeep_material_model=upkeep_material_model)
            if not seriesupinfo:
                seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_series=upsobj,
                                                               upkeep_material_model=upkeep_material_model)
            else:
                seriesupinfo = seriesupinfo[0]
            already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
            if already_quote:
                q = already_quote[0]
                q.labor = labor
                q.price = material_price
                q.sale_labor = sale_labor
                q.sale_price = sale_price
                q.save()
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, producer=producer_obj,
                                                 labor=labor, price=material_price, package=pkg_obj, sale_labor=sale_labor, sale_price=sale_price)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        msg = e
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def upkeep_banjin_quote_action(request):
    res = "success"
    msg = res
    try:
        producer_id = request.session['_auth_user_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        # items = request.POST['items']
        model_data = request.POST['model_data']
        model_list = model_data.split("#")[:-1]
        upkeep_series_id = request.POST['upkeepSeriesId']
        pkg_id = request.POST['pkgId']
        pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
        upsobj = UpkeepSeries.objects.get(pk=upkeep_series_id)

        for m in model_list:
            ns = m.split(",")
            sale_price_low = ns[0]
            sale_price_middle = ns[1]
            sale_price_high = ns[2]
            # material_price = ns[1]
            upkeep_id = ns[3]
            upobj = Upkeep.objects.get(pk=upkeep_id)
            if sale_price_low == '':
                sale_price_low = upobj.price_low
            if sale_price_middle == '':
                sale_price_middle = upobj.price_middle
            if sale_price_high == '':
                sale_price_high = upobj.price_high
            # if labor == '':
            #     if upsobj.series.brand.level.id == LEVEL_ONE:
            #         labor = upobj.price_high
            #     elif upsobj.series.brand.level.id == LEVEL_TWO:
            #         labor = upobj.price_middle
            #     else:
            #         labor = upobj.price_low
            seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_series=upsobj,
                                                           upkeep=upobj)
            print seriesupinfo.values()
            if not seriesupinfo:
                seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_series=upsobj,
                                                               upkeep=upobj)
            else:
                seriesupinfo = seriesupinfo[0]
            already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
            if already_quote:
                # q = already_quote[0]
                # q.sale_price_low = sale_price_low
                # q.sale_price_middle = sale_price_middle
                # q.sale_price_high = sale_price_high
                # q.price = material_price
                # q.save()
                SeriesUpkeepQuote.objects.filter(pk=already_quote[0].id).update(sale_price_low=sale_price_low, sale_price_middle=sale_price_middle, sale_price_high=sale_price_high)
                print already_quote[0].sale_price_low
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, package=pkg_obj, producer=producer_obj,
                                                 labor=0, sale_price_low=sale_price_low, sale_price_middle=sale_price_middle, sale_price_high=sale_price_high)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        res = "fail"
        msg = e
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def producer_detail(request):
    producer_id = request.user.id
    producer_data = producer_detail_data(producer_id)
    print producer_data['address']
    return render(request, 'background/user/producer/producer_info.html',
                  {'producer_id': producer_id, 'admin_data': producer_data})


def producer_detail_for_admin(request):
    producer_id = request.GET.get("user_id", "")
    producer_data = producer_detail_data(producer_id)
    return render(request, 'background/user/producer/admin_producer_info.html',
                  {'producer_id': producer_id, 'admin_data': producer_data})


def producer_detail_data(admin_id):
    admin_data = {}
    try:
        # admin_id = request.session["_auth_user_id"]
        # admin_id = request.GET['admin_id'] if 'admin_id' in request.GET else 1
        admin_obj = Producer.objects.get(pk=admin_id)
        admin_data["name"] = admin_obj.username
        admin_data["phone"] = admin_obj.phone
        admin_data['repair_phone'] = admin_obj.repair_phone
        admin_data["password"] = admin_obj.password
        admin_data["sex"] = admin_obj.information.sex if admin_obj.information else ""
        admin_data["id"] = admin_obj.id
        admin_data['area'] = admin_obj.area.city
        if admin_obj.information and admin_obj.information.portrait:
            admin_data["portrait"] = admin_obj.information.portrait.picture.path
        else:
            admin_data["portrait"] = ""
        admin_data["type"] = admin_obj.type
        admin_data['realname'] = admin_obj.information.name if admin_obj.information else ""
        admin_data["email"] = admin_obj.information.email if admin_obj.information else ""
        admin_data['province'] = admin_obj.area.province if admin_obj.area else ''
        admin_data['city'] = admin_obj.area.city if admin_obj.area else ''
        admin_data['birthday'] = datetime.datetime.strftime(admin_obj.information.birthday, '%Y-%m-%d %H:%M:%S') if admin_obj.information else ""
        admin_data['qq'] = admin_obj.information.qq if admin_obj.information else ""
        admin_data['weibo'] = admin_obj.information.weibo if admin_obj.information else ""
        admin_data['weixin'] = admin_obj.information.weixin if admin_obj.information else ""
        admin_data['tel'] = admin_obj.information.tel if admin_obj.information else ""
        admin_data['email'] = admin_obj.information.email if admin_obj.information else ""
        if admin_obj.information and admin_obj.information.addresses.all():
            admin_data['address'] = admin_obj.information.addresses.all()[0].address
            admin_data['longitude'] = admin_obj.information.addresses.all()[0].longitude
            admin_data['latitude'] = admin_obj.information.addresses.all()[0].latitude
        else:
            admin_data['address'] = ''
            admin_data['longitude'] = ''
            admin_data['latitude'] = ''
    except Exception as e:
        LOG.error('Can not get admin detail.%s' % e)
    return admin_data


def producer_detail_edit_data(admin_id):
    admin_data = {}
    try:
        admin_obj = Producer.objects.get(pk=admin_id)
        admin_data["id"] = admin_obj.id
        admin_data['repair_phone'] = admin_obj.repair_phone
        admin_data['area'] = admin_obj.area.city
        if admin_obj.information and admin_obj.information.portrait:
            admin_data["portrait"] = admin_obj.information.portrait.picture.path
        else:
            admin_data["portrait"] = ""
        admin_data["type"] = admin_obj.type
        admin_data['short_name'] = admin_obj.information.name if admin_obj.information else ""
        admin_data["email"] = admin_obj.information.email if admin_obj.information else ""
        admin_data['province'] = admin_obj.area.province if admin_obj.area else ''
        admin_data['city'] = admin_obj.area.city if admin_obj.area else ''
        admin_data['email'] = admin_obj.information.email if admin_obj.information else ""
        admin_data["name"] = admin_obj.username
        if admin_obj.information and admin_obj.information.addresses.all():
            admin_data['address'] = admin_obj.information.addresses.all()[0].address
            admin_data['longitude'] = admin_obj.information.addresses.all()[0].longitude
            admin_data['latitude'] = admin_obj.information.addresses.all()[0].latitude
        else:
            admin_data['address'] = ''
            admin_data['longitude'] = ''
            admin_data['latitude'] = ''
    except Exception as e:
        LOG.error('Can not get admin detail.%s' % e)
    return admin_data


def producer_phone(request):
    producer_id = request.GET['producer_id']
    probj = Producer.objects.get(pk=producer_id)
    return render(request, 'background/user/producer/producer_phone.html', {'probj': probj})


def producer_edit(request):
    res = "success"
    try:
        producer_id = request.POST['producerId']
        username = request.POST['producerName']
        sex = request.POST['sex']
        phone = request.POST['phone']
        email = request.POST['email']
        address = request.POST['address']
        longitude = request.POST['addrLongitude']
        latitude = request.POST['addrLatitude']
        admin_obj = Producer.objects.get(pk=producer_id)
        if "adminPic" in request.FILES:
            img = request.FILES['adminPic']
            img_path = deal_with_upload_file(img)
            if img_path:
                if admin_obj.information and admin_obj.information.portrait:
                    admin_obj.information.portrait.picture.name = img.name
                    admin_obj.information.portrait.picture.path = img_path
                    admin_obj.information.portrait.picture.save()
                    admin_obj.information.portrait.save()
                else:
                    pic_obj = Picture.objects.create(name=img.name, path=img_path)
                    por_obj = Portrait.objects.create(picture=pic_obj)
                    if admin_obj.information:
                        admin_obj.information.portrait = por_obj
                        admin_obj.information.email = email
                        admin_obj.information.sex = sex
                        admin_obj.phone = phone
                        admin_obj.information.save()
                    else:
                        info = UserInformation.objects.create(email=email, sex=sex, tel=phone, portrait=por_obj)
                        admin_obj.information = info
                        admin_obj.save()
        else:
            if admin_obj.information:
                admin_obj.information.email = email
                admin_obj.information.sex = sex
                admin_obj.phone = phone
                admin_obj.information.save()
                if admin_obj.information.addresses:
                    aadd = UserAddress.objects.filter(pk=admin_obj.information.addresses.all()[0].id)
                    aadd.update(address=address, longitude=longitude, latitude=latitude)
                else:
                    add = UserAddress.objects.create(address=address, created_at=timezone.now(), longitude=longitude, latitude=latitude)
                    admin_obj.information.addresses.add(add)
                    # admin_obj.information.save()
            else:
                info = UserInformation.objects.create(email=email, sex=sex, tel=phone)
                admin_obj.information = info
                admin_obj.save()
                add = UserAddress.objects.create(address=address, created_at=timezone.now())
                admin_obj.information.addresses.add(add)
        admin_obj.username = username
        admin_obj.save()

    except Exception as e:
        LOG.error('Can not edit producer info. %s' % e)
        res = 'fail'
    return HttpResponse(simplejson.dumps({"res": res}))


def admin_edit(request):
    producer_id = request.GET.get("producer_id", "")
    producer_obj = Producer.objects.get(pk=producer_id)
    creator_phone = producer_obj.creator.phone if producer_obj.creator else ""
    areas = [{"id": item.id, "province": item.province, "city": item.city,
              'is_checked': True if item == producer_obj.area else False}
             for item in SupportedArea.objects.all()]
    producer_data = producer_detail_edit_data(producer_id)
    return render(request, 'background/user/producer/edit.html', {"producer_obj": producer_obj, "areas": areas,
                                                                  "producer_data": producer_data,
                                                                  "creator_phone": creator_phone})


def admin_edit_action(request):
    res = "success"
    try:
        producer_id = request.POST['producer_id']
        obj = Producer.objects.get(pk=producer_id)
        producer_info_edit(request, obj)
        username = request.POST['username']
        salephone = request.POST["salePhone"]
        producer_rank = request.POST.get("producerRank", '4')
        repair_phone = request.POST['repairPhone']
        area_id = request.POST["supArea"]
        area = SupportedArea.objects.get(id=area_id)
        ac = request.POST.get("isActive", "")
        if ac == "on":
            is_active = True
        else:
            is_active = False
        recommend = request.POST.get("isRecommend", "")
        if recommend == "on":
            is_recommend = True
        else:
            is_recommend = False
        full_name = request.POST.get("fullName", "")
        obj.area = area
        obj.is_recommend = is_recommend
        obj.repair_phone = repair_phone
        obj.username = username
        obj.phone = salephone
        obj.full_name = full_name
        obj.type = producer_rank
        obj.is_active = is_active
        obj.save()
    except Exception as e:
        LOG.error('Can not edit producer info. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def producer_info_edit(request, producer_obj):
    res = "sucess"
    try:
        por_obj = ""
        if "portrait" in request.FILES:
            por = request.FILES["portrait"]
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)
        short_name = request.POST["shortName"]
        email = request.POST['email']
        if por_obj:
            producer_obj.information.name = short_name
            producer_obj.information.email = email
            producer_obj.information.portrait = por_obj
        else:
            producer_obj.information.name = short_name
            producer_obj.information.email = email
        if "InfoAddr" in request.POST:
            addr = request.POST["InfoAddr"]
            if not producer_obj.information.addresses.all():
                a1 = UserAddress.objects.create(address=addr)
                if "addrLongitude" in request.POST:
                    lon = request.POST['addrLongitude']
                    lat = request.POST['addrLatitude']
                    a1.longitude = float(lon)
                    a1.latitude = float(lat)
                    a1.save()
                producer_obj.information.addresses.add(a1)
            else:
                for ad in producer_obj.information.addresses.all():
                    ad.address = addr
                    if "addrLongitude" in request.POST:
                        lon = request.POST['addrLongitude']
                        lat = request.POST['addrLatitude']
                        ad.longitude = float(lon)
                        ad.latitude = float(lat)
                    ad.save()
        producer_obj.information.save()
    except Exception as e:
        LOG.error('Can not edit user information. %s' % e)
        res = "fail"
    return res


def edit_producer_information(request, producer_obj):
    res = "success"
    try:
        por_obj = ""
        if "portrait" in request.FILES:
            por = request.FILES["portrait"]
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST.get("infoName", "")
        sex = request.POST.get("sex", "")
        birthday = request.POST.get("birthday", "")
        tel = request.POST.get("tel", "")
        qq = request.POST.get('qq', "")
        weibo = request.POST.get("weibo", "")
        weixin = request.POST.get("weixin", "")
        email = request.POST.get("email", "")
        if producer_obj.information:
            if por_obj:

                producer_obj.information.name = name
                producer_obj.information.sex = sex
                producer_obj.information.portrait = por_obj
                producer_obj.information.tel = tel
                producer_obj.information.qq = qq
                producer_obj.information.weibo = weibo
                producer_obj.information.weixin = weixin
                producer_obj.information.email = email

            else:
                producer_obj.information.name = name
                producer_obj.information.sex = sex
                producer_obj.information.tel = tel
                producer_obj.information.qq = qq
                producer_obj.information.weibo = weibo
                producer_obj.information.weixin = weixin
                producer_obj.information.email = email

            if birthday and birthday != "None":
                producer_obj.information.birthday = birthday
            if "InfoAddr" in request.POST:
                addr = request.POST["InfoAddr"]
                if not producer_obj.information.addresses.all():
                    a1 = UserAddress.objects.create(address=addr)
                    if "addrLongitude" in request.POST:
                        lon = request.POST['addrLongitude']
                        lat = request.POST['addrLatitude']
                        a1.longitude = float(lon)
                        a1.latitude = float(lat)
                        a1.save()
                    producer_obj.information.addresses.add(a1)
                else:
                    for ad in producer_obj.information.addresses.all():
                        ad.address = addr
                        if "addrLongitude" in request.POST:
                            lon = request.POST['addrLongitude']
                            lat = request.POST['addrLatitude']
                            ad.longitude =float(lon)
                            ad.latitude = float(lat)
                        ad.save()
            producer_obj.information.save()
        else:
            if por_obj:
                info1 = UserInformation(name=name, portrait=por_obj, sex=sex,
                                        email=email, tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            else:
                info1 = UserInformation(name=name,  sex=sex, email=email,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            if birthday and birthday != "None":
                info1.birthday = birthday

            if "InfoAddr" in request.POST:
                addr = request.POST["InfoAddr"]
                if not producer_obj.information.addresses.all():
                    a1 = UserAddress.objects.create(address=addr)
                    if "addrLongitude" in request.POST:
                        lon = request.POST['addrLongitude']
                        lat = request.POST['addrLatitude']
                        a1.longitude = float(lon)
                        a1.latitude = float(lat)
                        a1.save()
                    info1.addresses.add(a1)
                    info1.save()
                else:
                    for ad in producer_obj.information.addresses.all():
                        ad.address = addr
                        if "addrLongitude" in request.POST:
                            lon = request.POST['addrLongitude']
                            lat = request.POST['addrLatitude']
                            ad.longitude = float(lon)
                            ad.latitude = float(lat)
                        ad.save()
            info1.save()
            producer_obj.information = info1
            producer_obj.save()
        # if "addrList" in request.POST:
        #     ids = request.POST.getlist("addrList")
        #     uads = UserAddress.objects.filter(id__in=ids)
        #     for uad in uads:
        #         info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Can not edit user information. %s' % e)
        res = "fail"
    return res


def name_validate(request):
    res = "no"
    try:
        producer_name = request.GET['user_name']
        producer = Producer.objects.filter(username=producer_name)
        if producer:
            res = "exists"
            return HttpResponse(res)
    except Exception as e:
        res = "error"
        LOG.error('validate producer name error. %s' % e)
    return HttpResponse(res)


def active_set(request):
    try:
        ac = request.GET['is_active']
        producer_id = request.GET['producer_id']
        pobj = Producer.objects.get(pk=producer_id)
        if ac == "true":
            pobj.is_active = False
            pobj.save()
        else:
            pobj.is_active = True
            pobj.save()
    except Exception as e:
        LOG.error('Can not change producer active status. %s' % e)
    return redirect('background-user-producer-index')


def password_change(request):
    user_id = request.GET['user_id']
    producer_data = producer_detail_data(user_id)
    return render(request, 'background/user/producer/password_change.html',
                  {"adminId": user_id, 'producer_data': producer_data, 'role': "producer"})


def admin_car_quote_weight(request):
    res = "success"
    try:
        car_quote_id = request.POST.get("carQuoteId", "")
        weight = request.POST.get("quoteWeight", "")

        if car_quote_id:
            car_quote = CarQuote.objects.get(pk=car_quote_id)
            car_quote.weight = weight
            car_quote.save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set car quote weight. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def phone(request):
    admin_id = request.session.get('_auth_user_id')
    admin_data = producer_detail_data(admin_id)
    role = request.GET.get("role", "m")
    return render(request, "background/user/producer/phone.html", {"phone": admin_data["phone"],
                                                                "admin_data": admin_data,
                                                                "role": role})


def batch_carquote(request):
    res = 'success'
    try:
        model_data = request.POST['model_data']
        model_list = model_data.split("#")[:-1]
        for m in model_list:
            ns = m.split(',')
            producer_id = ns[0]
            car_id = ns[1]
            car_obj = Car.objects.get(pk=car_id)
            quote_price = car_obj.price
            sale_price = ns[5]
            mail_price = ns[6]
            sale_time = ns[7]
            mail_time = ns[8]
            if mail_time == u'长效' or mail_time == '':
                mail_time = None
            if sale_time == u'长效' or sale_time == '':
                sale_time = None
            if not sale_price:
                sale_price = quote_price
            if not mail_price:
                mail_price = quote_price
            pro_obj = Producer.objects.get(pk=producer_id)
            quote_obj = CarQuote.objects.filter(car__id=car_id, producer__id=producer_id)
            if quote_obj:
                quote_obj.update(price=quote_price, sale_price=sale_price, mail_price=mail_price, sale_time=sale_time,
                                 mail_time=mail_time)
            else:
                CarQuote.objects.create(car=car_obj, producer=pro_obj, price=quote_price, sale_price=sale_price,
                                        mail_price=mail_price, sale_time=sale_time, mail_time=mail_time)
    except Exception as e:
        res = "fail"
        LOG.error('fail with do batch carquote. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def fix_quote(request):
    return render(request, 'background/user/producer/upkeep-quote-index.html', {'type_id': 1})


def upkeep_quote_detail(request):
    try:
        all_data = []
        type_id = request.GET['type_id']
        producer_id = request.session['_auth_user_id']
        pkg_list = UpkeepPackage.objects.filter(type_id=type_id)
        if int(type_id) == 1:
            upkeeps = Upkeep.objects.filter(package__type_id=1)
            for up in upkeeps:
                up_data = {}
                up_data['upkeep_id'] = up.id
                up_data['upkeep_name'] = up.name
                # 维修保养 各项目名称
                up_data['material_data'] = []
                up_data['sum_len'] = 0
                material = up.upkeepmaterial_set.all()
                if not material:
                    continue
                for m in material:
                    material_params = {}
                    material_params['ma_id'] = m.id
                    material_params['ma_brand'] = m.brand
                    material_params['model_data'] = []
                    material_params['ma_len'] = 0
                    model = m.upkeepmaterialmodel_set.all()
                    for md in model:
                        m_data = {}
                        m_data['model_id'] = md.id
                        # m_data['m_brand'] = md.material.brand
                        m_data['code'] = md.code
                        m_data['model'] = md.model
                        material_params['ma_len'] += 1
                        if md.type == 'G':
                            m_data['material_type'] = u"通用"
                        else:
                            m_data['material_type'] = u"原厂"
                        m_data['labor_price'] = md.time_labor_price
                        m_data['material_price'] = md.material_price
                        quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id). \
                            filter(series_upkeep_info__upkeep_material_model=md)
                        if quote:
                            m_data['sale_labor_price'] = quote[0].sale_labor_price
                            m_data['sale_material_price'] = quote[0].sale_material_price
                        else:
                            m_data['sale_labor_price'] = ''
                            m_data['sale_material_price'] = ''
                        material_params['model_data'].append(m_data)
                    up_data['sum_len'] += material_params['ma_len']
                    up_data['material_data'].append(material_params)
                if up_data in all_data:
                    continue
                all_data.append(up_data)
            return render(request, 'background/user/producer/upkeep-quote-pkg-detail1.html',
                          {"all_data": all_data})
        else:
            for pkg in pkg_list:
                pkg_p = {}
                pkg_p['pkg_id'] = pkg.id
                pkg_p['pkg_name'] = pkg.name
                pkg_p['upkeep_data'] = []
                upkeeps = pkg.upkeep_set.all()
                pkg_p['pkg_len'] = len(upkeeps)
                for up in upkeeps:
                    up_params = {}
                    up_params['upkeep_id'] = up.id
                    up_params['upkeep_name'] = up.name
                    quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id).filter(series_upkeep_info__upkeep=up)
                    up_params['price_low'] = up.price_low
                    up_params['price_middle'] = up.price_middle
                    up_params['price_high'] = up.price_high
                    if quote:
                        up_params['sale_price_low'] = quote[0].sale_price_low
                        up_params['sale_price_middle'] = quote[0].sale_price_middle
                        up_params['sale_price_high'] = quote[0].sale_price_high
                    else:
                        up_params['sale_price_low'] = ''
                        up_params['sale_price_middle'] = ''
                        up_params['sale_price_high'] = ''
                    pkg_p['upkeep_data'].append(up_params)
                all_data.append(pkg_p)
                print all_data
            return render(request, 'background/user/producer/upkeep-quote-pkg-detail2.html',
                          {"all_data": all_data})
    except Exception as e:
        LOG.error('Can not get pkg upkeeps .%s' % e)


def paint_quote(request):
    return render(request, 'background/user/producer/upkeep-quote-index.html', {'type_id': 2})


def fix_quote_action(request):
    res = "success"
    msg = res
    try:
        upkeep_id = request.POST['upkeep_id']
        sale_material_price = request.POST['sale_material_price']
        sale_labor_price = request.POST['sale_labor_price']
        model_id = request.POST['model_id']
        producer_id = request.session['_auth_user_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        upkeep_obj = Upkeep.objects.get(pk=upkeep_id)
        pkg_list = upkeep_obj.package.all()
        model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
        labor_price = model_obj.time_labor_price
        material_price = model_obj.material_price
        if sale_labor_price == '':
            sale_labor_price = labor_price
        if sale_material_price == '':
            sale_material_price = material_price
        seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_material_model=model_obj)
        if not seriesupinfo:
            seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_material_model=model_obj)
        else:
            seriesupinfo = seriesupinfo[0]
        already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
        for pkg in pkg_list:
            if already_quote:
                for q in already_quote:
                    if q.package == pkg:
                        q.sale_labor_price = sale_labor_price
                        q.sale_material_price = sale_material_price
                        q.save()
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, producer=producer_obj,
                                                 package=pkg, sale_labor_price=sale_labor_price,
                                                 sale_material_price=sale_material_price)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        msg = e
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def paint_quote_action(request):
    res = "success"
    msg = res
    try:
        producer_id = request.session['_auth_user_id']
        producer_obj = Producer.objects.get(pk=producer_id)
        model_data = request.POST['model_data']
        model_list = model_data.split("#")[:-1]
        pkg_id = request.POST['pkgId']
        pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
        for m in model_list:
            ns = m.split(",")
            sale_price_low = ns[0]
            sale_price_middle = ns[1]
            sale_price_high = ns[2]
            upkeep_id = ns[3]
            upobj = Upkeep.objects.get(pk=upkeep_id)
            if sale_price_low == '':
                sale_price_low = upobj.price_low
            if sale_price_middle == '':
                sale_price_middle = upobj.price_middle
            if sale_price_high == '':
                sale_price_high = upobj.price_high
            seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep=upobj)
            if not seriesupinfo:
                seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep=upobj)
            else:
                seriesupinfo = seriesupinfo[0]
            already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
            if already_quote:
                SeriesUpkeepQuote.objects.filter(pk=already_quote[0].id).update(sale_price_low=sale_price_low,
                                                                                sale_price_middle=sale_price_middle,
                                                                                sale_price_high=sale_price_high)
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, package=pkg_obj,
                                                 producer=producer_obj, sale_price_low=sale_price_low,
                                                 sale_price_middle=sale_price_middle, sale_price_high=sale_price_high)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        res = "fail"
        msg = e
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def upkeep_fix_batch_quote(request):
    res = "success"
    msg = res
    try:
        producer_id = request.session['_auth_user_id']
        data_list = request.POST['data_list']
        data_li = data_list.split("?")[:-1]
        for data_l in data_li:
            upkeep_id = data_l.split("%")[0]
            model_id = data_l.split("%")[1]
            sale_labor_price = data_l.split("%")[2]
            sale_material_price = data_l.split("%")[3]
            res = batch_fix_quote_action(producer_id, upkeep_id, sale_material_price, sale_labor_price, model_id)
    except Exception as e:
        LOG.error('error happened when upkeep fix batch quote. %s' % e)
        msg = e
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def upkeep_paint_batch_quote(request):
    res = "success"
    msg = res
    try:
        producer_id = request.session['_auth_user_id']
        data_list = request.POST['data_list']
        data_li = data_list.split("?")[:-1]
        for data_l in data_li:
            pkg_id = data_l.split("%")[0]
            model_data = data_l.split("%")[1]
            res = batch_paint_quote_action(producer_id, pkg_id, model_data)
    except Exception as e:
        LOG.error('error happened when upkeep fix batch quote. %s' % e)
        msg = e
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))


def batch_fix_quote_action(producer_id, upkeep_id, sale_material_price, sale_labor_price, model_id):
    res = "success"
    try:
        producer_obj = Producer.objects.get(pk=producer_id)
        upkeep_obj = Upkeep.objects.get(pk=upkeep_id)
        pkg_list = upkeep_obj.package.all()
        model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
        labor_price = model_obj.time_labor_price
        material_price = model_obj.material_price
        if sale_labor_price == '':
            sale_labor_price = labor_price
        if sale_material_price == '':
            sale_material_price = material_price
        seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_material_model=model_obj)
        if not seriesupinfo:
            seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_material_model=model_obj)
        else:
            seriesupinfo = seriesupinfo[0]
        already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
        for pkg in pkg_list:
            if already_quote:
                for q in already_quote:
                    if q.package == pkg:
                        q.sale_labor_price = sale_labor_price
                        q.sale_material_price = sale_material_price
                        q.save()
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, producer=producer_obj,
                                                 package=pkg, sale_labor_price=sale_labor_price,
                                                 sale_material_price=sale_material_price)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        res = "fail"
    return res



def batch_paint_quote_action(producer_id, pkg_id, model_data):
    res = "success"
    try:
        producer_obj = Producer.objects.get(pk=producer_id)
        model_list = model_data.split("#")[:-1]
        pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
        for m in model_list:
            ns = m.split(",")
            sale_price_low = ns[0]
            sale_price_middle = ns[1]
            sale_price_high = ns[2]
            upkeep_id = ns[3]
            upobj = Upkeep.objects.get(pk=upkeep_id)
            if sale_price_low == '':
                sale_price_low = upobj.price_low
            if sale_price_middle == '':
                sale_price_middle = upobj.price_middle
            if sale_price_high == '':
                sale_price_high = upobj.price_high
            seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep=upobj)
            if not seriesupinfo:
                seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep=upobj)
            else:
                seriesupinfo = seriesupinfo[0]
            already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
            if already_quote:
                SeriesUpkeepQuote.objects.filter(pk=already_quote[0].id).update(sale_price_low=sale_price_low,
                                                                                sale_price_middle=sale_price_middle,
                                                                                sale_price_high=sale_price_high)
            else:
                SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, package=pkg_obj,
                                                 producer=producer_obj, sale_price_low=sale_price_low,
                                                 sale_price_middle=sale_price_middle, sale_price_high=sale_price_high)
    except Exception as e:
        LOG.error('error happened when upkeep quote. %s' % e)
        res = "fail"
    return res


# 0108改，材料报价，改之前是以套餐作为一个报价模块
# def fix_quote_action(request):
#     res = "success"
#     msg = res
#     try:
#         pkg_id = request.POST['pkgId']
#         pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
#         producer_id = request.session['_auth_user_id']
#         producer_obj = Producer.objects.get(pk=producer_id)
#         model_data = request.POST['model_data']
#         model_list = model_data.split("#")[:-1]
#
#         for m in model_list:
#             ns = m.split(",")
#             model_id = ns[0]
#             upkeep_id = ns[1]
#             material_type = ns[2]
#             sale_labor_price = ns[3]
#             sale_material_price = ns[4]
#             model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
#             labor_price = model_obj.time_labor_price
#             material_price = model_obj.material_price
#             if sale_labor_price == '':
#                 sale_labor_price = labor_price
#             if sale_material_price == '':
#                 sale_material_price = material_price
#             upkeep_material_model = UpkeepMaterialModel.objects.get(pk=model_id)
#             seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_material_model=upkeep_material_model)
#             if not seriesupinfo:
#                 seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_material_model=upkeep_material_model)
#             else:
#                 seriesupinfo = seriesupinfo[0]
#             already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
#             if already_quote:
#                 q = already_quote[0]
#                 q.sale_labor_price = sale_labor_price
#                 q.sale_material_price = sale_material_price
#                 q.save()
#             else:
#                 SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, producer=producer_obj,
#                                                  package=pkg_obj, sale_labor_price=sale_labor_price,
#                                                  sale_material_price=sale_material_price)
#     except Exception as e:
#         LOG.error('error happened when upkeep quote. %s' % e)
#         msg = e
#         res = "fail"
#     return HttpResponse(simplejson.dumps({"res": res, "msg": msg}))
#
#


#     批量报价，函数替换
# def batch_fix_quote_action(producer_id, pkg_id, model_data):
#     res = "success"
#     try:
#         pkg_obj = UpkeepPackage.objects.get(pk=pkg_id)
#         producer_obj = Producer.objects.get(pk=producer_id)
#         model_list = model_data.split("#")[:-1]
#         for m in model_list:
#             ns = m.split(",")
#             model_id = ns[0]
#             upkeep_id = ns[1]
#             material_type = ns[2]
#             sale_labor_price = ns[3]
#             sale_material_price = ns[4]
#             model_obj = UpkeepMaterialModel.objects.get(pk=model_id)
#             labor_price = model_obj.time_labor_price
#             material_price = model_obj.material_price
#             if sale_labor_price == '':
#                 sale_labor_price = labor_price
#             if sale_material_price == '':
#                 sale_material_price = material_price
#             upkeep_material_model = UpkeepMaterialModel.objects.get(pk=model_id)
#             seriesupinfo = SeriesUpkeepInfo.objects.filter(upkeep_material_model=upkeep_material_model)
#             if not seriesupinfo:
#                 seriesupinfo = SeriesUpkeepInfo.objects.create(upkeep_material_model=upkeep_material_model)
#             else:
#                 seriesupinfo = seriesupinfo[0]
#             already_quote = SeriesUpkeepQuote.objects.filter(series_upkeep_info=seriesupinfo, producer=producer_obj)
#             if already_quote:
#                 q = already_quote[0]
#                 q.sale_labor_price = sale_labor_price
#                 q.sale_material_price = sale_material_price
#                 q.save()
#             else:
#                 SeriesUpkeepQuote.objects.create(series_upkeep_info=seriesupinfo, producer=producer_obj,
#                                                  package=pkg_obj, sale_labor_price=sale_labor_price,
#                                                  sale_material_price=sale_material_price)
#     except Exception as e:
#         LOG.error('error happened when upkeep quote. %s' % e)
#         res = "fail"
#     return res

def producer_action_info_new(request):
    info1 = None
    try:
        por = request.FILES.get("portrait", "")
        por_obj = ""
        if por:
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)
        short_name = request.POST["shortName"]
        email = request.POST['email']
        if por_obj or short_name:
            if por_obj:
                info1 = UserInformation(name=short_name, portrait=por_obj, email=email)
            else:
                info1 = UserInformation(name=short_name, email=email)
            info1.save()
        if "InfoAddr" in request.POST:
            info_addr = request.POST['InfoAddr']
            if info_addr:
                a1 = UserAddress.objects.create(address=info_addr)
                if "addrLongitude" in request.POST:
                    lon = request.POST['addrLongitude']
                    lat = request.POST['addrLatitude']
                    a1.longitude = float(lon)
                    a1.latitude = float(lat)
                    a1.save()
                info1.addresses.add(a1)
    except Exception as e:
        LOG.error('Error happened when create user info. %s' % e)
    return info1

