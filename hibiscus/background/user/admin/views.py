# -*- coding:utf-8 -*-
import simplejson
import logging
import hashlib

from django.db.models import Q
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone

from hibiscus.models import HcAdmin, UserInformation, UserAddress, Picture, Portrait, SupportedArea
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file, pwd_hash, DEFAULT_PASSWORD

LOG = logging.getLogger(__name__)
ADMIN_COLUMNS = ["username", "username", "type", "phone", "created_at", "last_login", "is_active"]


def index(request):
    return render(request, 'background/user/admin/index.html')


def new(request):
    areas = []
    for item in SupportedArea.objects.all():
        a_params = {}
        a_params['id'] = item.id
        a_params['province'] = item.province
        a_params['city'] = item.city
        if item.id == 2:
            continue
        areas.append(a_params)
    return render(request, 'background/user/admin/new.html', {"areas": areas})


def role_index(request):
    return render(request, 'background/user/admin/role-index.html')


def action_new(request):
    res = "success"
    try:
        info = action_info_new(request)
        username = request.POST['username']
        phone = request.POST["phone"]
        ad_t = request.POST["adminT"]
        password = request.POST['password']
        area_id = request.POST["supArea"]
        area = SupportedArea.objects.get(id=area_id)
        pwd = pwd_hash(password)
        is_active = True
        if "isActive" in request.POST:
            ac = request.POST['isActive']
            if ac == "off":
                is_active = False
        HcAdmin(information=info, username=username, phone=phone,
                password=pwd, type=ad_t, is_active=is_active, area=area).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


@data_table(columns=ADMIN_COLUMNS, model=HcAdmin)
def data(request):
    key_word = request.table_data["key_word"]
    try:
        sts = HcAdmin.objects.filter(
            Q(username__contains=key_word) | Q(phone__contains=key_word) | Q(password__contains=key_word)
        ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]

        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.username
            st_params["phone"] = st.phone
            st_params["type"] = st.type
            st_params["password"] = st.password
            st_params["is_active"] = st.is_active
            if st.information and st.information.portrait:
                st_params['portrait'] = st.information.portrait.picture.path
            else:
                st_params['portrait'] = None
            st_params['created_at'] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params['last_login'] = timezone.localtime(st.last_login).strftime('%Y-%m-%d %H:%M:%S') if st.last_login else ""
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Get admin index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                HcAdmin.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete user address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def validate(request):
    res = "no"
    try:
        address = request.GET['userAddr']
        ua = HcAdmin.objects.filter(address=address)
        if ua:
            res = "yes"
    except Exception as e:
        res = "error"
        LOG.error('Can not validate user address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def action_info_new(request):
    info1 = None
    try:
        por = request.FILES.get("portrait", "")
        por_obj = ""
        if por:
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST["infoName"]
        sex = request.POST["sex"]
        email = request.POST["email"]
        birthday = request.POST["birthday"]
        tel = request.POST["tel"]
        qq = request.POST['qq']
        weibo = request.POST["weibo"]
        weixin = request.POST["weixin"]
        info1 = UserInformation(name=name, sex=sex,
                                tel=tel, qq=qq, weibo=weibo, weixin=weixin, email=email)
        if por_obj:
            info1.portrait = por_obj
        if birthday:
            info1.birthday = birthday
        info1.save()
        if "InfoAddr" in request.POST:
            addr = request.POST["InfoAddr"]
            a1 = UserAddress.objects.create(address=addr)
            info1.addresses.add(a1)
        if "addrList" in request.POST:
            ids = request.POST.getlist("addrList")
            uads = UserAddress.objects.filter(id__in=ids)
            for uad in uads:
                info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Error happened when create user info. %s' % e)

    return info1


def admin_info(request):
    user_id = request.GET["user_id"]
    return render(request, 'background/user/admin/info-detail.html',
                  {"user_id": user_id})


def info_data(request):
    info_params = {}
    try:
        user_id = request.GET["user_id"]
        ad = HcAdmin.objects.get(id=user_id)
        info_params["name"] = ""
        info_params["sex"] = ""
        info_params["birthday"] = ""
        info_params["tel"] = ""
        info_params["qq"] = ""
        info_params["weibo"] = ""
        info_params["weixin"] = ""
        info_params["email"] = ""
        info_params["address"] = []
        info_params['username'] = ad.username
        info_params['type'] = ad.type
        info_params['phone'] = ad.phone
        info_params['area'] = ad.area.province+ "/" + ad.area.city
        info = ad.information
        if info:
            if info.addresses:
                info_params["address"] = [ad.address for ad in info.addresses.all()]
            else:
                info_params["address"] = []
            if info.portrait:
                info_params["por_path"] = info.portrait.picture.path
            else:
                info_params["por_path"] = None
            info_params["name"] = info.name if info.name else ""
            info_params["sex"] = info.sex if info.sex else ""
            info_params["birthday"] = info.birthday.strftime('%Y-%m-%d %H:%M:%S') if info.birthday else ""
            info_params["tel"] = info.tel if info.tel else ""
            info_params["qq"]  = info.qq if info.qq else ""
            info_params["weibo"] = info.weibo if info.weibo else ""
            info_params["weixin"] = info.weixin if info.weixin else ""
            info_params["email"] = info.email if info.email else ""

    except Exception as e:
        LOG.error('Get admin error. %s' % e)
    return HttpResponse(simplejson.dumps(info_params))


def admin_detail_data(admin_id):
    admin_data = {}
    try:
        # admin_id = request.session["_auth_user_id"]
        # admin_id = request.GET['admin_id'] if 'admin_id' in request.GET else 1
        admin_obj = HcAdmin.objects.get(pk=admin_id)
        admin_data["name"] = admin_obj.information.name if admin_obj.information else ""
        admin_data["phone"] = admin_obj.phone
        admin_data["password"] = admin_obj.password
        admin_data["sex"] = admin_obj.information.sex if admin_obj.information else ""
        admin_data["id"] = admin_obj.id
        if admin_obj.information and admin_obj.information.portrait:
            admin_data["portrait"] = admin_obj.information.portrait.picture.path
        else:
            admin_data["portrait"] = ""
        admin_data["type"] = admin_obj.type
        admin_data["email"] = admin_obj.information.email if admin_obj.information else ""
        admin_data["username"] = admin_obj.username
        admin_data["original_phone"] = admin_obj.phone
    except Exception as e:
        LOG.error('Can not get admin detail.%s' % e)
    return admin_data


def admin_detail(request):
    admin_id = request.session["_auth_user_id"]
    admin_data = admin_detail_data(admin_id)
    if admin_data['type'] == 'o':
        return render(request, 'background/user/admin/operator-index.html', {"admin_data": admin_data})
    return render(request, 'background/user/admin/index.html', {"admin_data": admin_data})


def operator_detail(request):
    operator_id = request.session['_auth_user_id']
    admin_data = admin_detail_data(operator_id)
    return render(request, 'background/user/admin/operator-index.html', {"admin_data": admin_data})


def edit(request):
    res = "success"
    try:
        admin_id = request.POST["adminId"]
        admin_name = request.POST["adminName"]
        email = request.POST.get("email", "")
        sex = request.POST["sex"]
        phone = request.POST["phone"]
        admin_obj = HcAdmin.objects.get(pk=admin_id)
        if admin_obj.information:
            admin_obj.information.name = admin_name
            admin_obj.information.email = email
            admin_obj.information.sex = sex
            admin_obj.information.save()
        else:
            information = UserInformation.objects.create(name=admin_name, email=email)
            admin_obj.information = information

        if "adminPic" in request.FILES:
            img = request.FILES['adminPic']
            img_path = deal_with_upload_file(img)
            if img_path:
                if admin_obj.information.portrait:
                    admin_obj.information.portrait.picture.name = img.name
                    admin_obj.information.portrait.picture.path = img_path
                else:
                    pic_obj = Picture.objects.create(name=img.name, path=img_path)
                    por_obj = Portrait.objects.create(picture=pic_obj)
                    admin_obj.information.portrait = por_obj
                admin_obj.information.portrait.picture.save()
                admin_obj.information.portrait.save()

        admin_obj.information.save()
        admin_obj.phone = phone
        admin_obj.save()

    except Exception as e:
        res = "fail"
        LOG.error('Edit admin primary info error. %s' % e)

    return HttpResponse(simplejson.dumps({"res": res}))


def phone(request):
    admin_id = request.session.get('_auth_user_id', 1)
    admin_data = admin_detail_data(admin_id)
    role = request.GET.get("role", "m")
    return render(request, "background/user/admin/phone.html", {"phone": admin_data["phone"],
                                                                "admin_data": admin_data,
                                                                "role": role})


def name_validate(request):
    res = "no"
    try:
        user_name = request.GET['user_name']
        user_id = request.GET.get('id', '')
        if not user_id:
            producer = HcAdmin.objects.filter(username=user_name)
        else:
            producer = HcAdmin.objects.filter(Q(username=user_name), ~Q(pk=user_id))
        if producer:
            res = "exists"
            return HttpResponse(res)
    except Exception as e:
        res = "error"
        LOG.error('validate producer name error. %s' % e)
    return HttpResponse(res)


def active_set(request):
    try:
        ac = request.GET['is_active']
        user_id = request.GET['user_id']
        pobj = HcAdmin.objects.get(pk=user_id)
        if ac == "true":
            pobj.is_active = False
            pobj.save()
        else:
            pobj.is_active = True
            pobj.save()
    except Exception as e:
        LOG.error('Can not change producer active status. %s' % e)
    return redirect('background-user-role')


def password_reset(request):
    res = "success"
    try:
        producer_id = request.GET['admin_id']
        p = HcAdmin.objects.get(pk=producer_id)
        p.password = pwd_hash(DEFAULT_PASSWORD)
        p.save()
    except Exception as e:
        res = "fail"
        LOG.error('Reset producer password error. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def admin_edit(request):
    user_id = request.GET["user_id"]
    user_obj = HcAdmin.objects.get(pk=user_id)
    areas = []
    for item in SupportedArea.objects.all():
        a_params = {}
        a_params['id'] = item.id
        a_params['province'] = item.province
        a_params['city'] = item.city
        if user_obj.area_id == item.id:
            a_params['is_checked'] = True
        else:
            a_params['is_checked'] = False
        if item.id == 2:
            continue
        areas.append(a_params)
    information = {}
    info_obj = UserInformation.objects.filter(pk=user_obj.information_id)
    if info_obj:
        try:
            information['portrait'] = info_obj[0].portrait.picture.path
            information['name'] = info_obj[0].name
            information['email'] = info_obj[0].email
            information['sex'] = info_obj[0].sex
            information['birthday'] = info_obj[0].birthday.strftime('%Y-%m-%d')
            information['tel'] = info_obj[0].tel
            information['qq'] = info_obj[0].qq
            information['weibo'] = info_obj[0].weibo
            information['weixin'] = info_obj[0].weixin
            information['address'] = info_obj[0].addresses.all()[0].address
            print information['address']
        except Exception as e:
            pass
    return render(request, 'background/user/admin/edit.html', {"user_id": user_id,
                                                                   "areas": areas, 'user': user_obj, 'info': information})


def info_edit_action(request):
    res = "success"
    try:
        info = action_info_new(request)
        user_id = request.POST['userId']
        username = request.POST['username']
        phone = request.POST["phone"]
        ad_t = request.POST["adminT"]
        area_id = request.POST["supArea"]
        area = SupportedArea.objects.get(id=area_id)
        is_active = True
        if "isActive" in request.POST:
            ac = request.POST['isActive']
            if ac == "off":
                is_active = False
        user = HcAdmin.objects.get(pk=user_id)
        information_edit(request, user)
        user.username = username
        user.phone = phone
        user.type = ad_t
        user.area = area
        user.is_active = is_active
        user.save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create user address. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def information_edit(request, user):
    res = "success"
    try:
        por_obj = ""
        if "portrait" in request.FILES:
            por = request.FILES["portrait"]
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST.get("infoName", "")
        sex = request.POST.get("sex", "")
        birthday = request.POST.get("birthday", "")
        tel = request.POST.get("tel", "")
        qq = request.POST.get('qq', "")
        weibo = request.POST.get("weibo", "")
        weixin = request.POST.get("weixin", "")
        email = request.POST.get("email", "")
        if user.information:
            if por_obj:
                user.information.portrait = por_obj
                user.information.name = name
                user.information.sex = sex
                user.information.birthday = birthday
                user.information.tel = tel
                user.information.qq = qq
                user.information.weibo = weibo
                user.information.weixin = weixin
                user.information.email = email
                user.information.save()
            else:
                user.information.name = name
                user.information.sex = sex
                user.information.birthday = birthday
                user.information.tel = tel
                user.information.qq = qq
                user.information.weibo = weibo
                user.information.weixin = weixin
                user.information.email = email
                user.information.save()
        else:
            if por_obj:
                info1 = UserInformation(name=name, portrait=por_obj, sex=sex,
                                        email=email, tel=tel, qq=qq, weibo=weibo, weixin=weixin, birthday=birthday)
            else:
                info1 = UserInformation(name=name, sex=sex, email=email,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin, birthday=birthday)
            info1.save()
            user.information = info1
            user.save()
        addr = request.POST["InfoAddr"]
        if not user.information.addresses:
            a1 = UserAddress.objects.create(address=addr)
            info1.addresses.add(a1)
        else:
            addr_id = user.information.addresses.all()[0].id
            UserAddress.objects.filter(pk=addr_id).update(address=addr)
        # if "addrList" in request.POST:
        #     ids = request.POST.getlist("addrList")
        #     uads = UserAddress.objects.filter(id__in=ids)
        #     for uad in uads:
        #         info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Can not edit user information. %s' % e)
        res = "fail"
    return res


def action_delete_obo(request):
    res = "success"
    try:
        user_id = request.GET["user_id"]
        HcAdmin.objects.filter(pk=user_id).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete user address. %s' % e)
    return HttpResponse(simplejson.dumps(res))
