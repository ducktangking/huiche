# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background.user.admin import views

urlpatterns = [
    url(r'^$', views.index, name='background-user-admin-index'),
    url(r'^data/$', views.data, name='background-user-admin-data'),
    url(r'^new/$', views.new, name='background-user-admin-new'),
    url(r'^delete/action/$', views.action_delete, name="background-user-admin-action-delete"),
    url(r'^new/action/$', views.action_new, name="background-user-admin-action-new"),
    url(r'^validate/$', views.validate, name="background-user-admin-validate"),
    url(r'^info/detail/$', views.admin_info, name="background-user-admin-info"),
    url(r'^info/data/$', views.info_data, name="background-user-admin-info-data"),
    url(r'^detail/$', views.admin_detail, name="background-user-admin-detail"),
    url(r'^update/$', views.edit, name="background-user-admin-edit"),
    url(r'^phone/$', views.phone, name="background-user-admin-phone"),
    url(r'^role/$', views.role_index, name="background-user-role"),
    url(r'^operator/detail/$', views.operator_detail, name="background-user-operator-detail"),
    url(r'^name/validate/$', views.name_validate, name="background-admin-name-validate"),
    url(r'^active/$', views.active_set, name='background-admin-active'),
    url(r'^pwd/reset/$', views.password_reset, name='background-admin-pwd-reset'),
    url(r'^info/edit/$', views.admin_edit, name='background-user-admin-info-edit'),
    url(r'edit/action/$', views.info_edit_action, name='background-user-admin-edit-action'),
    url(r'^delete/action/obo/$', views.action_delete_obo, name='background-user-admin-delete-action-obo')
]
