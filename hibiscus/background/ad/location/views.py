import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Location
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
LOCATION_COLUMNS = ["index", "name", "width", "height", "status", "created_at"]


def index(request):
    return render(request, 'background/ad/location/index.html')


@data_table(columns=LOCATION_COLUMNS, model=Location)
def data(request):
    try:
        sts = Location.objects.filter(
            Q(name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["index"] = st.index
            st_params["name"] = st.name
            st_params["width"] = st.width
            st_params["height"] = st.height
            st_params["status"] = True if hasattr(st, "ad") else False
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get ad location data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Location.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete Location. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/ad/location/new.html')


def action_new(request):
    res = "success"
    try:
        lo_index = request.POST["index"]
        name = request.POST["localName"]
        width = request.POST["adWidth"]
        height = request.POST["adHeight"]
        is_app = request.POST.get("is_app", None)
        if is_app == "on":
            is_val = True
        else:
            is_val = False
        Location(name=name, width=width, height=height, index=lo_index, is_app=is_val).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create location. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
