from django.conf.urls import url

from hibiscus.background.ad.location import views

urlpatterns = [
    url(r'^$', views.index, name="background-ad-location-index"),
    url(r'^data/$', views.data, name="background-ad-location-data"),
    url(r'^delete/action/$', views.action_delete, name="background-ad-location-action-delete"),
    url(r'^new/$', views.new, name="background-ad-location-new"),
    url(r'^new/action/$', views.action_new, name="background-ad-location-action-new"),

]