# -*- coding:utf-8 -*-
import simplejson
import logging
import time
import datetime
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Ad, SupportedArea, Location, Picture
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
AD_COLUMNS = ["name", "link", "consumer", "time"]


def index(request):
    return render(request, 'background/ad/adetail/index.html')


@data_table(columns=AD_COLUMNS, model=Ad)
def data(request):
    try:
        key_word = request.table_data["key_word"]
        sts = Ad.objects.filter(
            Q(name__contains=key_word) | Q(consumer__contains=key_word) |
            Q(location__name__contains=key_word) | Q(brief__contains=key_word)). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["location"] = u"页面:%s<br>名称:%s(%sx%s)" % \
                                    (st.location.index, st.location.name, st.location.width, st.location.height)\
                if st.location else ""
            st_params["link"] = st.link
            st_params["brief"] = st.brief
            st_params["consumer"] = st.consumer
            off_time_at = timezone.localtime(st.created_at) + datetime.timedelta(days=st.time)
            st_params["time"] = u"广告时间长:%s<br>过期时间:%s" % (st.time, off_time_at.strftime('%Y-%m-%d %H:%M:%S'))
            st_params["picture"] = st.picture.path
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params["is_timeout"] = st.is_timeout
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get ad detail data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Ad.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete ad detail. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/ad/adetail/new.html')


def action_new(request):
    res = "success"
    try:
        name = request.POST["adName"]
        area_id = request.POST['adArea']
        area_obj = SupportedArea.objects.get(id=area_id)
        pic = request.FILES["adPicture"]
        path = deal_with_upload_file(pic)
        pic_obj = Picture.objects.create(name=pic.name, path=path)

        link = request.POST['adLink']
        consumer = request.POST['adConsumer']

        ti = request.POST['adTime']

        desc = request.POST['adDesc']

        # location_select = request.POST.get("adlocationSelect", None)
        # if not location_select:
        #     location_id = request.POST["adLocation"]
        #     location = Location.objects.get(id=location_id)
        #     Ad(name=name, area=area_obj, location=location, picture=pic_obj, link=link,
        #        consumer=consumer, time=ti, brief=desc).save()
        # else:
        #     Ad(name=name, area=area_obj, picture=pic_obj, link=link,
        #        consumer=consumer, time=ti, brief=desc).save()
        Ad(name=name, area=area_obj, picture=pic_obj, link=link,
           consumer=consumer, time=ti, brief=desc).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create cartb. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new_data(request):
    data = {}
    try:
        areas = [{"id": item.id, "province": item.province, "city": item.city}
                 for item in SupportedArea.objects.all()]
        location = [{"id": item.id, "width": item.width, "height": item.height, "name": item.name}
                    for item in Location.objects.all()]
        data["areas"] = areas
        data["location"] = location
    except Exception as e:
        LOG.error('Can not get ad detail new data. %s' % e)
    return HttpResponse(simplejson.dumps(data))


def bind(request):
    ad_id = request.GET.get("aId", None)
    return render(request, 'background/ad/adetail/bind.html', {"aId": ad_id})


def action_bind(request):
    ad_id = request.POST.get("aId", None)
    location_id = request.POST.get("lId", None)
    res = "fail"
    if ad_id and location_id:
        try:
            ad = Ad.objects.get(id=ad_id)
            location = Location.objects.get(id=location_id)
            if ad and location:
                if hasattr(location, "ad"):
                    ad1 = location.ad
                    ad1.location = None
                    ad1.save()
                ad.location = location
                ad.save()
                res = "success"
        except Exception as e:
            LOG.error('Can not ad location bind. %s' % e)
    return HttpResponse(res)


def action_unbind(request):
    ad_id = request.POST.get("aId", None)
    res = "fail"
    if ad_id :
        try:
            ad = Ad.objects.get(id=ad_id)
            if ad :
                ad.location = None
                ad.save()
                res = "success"
        except Exception as e:
            LOG.error('Can not ad location unbind. %s' % e)
    return HttpResponse(res)
