from django.conf.urls import url

from hibiscus.background.ad.adetail import views

urlpatterns = [
    url(r'^$', views.index, name="background-ad-adetail-index"),
    url(r'^data/$', views.data, name="background-ad-adetail-data"),
    url(r'^delete/action/$', views.action_delete, name="background-ad-adetail-action-delete"),
    url(r'^new/$', views.new, name="background-ad-adetail-new"),
    url(r'^new/action/$', views.action_new, name="background-ad-adetail-action-new"),
    url(r'^new/data/$', views.new_data, name="background-ad-adetail-new-data"),


    url(r'^bind/$', views.bind, name="background-ad-adetail-bind"),
    url(r'^bind/bind/$', views.action_bind, name="background-ad-adetail-action-bind"),
    url(r'^bind/unbind/$', views.action_unbind, name="background-ad-adetail-action-unbind"),
]