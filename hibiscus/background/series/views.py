# -*-coding:utf8-*-
import simplejson
import logging

from django.db.models import Q
from django.shortcuts import render
from django.http import HttpResponse

from hibiscus.background.public import deal_with_upload_file
from hibiscus.models import Series, Brand, Picture, Logo, SeriesType,\
    SeriesCallOut, SeriesYearColor, Car, CarTypeColor, SeriesKind, SeriesPortalAd,\
    SeriesNewCar, SeriesPictureShow, SeriesCharacterShow
from hibiscus.decorators import data_table
from django.utils import timezone

LOG = logging.getLogger(__name__)
SERIES_COLUMNS = ["brand__name", "name", "name"]
FILE_FORMAT_ERROR = "file format error"


def index(request):
    brand_id = ""
    brand_obj = ""
    if "brandId" in request.GET:
        brand_id = request.GET["brandId"]
        if brand_id:
            brand_obj = Brand.objects.get(pk=brand_id)
    return render(request, 'background/series/index.html', {"brand_id": brand_id, "brand_obj": brand_obj})


def new(request):
    brand_id = ""
    brand_obj = ""
    if "brandId" in request.GET:
        brand_id = request.GET["brandId"]
        if brand_id:
            brand_obj = Brand.objects.get(pk=brand_id)
    brands = Brand.objects.filter(parent__isnull=False)
    topbrand = Brand.objects.filter(parent__isnull=True)
    series_kind_parent = SeriesKind.objects.filter(parent_id__isnull=True)
    series_kind_list = list()
    for parent_obj in series_kind_parent:
        parent_name = parent_obj.name
        parent_id = parent_obj.id
        series_kind_arr = SeriesKind.objects.filter(parent_id=parent_id)
        parent_param = dict()
        parent_param['name'] = parent_name
        parent_param['id'] = parent_id
        kind_list = list()
        for kind_obj in series_kind_arr:
            kind_param = dict()
            kind_param['id'] = kind_obj.id
            kind_param['name'] = kind_obj.name
            kind_list.append(kind_param)
        parent_param["kind"] = kind_list
        series_kind_list.append(parent_param)
    st = SeriesType.objects.all()
    series_call_out = SeriesCallOut.objects.all()
    series_new_car_position = SeriesNewCar.objects.all()
    return render(request, 'background/series/new.html',
                  {"brandId": brand_id, "brand_obj": brand_obj,
                    "brands": brands, "st": st,
                    "series_call_out": series_call_out,
                   "num":range(1, 9),
                   "topbrand": topbrand,
                   "new_car_position": series_new_car_position,
                   "seires_kind": simplejson.dumps(series_kind_list),
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def action_new(request):
    res = "success"
    try:
        img = request.FILES["seriesLogo"]
        brand_id = request.POST["brandId"]
        s_name = request.POST["serName"]
        series_call_out = request.POST.getlist('isHot')
        related_list = request.POST['relatedSeries']
        has_ad_check = request.POST.get('has_ad', "")
        is_new_car = request.POST.get("is_new_car", "")
        has_ad = True if str(has_ad_check)=='on' else False
        new_car = True if str(is_new_car)=='on' else False
        series_kind_id = request.POST.get("series_kind")
        img_path = deal_with_upload_file(img)
        if related_list:
            related_series = related_list.split(',')
        else:
            related_series = []
        if not img_path:
            return HttpResponse(FILE_FORMAT_ERROR)

        p_obj = Picture.objects.create(name=img.name, path=img_path)
        l_obj = Logo.objects.create(picture=p_obj)
        kind_obj = SeriesKind.objects.get(pk=series_kind_id)
        st = request.POST['seriesType']
        st_obj = SeriesType.objects.get(pk=st)

        ad_obj = None
        if has_ad:
            ad_img = request.FILES['ad_pic']
            ad_img_path = deal_with_upload_file(ad_img)
            ad_link = request.POST.get("ad_link", "")
            ad_position = request.POST.get("ad_position", "")
            ad_pic_obj = Picture.objects.create(name=ad_img.name, path=ad_img_path)
            is_exist = SeriesPortalAd.objects.filter(position=ad_position)
            for old_ad in is_exist:
                old_ad.delete()
            ad_obj = SeriesPortalAd.objects.create(link=ad_link, position=ad_position, pic=ad_pic_obj)
            # s.series_ad = ad_obj
        po_obj = None
        if new_car:
            position_id = request.POST.get("new_car", "")
            po_obj = SeriesNewCar.objects.get(pk=position_id)
            old_new = Series.objects.filter(new_car=po_obj)
            if old_new:
                old_new.update(new_car=None)
            # s.new_car = po_obj

        s = Series.objects.create(logo=l_obj, name=s_name, type=st_obj,
                                  kind=kind_obj, series_ad=ad_obj, new_car=po_obj)
        if brand_id:
            b_obj = Brand.objects.get(id=brand_id)
            s.brand = b_obj
            s.save()
        if series_call_out:
            sc_obj = SeriesCallOut.objects.filter(pk__in=series_call_out)
            for sc in sc_obj:
                s.is_hot.add(sc)
        if related_series:
            rs_obj = Series.objects.filter(pk__in=related_series)
            for rs in rs_obj:
                s.related_s.add(rs)
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create cart series. %s' % e)

    return HttpResponse(res)


@data_table(columns=SERIES_COLUMNS, model=Series)
def data(request):
    try:
        brand_id = request.GET["brand_id"]
        if brand_id:
            sts = Series.objects.filter(
                Q(brand__id=brand_id), Q(name__contains=request.table_data["key_word"])
            ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
            total_num  = Series.objects.filter(brand__id=brand_id).count()
        else:
            sts = Series.objects.filter(
                Q(name__contains=request.table_data["key_word"])
            ).order_by(request.table_data["order_cl"])[request.table_data["start"]:request.table_data["end"]]
            total_num = Series.objects.all().count()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["brand"] = st.brand.name
            if st.logo:
                st_params["pic_path"] = st.logo.picture.path
            else:
                st_params['pic_path'] = ""
            st_params['is_app'] = st.is_app
            st_params['is_pc'] = st.is_pc
            st_params['position'] = st.position
            st_params['page'] = st.page
            st_list.append(st_params)
        return st_list, total_num
    except Exception as e:
        LOG.error('Get area index data error. %s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Series.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def edit(request):
    edit_data = {}
    brand_params = []
    try:
        if 'source' in request.GET:
            is_brand = True
        else:
            is_brand = False
        series_id = request.GET['seriesId']
        s_obj = Series.objects.get(pk=series_id)
        edit_data['name'] = s_obj.name
        edit_data['is_hot'] = s_obj.is_hot.all()
        edit_data['img_path'] = s_obj.logo.picture.path
        edit_data['logo_id'] = s_obj.logo.id
        edit_data['id'] = series_id
        brand = s_obj.brand.name
        brands = Brand.objects.all()
        topbrand = Brand.objects.filter(parent__isnull=True)
        for b in brands:
            b_params = {}
            b_params['id'] = b.id
            if b.name == brand:
                b_params['is_checked'] = True
            else:
                b_params['is_checked'] = False
            b_params['name'] = b.name
            b_params['is_hot'] = b.is_hot
            brand_params.append(b_params)
        call_out_list = []
        call_out = SeriesCallOut.objects.all()
        for co in call_out:
            cp = {}
            cp['name'] = co.name
            cp['id'] = co.id
            if co in s_obj.is_hot.all():
                cp['is_checked'] = True
            else:
                cp['is_checked'] = False
            call_out_list.append(cp)
        related_series_list = []
        related_series = Series.objects.filter(related_series=s_obj)
        for rs in related_series:
            rs_params = {}
            rs_params['id'] = rs.id
            rs_params['name'] = rs.name
            related_series_list.append(rs_params)

        checked_series_kind_obj = s_obj.kind
        kind_id = checked_series_kind_obj.id if checked_series_kind_obj else -1
        kind_parent_id = checked_series_kind_obj.parent_id if checked_series_kind_obj else -1
        series_kind_parent = SeriesKind.objects.filter(parent_id__isnull=True)
        series_kind_list = list()
        for parent_obj in series_kind_parent:
            parent_name = parent_obj.name
            parent_id = parent_obj.id
            series_kind_arr = SeriesKind.objects.filter(parent_id=parent_id)
            parent_param = dict()
            parent_param['name'] = parent_name
            parent_param['id'] = parent_id
            parent_param['is_checked'] = True if parent_id == kind_parent_id else False
            kind_list = list()
            for kind_obj in series_kind_arr:
                kind_param = dict()
                kind_param['id'] = kind_obj.id
                kind_param['name'] = kind_obj.name
                kind_param['is_checked'] = True if kind_id == kind_obj.id else False
                kind_list.append(kind_param)
            parent_param["kind"] = kind_list
            series_kind_list.append(parent_param)
                #print related_series_list
        ad_obj = s_obj.series_ad
        ad_dict = dict()
        if ad_obj:
            ad_dict['link'] = ad_obj.link
            ad_dict['pic'] = ad_obj.pic.path
            ad_dict['position'] = int(ad_obj.position)

        new_car_obj = s_obj.new_car
        position_id = new_car_obj.id if new_car_obj else -1
        new_car_list = SeriesNewCar.objects.all()

    except Exception as e:
        LOG.error('Can not get series edit data. %s' % e)
    return render(request, 'background/series/edit.html',
                  {"edit_data": edit_data,
                   "brands": brand_params,
                   "call_out_list": call_out_list,
                   "is_brand": is_brand,
                   "topbrand": topbrand,
                   "related_series": related_series_list,
                   "series_kind":series_kind_list,
                   "series_kind_list":simplejson.dumps(series_kind_list),
                   "ad_dict":ad_dict,
                   "num":range(1, 9),
                   "position_id":position_id,
                   "new_car_list": new_car_list,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]
                   })


def edit_action(request):
    res = "success"
    try:
        seriesId = request.POST['seriesId']
        name = request.POST['serName']
        related_list = request.POST['relatedSeries']
        brandId = request.POST['seriesBrand']
        brand_obj = Brand.objects.get(pk=brandId)
        is_hot = request.POST.getlist('isHot')
        series_kind_id = request.POST.get("series_kind")
        has_ad_check = request.POST.get('has_ad', "")
        is_new_car = request.POST.get("is_new_car", "")
        new_car = True if str(is_new_car)=='on' else False
        has_ad = True if str(has_ad_check)=='on' else False

        if related_list:
            related_series = related_list.split(',')
        else:
            related_series = []
        if "logo" in request.FILES:
            img = request.FILES['logo']
            pic_path = deal_with_upload_file(img)
            pic = Picture.objects.create(name=img.name, path=pic_path)
            logo_obj = Logo.objects.create(picture=pic)
        else:
            logo_id = request.POST['originalLogoId']
            logo_obj = Logo.objects.get(pk=logo_id)
        sobj = Series.objects.get(pk=seriesId)

        ad_obj = None
        if has_ad:
            ad_img = request.FILES.get('ad_pic', None)
            if ad_img:
                ad_img_path = deal_with_upload_file(ad_img)
                ad_link = request.POST.get("ad_link", "")
                ad_position = request.POST.get("ad_position", "")
                ad_pic_obj = Picture.objects.create(name=ad_img.name, path=ad_img_path)
                is_exist = SeriesPortalAd.objects.filter(position=ad_position)
                for old_ad in is_exist:
                    old_ad.delete()
                ad_obj = SeriesPortalAd.objects.create(link=ad_link, position=ad_position, pic=ad_pic_obj)
            else:
                ad_link = request.POST.get("ad_link", "")
                ad_position = request.POST.get("ad_position", "")
                pic = sobj.series_ad.pic
                is_exist = SeriesPortalAd.objects.filter(position=ad_position)
                for old_ad in is_exist:
                    old_ad.delete()
                ad_obj = SeriesPortalAd.objects.create(link=ad_link, position=ad_position, pic=pic)
        else:
            if sobj.series_ad:
                sobj.series_ad.delete()
        kind_obj = SeriesKind.objects.get(pk=series_kind_id)

        po_obj = None
        if new_car:
            position_id = request.POST.get("new_car", "")
            po_obj = SeriesNewCar.objects.get(pk=position_id)
            old_new = Series.objects.filter(new_car=po_obj)
            if old_new:
                old_new.update(new_car=None)

        Series.objects.filter(pk=seriesId).\
            update(brand=brand_obj, name=name, logo=logo_obj, kind=kind_obj, series_ad=ad_obj, new_car=po_obj)
        if sobj:
            sobj.is_hot.clear()
            sobj.related_s.clear()
            if is_hot:
                sc_obj = SeriesCallOut.objects.filter(pk__in=is_hot)
                for sc in sc_obj:
                    sobj.is_hot.add(sc)
            if related_series:
                rs_obj = Series.objects.filter(pk__in=related_series)
                for rs in rs_obj:
                    sobj.related_s.add(rs)
    except Exception as e:
        LOG.error('Edit series info error. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def set_pc(request):
    res = "success"
    try:
        series_id = request.GET['series_id']
        brobj = Series.objects.filter(pk=series_id)
        if brobj:
            brobj[0].is_pc = not brobj[0].is_pc
            brobj[0].save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def set_un(request):
    res = "success"
    try:
        series_id = request.GET['series_id']
        brobj = Series.objects.filter(pk=series_id)
        if brobj:
            brobj[0].is_un = not brobj[0].is_un
            if brobj[0].is_un ==True:
                series_un = Series.objects.filter(is_un=True)
                if series_un:
                    res = 'exist'
                    for su in series_un:
                        su.is_un = False
                        su.save()
            brobj[0].save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def set_ad8(request):
    res = "success"
    try:
        series_id = request.GET['series_id']
        brobj = Series.objects.filter(pk=series_id)
        if brobj:
            brobj[0].is_ad8 = not brobj[0].is_ad8
            brobj[0].save()
            seriesnum = Series.objects.filter(is_ad8=True)
            if len(seriesnum) > 8:
                brobj[0].is_ad8 = not brobj[0].is_ad8
                brobj[0].save()
                res = 'enough'
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def set_newcar(request):
    res = "success"
    try:
        series_id = request.GET['series_id']
        brobj = Series.objects.filter(pk=series_id)
        if brobj:
            brobj[0].is_newcar = not brobj[0].is_newcar
            brobj[0].save()
            series_num = Series.objects.filter(is_newcar=True)
            if len(series_num) > 4:
                brobj[0].is_newcar = not brobj[0].is_newcar
                brobj[0].save()
                res = 'enough'
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def set_app(request):
    res = "success"
    try:
        series_id = request.GET['series_id']
        brobj = Series.objects.filter(pk=series_id)
        if brobj:
            brobj[0].is_app = not brobj[0].is_app
            brobj[0].save()
    except Exception as e:
        res = "fail"
        LOG.error('Can not set brand to pc. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def validate_name(request):
    try:
        res = "no"
        name = request.GET["name"]
        brand_id = request.GET["brand_id"]
        brand = Series.objects.filter(name=name, brand__id=brand_id)
        if brand:
            res = "exists"
    except Exception, e:
        res = "error"
        LOG.error('Error happened when get unique brand. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def get_relation_series(request):
    brands = []
    top_brands = []
    series = []
    try:
        top_brands = Brand.objects.filter(parent__isnull=True)
        if top_brands:
            brands = Brand.objects.filter(parent_id=top_brands[0].id).filter(parent__isnull=False)
            if brands:
                series = Series.objects.filter(brand__id=brands[0].id)
    except Exception as e:
        LOG.error('Can not get special series. %s' % e)
    return render(request, 'background/series/relation_series.html',
                  {"series": series,
                   "brands": brands,
                   "top_brands": top_brands,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)]})


def series_relation(request):
    brands = []
    top_brands = []
    series = []
    try:
        series_id = request.GET.get("seriesId", "")
        top_brands = Brand.objects.filter(parent__isnull=True)
        if top_brands:
            brands = Brand.objects.filter(parent_id=top_brands[0].id).filter(parent__isnull=False)
            if brands:
                series = Series.objects.filter(brand__id=brands[0].id)
    except Exception as e:
        LOG.error('Error happened when series_relation. %s' % e)
    return render(request, 'background/series/relation_series.html',
                  {"series": series,
                   "brands": brands,
                   "top_brands": top_brands,
                   "chars": [chr(i) for i in range(97, 97 + 26, 1)],
                   "series_id": series_id})


def related_series(request):
    data = {}
    try:
        initial = request.GET.get('initial', '')
        topbrandid = request.GET.get('topbrand_id', '')
        secbrandid = request.GET.get('secbrand_id', '')
        existseries_id_list = request.GET.get('existseries_id','')
        secbrand = None
        topbrand = None
        existseries_id = existseries_id_list.split(',')
        try:
            i = 0
            if existseries_id:
                for ei in existseries_id:
                    existseries_id[i] = int(ei)
                    i+=1
        except:
            existseries_id = []
        if initial:
            topbrand = Brand.objects.filter(Q(initial=initial), Q(parent__isnull=True))
            if topbrand:
                secbrand = Brand.objects.filter(parent_id=topbrand[0].id)
                series = Series.objects.filter(brand_id=secbrand[0].id)
        if topbrandid:
            secbrand = Brand.objects.filter(parent_id=topbrandid)
            series = Series.objects.filter(brand_id=secbrand[0].id)
        if secbrandid:
            series = Series.objects.filter(brand_id=secbrandid)
        series_list = []
        for se in series:
            se_params = {}
            se_params['id'] = se.id
            if se_params['id'] in existseries_id:
                continue
            se_params['name'] = se.name
            series_list.append(se_params)
        if topbrand:
            topbrand_list =[]
            for tp in topbrand:
                tp_params = {}
                tp_params['id'] = tp.id
                tp_params['name'] = tp.name
                topbrand_list.append(tp_params)
            secbrand_list =[]
            for sc in secbrand:
                sc_params = {}
                sc_params['id'] = sc.id
                sc_params['name'] = sc.name
                secbrand_list.append(sc_params)
            data['topbrand'] = topbrand_list
            data['secbrand'] = secbrand_list
            data['series'] = series_list
        if secbrand and not topbrand:
            secbrand_list = []
            for sc in secbrand:
                sc_params = {}
                sc_params['id'] = sc.id
                sc_params['name'] = sc.name
                secbrand_list.append(sc_params)
            data['topbrand'] = ''
            data['secbrand'] = secbrand_list
            data['series'] = series_list
        if secbrandid:
            data['topbrand'] = ''
            data['secbrand'] = ''
            data['series'] = series_list
    except Exception, e:
        LOG.error('Error happened when search related series. %s' % e)
    return HttpResponse(simplejson.dumps(data), content_type="application/json")


def series_set_ad(request):
    res = 'fail'
    try:
        position = request.GET.get('position', '')
        series_id = request.GET.get('series_id', '')
        se = Series.objects.filter(pk=series_id).update(position=position)
        if se:
            res= 'success'
    except Exception as e:
        LOG.error('Error happened when series set ad. %s' % e)
    return HttpResponse(simplejson.dumps(res))


def series_index_po(request):
    res = 'fail'
    try:
        page = request.GET.get('page', '')
        series_id = request.GET.get('series_id', '')
        se = Series.objects.filter(pk=series_id).update(page=page)
        if se :
            res = 'success'
    except Exception as e:
        LOG.error('Error happened when series index po. %s' % e)
    return HttpResponse(simplejson.dumps(res))


def color_index(request):
    try:
        series_id = request.GET.get("series_id")
    except Exception as e:
        LOG.error("Error happened in serires-color-index: {0}".format(str(e)))
    return render(request, "background/series/car_color.html", {"series_id":series_id})


@data_table(columns=["year", "color__side", "color__name", "created_at", "created_at"], model=SeriesYearColor)
def color_data(request):
    data = list()
    try:
        series_id = request.GET.get("series_id")
        # series = Series.objects.get(pk=series_id)
        series_color_list = SeriesYearColor.objects.filter(
            Q(series_id=series_id),
            Q(year__contains=request.table_data["key_word"])|
            Q(color__name__contains=request.table_data["key_word"]) |
            Q(color__rgb__contains=request.table_data["key_word"])).distinct().order_by(request.table_data["order_cl"])
        for color in series_color_list:
            for i in range(0,2):
                data_param = dict()
                data_param['DT_RowId'] = color.id
                data_param['year'] = color.year
                data_param['side'] = i
                data_param['created_at'] = timezone.localtime(color.created_at).strftime('%Y-%m-%d %H:%M:%S')
                data_param['color'] = list()
                color_obj_list = color.color.all().filter(side=i)
                if not color_obj_list:
                    continue
                else :
                    for color_obj in color_obj_list:
                        color_param = dict()
                        color_param['id'] = color_obj.id
                        color_param['name'] = color_obj.name
                        color_param['rgb'] = color_obj.rgb
                        data_param['color'].append(color_param)
                data.append(data_param)
        return data[request.table_data["start"]: request.table_data["end"]], len(data)

    except Exception as e:
        LOG.error("Error happened in series-color-data: {0}".format(str(e)))


def color_new(request):
    try:
        series_id = request.GET.get("series_id")
        # series_obj = Series.objects.get(pk=series_id)
        car_year_list = Car.objects.filter(series_id=series_id).values("produce_year").distinct()
        year_list = list()
        for car_year in car_year_list:
            year_list.append(car_year['produce_year'])

    except Exception as e:
        LOG.error("Error happened in serires-color-new: {0}".format(str(e)))
    return render(request, "background/series/series_color_new.html", {"series_id":series_id, "year":year_list})


def color_new_action(request):
    res = "success"
    try:
        series_id = request.POST['series_id']
        year = request.POST['year']
        color_str = request.POST['color_str']
        single_color_str = color_str.split(";")[:-1] if color_str else list()
        series_obj = Series.objects.get(pk=series_id)
        color_list = list()
        for s_color in single_color_str:
            color_param = s_color.split(",")
            name = color_param[0]
            color_type = color_param[1]
            color_value = color_param[2]
            a = CarTypeColor(name=name, side=color_type,rgb=color_value)
            a.save()
            color_list.append(a)
        series_year_color = SeriesYearColor.objects.filter(Q(series_id=series_id), Q(year=year))
        color_old_list = series_year_color[0].color.all() if series_year_color else list()
        if color_old_list:
            series_year_color[0].delete()

        b = SeriesYearColor(series=series_obj, year=year)
        b.save()
        for c in  color_old_list:
            b.color.add(c)
        for d in color_list:
            b.color.add(d)

        # b.color.add(c for c in color_old_list+color_list)
            # print name,color_value

    except Exception as e:
        LOG.error("Error happened in serires-color-new-action: {0}".format(str(e)))
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def color_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_side in id_l:
            if id_side:
                print id_side
                id_str = id_side.split(",")
                id_v = id_str[1]
                side = id_str[0]
                aa = SeriesYearColor.objects.filter(pk=id_v)
                color_list_all = aa[0].color.all()
                color_list = color_list_all.filter(side=side)

                color_list.delete()
                if len(color_list_all) == len(color_list):
                    aa.delete()
                # Brand.objects.filter(pk=id_v).delete()
    except Exception, e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def color_edit(request):
    try:
        series_year_id = request.GET.get("series_year_id")
        side = request.GET.get("side")
        series_color_obj = SeriesYearColor.objects.get(pk=series_year_id)
        color_list = series_color_obj.color.all().filter(side=side)
        series_id = series_color_obj.series_id
        year = series_color_obj.year
        name = series_color_obj.series.name
        color_value_list = list()
        for color in color_list:
            color_param = dict()
            color_param['id'] = color.id
            color_param['side'] = color.side
            color_param['name'] = color.name
            color_param['value'] = color.rgb
            color_value_list.append(color_param)
    except Exception as e:
        LOG.error("Error happened when oped color edit index .%s" % e)
    return render(request, "background/series/series_color_edit.html",
                  {"color_list": color_value_list, "year": year,
                   "name": name, "series_id": series_id,
                   "side": int(side), "series_color_id":series_color_obj.id})


def color_edit_action(request):
    res = "success"
    try:
        color_str = request.POST['color_str']
        series_color_id = request.POST['series_color_id']
        delete_id_str = request.POST['delete_id']
        # print color_str,series_color_id,delete_id_str
        # pass
        single_color_list = color_str.split(";")[:-1] if color_str else list()
        color_list = list()
        for single_color in single_color_list:
            color_agg_list = single_color.split(",")
            color_id = color_agg_list[0]
            color_name = color_agg_list[1]
            color_side = color_agg_list[2]
            color_value = color_agg_list[3]
            a = CarTypeColor.objects.filter(pk=color_id).update(name=color_name, side=color_side, rgb=color_value)
            color_list.append(a)
        delete_id_list = delete_id_str.split(",") if delete_id_str else list()
        CarTypeColor.objects.filter(id__in=delete_id_list).delete()
        # series_color_obj = SeriesYearColor.objects.get(pk=series_color_id)
        # series_color_obj.color.all().filter(~Q(id__in=delete_id_list))
        # for color in color_list:
        #     series_color_obj.color.add(color)
    except Exception as e:
        LOG.error("Error happened in serires-color-new-action: {0}".format(str(e)))
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


# def change_series_kind()
def compare_price(price):
    price_list = [
        {"min":0, "max":5, "tag":u'5万以下'}, {"min":5, "max":8, "tag":u'5-8万'},
        {"min":8, "max":12, "tag":u'8-12万'}, {"min":12, "max":18, "tag":u'12-18万'},
        {"min":18, "max":25, "tag":u'18-25万'}, {"min":25, "max":40, "tag":u'25-40万'},
        {"min":40, "max":10000, "tag":u'40万以上'}
    ]
    for price_dict in price_list:
        min = price_dict['min']
        max = price_dict['max']
        if min<price<=max:
            return price_dict['tag']
    return ""


def transfrom_car_show(kind):
    kind_dict = {
        u"微型":u"小型车",
        u"小型":u"小型车",
        u"紧凑型":u"紧凑型车",
        u"中大型":u"中型车",
        u"豪华型":u"豪华车"
    }
    return kind_dict[kind] if kind_dict.has_key(kind) else ""

def transfrom_suv_show(kind):
    kind_dict ={
        u"小型":u"小型车",
        u"紧凑型": u"紧凑型车",
        u"中大型": u"中型车",
        u"全尺寸": u"豪华车"
    }
    return kind_dict[kind] if kind_dict.has_key(kind) else ""


def series_charcacter_pic_show(request):
    suv_dict = dict()
    hot_dict = dict()
    main_dict = dict()
    whole_price_list = list()
    price_list = [
        {"min":0, "max":5, "tag":u'5万以下'}, {"min":5, "max":8, "tag":u'5-8万'},
        {"min":8, "max":12, "tag":u'8-12万'}, {"min":12, "max":18, "tag":u'12-18万'},
        {"min":18, "max":25, "tag":u'18-25万'}, {"min":25, "max":40, "tag":u'25-40万'},
        {"min":40, "max":10000, "tag":u'40万以上'}
    ]

    try:
        series_id = request.GET.get("series_id")
        series_obj = Series.objects.get(pk=series_id)
        series_kind = series_obj.kind if series_obj.kind else SeriesKind.objects.filter(parent_id__isnull=False)[0]
        chosen_char_id = series_obj.pc_character.id if series_obj.pc_character else -1
        chosen_pic_id = series_obj.pc_picture.id if series_obj.pc_picture else -1
        # car_list = Car.objects.filter(series_id=series_id).order_by("price")

        # car_price = car_list[0].price if car_list else -1
        # price_tag = compare_price(car_price)
        # (price_page_list, price_page) = (list(), -1)

        # 用来记录图片部分
        price_pic_dict = dict()
        suv_pic_dict = dict()
        hot_pic_dict = dict()
        main_pic_dict = dict()
        # 用来记录文字部分
        for price in price_list:
            price_param = dict()
            (price_param['char'], price_param['pic'], price_param['page']) = get_sheet_info(price['tag'],series_kind.name, chosen_char_id, chosen_pic_id)
            price_param['sheet'] = price['tag']
            whole_price_list.append(price_param)
            # (price_page_list, price_pic_dict, price_page) = get_sheet_info(price_tag, series_kind.name, chosen_char_id, chosen_pic_id)
        is_suv = False
        (suv_page_list,suv_page) = (list(), -1)
        if series_kind.parent.name.upper() == "SUV":
            is_suv = True
            (suv_page_list,suv_pic_dict, suv_page) = get_sheet_info("SUV", series_kind.name, chosen_char_id, chosen_pic_id)


        kind_name = transfrom_suv_show(series_kind.name) if is_suv else transfrom_car_show(series_kind.name)
        (hot_page_list,hot_pic_dict, hot_page) = get_sheet_info(u"热门车", kind_name,  chosen_char_id, chosen_pic_id)

        (main_page_list, main_pic_dict, main_page) = get_sheet_info(u"主打车", kind_name,  chosen_char_id, chosen_pic_id)


        suv_dict["pic"] =suv_pic_dict
        suv_dict["char"] =suv_page_list
        suv_dict["sheet"] = "SUV"
        suv_dict['page'] = suv_page

        hot_dict["pic"] =hot_pic_dict
        hot_dict["char"] =hot_page_list
        hot_dict["sheet"] = u"热门车"
        hot_dict['page'] = hot_page

        main_dict["pic"] =main_pic_dict
        main_dict["char"] =main_page_list
        main_dict["sheet"] = u"主打车"
        main_dict['page'] = main_page
    except Exception as e:
        LOG.error("Error happened in show character and pic html {0}".format(str(e)))
    # print simplejson.dumps(hot_dict)
    return render(request, "background/series/model_html.html", {
        "price_tag_list":whole_price_list,
        "suv_dict": suv_dict,
        "hot_dict": hot_dict,
        "main_dict": main_dict,
        "series_id": series_id
    })


def get_sheet_info(sheet, series_kind_name, chosen_char_id, chosen_pic_id):
    data_page_list = list()
    data_page = SeriesCharacterShow.objects.filter(sheet=sheet).order_by("row","col")
    data_row_len = len(data_page) / 4
    page_id = -1
    if data_row_len:
        for i in range(0, 4):
            data_row_list = list()
            data_row_dict = dict()
            for j in range(0, data_row_len):
                data = data_page[data_row_len * i + j]
                data_param = dict()
                data_param['id'] = data.id
                data_param['row'] = data.row
                data_param['col'] = data.col
                data_param['row_name'] = data.row_name
                data_param['is_active'] = True if series_kind_name == data.row_name else False
                data_param['is_checked'] = True if chosen_char_id == data.id else False
                data_row_dict["label"] = data.row_name
                data_row_list.append(data_param)
                page_id = data.page
            data_row_dict["row"] = data_row_list
            data_row_dict["is_active"] = True if data_row_list[0]['is_active'] else False
            data_page_list.append(data_row_dict)
    data_pic_dict = dict()
    data_pic_page = SeriesPictureShow.objects.filter(sheet=sheet)
    data_pic_dict['id'] = data_pic_page[0].id
    data_pic_dict['is_checked'] = True if chosen_pic_id == data_pic_page[0].id else False
    return (data_page_list, data_pic_dict, page_id)


def set_character_pic_position(request):
    series_id  = request.POST.get("series_id")
    character_id = request.POST.get("char_id")
    pic_id = request.POST.get("pic_id")
    data = dict()
    data['res'] = "fail"
    try:
        series_obj = Series.objects.filter(pk=series_id)
        char_obj = None
        if character_id and int(character_id) != -1:
            char_obj = SeriesCharacterShow.objects.get(pk=character_id)
            old_char = Series.objects.filter(pc_character=char_obj)
            if old_char:
                old_char.update(pc_character=None)
        pic_obj = None
        if pic_id and int(pic_id) != -1:
            pic_obj = SeriesPictureShow.objects.get(pk=pic_id)
            old_pic = Series.objects.filter(pc_picture=pic_obj)
            if old_pic:
                old_pic.update(pc_picture=None)
        series_obj.update(pc_character=char_obj, pc_picture=pic_obj)
        data['res'] ="success"
    except Exception as e:
        LOG.error("Error happened in update character and pic action {0}".format(str(e)))
        # print simplejson.dumps(hot_dict)
    return HttpResponse(simplejson.dumps(data))