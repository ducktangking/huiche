__author__ = 'server'

from django.conf.urls import url

from hibiscus.background.series import views

urlpatterns = [
    url(r'^$', views.index, name="background-series-index"),
    url(r'^add/action/$', views.action_new, name="background-series-action-new"),
    url(r'^new/$', views.new, name="background-series-new"),
    url(r'^data/$', views.data, name="background-series-data"),
    url(r'^del/action/$', views.action_delete, name='background-series-action-delete'),
    url(r'^edit/$', views.edit, name='background-series-edit'),
    url(r'^edit/action/$', views.edit_action, name='background-series-edit-action'),
    url(r'^set/app/$', views.set_app, name='background-series-set-app'),
    url(r'^set/pc/$', views.set_pc, name='background-series-set-pc'),
    url(r'^set/un/$', views.set_un, name='background-series-set-un'),
    url(r'^set/ad8/$', views.set_ad8, name='background-series-set-ad8'),
    url(r'^set/newcar/$', views.set_newcar, name='background-series-set-newcar'),
    url(r'^validate/$', views.validate_name, name='background-validate-series-name'),
    url(r'^relation/data/$', views.get_relation_series, name='background-series-relation-series'),
    url(r'^relation/$', views.series_relation, name='background-series-relation'),
    url(r'related/series/$', views.related_series, name='background-related-series'),
    url(r'set/ad/$', views.series_set_ad, name='background-series-set-ad'),
    url(r'index/po/$', views.series_index_po, name='background-series-index-po'),

    url(r'^color/index/$', views.color_index, name='background-series-color-index'),
    url(r'^color/data/$', views.color_data, name="background-series-color-data"),
    url(r'^color/new/$', views.color_new, name="background-series-color-new"),
    url(r'^color/new/action/$', views.color_new_action, name="background-series-color-new-action"),
    url(r'^color/delete/action/$', views.color_delete, name="background-series-color-delete-action"),
    url(r'^color/edit/$', views.color_edit, name="background-series-color-edit"),
    url(r'^color/edit/action/$', views.color_edit_action, name="background-series-color-edit-action"),
    url(r'^pc/page/model/$', views.series_charcacter_pic_show, name="background-series-pc-page-show"),
    url(r'^pc/page/set/action$', views.set_character_pic_position, name="background-series-char-pic-action"),
]

