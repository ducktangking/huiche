# -*-coding:utf8-*-
import logging

import simplejson
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone

from hibiscus.decorators import data_table
from hibiscus.models import Car, Series, CarTypeColor, CarTypeGrade, CarTypeBody, CarTypeEnergy, CarTypeCountry, \
    CarTypeGearbox, CarTypeCC, CarTypeDrive, CarTypeDischarge, CarTypeDoor, CarTypeSeat, CarTypeExtension, CarItemDetail, \
    CarItemTypeDetail, CarDetail, CarTypePicture, Brand, Picture, CarPicture, CarStall,SeriesYearColor

from hibiscus.background.brand.views import brand_car
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
CAR_COLUMNS = ["name", "produce_year", "price", "price_min", "price_max", "is_sell", "is_hot", "is_new", "created_at"]
CAR_COLOR_COLUMNS = ["name", "side", "rgb", "created_at"]
CAR_PIC_COLUMNS = ["series__brand__name", "series__name", "produce_year", "name"]


def index(request):
    series_id = request.GET.get("seriesId", "")
    series_obj = ""
    if series_id:
        series_obj = Series.objects.get(pk=series_id)
    return render(request, 'background/car/index.html', {"series_id": series_id, "series_obj": series_obj})


@data_table(columns=CAR_COLUMNS, model=Car)
def data(request):
    series_id = request.GET["series_id"]
    try:
        if series_id:
            series_obj = Series.objects.get(pk=series_id)
            series_cars = Car.objects.filter(series=series_obj)
            sts = Car.objects.filter(series=series_obj).filter(
                Q(name__contains=request.table_data["key_word"]) |
                Q(series__brand__name__contains=request.table_data["key_word"]) |
                Q(series__name__contains=request.table_data["key_word"])). \
              order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        else:
            sts = Car.objects.filter(Q(name__contains=request.table_data["key_word"]) |
                            Q(series__brand__name__contains=request.table_data['key_word']) |
                            Q(series__name__contains=request.table_data["key_word"])). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["produce_year"] = st.produce_year.split(".", 1)[0]
            st_params["price"] = st.price
            st_params["is_hot"] = st.is_hot
            st_params["brand"] = st.series.brand.name
            st_params['series'] = st.series.name
            st_params['cc'] = st.cc.name
            st_params['gearbox'] = st.gearbox.name
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        if series_id:
            return st_list, len(series_cars)
        else:
            return st_list
    except Exception as e:
        LOG.error('Can not get cart type car data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Car.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete car. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    series_obj = ""
    series_id = request.GET.get("seriesId", "")
    colors_outside = [{"id": item.id, "name": item.name, "rgb": item.rgb}
                      for item in CarTypeColor.objects.filter(side=0)]
    colors_inside = [{"id": item.id, "name": item.name, "rgb": item.rgb}
                      for item in CarTypeColor.objects.filter(side=1)]
    series = [{"id": item.id, "name": item.name} for item in Series.objects.all()]
    grades = [{"id": item.id, "name": item.name} for item in CarTypeGrade.objects.all()]
    bodies = [{"id": item.id, "name": item.name} for item in CarTypeBody.objects.all()]
    countries = [{"id": item.id, "name": item.name} for item in CarTypeCountry.objects.all()]
    gearboxes = [{"id": item.id, "name": item.name} for item in CarTypeGearbox.objects.all()]
    cc = [{"id": item.id, "name": item.name} for item in CarTypeCC.objects.all()]
    drives = [{"id": item.id, "name": item.name} for item in CarTypeDrive.objects.all()]
    discharges = [{"id": item.id, "name": item.name} for item in CarTypeDischarge.objects.all()]
    doors = [{"id": item.id, "name": item.name} for item in CarTypeDoor.objects.all()]
    seats = [{"id": item.id, "name": item.name} for item in CarTypeSeat.objects.all()]

    extensions = [{"id": item.id, "name": item.name} for item in CarTypeExtension.objects.all()]
    stalls = [{"id": item.id, "name": item.name} for item in CarStall.objects.all()]

    # energies = [{"id": item.id, "name": item.name} for item in CarTypeEnergy.objects.all()]
    energies = list()
    engine_list = CarTypeEnergy.objects.filter(parent_id__isnull=True)
    for engine in engine_list:
        eng_param = dict()
        eng_param['id'] = engine.id
        eng_param['name'] = engine.name
        child_list = CarTypeEnergy.objects.filter(parent_id=engine.id)
        child_arr = list()
        for child in child_list:
            ch_param = dict()
            ch_param['id'] = child.id
            ch_param['name'] = child.name
            child_arr.append(ch_param)
        eng_param['energy'] = child_arr
        energies.append(eng_param)
    # types = []
    # for tp in CarItemTypeDetail.objects.all():
    #     t = {}
    #     t["id"] = tp.id
    #     t["name"] = tp.name
    #     t["details"] = []
    #     for item in tp.caritemdetail_set.all():
    #         t["details"].append({"id": item.id, "name": item.name})
    #     types.append(t)
    if series_id != '':
        series_obj = Series.objects.get(pk=series_id)

    return render(request, 'background/car/new.html', {"series_id": series_id,
                                                       "colors_outside": colors_outside,
                                                       "colors_inside": colors_inside,
                                                       "series": series,
                                                       "grades": grades,
                                                       "bodies": bodies,
                                                       "energies": simplejson.dumps(energies),
                                                       "countries": countries,
                                                       "gearboxes": gearboxes,
                                                       "cc": cc,
                                                       "drives": drives,
                                                       "discharges": discharges,
                                                       "doors": doors,
                                                       "seats": seats,
                                                       "extensions": extensions,
                                                       # "types": types,
                                                       "series_obj": series_obj,
                                                       "stalls": stalls})


def action_new(request):
    res = "success"
    try:
        name = request.POST["CarName"]
        produce_year = request.POST["CarProducerYear"]
        price = request.POST["CarPrice"]
        price_min = request.POST["CarPriceMin"]
        price_max = request.POST["CarPriceMax"]
        power = request.POST["CarPower"]
        real_cc = request.POST['realCC']
        stall_id = request.POST['stall']
        stall_obj = CarStall.objects.get(pk=stall_id)
        is_sell = True if request.POST.get("IsSell", None) == "on" else False
        is_hot = True if request.POST.get("IsHot", None) == "on" else False
        is_new = True if request.POST.get("IsNew", None) == "on" else False
        series = Series.objects.get(id=request.POST["series"])
        body = CarTypeBody.objects.get(id=request.POST["body"])
        cc_p = request.POST['cc']
        if cc_p == "-1":
            cc_manual = request.POST['carCC']
            cc_obj = CarTypeCC.objects.filter(name=cc_manual)
            if cc_obj:
               cc = cc_obj[0]
            else:
                cc = CarTypeCC.objects.create(name=cc_manual)
        else:
            cc = CarTypeCC.objects.get(id=request.POST["cc"])
        country = CarTypeCountry.objects.get(id=request.POST["country"])
        energy = CarTypeEnergy.objects.get(id=request.POST["energy"])
        grade = CarTypeGrade.objects.get(id=request.POST["grade"])
        gearbox = CarTypeGearbox.objects.get(id=request.POST["gearbox"])
        drive = CarTypeDrive.objects.get(id=request.POST["drive"])
        discharge = CarTypeDischarge.objects.get(id=request.POST["discharge"])
        door = CarTypeDoor.objects.get(id=request.POST["door"])
        seat = CarTypeSeat.objects.get(id=request.POST["seat"])

        car = Car(series=series, body=body, cc=cc, country=country, name=name, real_cc=real_cc,
                  energy=energy, grade=grade, gearbox=gearbox, drive=drive, stall=stall_obj,
                  discharge=discharge, door=door, seat=seat, produce_year=produce_year,
                  price=price, price_max=price_max, price_min=price_min, is_sell=is_sell,
                  is_hot=is_hot, is_new=is_new, power=power)
        car.save()
        for key, value in request.POST.items():
            if value != "":
                if key.startswith("extensions_"):
                    car.extensions.add(CarTypeExtension.objects.get(id=key.split('_')[1]))
                if key.startswith("colors_"):
                    car.colors.add(CarTypeColor.objects.get(id=key.split('_')[1]))

    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create car. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def car_detail_action(request):
    res = "success"
    try:
        carId = request.POST['carId']
        car = Car.objects.get(pk=carId)
        car.cardetail_set.all().delete()
        for key, value in request.POST.items():
            if key.startswith("details_"):
                car_item = CarItemDetail.objects.get(id=key.split('_')[1])
                car_detail = CarDetail(item=car_item, car=car, value=value)
                car_detail.save()
    except Exception as e:
        LOG.error('Can not add car detail %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res": res}))


def car_params(request):
    try:
        carId = request.GET.get("carId", None)
        if carId:
            car_obj = Car.objects.get(pk=carId)
            details = car_obj.cardetail_set.all()  # cardetail obj
            if details:
                types = []
                for tp in CarItemTypeDetail.objects.all():
                    t = {}
                    t["id"] = tp.id
                    t["name"] = tp.name
                    t["details"] = []

                    for item in tp.caritemdetail_set.all():
                        car_detail_item = item.cardetail_set.all().filter(car=car_obj)
                        t["details"].append({"id": item.id, "name": item.name,
                                             "value": car_detail_item[0].value if car_detail_item else ""})
                    types.append(t)
                return render(request, 'background/car/params.html', {"action": "edit", "types": types, "carId": carId})
            else:
                types = []
                for tp in CarItemTypeDetail.objects.all():
                    t = {}
                    t["id"] = tp.id
                    t["name"] = tp.name
                    t["details"] = []

                    for item in tp.caritemdetail_set.all():
                        t["details"].append({"id": item.id, "name": item.name})
                    types.append(t)
                return render(request, 'background/car/params.html', {"action": "new", "types": types,  "carId": carId})

    except Exception as e:
        LOG.error('Can not get car params. %s' % e)


def car_pic_index(request):

    return render(request, 'background/car/car-pic-index.html')



@data_table(columns=CAR_PIC_COLUMNS, model=CarPicture)
def car_pic_data(request):
    try:
        key_word = request.table_data["key_word"]
        sts = Car.objects.filter(
            Q(series__brand__name__contains=key_word) |
            Q(name__contains=key_word) |
            Q(series__name__contains=key_word) |
            Q(produce_year__contains=key_word)).order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["brand"] = st.series.brand.name
            st_params["series"] = st.series.name
            st_params["produce_year"] = st.produce_year.split('.', 1)[0]
            st_params["name"] = st.name
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get car pic data. %s' % e)


def car_pic_detail(request):
    try:
        car_id = request.GET['carId']
        type_id = request.GET['typeId']
    except Exception as e:
        LOG.error('Can not get car pic detail')
    return render(request, 'background/car/car_pic_detail.html', {"car_id": car_id, "type_id": type_id})


def car_pic_new(request):
    # pic_types = CarTypePicture.objects.all()
    # brands = Brand.objects.all()
    # years_list = []
    # if brands:
    #     br_obj = brands[0]
    #     cars = brand_car(br_obj)
    #     if cars:
    #         car_obj = cars[0]
    #         years = Car.objects.filter(name=car_obj['car_name'])
    #         for y in years:
    #             years_list.append(y.produce_year)
    real_cc_list = []
    car_list = []
    type_id = request.GET['typeId']
    car_id = request.GET['carId']
    car_obj = Car.objects.get(pk=car_id)
    the_car_name = car_obj.series.name + " " + car_obj.produce_year + u"款"
    car_obj_list = Car.objects.filter(Q(series_id=car_obj.series.id), ~Q(pk=car_id), Q(produce_year=car_obj.produce_year))
    for col in car_obj_list:
        if col.real_cc in real_cc_list:
            continue
        else:
            real_cc_list.append(col.real_cc)
    for col in car_obj_list:
        col_params = {}
        col_params['id'] = col.id
        col_params['name'] = col.series.name + " " + col.name
        col_params['real_cc'] = col.real_cc
        car_list.append(col_params)
    if type_id == u'1':
        colors = CarTypeColor.objects.filter(side=0)
    elif type_id == u'2':
        colors = CarTypeColor.objects.filter(side=1)
    else:
        colors = ''
    return render(request, 'background/car/car-pic-new.html', locals())


def cars_by_brand(request):
    years_list = []
    car_data = []
    try:
        brand_id = request.GET['brand_id']
        brand_obj = Brand.objects.get(pk=brand_id)
        cars = brand_car(brand_obj)
        if cars:
            car_obj = cars[0]
            years = Car.objects.filter(name=car_obj['car_name'])
            for y in years:
                years_list.append(y.produce_year)
        car_data.append(cars)
        car_data.append(years_list)
    except Exception as e:
        LOG.error('Can not get cars by brand. %s' % e)
    return HttpResponse(simplejson.dumps(car_data))


def car_pic_new_action(request):
    res = "success"
    try:
        carId = request.POST['carId']
        typeId = request.POST['typeId']
        # car_name = request.POST['carPicCar']
        # producer_year = request.POST['carPicYear']
        # pic_type = request.POST['car_type']
        if typeId == u'1' or typeId == u'2':
            pic_color = request.POST['carPicColor']
        else:
            pic_color = ''
        t_obj = CarTypePicture.objects.get(pk=typeId)
        if pic_color:
            c_obj = CarTypeColor.objects.get(pk=pic_color)
        else:
            c_obj = None
        car_obj = Car.objects.get(pk=carId)
        img = request.FILES['carPic']
        img_path = deal_with_upload_file(img)
        pic_name = request.POST['carPicName']

        p_obj = Picture.objects.create(name=pic_name, path=img_path)
        car_list = request.POST.getlist('carList')
        CarPicture.objects.create(car=car_obj, picture=p_obj, type=t_obj, color=c_obj)
        if car_list:
            for ca in car_list:
                CarPicture.objects.create(car_id=ca, picture=p_obj, type=t_obj, color=c_obj)
    except Exception as e:
        LOG.error('Error happened when create car pic. %s' % e)
        res = "fail"
    return HttpResponse(res)


def edit(request):
    if 'source' in request.GET:
        is_car = True
    else:
        is_car = False
    carId = request.GET.get("carId", "")
    carobj = Car.objects.get(pk=carId)
    colors_outside = [{"id": item.id, "name": item.name, "rgb": item.rgb, 'is_checked': True if item in carobj.colors.all() else False}
                      for item in CarTypeColor.objects.filter(side=0)]

    colors_inside = [{"id": item.id, "name": item.name, "rgb": item.rgb, "is_checked": True if item in carobj.colors.all() else False}
                 for item in CarTypeColor.objects.filter(side=1)]

    series = [{"id": item.id, "name": item.name, "is_checked": True if item == carobj.series else False}
              for item in Series.objects.all()]
    grades = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.grade else False} for item in CarTypeGrade.objects.all()]
    bodies = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.body else False} for item in CarTypeBody.objects.all()]
    energies = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.energy else False} for item in CarTypeEnergy.objects.all()]
    countries = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.country else False} for item in CarTypeCountry.objects.all()]
    gearboxes = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.gearbox else False} for item in CarTypeGearbox.objects.all()]
    cc = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.cc else False} for item in CarTypeCC.objects.all()]
    drives = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.drive else False} for item in CarTypeDrive.objects.all()]
    discharges = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.discharge else False} for item in CarTypeDischarge.objects.all()]
    doors = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.door else False} for item in CarTypeDoor.objects.all()]
    seats = [{"id": item.id, "name": item.name, 'is_checked': True if item == carobj.seat else False} for item in CarTypeSeat.objects.all()]

    extensions = [{"id": item.id, "name": item.name, 'is_checked': True if item in carobj.extensions.all() else False} for item in CarTypeExtension.objects.all()]
    stalls = [{"id": item.id, "name": item.name, "is_checked": True if item == carobj.stall else False} for item in CarStall.objects.all()]
    eng_id = carobj.energy.id if carobj.energy else -1
    parent_eng_id = carobj.energy.parent_id if carobj.energy else -1

    energies = list()
    engine_list = CarTypeEnergy.objects.filter(parent_id__isnull=True)
    for engine in engine_list:
        eng_param = dict()
        eng_param['id'] = engine.id
        eng_param['name'] = engine.name
        eng_param['is_checked'] = True if parent_eng_id == engine.id else False
        child_list = CarTypeEnergy.objects.filter(parent_id=engine.id)
        child_arr = list()
        for child in child_list:
            ch_param = dict()
            ch_param['id'] = child.id
            ch_param['name'] = child.name
            ch_param['is_checked'] = True if eng_id == child.id else False
            child_arr.append(ch_param)
        eng_param['energy'] = child_arr
        energies.append(eng_param)

    return render(request, 'background/car/edit.html', {"carId": carId, "carobj": carobj,
                                                       "colors_outside": colors_outside,
                                                       "colors_inside": colors_inside,
                                                       "series": series,
                                                       "grades": grades,
                                                       "bodies": bodies,
                                                       "energies": energies,
                                                       "energy_list" : simplejson.dumps(energies),
                                                       "countries": countries,
                                                       "gearboxes": gearboxes,
                                                       "cc": cc,
                                                       "drives": drives,
                                                       "discharges": discharges,
                                                       "doors": doors,
                                                       "seats": seats,
                                                       "extensions": extensions,
                                                        "stalls": stalls,
                                                        "is_car":is_car
                                                       })


def edit_action(request):
    res = "success"
    try:
        carId = request.POST.get("carId", "")
        name = request.POST["CarName"]
        produce_year = request.POST["CarProducerYear"]
        price = request.POST["CarPrice"]
        price_min = request.POST["CarPriceMin"]
        price_max = request.POST["CarPriceMax"]
        power = request.POST["CarPower"]
        real_cc = request.POST['realCC']
        stall_id = request.POST['stall']
        stall_obj = CarStall.objects.get(pk=stall_id)
        is_sell = True if request.POST.get("IsSell", None) == "on" else False
        is_hot = True if request.POST.get("IsHot", None) == "on" else False
        is_new = True if request.POST.get("IsNew", None) == "on" else False
        series = Series.objects.get(id=request.POST["series"])
        body = CarTypeBody.objects.get(id=request.POST["body"])
        cc_p = request.POST['cc']
        if cc_p == "-1":
            cc_manual = request.POST['carCC']
            cc_obj = CarTypeCC.objects.filter(name=cc_manual)
            if cc_obj:
                cc = cc_obj[0]
            else:
                cc = CarTypeCC.objects.create(name=cc_manual)
        else:
            cc = CarTypeCC.objects.get(id=request.POST["cc"])
        country = CarTypeCountry.objects.get(id=request.POST["country"])
        energy = CarTypeEnergy.objects.get(id=request.POST["energy"])
        grade = CarTypeGrade.objects.get(id=request.POST["grade"])
        gearbox = CarTypeGearbox.objects.get(id=request.POST["gearbox"])
        drive = CarTypeDrive.objects.get(id=request.POST["drive"])
        discharge = CarTypeDischarge.objects.get(id=request.POST["discharge"])
        door = CarTypeDoor.objects.get(id=request.POST["door"])
        seat = CarTypeSeat.objects.get(id=request.POST["seat"])
        carobj = Car.objects.filter(pk=carId)

        carobj.update(series=series, body=body, cc=cc, country=country, name=name,
                  energy=energy, grade=grade, gearbox=gearbox, drive=drive,
                  discharge=discharge, door=door, seat=seat, produce_year=produce_year,
                  price=price, price_max=price_max, price_min=price_min, is_sell=is_sell,
                  is_hot=is_hot, is_new=is_new, power=power, real_cc=real_cc, stall=stall_obj)
        # car.save()
        carobj[0].extensions.clear()
        carobj[0].colors.clear()
        for key, value in request.POST.items():
            if value != "":
                if key.startswith("extensions_"):
                    carobj[0].extensions.add(CarTypeExtension.objects.get(id=key.split('_')[1]))
                if key.startswith("colors_"):
                    carobj[0].colors.add(CarTypeColor.objects.get(id=key.split('_')[1]))
    except Exception as e:
        res = "fail"
        LOG.error('Edit car primary info error. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new_pic_index(request):
    car_id = request.GET['carId']
    car_obj = Car.objects.get(pk=car_id)
    name = car_obj.series.brand.name +" "+ car_obj.series.name +" "+ car_obj.name
    return render(request, 'background/car/new_pic_index.html', {'name': name, 'car_id': car_id})


def new_pic_index_data(request):
    car_id = request.POST['car_id']
    type_id = request.POST['type_id']
    car_obj = Car.objects.get(pk=car_id)
    carpics = CarPicture.objects.filter(car=car_obj).filter(type_id=type_id)
    all_data = {}
    all_data['car_pics_list'] = []
    all_data['car_color_list'] = []
    if carpics:
        if carpics[0].type.id == 1 or carpics[0].type.id == 2:
            for pics in carpics:
                pics_params = {}
                pics_params['id'] = pics.id
                pics_params['pic'] = pics.picture.path
                pics_params['name'] = pics.picture.name
                if pics.color:
                    pics_params['color'] = pics.color.name
                    pics_params['color_id'] = pics.color.id
                else:
                    pics_params['color'] = ''
                    pics_params['color_id'] = ''
                all_data['car_pics_list'].append(pics_params)
            for pics in carpics:
                if pics.color:
                    pics_params = {}
                    pics_params['color_id'] = pics.color.id
                    pics_params['color'] = pics.color.name
                if pics_params in all_data['car_color_list']:
                    continue
                all_data['car_color_list'].append(pics_params)
        else:
            for pics in carpics:
                pics_params = {}
                pics_params['id'] = pics.id
                pics_params['pic'] = pics.picture.path
                pics_params['name'] = pics.picture.name
                all_data['car_pics_list'].append(pics_params)
    return HttpResponse(simplejson.dumps(all_data), content_type="application/json")


def batch_car_pic_new(request):
    real_cc_list = []
    car_list = []
    type_id = request.GET['typeId']
    car_id = request.GET['carId']
    car_obj = Car.objects.get(pk=car_id)
    the_car_name = car_obj.series.name + " " + car_obj.produce_year + u"款"
    car_obj_list = Car.objects.filter(Q(series_id=car_obj.series.id), ~Q(pk=car_id), Q(produce_year=car_obj.produce_year))
    for col in car_obj_list:
        if col.real_cc in real_cc_list:
            continue
        else:
            real_cc_list.append(col.real_cc)
    for col in car_obj_list:
        col_params = {}
        col_params['id'] = col.id
        col_params['name'] = col.series.name + " " + col.name
        col_params['real_cc'] = col.real_cc
        car_list.append(col_params)
    if type_id == u'1':
        colors = CarTypeColor.objects.filter(side=0)
    elif type_id == u'2':
        colors = CarTypeColor.objects.filter(side=1)
    else:
        colors = ''
    return render(request, 'background/car/batch-car-pic-new.html', locals())


def batch_car_pic_new_action(request):
    res = "success"
    try:
        carId = request.POST['carId']
        typeId = request.POST['typeId']
        if typeId == u'1' or typeId == u'2' :
            pic_color = request.POST['carPicColor']
        else:
            pic_color = ''
        t_obj = CarTypePicture.objects.get(pk=typeId)
        if pic_color:
            c_obj = CarTypeColor.objects.get(pk=pic_color)
        else:
            c_obj = None
        car_obj = Car.objects.get(pk=carId)
        images = request.FILES.getlist('carPic')
        car_list = request.POST.getlist('carList')
        # pic_name = request.POST['carPicName']
        # pic_name = 'haha'
        if images:
            for img in images:
                img_path = deal_with_upload_file(img)
                pic_name = str(img).split('.')[0]
                p_obj = Picture.objects.create(name=pic_name, path=img_path)
                CarPicture.objects.create(car=car_obj, picture=p_obj, type=t_obj, color=c_obj)
                if car_list:
                    for ca in car_list:
                        CarPicture.objects.create(car_id=ca, picture=p_obj, type=t_obj, color=c_obj)
    except Exception as e:
        LOG.error('Error happened when create car pic. %s' % e)
        res = "fail"
    return HttpResponse(res)


def car_pic_action_delete(request):
    res = "success"
    try:
        car_pic_id_list = request.POST['car_pic_id_list']
        pic_list = car_pic_id_list.split(",")[:-1]
        for ids in pic_list:
            CarPicture.objects.filter(pk=ids).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete car picture. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def color_index(request):
    car_id = request.GET.get("car_id", "")
    return render(request, 'background/car/color.html', {'car_id': car_id})


@data_table(columns=CAR_COLOR_COLUMNS, model=CarTypeColor)
def color_data(request):
    st_list = []
    try:
        car_id = request.GET.get("car_id", "")
        if car_id:
            car_obj = Car.objects.filter(pk=car_id)
            car_forbid_color_list = car_obj[0].car_color.all()
            series_id = car_obj[0].series_id
            year = car_obj[0].produce_year if car_obj[0].produce_year else ""
            series_color_obj = SeriesYearColor.objects.filter(Q(series_id=series_id), Q(year=year))

            if series_color_obj:
                sts = series_color_obj[0].color.all().filter(Q(name__contains=request.table_data["key_word"]) |
                                                  Q(rgb__contains=request.table_data["key_word"])).\
                                        order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
                for st in sts:
                    st_params = {}
                    if st in car_forbid_color_list:
                        st_params['forbid'] = 1
                    else:
                        st_params['forbid'] = 0
                    st_params["DT_RowId"] = st.id
                    st_params["name"] = st.name
                    st_params["rgb"] = st.rgb
                    #0, 'outside',1, 'inside'
                    st_params['side'] = st.side

                    st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
                    st_list.append(st_params)
    except Exception as e:
        LOG.error("Error happened when get car color. %s" % e)
    return st_list, len(st_list)


def car_color_new(request):
    try:
        car_id = request.GET.get("carId", "")
    except Exception as e:
        LOG.error('Error happened when get color new html. %s' % e)
    return render(request, "background/car/color_new.html", {"car_id": car_id})


def car_color_forbid(request):
    res = "success"
    try:
        car_id = request.GET.get("car_id")
        color_id = request.GET.get("color_id")
        car_obj = Car.objects.get(pk=car_id)
        color_obj = CarTypeColor.objects.get(pk=color_id)
        # color_obj = color_obj_temp[0] if color_obj_temp else ""
        # series_id = car_obj[0].series_id
        # year = car_obj[0].produce_year if car_obj[0].produce_year else ""
        # series_color_obj_temp = SeriesYearColor.objects.filter(Q(series_id=series_id), Q(year=year))
        # series_color_obj = series_color_obj_temp[0]
        color_list = car_obj.car_color.all()
        # color_id_list = list(color.id for color in color_list)
        # color_list_temp = color_list.filter(id=color_id)
        if color_obj not in color_list:
            car_obj.car_color.add(color_obj)
        else:
            # color_list.remove(color_obj)
            car_obj.car_color.clear()
            for color in color_list:
                if color != color_obj:
                    car_obj.car_color.add(color)
    except Exception as e:
        LOG.error('Error happened when do color forbidden. %s' % e)
        res = "fail"
    return HttpResponse(simplejson.dumps({"res":res}))
