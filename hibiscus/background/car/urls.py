from django.conf.urls import url

from hibiscus.background.car import views

urlpatterns = [
    url(r'^$', views.index, name="background-car-index"),
    url(r'^data/$', views.data, name="background-car-data"),
    url(r'^delete/action/$', views.action_delete, name="background-car-action-delete"),
    url(r'^new/$', views.new, name="background-car-new"),
    url(r'^new/action/$', views.action_new, name="background-car-action-new"),
    url(r'^pic/index/$', views.car_pic_index, name="background-car-pic-index"),
    url(r'^pic/new/$', views.car_pic_new, name="background-car-pic-new"),
    url(r'^pic/brand/cars/$', views.cars_by_brand, name="background-car-by-brand"),
    url(r'^pic/new/action/$', views.car_pic_new_action, name="background-car-pic-new-action"),
    url(r'^pic/data/$', views.car_pic_data, name="background-car-pic-data"),
    url(r'^pic/action/delete/$', views.car_pic_action_delete, name="background-car-pic-action-delete"),
    url(r'^params/$', views.car_params, name="background-car-params"),
    url(r'^detail/action/$', views.car_detail_action, name="background-car-detail-action"),
    url(r'^pic/detail/$', views.car_pic_detail, name="background-car-pic-detail"),
    url(r'^edit/$', views.edit, name='background-car-edit'),
    url(r'^edit/action/$', views.edit_action, name='background-car-edit-action'),
    url(r'^new/pic/index/$', views.new_pic_index, name='background-car-new-pic-index'),
    url(r'^new/pic/index/data/$', views.new_pic_index_data, name='background-car-new-pic-index-data'),
    url(r'^pic/new/batch/$', views.batch_car_pic_new, name="background-batch-car-pic-new"),
    url(r'^pic/new/batch/action/$', views.batch_car_pic_new_action, name="background-batch-car-pic-new-action"),

    url(r'^color/$', views.color_index, name="background-car-color-index"),
    url(r'^color/data/$', views.color_data, name="background-car-color-data"),
    url(r'color/new/$', views.car_color_new, name="background-car-color-new"),
    url(r'color/forbid/$', views.car_color_forbid, name="background-car-color-forbid"),

]