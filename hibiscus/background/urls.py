# -*- coding:utf-8 -*-
from django.conf.urls import url, include
from hibiscus.background import views

urlpatterns = [
    url(r'^$', views.index, name="background-index"),
    url(r'^auth/', include("hibiscus.background.auth.urls")),
    url(r'^area/', include("hibiscus.background.area.urls")),
    url(r'^brand/', include("hibiscus.background.brand.urls")),
    url(r'^series/', include("hibiscus.background.series.urls")),
    url(r'^cart/', include("hibiscus.background.cart.urls")),
    url(r'^cari/', include("hibiscus.background.cari.urls")),
    url(r'^car/', include("hibiscus.background.car.urls")),
    url(r'^user/', include("hibiscus.background.user.urls")),
    url(r'^picture/', include("hibiscus.background.picture.urls")),
    url(r'^ad/', include("hibiscus.background.ad.urls")),
    url(r'^article/', include("hibiscus.background.article.urls")),
    url(r'^upkeep/', include("hibiscus.background.upkeep.urls")),
    url(r'^group/', include("hibiscus.background.group.urls")),
    url(r'^clue/', include("hibiscus.background.clue.urls")),
    url(r'^sale/', include("hibiscus.background.sale.urls")),
]
