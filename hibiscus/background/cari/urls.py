from django.conf.urls import url, include

urlpatterns = [
    url(r'^types/', include("hibiscus.background.cari.types.urls")),
    url(r'^detail/', include("hibiscus.background.cari.detail.urls")),

]

