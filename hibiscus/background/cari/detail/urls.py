from django.conf.urls import url

from hibiscus.background.cari.detail import views

urlpatterns = [
    url(r'^$', views.index, name="background-cari-detail-index"),
    url(r'^data/$', views.data, name="background-cari-detail-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cari-detail-action-delete"),
    url(r'^new/$', views.new, name="background-cari-detail-new"),
    url(r'^new/action/$', views.action_new, name="background-cari-detail-action-new"),

]