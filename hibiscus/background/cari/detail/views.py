import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import CarItemDetail, CarItemTypeDetail
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
DETAIL_DETAIL_COLUMNS = ["name", "created_at"]


def index(request):
    type_id = request.GET["type_id"]
    return render(request, 'background/cari/detail/index.html', {"type_id": type_id})


@data_table(columns=DETAIL_DETAIL_COLUMNS, model=CarItemDetail)
def data(request):
    try:
        type_id = request.GET["type_id"]
        type_obj = CarItemTypeDetail.objects.get(pk=type_id)
        sts = CarItemDetail.objects.filter(type=type_obj).filter(Q(name__contains=request.table_data["key_word"])).\
            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list, CarItemDetail.objects.filter(type=type_obj).count()
    except Exception as e:
        LOG.error('Can not get cart type item detail data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                CarItemDetail.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete item detail. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    type_id = request.GET["type_id"]
    return render(request, 'background/cari/detail/new.html', {"type_id": type_id})


def action_new(request):
    res = "success"
    try:
        name = request.POST["caridetailName"]
        type_id = request.POST["type_id"]
        type_obj = CarItemTypeDetail.objects.get(pk=type_id)
        CarItemDetail(type=type_obj, name=name).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create item detail. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
