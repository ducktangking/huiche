from django.conf.urls import url

from hibiscus.background.cari.types import views

urlpatterns = [
    url(r'^$', views.index, name="background-cari-types-index"),
    url(r'^data/$', views.data, name="background-cari-types-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cari-types-action-delete"),
    url(r'^new/$', views.new, name="background-cari-types-new"),
    url(r'^new/action/$', views.action_new, name="background-cari-types-action-new"),

]