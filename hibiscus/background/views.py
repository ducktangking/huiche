# -*- coding:utf-8 -*-

from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views.decorators.http import require_GET


@require_GET
def index(request):
    if hasattr(request.user, "is_anonymous"):
        if request.user.is_anonymous():
            return HttpResponseRedirect(reverse("background-login"))
        else:
            return render(request, "background/index.html")
    else:
        return HttpResponseRedirect(reverse("background-login"))
