from hibiscus.models import Producer, HcAdmin, Adviser
from hibiscus.background.public import pwd_hash


class AdminBackend(object):
    def authenticate(self, username=None, password=None, **kwargs):
        try:
            user = HcAdmin.objects.get(username=username)
            if user.password == pwd_hash(password):
                return user
        except Exception as e:
            print "producer authenticate error. %s" % e
            return None

    def get_user(self, user_id):
        try:
            return HcAdmin.objects.get(pk=user_id)
        except Exception as e:
            print "producer backend get user error.%s" % e
            return None