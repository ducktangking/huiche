from django.conf.urls import url

from hibiscus.background.clue.carClue import views

urlpatterns = [
    url(r'^$', views.index, name="background-clue-car-index"),
    url(r'^data/$', views.data, name="background-clue-car-data"),
    url(r'^dis/$', views.distribute_clue, name="background-clue-distribute-adviser"),
]