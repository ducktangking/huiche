from django.conf.urls import url

from hibiscus.background.clue.upkeepClue import views

urlpatterns = [
    url(r'^$', views.index, name="background-clue-upkeep-index"),
    url(r'^data/$', views.data, name="background-clue-upkeep-data"),
    url(r'^dis/$', views.distribute_clue, name="background-clue-upkeep-distribute-adviser"),
]