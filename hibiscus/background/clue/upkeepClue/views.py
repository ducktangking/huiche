import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import CarUpkeepInquiry, Adviser
from hibiscus.decorators import data_table

CAR_CLUE_COLUMNS = ['car__name', 'name', 'phone', 'status']
LOG = logging.getLogger(__name__)


def index(request):
    return render(request, 'background/clue/carClue/index.html')


@data_table(columns=CAR_CLUE_COLUMNS, model=CarUpkeepInquiry)
def data(request):
    st_list = []
    try:
        sts = CarUpkeepInquiry.objects.filter(
            Q(car__name__contains=request.table_data["key_word"]) | Q(producer__username__contains=request.table_data['key_word'])
             | Q(phone__contains=request.table_data['key_word']) | Q(name__contains=request.table_data['key_word'])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["car_name"] = st.car.name
            st_params["username"] = st.name
            st_params["phone"] = st.phone
            st_params["status"] = st.status
            if st.adviser:
                st_params['adviser_name'] = st.adviser.username
            else:
                st_params['adviser_name'] = ""
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)
    return st_list


def distribute_clue(request):
    res = "fail"
    try:
        clue_id = request.POST.get("clueId", "")
        adviser_id = request.POST.get("producerAdviser", "")
        clue = CarUpkeepInquiry.objects.filter(pk=clue_id)
        adviser = Adviser.objects.filter(pk=adviser_id)
        if clue:
            if adviser_id:
                clue.update(adviser=adviser[0])
    except Exception as e:
        LOG.error('Error happend when distribute_clue. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
