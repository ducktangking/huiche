from django.conf.urls import url, include

urlpatterns = [
    url(r'^car/', include("hibiscus.background.clue.carClue.urls")),
    url(r'^upkeep/', include("hibiscus.background.clue.upkeepClue.urls")),
]