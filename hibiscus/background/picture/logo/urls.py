from django.conf.urls import url
from hibiscus.background.picture.logo import views

urlpatterns = [
    url(r'^$', views.index, name="background-pic-logo-index"),
    url(r'^data/$', views.data, name="background-pic-logo-data"),
    url(r'^delete/action/$', views.action_delete, name="background-pic-logo-action-delete"),
    url(r'^new/$', views.new, name="background-pic-logo-new"),
    url(r'^new/action/$', views.action_new, name="background-pic-logo-action-new"),

]