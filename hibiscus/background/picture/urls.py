from django.conf.urls import url, include

urlpatterns = [
    url(r'^logo/', include("hibiscus.background.picture.logo.urls")),
    url(r'^portrait/', include("hibiscus.background.picture.portrait.urls")),

]