from django.conf.urls import url
from hibiscus.background.picture.portrait import views

urlpatterns = [
    url(r'^$', views.index, name="background-pic-portrait-index"),
    url(r'^data/$', views.data, name="background-pic-portrait-data"),
    url(r'^delete/action/$', views.action_delete, name="background-pic-portrait-action-delete"),
    url(r'^new/$', views.new, name="background-pic-portrait-new"),
    url(r'^new/action/$', views.action_new, name="background-pic-portrait-action-new"),

]