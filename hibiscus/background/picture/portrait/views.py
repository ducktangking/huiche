import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Portrait, Picture
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file

LOG = logging.getLogger(__name__)
PORTRAIT_COLUMNS = ["picture"]


def index(request):
    return render(request, 'background/picture/portrait/index.html')


@data_table(columns=PORTRAIT_COLUMNS, model=Portrait)
def data(request):
    try:
        sts = Portrait.objects.filter(
            Q(picture__name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        # sts = Logo.objects.all()
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.picture.name
            st_params["path"] = st.picture.path
            st_params["created_at"] = timezone.localtime(st.picture.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                Portrait.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete portrait. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/picture/portrait/new.html')


def action_new(request):
    res = "success"
    try:
        f = request.FILES["logoFile"]
        path = deal_with_upload_file(f)
        p_obj = Picture.objects.create(name=f.name, path=path)
        Portrait(picture=p_obj).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create logo. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
