from django.conf.urls import url

from hibiscus.background.cart.country import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-country-index"),
    url(r'^data/$', views.data, name="background-cart-country-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-country-action-delete"),
    url(r'^new/$', views.new, name="background-cart-country-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-country-action-new"),

]