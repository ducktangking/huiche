from django.conf.urls import url

from hibiscus.background.cart.picture import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-picture-index"),
    url(r'^data/$', views.data, name="background-cart-picture-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-picture-action-delete"),
    url(r'^new/$', views.new, name="background-cart-picture-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-picture-action-new"),

]