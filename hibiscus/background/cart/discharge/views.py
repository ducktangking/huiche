import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import CarTypeDischarge
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
DISCHARGE_COLUMNS = ["name", "created_at"]


def index(request):
    return render(request, 'background/cart/discharge/index.html')


@data_table(columns=DISCHARGE_COLUMNS, model=CarTypeDischarge)
def data(request):
    try:
        sts = CarTypeDischarge.objects.filter(
            Q(name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type discharge data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                CarTypeDischarge.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete discharge. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/cart/discharge/new.html')


def action_new(request):
    res = "success"
    try:
        name = request.POST["cartdischargeName"]
        CarTypeDischarge(name=name).save()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create discharge. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
