from django.conf.urls import url

from hibiscus.background.cart.discharge import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-discharge-index"),
    url(r'^data/$', views.data, name="background-cart-discharge-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-discharge-action-delete"),
    url(r'^new/$', views.new, name="background-cart-discharge-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-discharge-action-new"),

]