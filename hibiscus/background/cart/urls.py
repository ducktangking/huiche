__author__ = 'server'

from django.conf.urls import url, include

urlpatterns = [
    url(r'^grade/', include("hibiscus.background.cart.grade.urls")),
    url(r'^body/', include("hibiscus.background.cart.body.urls")),
    url(r'^energy/', include("hibiscus.background.cart.energy.urls")),
    url(r'^color/', include("hibiscus.background.cart.color.urls")),
    url(r'^country/', include("hibiscus.background.cart.country.urls")),
    url(r'^gearbox/', include("hibiscus.background.cart.gearbox.urls")),
    url(r'^cc/', include("hibiscus.background.cart.cc.urls")),
    url(r'^drive/', include("hibiscus.background.cart.drive.urls")),
    url(r'^discharge/', include("hibiscus.background.cart.discharge.urls")),
    url(r'^door/', include("hibiscus.background.cart.door.urls")),
    url(r'^seat/', include("hibiscus.background.cart.seat.urls")),
    url(r'^extension/', include("hibiscus.background.cart.extension.urls")),
    url(r'^picture/', include("hibiscus.background.cart.picture.urls")),

]

