from django.conf.urls import url

from hibiscus.background.cart.body import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-body-index"),
    url(r'^data/$', views.data, name="background-cart-body-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-body-action-delete"),
    url(r'^new/$', views.new, name="background-cart-body-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-body-action-new"),

]