from django.conf.urls import url

from hibiscus.background.cart.cc import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-cc-index"),
    url(r'^data/$', views.data, name="background-cart-cc-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-cc-action-delete"),
    url(r'^new/$', views.new, name="background-cart-cc-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-cc-action-new"),

]