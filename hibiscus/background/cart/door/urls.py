from django.conf.urls import url

from hibiscus.background.cart.door import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-door-index"),
    url(r'^data/$', views.data, name="background-cart-door-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-door-action-delete"),
    url(r'^new/$', views.new, name="background-cart-door-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-door-action-new"),

]