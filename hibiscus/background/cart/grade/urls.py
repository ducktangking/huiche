from django.conf.urls import url

from hibiscus.background.cart.grade import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-grade-index"),
    url(r'^data/$', views.data, name="background-cart-grade-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-grade-action-delete"),
    url(r'^new/$', views.new, name="background-cart-grade-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-grade-action-new")
]
