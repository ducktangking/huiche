from django.conf.urls import url

from hibiscus.background.cart.extension import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-extension-index"),
    url(r'^data/$', views.data, name="background-cart-extension-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-extension-action-delete"),
    url(r'^new/$', views.new, name="background-cart-extension-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-extension-action-new"),

]