import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import CarTypeColor, Car
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)
CART_COLOR_COUMNS = ["name", "created_at"]


def index(request):
    return render(request, 'background/cart/color/index.html')


@data_table(columns=CART_COLOR_COUMNS, model=CarTypeColor)
def data(request):
    try:
        sts = CarTypeColor.objects.filter(
            Q(name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["name"] = st.name
            st_params["rgb"] = st.rgb
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                CarTypeColor.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    return render(request, 'background/cart/color/new.html')


def action_new(request):
    res = "success"
    try:
        name = request.POST["cartcoName"]
        car_id = request.POST.get("car_id", "")
        col_rgb = "#" + request.POST["carTyCol"]
        ac = CarTypeColor.objects.create(name=name, rgb=col_rgb)
        if car_id:
            car = Car.objects.filter(pk=car_id)
            if car:
                car[0].car_color.add(ac)
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when create cartco. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
