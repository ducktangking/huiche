from django.conf.urls import url

from hibiscus.background.cart.color import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-color-index"),
    url(r'^data/$', views.data, name="background-cart-color-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-color-action-delete"),
    url(r'^new/$', views.new, name="background-cart-color-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-color-action-new"),

]