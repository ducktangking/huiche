from django.conf.urls import url

from hibiscus.background.cart.drive import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-drive-index"),
    url(r'^data/$', views.data, name="background-cart-drive-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-drive-action-delete"),
    url(r'^new/$', views.new, name="background-cart-drive-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-drive-action-new"),

]