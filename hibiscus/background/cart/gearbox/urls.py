from django.conf.urls import url

from hibiscus.background.cart.gearbox import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-gearbox-index"),
    url(r'^data/$', views.data, name="background-cart-gearbox-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-gearbox-action-delete"),
    url(r'^new/$', views.new, name="background-cart-gearbox-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-gearbox-action-new"),

]