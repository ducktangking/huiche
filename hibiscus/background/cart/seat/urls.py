from django.conf.urls import url

from hibiscus.background.cart.seat import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-seat-index"),
    url(r'^data/$', views.data, name="background-cart-seat-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-seat-action-delete"),
    url(r'^new/$', views.new, name="background-cart-seat-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-seat-action-new"),

]