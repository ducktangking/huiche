from django.conf.urls import url

from hibiscus.background.cart.energy import views

urlpatterns = [
    url(r'^$', views.index, name="background-cart-energy-index"),
    url(r'^data/$', views.data, name="background-cart-energy-data"),
    url(r'^delete/action/$', views.action_delete, name="background-cart-energy-action-delete"),
    url(r'^new/$', views.new, name="background-cart-energy-new"),
    url(r'^new/action/$', views.action_new, name="background-cart-energy-action-new"),

]