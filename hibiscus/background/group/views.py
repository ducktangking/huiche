import simplejson
import logging
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.core.urlresolvers import reverse

from hibiscus.models import Logo, Picture, Article, Brand, SupportedArea, Car, Series, ArticleType, GroupPurchase, GroupPurchaseUser
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file
from hibiscus.background.brand.views import brand_car

LOG = logging.getLogger(__name__)
GROUP_COLUMNS = ["series__name", "name", "brief", "area", "time_begin", "time_end", "time_end", "is_timeout", "status", "created_at"]
GROUP_USERS_COLUMNS = ["series", "name", "phone", "city", "buy_type", "created_at"]


def index(request):
    return render(request, 'background/group/index.html')


@data_table(columns=GROUP_COLUMNS, model=GroupPurchase)
def data(request):
    try:
        sts = GroupPurchase.objects.filter(
            Q(name__contains=request.table_data["key_word"])). \
                  order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {"DT_RowId": st.id, "name": st.name, "brief": st.brief,
                         "area": "%s/%s" % (st.area.province, st.area.city), "series": st.series.name}
            days = (timezone.localtime(st.time_end) - timezone.now()).days
            st_params["time_begin"] = timezone.localtime(st.time_begin).strftime('%Y-%m-%d %H:%M:%S')
            st_params["time_end"] = timezone.localtime(st.time_end).strftime('%Y-%m-%d %H:%M:%S')
            st_params["days_el"] = days if days > 0 else 0
            st_params["is_timeout"] = st.is_timeout
            st_params["status"] = st.status
            st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
            st_params["article"] = st.article.id if st.article else -1
            st_params['is_left'] = st.is_left
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception as e:
        LOG.error('Can not get cart type grade data.%s' % e)


def action_delete(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                GroupPurchase.objects.filter(pk=id_v).delete()
    except Exception as e:
        res = "fail"
        LOG.error('Error happened when delete GroupPurchase. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))


def new(request):
    brands = Brand.objects.filter(parent__isnull=False)
    return render(request, 'background/group/new.html', {"brands": brands})


def action_new(request):
    res = {"res": "fail"}
    area_id = request.POST.get("area", None)
    name = request.POST.get("name", None)
    brief = request.POST.get("brief", None)
    time_begin = request.POST.get("time_begin", None)
    time_end = request.POST.get("time_end", None)
    series_id = request.POST['upSeries']
    if area_id and name and brief and time_begin and time_end and series_id:
        try:
            area = SupportedArea.objects.get(id=area_id)
            series = Series.objects.get(pk=series_id)
            if area and series:
                group = GroupPurchase.objects.create(name=name, brief=brief, time_begin=time_begin, time_end=time_end, area=area, series=series)
                group.save()
            res["res"] = "success"
        except Exception as e:
            LOG.error('Error happened when create group. %s' % e)
    return HttpResponse(simplejson.dumps(res))


def action(request):
    action_operator = request.POST.get("action", None)
    group_id = request.GET.get("gId", None)

    res = {"res": "fail"}
    if action_operator and action_operator in ["start", "stop"] and group_id:
        group = GroupPurchase.objects.get(id=group_id)
        if group:
            if action_operator == "start":
                group.status = True
            else:
                group.status = False
            group.save()
            res["res"] = "success"
    return HttpResponse(simplejson.dumps(res))


def users(request):
    return render(request, 'background/group/users.html', {"gId": request.GET.get("gId")})


@data_table(columns=GROUP_USERS_COLUMNS, model=GroupPurchaseUser)
def users_data(request):
    group_id = request.GET.get("gId", None)
    st_list = []
    if group_id:
        try:
            sts = GroupPurchaseUser.objects.filter(
                Q(name__contains=request.table_data["key_word"]), Q(group_purchase=group_id)). \
                      order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]

            for st in sts:
                st_params = {"DT_RowId": st.id, "name": st.name, "phone": st.phone,
                             "city": st.city, "buy_type": st.buy_type, "created_at": timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')}
                # st_params["score"] = st.score
                st_list.append(st_params)

        except Exception as e:
            LOG.error('Can not get group users data.%s' % e)
    return st_list


def pics(request):
    group_id = request.GET.get("gId", None)
    pic_list = []
    if group_id:
        try:
            group = GroupPurchase.objects.get(id=group_id)
            if group:
                for p in group.picture.all():
                    cpp = {'id': p.id, 'pic': p.path, 'pic_name': p.name}
                    pic_list.append(cpp)
        except Exception as e:
            LOG.error('Can not get group pic detail')
    return render(request, 'background/group/group_pics.html', {"gId": group_id, "pic_list": pic_list})


def pics_new(request):
    return render(request, 'background/group/group-pic-new.html', {'gId': request.GET.get("gId", None)})


def pics_new_action(request):
    res = "fail"
    group_id = request.POST.get("gId", None)
    if group_id:
        try:
            group = GroupPurchase.objects.get(id=group_id)
            if group:

                img = request.FILES['groupPic']
                img_path = deal_with_upload_file(img)
                pic_name = request.POST['groupPicName']

                p_obj = Picture.objects.create(name=pic_name, path=img_path)
                group.picture.add(p_obj)
                res = "success"
        except Exception as e:
            LOG.error('Error happened when create group pic. %s' % e)
            res = "fail"
    return HttpResponse(res)


def pics_delete_action(request):
    res = "fail"
    group_id = request.POST.get("gId", None)
    group_pic_id = request.POST.get("groupPicId", None)
    if group_id and group_pic_id:
        try:
            group = GroupPurchase.objects.get(id=group_id)
            picture = Picture.objects.get(id=group_pic_id)
            if group:
                group.picture.remove(picture)
            res = "success"
        except Exception as e:
            res = "fail"
            LOG.error('Error happened when delete group picture. %s' % e)
    return HttpResponse(res)


def articles(request):
    group_id = request.GET.get("gId", None)
    return render(request, 'background/group/articles.html', {"gId": group_id})


def article_bind(request):
    res = "fail"
    group_id = request.POST.get("gId", None)
    article_id = request.POST.get("arId", None)
    if group_id and article_id:
        try:
            group = GroupPurchase.objects.get(id=group_id)
            article = Article.objects.get(id=article_id)
            if group and article:
                group.article = article
                group.save()
                res = "success"
        except Exception as e:
            res = "fail"
            LOG.error('Error happened when delete group article_bind. %s' % e)
    return HttpResponse(res)


def article_unbind(request):
    res = "fail"
    group_id = request.POST.get("gId", None)
    if group_id :
        try:
            group = GroupPurchase.objects.get(id=group_id)
            if group:
                group.article = None
                group.save()
                res = "success"
        except Exception as e:
            res = "fail"
            LOG.error('Error happened when delete group article_unbind. %s' % e)
    return HttpResponse(res)


def article(request):
    group_id = request.GET.get("gId", None)
    article_id = request.GET.get("aId", None)
    if group_id and article_id:
        try:
            group = GroupPurchase.objects.get(id=group_id)
        except Exception as e:
            LOG.error('Error happened when delete group article_bind. %s' % e)

    return render(request, 'background/group/article.html', {'aId': article_id, "gId": group_id})


def left_show(request):
    try:
        group_id = request.GET['gId']
        is_left = request.GET['is_left']
        if is_left == "true":
            st = False
        else:
            st = True
        if st == True:
            area = GroupPurchase.objects.filter(pk=group_id)[0].area
            left_group = GroupPurchase.objects.filter(Q(is_left=True),Q(area=area))
            if left_group:
                GroupPurchase.objects.filter(pk=left_group[0].id).update(is_left=False)
        GroupPurchase.objects.filter(pk=group_id).update(is_left=st)
    except Exception as e:
        LOG.error('Switch is_left error. %s' % e)
    return redirect('background-group-index')


# def preview_data(request):
#     pre_data = {"res": "success", "reason": "", "content": ""}
#     try:
#         ar_id = request.GET["ar_id"]
#         ar_obj = Article.objects.get(pk=ar_id)
#         pre_data["content"] = ar_obj.content
#     except Exception, e:
#         pre_data["res"] = "fail"
#         pre_data["reason"] = "Get preview data failed."
#         LOG.error('Can not get preview data. %s' % e)
#     return HttpResponse(simplejson.dumps(pre_data))
#
#
# def detail(request):
#     try:
#         arId = request.GET['arId']
#     except Exception as e:
#         LOG.error('Can not get article detail. %s' % e)
#     return render(request, 'background/article/detail.html', {'arId': arId})
#
#
# def detail_data(request):
#     ar_detail = {}
#     try:
#         arId = request.GET['arId']
#         ar_obj = Article.objects.get(pk=arId)
#         ar_detail['title'] = ar_obj.title
#         ar_detail['producer'] = ""
#         ar_detail['admin'] = ""
#         if ar_obj.producer:
#             ar_detail['producer'] = ar_obj.producer.username
#         if ar_obj.admin:
#             ar_detail['admin'] = ar_obj.admin.username
#         ar_detail['created_at'] = timezone.localtime(ar_obj.created_at).strftime('%Y-%m-%d %H:%M:%S')
#         ar_detail['status'] = ar_obj.status
#         ar_detail['content'] = ar_obj.content
#         ar_detail['type'] = ar_obj.type.name
#     except Exception as e:
#         LOG.error('Can not get article detail data. %s' % e)
#     return HttpResponse(simplejson.dumps(ar_detail))
#
#
# def audit_index(request):
#     return render(request, 'background/article/audit-index.html')
#
#
# @data_table(columns=ARTICLE_COLUMNS, model=Article)
# def audit_data(request):
#     try:
#         sts = Article.objects.filter(
#             Q(title__contains=request.table_data["key_word"])). \
#                   order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
#         st_list = []
#         for st in sts:
#             st_params = {}
#             st_params["DT_RowId"] = st.id
#             st_params["title"] = st.title
#             st_params["created_type"] = st.created_type
#             st_params["created_at"] = timezone.localtime(st.created_at).strftime('%Y-%m-%d %H:%M:%S')
#             st_params['status'] = st.status
#             st_list.append(st_params)
#         return st_list
#     except Exception as e:
#         LOG.error('Can not get cart type grade data.%s' % e)
#
#
# def switch_status(request):
#     try:
#         arId = request.GET["arId"]
#         status = request.GET['status']
#         if status == "true":
#             st = False
#         else:
#             st = True
#         Article.objects.filter(pk=arId).update(status=st)
#     except Exception as e:
#         LOG.error('Switch status error. %s' % e)
#     return redirect('background-article-audit-index')
#
#
# def article_type_two(request):
#     two_level_data = []
#     try:
#         type_id = request.GET['type_id']
#         parent_obj = ArticleType.objects.get(pk=type_id)
#         two_levels = ArticleType.objects.filter(parent=parent_obj)
#         for t in two_levels:
#             t_p = {}
#             t_p['id'] = t.id
#             t_p['name'] = t.name
#             two_level_data.append(t_p)
#     except Exception as e:
#         LOG.error('Can not get article level two type. %s' % e)
#     return HttpResponse(simplejson.dumps(two_level_data))
