from django.conf.urls import url
from hibiscus.background.group import views

urlpatterns = [
    url(r'^$', views.index, name="background-group-index"),
    url(r'^data/$', views.data, name="background-group-data"),
    url(r'^delete/action/$', views.action_delete, name="background-group-action-delete"),
    url(r'^new/$', views.new, name="background-group-new"),
    url(r'^new/action/$', views.action_new, name="background-group-action-new"),
    url(r'^action/$', views.action, name="background-group-action"),
    url(r'^users/$', views.users, name="background-group-users"),
    url(r'^users/data/$', views.users_data, name="background-group-users-data"),
    url(r'^pics/$', views.pics, name="background-group-pictures"),
    url(r'^pics/new/$', views.pics_new, name="background-group-pictures-new"),
    url(r'^pics/new/action/$', views.pics_new_action, name="background-group-pic-new-action"),
    url(r'^pic/delete/$', views.pics_delete_action, name="background-group-pic-action-delete"),
    url(r'^articles/$', views.articles, name="background-group-articles"),
    url(r'^article/bind/$', views.article_bind, name="background-group-article-bind"),
    url(r'^article/unbind/$', views.article_unbind, name="background-group-article-unbind"),
    url(r'^article/$', views.article, name="background-group-article"),
    url(r'^left/show/$', views.left_show, name="background-group-left-show")
    # url(r'^preview/data/$', views.preview_data, name="background-article-preview-data"),
    # url(r'^detail/$', views.detail, name="background-article-detail"),
    # url(r'^audit/$', views.audit_index, name="background-article-audit-index"),
    # url(r'^audit/data/$', views.audit_data, name="background-article-audit-data"),
    # url(r'^detail/data/$', views.detail_data, name="background-article-detail-data"),
    # url(r'^switch/status/$', views.switch_status, name="background-article-switch_status"),
    # url(r'^type/level/two/data/$', views.article_type_two, name="background-article-level-two-data"),
]