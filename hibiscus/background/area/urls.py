__author__ = 'gmj'
from django.conf.urls import url, include
from hibiscus.background.area import views

urlpatterns = [
    url(r'^$', views.index, name="background-area-index"),
    url(r'^new/$', views.new_area_info, name="background-area-new-info"),
    url(r'^new/action/$', views.new_area_action, name="background-area-action-new"),
    url(r'^unique/$', views.validate_unique, name="background-area-validate-unique"),
    url(r'^data/$', views.index_data, name='background-area-data'),
    url(r'^delete/form/$', views.delete_form, name='delete_form'),
    url(r'^delete/action/$', views.area_delete_action, name='background-area-delete-action'),

    #data
    url(r'^areas/$', views.ajax_data, name='background-area-ajax-data'),
]