__author__ = 'gmj'

import simplejson
import logging

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db.models import Q

from hibiscus.models import SupportedArea, UpkeepMaterialModel
from hibiscus.decorators import data_table


LOG = logging.getLogger(__name__)

def index(request):
    return render(request, 'background/area/index.html')


def new_area_info(request):
    return render(request, 'background/area/new.html')


def new_area_action(request):
    res = {"res": "success"}
    try:
        province = request.POST["province"]
        cities = request.POST.getlist("cities")
        for c in cities:
            SupportedArea(province=province, city=c).save()

    except Exception, e:
        res = {"res": "fail"}
        LOG.error('Error happened when create area.%s' % e)
    return HttpResponse(simplejson.dumps(res))


def validate_unique(request):
    res = "no"
    try:
        province = request.GET["province"]
        city = request.GET["city"]
        s = SupportedArea.objects.filter(province=province, city=city)
        if s:
            res = "yes"
    except Exception, e:
        res = "error"
        LOG.error('Get unique data error. %s' % e)
    return HttpResponse(res)


AREA_COLUMNS = ["province", "city"]
@data_table(columns=AREA_COLUMNS, model=SupportedArea)
def index_data(request):
    try:
        sts = SupportedArea.objects.filter(
            Q(province__contains=request.table_data["key_word"]) |
            Q(city__contains=request.table_data["key_word"])).\
            order_by(request.table_data["order_cl"])[request.table_data["start"]: request.table_data["end"]]
        st_list = []
        for st in sts:
            st_params = {}
            st_params["DT_RowId"] = st.id
            st_params["province"] = st.province
            st_params["city"] = st.city
            # st_params["score"] = st.score
            st_list.append(st_params)
        return st_list
    except Exception, e:
        LOG.error('Get area index data error. %s' % e)


def delete_form(request):
    ids = request.GET["ids"]
    return render(request, 'background/area/area-delete.html', {"ids": ids})


def area_delete_action(request):
    res = "success"
    try:
        ids = request.POST["ids"]
        id_l = ids.split("_")
        for id_v in id_l:
            if id_v:
                SupportedArea.objects.filter(pk=id_v).delete()
    except Exception, e:
        res = "fail"
        LOG.error('Error happened when delete area. %s' % e)
    return HttpResponse(simplejson.dumps({"res": res}))
# def ajax_data(request):
#     start = request.GET["start"]
#     length = request.GET["length"]
#     draw = request.GET["draw"]
#     filter_data = request.GET["search[value]"]
#     end = int(start) + int(length)
#     if filter_data:
#         key_word = filter_data
#     else:
#         key_word = ""
#     # Entry.objects.filter(headline__contains='%')
#     sts = Students.objects.filter(name__contains=key_word)[int(start):end]
#     total_num = len(Students.objects.all())
#     st_list = []
#     st_dict = {}
#     for st in sts:
#         st_params = {}
#         st_params["DT_RowId"] = st.id
#         st_params["name"] = st.name
#         st_params["age"] = st.age
#         st_params["score"] = st.score
#         st_list.append(st_params)
#     st_dict["draw"] = draw
#     st_dict["recordsTotal"] = len(sts)
#     st_dict["recordsFiltered"] = total_num
#     st_dict["data"] = st_list
#
#     return HttpResponse(json.dumps(st_dict))

def ajax_data(request):
    area_data = []
    m_areas = []
    try:
        model_id = request.GET.get("model_id", "")
        if model_id:
            mobj = UpkeepMaterialModel.objects.get(pk=model_id)
            m_areas = list(mobj.area.all())
        area = SupportedArea.objects.all()
        for a in area:
            a_params = {}
            a_params["id"] = a.id
            a_params["province"] = a.province
            a_params["city"] = a.city
            a_params['is_checked'] = True if a in m_areas else False
            area_data.append(a_params)
    except Exception as e:
        LOG.error('Can not get area data. %s' % e)
    return HttpResponse(simplejson.dumps(area_data))