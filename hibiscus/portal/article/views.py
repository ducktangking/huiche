# -*- coding:utf-8 -*-
from hibiscus.models import Article, Series, Car, ProducerCarSeries
import logging
import simplejson
LOG = logging.getLogger(__name__)


def detail_content(request):
    """
        新闻详情，如果新闻绑定车型，+ 关联车型&头部样式， 如果新闻由经销商创建，+经销商代理车型，地址，名称
    """
    article = {}
    try:
        article_id = request.GET.get("article_id", None)
        art = Article.objects.get(pk=article_id)
        article['res'] = 'success'
        article['title'] = art.title
        article['id'] = art.id
        article['content'] = art.content
        article['create_type'] = art.created_type
        article['reprint_addr'] = art.reprint_addr
        article['created_at'] = art.created_at.strftime("%Y-%m-%d %H:%M:%S")
        article['article_type'] = art.type.parent.name if art.type.parent else art.type.name
        if art.series:
            article['s_ex'] = True
        else:
            article['s_ex'] = False
        if art.producer:
            article['is_producer'] = True
        else:
            article['is_producer'] = False
    except Exception as e:
        LOG.error('Error happened while show article details. %s' % e)
        article['res'] = 'fail'
    return article


def bind_series(request):
    b_dict = {}
    try:
        b_dict['res'] = 'success'
        article_id = request.GET.get("article_id", None)
        art = Article.objects.get(pk=article_id)
        b_dict['series_id'] = art.series.id
        b_dict['series'] = art.series.name
        b_dict['b_logo'] = art.series.brand.parent.logo.picture.path
        cars = Car.objects.filter(series=art.series)
        produce_year = []
        for ca in cars:
            if ca.produce_year in produce_year:
                continue
            produce_year.append(ca.produce_year)
        b_dict['y_list'] = produce_year
        r_series = art.series.related_s.all()
        series_list = []
        if r_series:
            i = 0
            for r in r_series:
                r_params = {}
                r_params['id'] = r.id
                r_params['name'] = r.name
                r_params['pic'] = r.logo.picture.path
                if i >=8:
                    break
                i += 1
                series_list.append(r_params)
        b_dict['s_list'] = series_list
    except Exception as e:
        LOG.error('Error happened while get related series . %s' % e)
        b_dict['res'] = 'fail'
    return b_dict


def author_producer(request):
    a_dict = {}
    try:
        a_dict['res'] = 'success'
        article_id = request.GET.get("article_id", None)
        art = Article.objects.get(pk=article_id)
        a_dict['pro_name'] = art.producer.information.name
        a_dict['pro_add'] = art.producer.information.addresses.all()[0].address if \
            art.producer.information.addresses.all() else ''
        a_dict['pro_tel'] = art.producer.phone
        pro_obj = ProducerCarSeries.objects.filter(producer=art.producer)
        a_dict['pro_brand'] = pro_obj[0].brand.name if pro_obj and pro_obj[0].brand else ''
    except Exception as e:
        LOG.error('Error happened while get author infomation if author is a producer . %s' % e)
        a_dict['res'] = 'fail'
    return a_dict

