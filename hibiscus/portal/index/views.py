#-*-coding:utf-8 -*-
import simplejson
import logging
from hibiscus.models import Brand, Series, Car, Ad, Article, GroupPurchase, GroupPurchaseUser,\
    CarQuote, SeriesCallOut, Producer, SupportedArea
from django.utils import timezone
from django.db.models import Q
LOG = logging.getLogger(__name__)


def hotBrandPic(request):
    brand = Brand.objects.filter(parent__is_hot__name=u"热门品牌")
    BrandPic_list = []
    try:
        for bp in brand:
            bp_params = {}
            bp_params['id'] = bp.parent_id
            bp_params['pic'] = bp.parent.logo.picture.path
            bp_params['name'] = bp.parent.name
            if bp_params in BrandPic_list:
                continue
            BrandPic_list.append(bp_params)
    except Exception as e:
        LOG.error('Error happened when index top hot brand pictures. %s' % e)
    return BrandPic_list[0:12]


def hot_series_pic(request):
    """
    0:紧凑型， 1：中型， 2：SUV， 3：MPV
    """
    cars = Car.objects.filter(is_hot=True)
    car_list = []
    car_nums = [0, 0, 0, 0]
    try:
        if cars:
            for ca in cars:
                ca_params = {}
                ca_params['id'] = ca.series_id
                ca_params['name'] = ca.series.name
                if ca.grade.name == u'紧凑型车':
                    i = 0
                elif ca.grade.name == u'中型车':
                    i = 1
                elif ca.grade.name == u'SUV':
                    i = 2
                elif ca.grade.name == u'MPV':
                    i = 3
                else:
                    continue
                if ca_params in car_list:
                    continue
                ca_params['type'] = i
                if car_nums[i] >= 6:
                    continue
                else:
                    car_nums[i] += 1
                car_list.append(ca_params)
    except Exception as e:
        LOG.error('Error happened while filter hot series car. %s' % e)
    return car_list


def new_car_and_suv_pic(request):
    car_list = []
    id_1_list = []
    id_2_list = []
    try:
        cars_1 = Car.objects.filter(is_new=True)
        cars_2 = Car.objects.filter(series__is_pc=True).filter(grade__name=u'SUV')
        i = 0
        j = 0
        if cars_1:
            for ca in cars_1:
                ca_params = {}
                ca_params['id'] = ca.series_id
                if ca.series_id in id_1_list:
                    continue
                id_1_list.append(ca.series_id)
                ca_params['name'] =ca.series.name
                i = j/7
                j += 1
                if i>=4:
                    continue
                ca_params['type'] = i
                if ca_params in car_list:
                    continue
                car_list.append(ca_params)
        if cars_2:
            j = 28
            for ca in cars_2:
                ca_params = {}
                ca_params['id'] = ca.series_id
                if ca.series_id in id_1_list:
                    continue
                id_1_list.append(ca.series_id)
                ca_params['name'] = ca.series.name
                i = j / 7
                j += 1
                if i >= 8:
                    continue
                ca_params['type'] = i
                if ca_params in car_list:
                    continue
                car_list.append(ca_params)
    except Exception as e:
        LOG.error('Error happened while filter new cars and SUV type cars. %s' % e)
    return car_list


def car_by_price(request):
    car_list = []
    id_list = []
    cars = Car.objects.filter(series__is_pc=True)
    car_num = [0, 0, 0, 0, 0, 0, 0]
    try:
        if cars:
            i = 4
            j = 0
            n = 0
            for ca in cars:
                ca_params = {}
                ca_params['id'] = ca.series_id
                if ca.series_id in id_list:
                    continue
                id_list.append(ca.series_id)
                ca_params['name'] = ca.series.name
                if ca.price <= 5:
                    i = 4
                if 5 < ca.price <= 8:
                    i = 5
                if 8 < ca.price <= 12:
                    i = 6
                if 12 < ca.price <= 18:
                    i = 7
                if 18 < ca.price <= 25:
                    i = 8
                if 25 < ca.price <= 40:
                    i = 9
                if ca.price > 40:
                    i = 10
                j = i - 4
                n = car_num[j]/7
                car_num[j] += 1
                if n > 4:
                    continue
                ca_params['type'] = i
                ca_params['level'] = n
                if ca_params in car_list:
                    continue
                car_list.append(ca_params)
    except Exception as e:
        LOG.error('Error happened when choose car pictures by price .%s' % e)
    return car_list


# 首页右侧 车型图片动态展示
def pic_show(request):
    cars = Series.objects.filter(page__in=range(1, 6))
    car_list = []
    car_num = []
    i = 0
    try:
        if cars:
            for ca in cars:
                ca_params = {}
                ca_params['id'] = ca.id
                ca_params['name'] = ca.brand.parent.name + ca.name
                ca_params['pic'] = ca.logo.picture.path
                pri_car = Car.objects.filter(series=ca)
                if pri_car:
                    min_pri = pri_car.order_by('price')[0].price
                    max_pri = pri_car.order_by('-price')[0].price
                    ca_params['price'] = ('%5.2f - %5.2f' % (min_pri, max_pri))
                else:
                    ca_params['price'] = ' 0 - 0 '
                if ca.page == 4:
                    if pri_car[0].price <= 5:
                        i = 4
                    if 5 < pri_car[0].price <= 8:
                        i = 5
                    if 8 < pri_car[0].price <= 12:
                        i = 6
                    if 12 < pri_car[0].price <= 18:
                        i = 7
                    if 18 < pri_car[0].price <= 25:
                        i = 8
                    if 25 < pri_car[0].price <= 40:
                        i = 9
                    if pri_car[0].price > 40:
                        i = 10
                elif ca.page == 5:
                    i = 11
                else:
                    i = ca.page
                if i in car_num:
                    continue
                car_num.append(i)
                ca_params['type'] = i
                car_list.append(ca_params)
    except Exception as e:
        LOG.error('Error happened while show index top series pictures.% s' % e)
    return car_list


def series_ad_show(request):
    """
        车型图片广告，共八个，一对一
    """
    series = Series.objects.filter(position__in=range(1, 9))
    car_list = []
    car_num = []
    try:
        if series:
            for se in series:
                se_params = {}
                se_params['id'] = se.id
                se_params['name'] = se.name
                se_params['pic'] = se.logo.picture.path
                se_params['type'] = se.position - 1
                if se.position in car_num:
                    continue
                car_num.append(se.position)
                car_list.append(se_params)
    except Exception as e:
        LOG.error('Error happened while set ad8 . %s'% e)
    return car_list


def search_brand(request):
    brand = Brand.objects.filter(parent__isnull=False)
    brand_list = []
    for b in brand:
        b_params = {}
        b_params['id'] = b.id
        b_params['name'] = b.name
        brand_list.append(b_params)
    return brand_list


def search_series(request):
    brandid = request.GET.get('brandid', '')
    series = Series.objects.filter(brand=brandid)
    series_list = []
    if series:
        for s in series:
            s_params ={}
            s_params['id'] = s.id
            s_params['name'] = s.name
            series_list.append(s_params)
    return series_list


def carousel_article(request):
    article_list = []
    ar_num = []
    article = Article.objects.filter(Q(position__in=range(1, 21)), Q(area_id=2))
    ads_2 = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                              Q(area_id=2), Q(location__name__contains=u'新闻-轮播-第'))
    try:
        if ads_2:
            for a in ads_2:
                l_name = a.location.name
                position = l_name.split('...')[1]
                ar_num.append(int(position))
        if article:
            for ar in article:
                ar_params  = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['pic'] = ar.default_picture.path if ar.default_picture else ''
                ar_params['position'] = ar.position
                if ar.position in ar_num:
                    continue
                ar_num.append(ar.position)
                article_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened while get news carousel')
    return article_list


def all_area_headline(request):
    """
        总站的头条讯息，一共两条，按照修改时间排列，修改时间可根据设置最新修改
    """
    head_list = []
    art = Article.objects.filter(~Q(position__in=range(1, 21)), Q(is_headline=True), Q(area_id=2),
                                 Q(status=True), Q(type_id=1) | Q(type__parent_id=1)).order_by('-updated_at')
    try:
        if art:
            i = 0
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['type'] = i
                i += 1
                if i >= 2:
                    break
                head_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened when get all area headline. %s') %e
    return head_list


def global_news(request):
    """
        第一个模块10条，第二个模块34条,type模块，level分条
    """
    ar_list = []
    art = Article.objects.filter(~Q(position__in=range(1, 21)), Q(is_headline=False), Q(area_id=2),
                                 Q(status=True), Q(type_id=1) | Q(type__parent_id=1)).order_by('-updated_at')
    try:
        if art:
            i = 0
            j = 1
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['type'] = j
                ar_params['level'] = i
                if i > 10:
                    j = 2
                    ar_params['type'] = j
                    ar_params['level'] = j-10
                elif i > 44:
                    break
                i += 1
                ar_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened while get global news . %s ' % e)
    return ar_list


def factory_set(request):
    """
        测评左为 1, 厂商动态右为 2,测评有头条 共7条，头条为最新一项
    """
    art_list = []
    art_1 = Article.objects.filter(Q(area_id=2), Q(status=True),
                                   Q(type_id=3) | Q(type__parent_id=3)).order_by('-updated_at')
    art_2 = Article.objects.filter(Q(area_id=2), Q(status=True),
                                   Q(type_id=2) | Q(type__parent_id=2), ~Q(type_id=33)).order_by('-updated_at')
    try:
        if art_1:
            i = 0
            for ar in art_1:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['type'] = i
                ar_params['kind'] = 1
                if i >= 7:
                    continue
                i += 1
                art_list.append(ar_params)
        if art_2:
            i = 0
            for ar in art_2:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['type'] = i
                ar_params['kind'] = 2
                if i >= 10:
                    continue
                i += 1
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened while get data from business movement and assess. %s'% e)
    return art_list


def new_car_appear(request):
    """
        新车上市， 一共四个，单独筛选
    """
    art_list = []
    art = Article.objects.filter(Q(area_id=2), Q(status=True),
                                 Q(type_id=33)).order_by('-updated_at')
    try:
        if art:
            i = 0
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['type'] = i
                if ar.series:
                    ar_params['name'] = ar.brand.parent.name + ar.series.name
                    ar_params['pic'] = ar.series.logo.picture.path
                else:
                    ar_params['name'] = ''
                    ar_params['pic'] = ""
                if i >= 4:
                    break
                i += 1
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened while get data from new car appear. %s'% e)
    return art_list


def say_things(request):
    """
        说客，共十五条，按照更新时间排序
    """
    art_list = []
    art = Article.objects.filter(Q(type_id=4) | Q(type__parent_id=4),
                                 Q(status=True), Q(is_new=True)).order_by('-updated_at')
    try:
        if art:
            i = 0
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['pic'] = ar.default_picture.path if ar.default_picture else ''
                ar_params['type'] = i
                if i >= 15:
                    break
                i += 1
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened while get data from sar things. %s'% e)
    return art_list


def area_index_top(request):
    area_id = request.GET.get('area_id', 1)
    art_list = []
    try:
        art = Article.objects.filter(Q(position__in=range(1, 8)), Q(area_id=area_id))
        if art:
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                ar_params['pic'] = ar.default_picture.path if ar.default_picture else ''
                ar_params['type'] = ar.position
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened when get data from area piotion. %s' % e)
    return art_list


def area_news_headline(request):
    area_id = request.GET.get('area_id', '')
    art_list = []
    try:
        art = Article.objects.filter(~Q(position__in=range(1, 8)), Q(is_headline=True),
                                     Q(area_id=area_id)).order_by('-updated_at')
        if art:
            i = 0
            for ar in art:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                if i<2:
                    ar_params['type'] = 1
                    ar_params['level'] = i
                else:
                    ar_params['type'] = 2
                    ar_params['level'] = i - 2
                if i >= 5:
                    break
                i += 1
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened when area news headline. %s'% e)
    return art_list


def area_news_article(request):
    """
        分站文章，前端页面一共分两块，14 *2, 10 * 3, 横向排列
    """
    area_id = request.GET.get('area_id', '')
    art_list = []
    try:
        art_1 = Article.objects.filter(~Q(position__in=range(1, 8)), Q(is_headline=False),
                                       Q(area_id=area_id), Q(type_id=45) | Q(type__parent_id=45),
                                       ~Q(type__name=u'人物访谈')).order_by('-updated_at')
        art_2 = Article.objects.filter(~Q(position__in=range(1, 8)), Q(is_headline=False),
                                       Q(area_id=area_id), Q(type_id=1) | Q(type__parent_id=1),
                                       ~Q(type__name=u'销售促销'), ~Q(type__name=u'售后促销'),
                                       ~Q(type__name=u'人物访谈')).order_by('-updated_at')
        if art_1:
            i = 0
            j = 1
            for ar in art_1:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                if i >= 28:
                    break
                ar_params['type'] = i
                ar_params['level'] = j
                i += 1
                art_list.append(ar_params)
        if art_2:
            i = 0
            j = 2
            for ar in art_2:
                ar_params = {}
                ar_params['id'] = ar.id
                ar_params['title'] = ar.title
                if i >= 30:
                    break
                ar_params['type'] = i
                ar_params['level'] = j
                i += 1
                art_list.append(ar_params)
    except Exception as e:
        LOG.error('Error happened when area news headline. %s' % e)
    return art_list


def area_hot_series(request):
    """
        区域 热门车型 筛选 热门车是由 车款页勾选的热门车, 猜你喜欢是由车型页面勾选, 车型由经销商报价得出， 去权重高者，权重相同，取价格低者
    """
    car_list = []
    ju_list = []
    car_num = [0, 0, 0, 0, 0, 0, 0, 0]
    try:
        area_id = request.GET.get('area_id', 1)
        ca_q = CarQuote.objects.filter(Q(producer__area_id=area_id), Q(car__is_hot=True))
        if ca_q:
            for cq in ca_q:
                cq_params = {}
                cq_params['id'] = cq.car_id
                cq_params['logo'] = cq.car.series.brand.parent.logo.picture.path
                cq_params['pre_price'] = cq.car.price
                cq_params['name'] = cq.car.series.name + cq.car.produce_year + u'款'
                # 同区域，权重最高的报价经销商
                pro_car = CarQuote.objects.filter(Q(producer__area_id=area_id), Q(car=cq.car)).order_by('-weight')
                # 同权重，报价更低的经销商
                weight_producer = CarQuote.objects.filter(Q(producer__area_id=area_id),
                                                          Q(car=cq.car), Q(weight=pro_car[0].weight)).order_by('sale_price')
                cq_params['price'] = weight_producer[0].sale_price
                cq_params['producer'] = weight_producer[0].producer.information.name
                if cq.car.series.is_hot.filter(name=u'猜您喜欢'):
                    cq_params['type'] = 1
                elif cq_params['price'] <= 5:
                    cq_params['type'] = 2
                elif 5 < cq_params['price'] <= 8:
                    cq_params['type'] = 3
                elif 8 < cq_params['price'] <= 12:
                    cq_params['type'] = 4
                elif 12 < cq_params['price'] <= 18:
                    cq_params['type'] = 5
                elif 18 < cq_params['price'] <= 25:
                    cq_params['type'] = 6
                elif 25 < cq_params['price'] <= 40:
                    cq_params['type'] = 7
                else:
                    cq_params['type'] = 8
                i = cq_params['type'] - 1
                cq_params['level'] = car_num[i]
                if car_num[i] >= 10:
                    continue
                car_num[i] += 1
                ju_params = {}
                ju_params['id'] = cq_params['id']
                ju_params['name'] = cq_params['name']
                ju_params['producer'] = cq_params['producer']
                ju_params['price'] = cq_params['price']
                if ju_params in ju_list:
                    continue
                ju_list.append(ju_params)
                car_list.append(cq_params)
    except Exception as e:
        LOG.debug("Error when index area is_hot car, error is %s" % e)
    return car_list


def re_producer(request):
    producer_list = []
    try:
        area_id = request.GET.get('area_id', 1)
        # area_id = 1
        recommend_pro = Producer.objects.filter(Q(is_active=True), Q(is_recommend=True),
                                                Q(area_id=area_id)).order_by('-created_at')
        if recommend_pro:
            i = 0
            for rep in recommend_pro:
                rep_params = {}
                rep_params['id'] = rep.id
                rep_params['name'] = rep.information.name
                rep_params['del'] = rep.phone
                rep_params['pic'] = rep.information.portrait.picture.path
                if rep.type == '4':
                    rep_params['type'] = u"4S店"
                elif rep.type == 'b':
                    rep_params['type'] = u"维修保养"
                elif rep.type == 'q':
                    rep_params['type'] = u"快修店"
                else:
                    rep_params['type'] = u"综合销售"
                if i >= 64:
                    break
                i += 1
                producer_list.append(rep_params)
    except Exception as e:
        LOG.debug("Error when index area recommend producer, error is %s" % e)
    return producer_list


def area_sales(request):
    area_sales_list = []
    sale_num = [0,0]
    try:
        area_id = request.GET.get('area_id', 1)
        # area_id = 1
        sales_article = Article.objects.filter(Q(status=True),
                                               Q(area_id=area_id), Q(type__name=u'销售促销') | Q(type__name=u'售后促销'))
        if sales_article:
            for sa in sales_article:
                sa_params = {}
                sa_params['id'] = sa.id
                sa_params['title'] = sa.title
                if sa.type.name == u'销售促销':
                    sa_params['type'] = 1
                else:
                    sa_params['type'] = 2
                i = sa_params['type'] - 1
                if sale_num[i] >= 10:
                    continue
                sa_params['level'] = sale_num[i]
                sale_num[i] += 1
                area_sales_list.append(sa_params)
    except Exception as e:
        LOG.debug("Error when index area sales, error is %s" % e)
    return area_sales_list


def famous_interviews(request):
    ar_list = []
    try:
        area_id = request.GET.get('area_id', 1)
        art = Article.objects.filter(Q(status=True),
                                     Q(area_id=area_id), Q(type__name=u'人物访谈')).order_by('-updated_at')
        if art:
            i = 0
            for a in art:
                a_params = {}
                a_params['id'] = a.id
                a_params['pic'] = a.default_picture.path if a.default_picture else ''
                a_params['title'] = a.title
                a_params['type'] = i
                if i >= 10:
                    break
                i += 1
                ar_list.append(a_params)
    except Exception as e:
        LOG.error('Error happened while get data from famous_interviews. %s' % e)
    return ar_list


def hot_series(request):
    series_list =[]
    hot_series = Series.objects.filter(Q(is_hot__name=u"热门车"), Q(is_pc=True))
    if hot_series:
        for hs in hot_series:
            hs_params = {}
            hs_params['pic'] = hs.logo.picture.path
            hs_params['name'] = hs.name
            hs_params['id'] = hs.id
            hs_params['block'] = 'hot'
            hs_params['type'] = ''
            series_list.append(hs_params)
    buy_series = Series.objects.filter(Q(is_hot__name=u"推荐车"), Q(is_pc=True))
    if buy_series:
        for bs in buy_series:
            bs_params = {}
            bs_params['type'] = Car.objects.filter(series_id=bs.id)[0].grade.name
            bs_params['block'] = 'buy'
            bs_params['id'] = bs.id
            bs_params['name'] = bs.name
            series_list.append(bs_params)
    return series_list


def all_buying_car(request):
    """
        由车型编辑中 推荐车 得出，不需要通过is_pc选定
    """
    car_list = []
    id_list = []
    car_num = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    try:
        cars = Car.objects.filter(series__is_hot__name=u"推荐车")
        if cars:
            for ca in cars:
                ca_params = {}
                ca_params['id'] = ca.series_id
                if ca.series_id in id_list:
                    continue
                id_list.append(ca.series_id)
                ca_params['name'] = ca.series.brand.parent.name + ca.series.name
                if len(ca_params['name']) > 6:
                    ca_params['name'] = ca.series.name
                ca_params['type'] = 0
                if car_num[0] >= 10:
                    if ca.grade.name == u'微型车':
                        ca_params['type'] = 1
                    elif ca.grade.name == u'小型车':
                        ca_params['type'] = 2
                    elif ca.grade.name == u'中型车':
                        ca_params['type'] = 3
                    elif ca.grade.name == u'中大型车':
                        ca_params['type'] = 4
                    elif ca.grade.name == u'豪华车':
                        ca_params['type'] = 5
                    elif ca.grade.name == u'SUV':
                        ca_params['type'] = 6
                    elif ca.grade.name == u'跑车':
                        ca_params['type'] = 7
                    elif ca.grade.name == u'MPV':
                        ca_params['type'] = 8
                    else:
                        continue
                if ca_params['type'] == 0:
                    ca_params['level'] = car_num[0]
                else:
                    ca_params['level'] = car_num[ca_params['type']]
                    car_num[ca_params['type']] += 1
                if car_num[ca_params['type']] >=10:
                    continue
                car_num[0] += 1
                car_list.append(ca_params)
    except Exception as e:
        LOG.error('Error happened while get data from all people buying. %s' % e)
    return car_list


def global_ad(request):
    ad_list = []
    try:
        ads_1 = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                                  Q(area_id=2), ~Q(location__name__contains=u'新闻-轮播-第'))
        ads_2 = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                                  Q(area_id=2), Q(location__name__contains=u'新闻-轮播-第'))
        if ads_1:
            for a in ads_1:
                a_params = {}
                a_params['link'] = a.link
                a_params['pic'] = a.picture.path
                a_params['name'] = a.name
                a_params['width'] = a.location.width
                a_params['height'] = a.location.height
                a_params['position'] = 0
                l_name = a.location.name
                type = l_name.split('...')[1]
                a_params['type'] = int(type)
                ad_list.append(a_params)
        if ads_2:
            for a in ads_2:
                a_params = {}
                a_params['link'] = a.link
                a_params['pic'] = a.picture.path
                a_params['name'] = a.name
                a_params['width'] = a.location.width
                a_params['height'] = a.location.height
                a_params['type'] = 0
                l_name = a.location.name
                position = l_name.split('...')[1]
                a_params['position'] = int(position)
                ad_list.append(a_params)
    except Exception as e:
        LOG.error('Error happened while get data from all area ad . %s' % e)
    return ad_list


def area_ad(request):
    ad_list = []
    try:
        area_id = request.GET.get('area_id', 1)
        ads = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                                Q(area_id=area_id), ~Q(location__name__contains=u'热销车'))
        if ads:
            for a in ads:
                a_params = {}
                a_params['link'] = a.link
                a_params['pic'] = a.picture.path
                a_params['name'] = a.name
                a_params['width'] = a.location.width
                a_params['height'] = a.location.height
                l_name = a.location.name
                type = l_name.split('...')[1]
                a_params['type'] = int(type)
                ad_list.append(a_params)
    except Exception as e:
        LOG.error('Error happened while get data from area ad except hot_saling car. %s' % e)
    return ad_list


def hot_saling_car(request):
    ad_list = []
    try:
        area_id = request.GET.get('area_id', 1)
        ads = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                                Q(area_id=area_id), Q(location__name__contains=u'热销车'))
        if ads:
            for a in ads:
                a_params = {}
                a_params['link'] = a.link
                a_params['pic'] = a.picture.path
                a_params['name'] = a.name
                a_params['width'] = a.location.width
                a_params['height'] = a.location.height
                l_name = a.location.name
                type = l_name.split('-')[1]
                a_params['type'] = int(type)
                ad_list.append(a_params)
    except Exception as e:
        LOG.error('Error happened while get data from area hot_saling car. %s' % e)
    return ad_list


def get_area(request):
    # 从前端获取地址信息，解析获得area_id并返回
    data = {}
    try:
        province = request.GET.get('province', '')
        city = request.GET.get('city', '')
        area = SupportedArea.objects.filter(Q(province__contains=province), Q(city__contains=city))
        if area:
            data['area_id'] = area[0].id
            data['city'] = area[0].city
        else:
            data['area_id'] = 1
            data['city'] = u"南京"
    except Exception as e:
        LOG.error('Error happened while analyze area. %s' % e)
    return data


def bind_series_ad(request):
    ad_list = []
    try:
        ads = Ad.objects.filter(Q(location__index=u"前端-首页"), Q(location__is_app=False),
                                Q(location__name__contains=u'车型绑定广告'))
        if ads:
            for a in ads:
                a_params = {}
                a_params['link'] = a.link
                a_params['pic'] = a.picture.path
                a_params['name'] = a.name
                a_params['width'] = a.location.width
                a_params['height'] = a.location.height
                l_name = a.location.name
                type = l_name.split('-')[1]
                a_params['type'] = int(type)
                ad_list.append(a_params)
    except Exception as e:
        LOG.error('Error happened while get bind series ad-8. %s' % e)
    return ad_list


