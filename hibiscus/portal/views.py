# -*- coding:utf-8 -*-
import simplejson

from django.http import HttpResponse
from hibiscus.portal.auth.views import register as portal_register, login as portal_login, logout as portal_logout
from hibiscus.portal.index.views import hotBrandPic as index_hotBrandPic, hot_series_pic as index_hot_series_pic,\
    new_car_and_suv_pic as index_new_car_and_suv_pic, car_by_price as index_car_by_price, pic_show as index_pic_show, \
    series_ad_show as index_series_ad_show, search_brand as index_search_brand, search_series as index_search_series,\
    carousel_article as index_carousel_article, all_area_headline as index_all_area_headline, \
    global_news as index_global_news, factory_set as index_factory_set, new_car_appear as index_new_car_appear,\
    say_things as index_say_things, area_index_top as index_area_index_top, area_news_headline as index_area_news_headline,\
    area_news_article as index_area_news_article, area_hot_series as index_area_hot_series, re_producer as index_re_producer,\
    area_sales as index_area_sales, global_ad as index_global_ad, famous_interviews as index_famous_interviews, \
    all_buying_car as index_all_buying_car, area_ad as index_area_ad, hot_saling_car as index_hot_saling_car, get_area as index_get_area, \
    bind_series_ad as index_bind_series_ad

from hibiscus.portal.article.views import detail_content as article_detail_content, bind_series as article_bind_series, \
    author_producer as article_author_producer


def register(request):
    res = portal_register(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def login(request):
    res = portal_login(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def logout(request):
    res = portal_logout(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_area(request):
    res = index_get_area(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")

def hotBrandPic(request):
    res = index_hotBrandPic(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def hot_series_car(request):
    res = index_hot_series_pic(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def new_car_and_suv(request):
    res = index_new_car_and_suv_pic(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_by_price(request):
    res = index_car_by_price(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def pic_show(request):
    res = index_pic_show(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def series_ad_show(request):
    res = index_series_ad_show(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def search_brand(request):
    res = index_search_brand(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def search_series(request):
    res = index_search_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def carousel_article(request):
    res = index_carousel_article(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def all_area_headline(request):
    res = index_all_area_headline(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def global_news(request):
    res = index_global_news(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def factory_set(request):
    res = index_factory_set(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def new_car_appear(request):
    res = index_new_car_appear(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def say_things(request):
    res = index_say_things(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_index_top(request):
    res = index_area_index_top(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_news_headline(request):
    res = index_area_news_headline(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_news_article(request):
    res = index_area_news_article(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_hot_series(request):
    res = index_area_hot_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def re_producer(request):
    res = index_re_producer(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_sales(request):
    res = index_area_sales(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def famous_interviews(request):
    res = index_famous_interviews(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def all_buying_car(request):
    res = index_all_buying_car(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def global_ad(request):
    res = index_global_ad(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_ad(request):
    res = index_area_ad(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def hot_saling_car(request):
    res = index_hot_saling_car(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def detail_content(request):
    res = article_detail_content(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def bind_series(request):
    res = article_bind_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def author_producer(request):
    res = article_author_producer(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_area(request):
    res = index_get_area(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def bind_series_ad(request):
    res = index_bind_series_ad(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")
