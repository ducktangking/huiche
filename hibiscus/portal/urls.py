# -*- coding:utf-8 -*-

from django.conf.urls import url, include
from hibiscus.portal import views

urlpatterns = [
    # 首页
    url(r'^register$', views.register, name="portal-register"),
    url(r'^login$', views.login, name="portal-login"),
    url(r'^logout$', views.logout, name="portal-logout"),
    url(r'^index/hotbrandpic$', views.hotBrandPic, name="portal-index-hotbrandpic"),
    url(r'^index/hot/series/car$', views.hot_series_car, name="portal-index-hot-series-car"),
    url(r'^index/new/car/suv$', views.new_car_and_suv, name="portal-index-new-car-suv"),
    url(r'^index/car/by/price$', views.car_by_price, name='portal-index-car-by-price'),
    url(r'^index/pic/show$', views.pic_show, name='portal-index-pic-show'),
    url(r'^index/series/ad/show$', views.series_ad_show, name='portal-index-series-ad-show'),
    url(r'^index/search/brand$', views.search_brand, name='portal-index-search-brand'),
    url(r'^index/search/series$', views.search_series, name='portal-index-search-series'),
    url(r'^index/carousel/article$', views.carousel_article, name='portal-index-carousel-article'),
    url(r'^index/all/area/headline$', views.all_area_headline, name='portal-index-all-area-headline'),
    url(r'^index/global/news$', views.global_news, name='portal-index-global-news'),
    url(r'^index/factory/set$', views.factory_set, name='portal-index-business-and-assess'),
    url(r'^index/new/car/appear$', views.new_car_appear, name='portal-index-new-car-appear'),
    url(r'^index/say/things$', views.say_things, name='portal-index-say-things'),
    url(r'^index/area/index/top$', views.area_index_top, name='portal-index-area-index-top'),
    url(r'^index/area/news/headline$', views.area_news_headline, name='portal-index-area-news-headline'),
    url(r'^index/area/news/article$', views.area_news_article, name='portal-index-area-news-article'),
    url(r'^index/area/hot/series$', views.area_hot_series, name='portal-index-area-hot-car'),
    url(r'^index/area/recommend/producer$', views.re_producer, name='portal-index-re-producer'),
    url(r'^index/area/sales$', views.area_sales, name='portal-index-area-sales'),
    url(r'^index/area/famous/interviews$', views.famous_interviews, name='portal-index-area-famous-interviews'),
    url(r'^index/all/buying/car$', views.all_buying_car, name='portal-index-all-buying-car'),
    url(r'^index/global/ad$', views.global_ad, name="portal-index-global-ad"),
    url(r'^index/area/ad$', views.area_ad, name='portal-index-area-ad'),
    url(r'^index/hot/saling/car$', views.hot_saling_car, name='portal-index-hot-saling-car'),
#   文章详情
    url(r'^article/detail/content$', views.detail_content, name='portal-article-detail-content'),
    url(r'^article/bind/series$', views.bind_series, name='portal-article-bind-series'),
    url(r'^article/author/producer$', views.author_producer, name='portal-article-author-producer'),
    url(r'^index/get/area$', views.get_area, name='portal-index-get-area'),
    url(r'^index/bind/series/ad$', views.bind_series_ad, name='portal-index-bind-series-ad'),
]
