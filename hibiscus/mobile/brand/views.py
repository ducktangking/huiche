from django.contrib.auth.models import User

from hibiscus.mobile.series.views import get_series_car_cc, get_series_car_pic_num

__author__ = 'gmj'
import logging

from django.db.models import Min, Max
from hibiscus.models import Brand, BrandLevel, Car, BrandCallOut, CarQuote, UpkeepSeries, Series, Users
from hibiscus.decorators import page, login_info
from hibiscus.mobile.series.views import get_series_car_price_min_and_max

LOG = logging.getLogger(__name__)


def get_hot_brands(request):
    sts = Brand.objects.filter(is_hot__id=1)
    return [{"id": st.id,
                  "name": st.name,
                  "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)} for st in sts]


def get_brands_by_level(request, level_id=1):
    brand_level = BrandLevel.objects.get(id=level_id)
    res = []
    if brand_level:
        res = [{brand_level.name : [{"id": brand.id,
                            "name": brand.name,
                            "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                   request.META["HTTP_HOST"], brand.logo.picture.path)
                            } for brand in brand_level.brand_set.all()]}]
    return res


@page
def get_brands(request):
    brands = Brand.objects.order_by("initial")
    return [{"id": st.id,
             "initial": st.initial,
             "name": st.name,
             "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)} for st in brands]


@page
def get_top_brands(request):
    brands = Brand.objects.filter(parent=None).order_by("initial")
    return [{"id": st.id,
             "initial": st.initial,
             "name": st.name,
             "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)} for st in brands]


def get_second_brands(request):
    brand_id = request.GET.get("brand_id", None)
    second_brands = []
    if brand_id:
        brands = Brand.objects.filter(parent_id=brand_id).order_by("initial")
        for b in brands:
            bp = {}
            cars_min = CarQuote.objects.filter(car__series__brand=b).aggregate(Min('price'))
            cars_max = CarQuote.objects.filter(car__series__brand=b).aggregate(Max("price"))
            bp['min_price'] = cars_min['price__min']
            bp['max_price'] = cars_max['price__max']
            bp['id'] = b.id
            bp['initial'] = b.initial
            bp['name'] = b.name
            bp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], b.logo.picture.path)
            second_brands.append(bp)
    return second_brands


def get_series_car_quote_min_and_max(series_id):
    data = {"producer_min_price": 0, "producer_max_price": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        quotes = [quote.price for quote in CarQuote.objects.filter(car__in=list(cars))]
        data['producer_min_price'] = min(quotes) if quotes else 0
        data['producer_max_price'] = max(quotes) if quotes else 0
    except Exception as e:
        LOG.error('get_series_car_quote_min_and_max. %s' % e)
    return data


def get_series_car_upkeep_year(series_id):
    data = {"start": 2001, "end": 2016}
    try:
        upkeep_series = UpkeepSeries.objects.filter(series_id=series_id)
        if upkeep_series:
            years = [upkeep_s.produce_year.year for upkeep_s in upkeep_series]
            data['start'] = min(years)
            data['end'] = max(years)
    except Exception as e:
        LOG.error('get_series_car_upkeep_year failed. %s' % e)
    return data


@login_info
def get_second_brands_and_series(request):
    brand_id = request.GET.get("brand_id", None)
    series_type = request.GET.get("series_type", None)
    if series_type == "parallel":
        res = []
        if brand_id:
            series = Series.objects.filter(brand__id=brand_id, is_hot__id=6)
        else:
            series = Series.objects.filter(is_hot__id=6)
        for s in series:
            sp = {}
            sp['id'] = s.id
            sp['name'] = s.name
            sp['producer_price'] = get_series_car_quote_min_and_max(s.id)
            sp['price'] = get_series_car_price_min_and_max(s.id)
            sp['year'] = get_series_car_upkeep_year(s)
            sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], s.logo.picture.path) if s.logo else ""
            sp['cc'] = get_series_car_cc(s.id)
            sp['pic_num'] = get_series_car_pic_num(s.id)
            sp['country'] = s.brand.country.name if s.brand.country else ""
            res.append(sp)
        return res
    else:
        if brand_id:
            user = getattr(request, "app_user", None)
            fav_series = []
            if user:
                fav_series = list(user.fav_series.all())
            brobj = Brand.objects.filter(pk=brand_id)
            if brobj:
                if brobj[0].parent:
                    brands = brobj
                else:
                    brands = Brand.objects.filter(parent_id=brand_id).order_by("initial")
                return [{"id": st.id,
                         "initial": st.initial,
                         "name": st.name,
                         "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path),
                         "series": [{"id": series.id,
                                     "name": series.name,
                                     "producer_price": get_series_car_quote_min_and_max(series.id),
                                     "price": get_series_car_price_min_and_max(series.id),
                                     "year": get_series_car_upkeep_year(series),
                                     "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], series.logo.picture.path) if series.logo else "",
                                     "cc": get_series_car_cc(series.id),
                                     "pic_num": get_series_car_pic_num(series.id),
                                     'country': series.brand.country.name,
                                     "is_fav": True if series in fav_series else False
                                     } for series in st.series_set.all()]} for st in brands]
    return []


def get_country_brands(request):
    brand_countries = BrandCallOut.objects.all()
    brand_list = []
    res = {"brand_hot": [], "brand_japan": [], "brand_america": [], "brand_china": [], "brand_high": []}
    for bc in brand_countries:
        for brand in bc.brand_set.all().filter(parent__isnull=True):
            if bc.id == 1:
                res["brand_hot"].append({"id": brand.id, "name": brand.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path)})
            if bc.id == 2:
                res["brand_japan"].append({"id": brand.id, "name": brand.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path)})
            if bc.id == 3:
                res["brand_america"].append({"id": brand.id, "name": brand.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path)})
            if bc.id == 4:
                res["brand_china"].append({"id": brand.id, "name": brand.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path)})
            if bc.id == 5:
                res["brand_high"].append({"id": brand.id, "name": brand.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path)})

    return res


def get_brand_details(request):
    brand_id = request.GET.get("brand_id", None)
    res = {}
    if brand_id:
        try:
            brand = Brand.objects.get(id=brand_id)
            if brand:
                res = {"id": brand.id,
                       "name": brand.name,
                       "type": brand.type.name,
                       "level": brand.level.name,
                       "country": brand.country.name if brand.country else "",
                       "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], brand.logo.picture.path) if brand.logo else "",
                       # "is_hot": brand.is_hot,
                       "story": brand.story,
                       "logo_story": brand.logo_story}
        except Exception as e:
            LOG.debug("get_brand_details failed! error is %s " % e)
            res = {}

    return res


def get_series_brands(request):
    res = []
    try:
        series = Series.objects.filter(is_hot__id=6)
        for s in series:
            brand = s.brand
            bp = {"id": brand.id,
                   "name": brand.name,
                   "type": brand.type.name,
                   "level": brand.level.name,
                   "country": brand.country.name if brand.country else "",
                   "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"],
                                          brand.logo.picture.path) if brand.logo else "",
                   "story": brand.story,
                   "logo_story": brand.logo_story}
            res.append(bp)
    except Exception as e:
        LOG.error('Error happened get_series_brands %s' % e)
    return res