# -*- coding:utf-8 -*-
import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Portrait, Picture
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file
from hibiscus.decorators import require_app_login

LOG = logging.getLogger(__name__)


@require_app_login
def new(request):
    res = {"res": "fail"}
    try:
        f = request.FILES["file"]
        path = deal_with_upload_file(f)
        p_obj = Picture.objects.create(name=f.name, path=path)
        portrait = Portrait(picture=p_obj)
        portrait.save()
        user = request.app_user
        if user:
            user.information.portrait = portrait
            user.save()
            res["pic"] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], path)
            res["res"] = "success"
        else:
            res = {"res": "fail", "description": "更换头像失败"}
    except Exception as e:
        res = {"res": "fail", "description": "上传失败"}
        LOG.error('Error happened when mobile picture upload. %s' % e)
    return res
