import logging
from django.utils import timezone

from hibiscus.models import ArticleType, Article, Users
from hibiscus.decorators import page, require_app_login

LOG = logging.getLogger(__name__)
DEFAULT_ARTICLE_TYPE = [1, 2, 3]


def get_article_type(request):
    category = request.GET.get("category", "parent")
    if category == "parent":
        article_types = ArticleType.objects.filter(parent=None)
        return [{"id": article_type.id,
                 "name": article_type.name} for article_type in article_types]
    else:
        article_types = ArticleType.objects.filter(parent_id=category)
        return [{"id": article_type.id,
                 "name": article_type.name} for article_type in article_types]


@page
def get_articles(request):
    series_id = request.GET.get("series_id", "")
    category_id = request.GET.get("category_id", 1)
    producer_id = request.GET.get("producer_id")
    if producer_id:
        articles = Article.objects.filter(type=category_id, producer__id=producer_id).order_by("-created_at")
    if series_id:
        articles = Article.objects.filter(type=category_id, series__id=series_id).order_by("-created_at")

    if not producer_id and not series_id:
        articles = Article.objects.filter(type=category_id).order_by("-created_at")
    return [{"id": article.id,
             "create_type": article.created_type,
             "title": article.title,
             "style_type": article.article_style_type,
             "type": article.type.name,
             "created_at": timezone.localtime(article.created_at).strftime('%Y-%m-%d %H:%M:%S'),
             "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                    request.META["HTTP_HOST"],
                                    article.default_picture.path) if article.default_picture else ""
             } for article in articles]


@page
def search_articles(request):
    keywords = request.GET.get("keywords", None)
    articles = []
    if keywords:
        articles = Article.objects.filter(title__contains=keywords)
        return [{"id": article.id,
                 "create_type": article.created_type,
                 "title": article.title,
                 "style_type": article.article_style_type,
                 "created_at": timezone.localtime(article.created_at).strftime('%Y-%m-%d %H:%M:%S'),
                 "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                        request.META["HTTP_HOST"],
                                        article.default_picture.path) if article.default_picture else ""
                 } for article in articles]
    return articles


def get_article(request):
    article_id = request.GET.get("article_id", None)
    res = {}
    if article_id:
        article = Article.objects.get(id=article_id)
        res = {"id": article.id,
               "brand": article.brand.name if article.brand else "",
               "cart": article.series.name if article.series else "",
               "area": article.area.city if article.area else "",
               "producer": article.producer.username if article.producer else "",
               "title": article.title,
               "style_type": article.article_style_type,
               "reprint_addr": article.reprint_addr,
               "author": get_article_auth(article),
               "content": article.content.replace("/static", "%s://%s/static" % (request.META["wsgi.url_scheme"],
                                                                                 request.META["HTTP_HOST"])),
               "clicks": article.clicks,
               "created_at": timezone.localtime(article.created_at).strftime('%Y-%m-%d %H:%M:%S'),
               "created_type": article.created_type}
    return res


@page
def get_latest_articles(request):
    res = []
    try:
        series_id = request.GET.get("series_id", "")
        if series_id:
            articles = Article.objects.filter(is_new=True, series__id=series_id).order_by("-created_at")
        else:
            articles = Article.objects.filter(is_new=True).order_by("-created_at")
        res = [{"id": article.id,
                 "create_type": article.created_type,
                 "title": article.title,
                 "type": article.type.name,
                 "style_type": article.article_style_type,
                 "created_at": timezone.localtime(article.created_at).strftime('%Y-%m-%d %H:%M:%S'),
                 "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                        request.META["HTTP_HOST"],
                                        article.default_picture.path) if article.default_picture else ""
                 } for article in articles]
    except Exception as e:
        LOG.debug("get_latest_articles is error! error is %s" % e)
    return res


def get_anonymous_article_types(request):
    res = {}
    selected_types = []
    unselected_types = []
    try:
        article_types = list(ArticleType.objects.filter(parent__isnull=True))
        for a in article_types:
            ap = {}
            ap['id'] = a.id
            ap['name'] = a.name
            if a.id in DEFAULT_ARTICLE_TYPE:
                selected_types.append(ap)
            else:
                unselected_types.append(ap)
        res = {"selected": selected_types, "unselected": unselected_types}
    except Exception as e:
        LOG.error('Error happened when get_user_article_types. %s' % e)
    return res


@require_app_login
def get_user_article_types(request):
    res = {}
    selected_types = []
    unselected_types = []
    try:
        # user = Users.objects.get(pk=2)
        user = request.app_user
        article_types = list(ArticleType.objects.filter(parent__isnull=True))
        if user.article_types.all():
            for t in user.article_types.all():
                tp = {}
                tp['id'] = t.id
                tp['name'] = t.name
                selected_types.append(tp)
                if t in article_types:
                    article_types.remove(t)
            for u in article_types:
                up = {}
                up['id'] = u.id
                up['name'] = u.name
                unselected_types.append(up)
        else:
            for a in article_types:
                ap = {}
                ap['id'] = a.id
                ap['name'] = a.name
                unselected_types.append(ap)
        res = {"selected": selected_types, "unselected": unselected_types}
    except Exception as e:
        LOG.error('Error happened when get_user_article_types. %s' % e)
    return res


@require_app_login
def set_user_article_types(request):
    res = {}
    selected_types = []
    unselected_types = []
    try:
        user = request.app_user
        # user = Users.objects.get(pk=2)
        category_id = request.GET.get("category_id", '')
        all_types = list(ArticleType.objects.filter(parent__isnull=True))
        if category_id:
            special_ar = ArticleType.objects.filter(pk=category_id)
            if special_ar:
                user_articles = list(user.article_types.all())
                if special_ar[0] in user_articles:
                    user_articles.remove(special_ar[0])
                    user.article_types.clear()
                    for u in user_articles:
                        user.article_types.add(u)
                    user.save()
                else:
                    user.article_types.add(special_ar[0])
                    user.save()
        for u in user.article_types.all():
            up = {}
            up['id'] = u.id
            up['name'] = u.name
            if u in all_types:
                all_types.remove(u)
            selected_types.append(up)
        for s in all_types:
            sp = {}
            sp['id'] = s.id
            sp['name'] = s.name
            unselected_types.append(sp)
        res['selected'] = selected_types
        res['unselected'] = unselected_types
    except Exception as e:
        LOG.error('Error happened when set_user_article_types. %s' % e)
    return res


def get_article_auth(article):
    if article.admin:
        return article.admin.information.name if article.admin.information else ""
    if article.producer:
        return article.producer.information.name if article.producer.information else ""
    if article.auditor:
        return article.auditor.information.name if article.auditor.information else ""
