import simplejson
import logging
import time
import datetime
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q

from hibiscus.models import Ad, SupportedArea, Location

LOG = logging.getLogger(__name__)


def get_ads(request):
    type = request.GET.get("type", "app")
    area_id = int(request.GET.get("area_id", "1"))
    if type == "app":
        ads = Ad.objects.filter(area_id=area_id).filter(Q(location__is_app=True))
        return [{"id": ad.id,
                 "picture": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], ad.picture.path),
                 "name": ad.name,
                 "brief": ad.brief,
                 "link": ad.link,
                 "index": ad.location.index} for ad in ads]
    else:
        ads = Ad.objects.filter(area_id=area_id).filter(Q(location__is_app=False))
        return [{"id": ad.id,
                 "picture": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], ad.picture.path),
                 "name": ad.name,
                 "brief": ad.brief,
                 "link": ad.link,
                 "index": ad.location.index} for ad in ads]

