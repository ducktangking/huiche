# -*-coding:utf-8-*-
import logging
import datetime
import simplejson
from datetime import timedelta

from django.db.models import Q
from django.utils import timezone

from hibiscus.models import Adviser, CarInquiry, CarUpkeepInquiry, CustomerInfo, Users
from hibiscus.decorators import require_adviser_login, page

LOG = logging.getLogger(__name__)

TEXT_MAIL_1 = u"##customer####sex##，您好，感谢您关注##car##，欢迎您到店看车试驾，##shop##竭诚为您服务。电话：##adviser_phone## ##adviser_name##，地址：##address##"
REMIND_TIME = 3

def get_all_clues(request):
    res = []
    try:
        clues = CarInquiry.objects.filter(status=False).order_by('created_at')
        for c in clues:
            cp = {}
            cp['id'] = c.id
            cp['username'] = c.name
            cp['phone'] = c.phone
            cp['car_name'] = c.car.name
            cp['created_at'] = timezone.localtime(c.created_at).strftime('%Y-%m-%d %H:%M:%S')
            res.append(cp)
    except Exception as e:
        LOG.error('Error happened get_all_clues. %s' % e)
    return res


@page
@require_adviser_login
def get_adviser_clues(request):
    # 1:未联系    2:已联系    3:已建档    4:已战败
    res = []
    try:
        # info = simplejson.loads(request.body, encoding="utf-8")
        # sort_id = info.get("sort_id", None)
        sort_id = request.GET.get('sort_id', u'1')
        kind_id = request.GET.get('kind_id', u'1')
        adviser = request.app_user
        if kind_id == u'1':
            clue_obj = CarInquiry.objects.filter(Q(adviser__id=adviser.id), Q(is_token=True)).order_by('-created_at')
        if kind_id == u'2':
            clue_obj = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                       Q(is_token=True), Q(type_id=1)).order_by('-created_at')
        if kind_id == u'3':
            clue_obj = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                       Q(is_token=True), Q(type_id=2)).order_by('-created_at')
        if sort_id == u'1':
            clues = clue_obj.filter(status=False)
        if sort_id == u'2':
            clues = clue_obj.filter(status=True)
        if sort_id == u'3':
            clues = clue_obj.filter(content__isnull=False)
        if sort_id == u'4':
            clues = clue_obj.filter(is_defeat=True)
        if clues:
            for c in clues:
                cp = {}
                cp['id'] = c.id
                cp['username'] = c.customer.name
                cp['phone'] = c.customer.phone
                cp['sex'] = c.customer.sexual
                cp['car_name'] = c.customer.car.series.name + " " + c.customer.car.produce_year + u"款" + " " + c.customer.car.name
                cp['area'] = c.area.city
                cp['created_date'] = timezone.localtime(c.created_at).strftime('%Y-%m-%d')
                cp['created_time'] = timezone.localtime(c.created_at).strftime('%H:%M:%S')
                cp['kind_id'] = int(kind_id)
                res.append(cp)
    except Exception as e:
        LOG.error('Error happened when get_adviser_clues. %s' % e)
    return res


@require_adviser_login
def index_nums(request):
    res = {"car_clues_num": 0, "fix_clues_num": 0, "paint_clues_num": 0,  "insurance_num": 0}
    try:
        sort_id = request.GET.get('sort_id', 1)
        adviser = request.app_user
        if sort_id == u'1':
            car_clues = CarInquiry.objects.filter(adviser__id=adviser.id).filter(status=False).filter(is_token=True)
            fix_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                        Q(type_id=1)).filter(status=False).filter(is_token=True)
            paint_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                          Q(type_id=2)).filter(status=False).filter(is_token=True)
        if sort_id == u'2':
            car_clues = CarInquiry.objects.filter(adviser__id=adviser.id).filter(status=True).filter(is_token=True)
            fix_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                        Q(type_id=1)).filter(status=True).filter(is_token=True)
            paint_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                          Q(type_id=2)).filter(status=True).filter(is_token=True)
        if sort_id == u'3':
            car_clues = CarInquiry.objects.filter(adviser__id=adviser.id).filter(content__isnull=False).filter(is_token=True)
            fix_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                        Q(type_id=1)).filter(content__isnull=False).filter(is_token=True)
            paint_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                          Q(type_id=2)).filter(content__isnull=False).filter(is_token=True)
        if sort_id == u'4':
            car_clues = CarInquiry.objects.filter(adviser__id=adviser.id).filter(content__isnull=True).filter(is_token=True)
            fix_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                        Q(type_id=1)).filter(content__isnull=True).filter(is_token=True)
            paint_clues = CarUpkeepInquiry.objects.filter(Q(adviser__id=adviser.id),
                                                          Q(type_id=2)).filter(content__isnull=True).filter(is_token=True)
        res['car_clues_num'] = len(car_clues)
        res['fix_clues_num'] = len(fix_clues)
        res['paint_clues_num'] = len(paint_clues)
    except Exception as e:
        LOG.error('Error happened when get_each_clues_numbers. %s' % e)
    return res


def clue_detail(request):
    res = {"res": "fail"}
    try:
        #kind_id:购车/保养/做漆/保险
        clue_id = request.GET.get('clue_id', 1)
        kind_id = request.GET.get('kind_id', 1)
        # text_mail = Textmail.objects.filter(Q(usefor=kind_id),Q(status=True))
        if kind_id == u'1':
            car_clues = CarInquiry.objects.filter(pk=clue_id)
        if kind_id == u'2':
            car_clues = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if kind_id == u'3':
            car_clues = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if car_clues:
            res['res'] = 'success'
            res['name'] = car_clues[0].customer.name
            res['phone'] = car_clues[0].customer.phone
            res['area'] = car_clues[0].area.city
            res['resource'] = car_clues[0].resource
            res['sex'] = car_clues[0].customer.sexual
            res['car'] = car_clues[0].customer.car.series.name + " " + car_clues[0].customer.car.produce_year + u"款" + " " +car_clues[0].customer.car.name
    except Exception as e:
        LOG.error('Error happened when enter clue detail. %s' % e)
    return res


def complete_information(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            cus_name = info.get("name", None)
            sex = info.get('sex', 1)
            kind_id = info.get('kind_id', 1)
            clue_id = info.get('clue_id', 1)
            if cus_name and sex:
                if kind_id == 1:
                    customer_id = CarInquiry.objects.get(pk=clue_id).customer_id
                    clue = CustomerInfo.objects.filter(pk=customer_id).update(name=cus_name, sexual=sex)
                if kind_id == 2:
                    customer_id = CarInquiry.objects.get(pk=clue_id)
                    clue = CustomerInfo.objects.filter(pk=customer_id).update(name=cus_name, sexual=sex)
                if kind_id == 3:
                    customer_id = CarInquiry.objects.get(pk=clue_id)
                    clue = CustomerInfo.objects.filter(pk=customer_id).update(name=cus_name, sexual=sex)
                if clue:
                    res['res'] = 'success'
        except Exception as e:
            LOG.error('Error happened when complete customer information. %s' % e)
    return res


def remark_and_record(request):
    res = {'res': 'fail'}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            level = info.get("level", None)
            manner = info.get("manner", None)
            remark = info.get("remark", None)
            kind_id = info.get("kind_id", 1)
            clue_id = info.get("clue_id", 1)
            if level and manner:
                if level == u'Z':
                    is_defeat = True
                else:
                    is_defeat = False
                if level == u'O':
                    is_order = True
                else:
                    is_order = False
                if kind_id == 1:
                    clue = CarInquiry.objects.filter(pk=clue_id)
                if kind_id == 2:
                    clue = CarUpkeepInquiry.objects.filter(pk=clue_id)
                if kind_id == 3:
                    clue = CarUpkeepInquiry.objects.filter(pk=clue_id)
                if kind_id == 4:
                    return res
                if level == u'O':
                    clue.update(is_order=is_order)
                if not is_defeat:
                    if level == u'O':
                        clue.update(is_defeat=is_defeat, manner=manner,
                                    status=True, record_date=timezone.now())
                    else:
                        clue.update(is_defeat=is_defeat, level=level, manner=manner,
                                    status=True, record_date=timezone.now())
                    date = timezone.now().strftime('%Y-%m-%d')
                    old_content = clue[0].content
                    if not old_content:
                        if manner == 'P':
                            manner = u"电话"
                        elif manner == 'B':
                            manner = u"电话&短信"
                        else:
                            manner = u"短信"
                        content =date + " " + manner + " " + level + u"级 客户意见 " + remark
                        clue.update(content=content)
                    else:
                        if manner == 'P':
                            manner = u"电话"
                        elif manner == 'B':
                            manner = u"电话&短信"
                        else:
                            manner = u"短信"
                        content =old_content + '#%$#%$#%#' + date + " " + manner + " " + level + u"级 客户意见 " + remark
                        clue.update(content=content)
                else:
                    clue.update(is_defeat=is_defeat, status=True)
                res['res'] = "success"
        except Exception as e:
            LOG.error('Error happened when create customer record. %s' % e)
    return res


@page
def take_order(request):
    order_list = []
    new_order_list = []
    try:
        car_obj = CarInquiry.objects.filter(is_token=False).order_by('-created_at')
        fix_obj = CarUpkeepInquiry.objects.filter(Q(is_token=False), Q(type_id=1)).order_by('-created_at')
        paint_obj = CarUpkeepInquiry.objects.filter(Q(is_token=False), Q(type_id=2)).order_by('-created_at')
        if car_obj:
            for obj in car_obj:
                obj_params = {}
                obj_params['date'] = obj.created_at.strftime('%Y-%m-%d')
                obj_params['time'] = obj.created_at.strftime('%H:%M:%S')
                obj_params['order_time'] = obj.order_created_at.strftime('%Y%m%d%H%M%S') if obj.order_created_at else timezone.now().strftime('%Y%m%d%H%M%S')
                obj_params['name'] = obj.customer.name
                obj_params['sex'] = obj.customer.sexual
                obj_params['car'] = obj.customer.car.series.name + " " + obj.customer.car.name
                obj_params['area'] = obj.area.city
                obj_params['id'] = obj.id
                obj_params['kind_id'] = 1
                order_list.append(obj_params)
        if fix_obj:
            for obj in fix_obj:
                obj_params = {}
                obj_params['date'] = obj.created_at.strftime('%Y-%m-%d')
                obj_params['time'] = obj.created_at.strftime('%H:%M:%S')
                obj_params['order_time'] = obj.created_at.strftime('%Y%m%d%H%M%S') if obj.order_created_at else timezone.now().strftime('%Y%m%d%H%M%S')
                obj_params['name'] = obj.customer.name
                obj_params['sex'] = obj.customer.sexual
                obj_params['car'] = obj.customer.car.series.name + " " + obj.customer.car.name
                obj_params['area'] = obj.area.city
                obj_params['id'] = obj.id
                obj_params['kind_id'] = 2
                order_list.append(obj_params)
        if paint_obj:
            for obj in paint_obj:
                obj_params = {}
                obj_params['date'] = obj.created_at.strftime('%Y-%m-%d')
                obj_params['time'] = obj.created_at.strftime('%H:%M:%S')
                obj_params['order_time'] = obj.created_at.strftime('%Y%m%d%H%M%S') if obj.order_created_at else timezone.now().strftime('%Y%m%d%H%M%S')
                obj_params['name'] = obj.customer.name
                obj_params['sex'] = obj.customer.sexual
                obj_params['car'] = obj.customer.car.series.name + " " + obj.customer.car.name
                obj_params['area'] = obj.area.city
                obj_params['id'] = obj.id
                obj_params['kind_id'] = 3
                order_list.append(obj_params)
        time_list = []
        if order_list:
            for order in order_list:
                time_list.append(order['order_time'])
            time_list.sort(reverse=True)
            for time in time_list:
                for order in order_list:
                    if order in new_order_list:
                        continue
                    if time == order['order_time']:
                        new_order_list.append(order)
    except Exception as e:
        LOG.error('Error happened when create customer record. %s' % e)
    return new_order_list


@require_adviser_login
def is_order_token(request):
    res = {'res': 'lost'}
    try:
        clue_id = request.GET.get("clue_id", None)
        kind_id = request.GET.get("kind_id", None)
        adviser = request.app_user
        if kind_id == u'1':
            clue = CarInquiry.objects.filter(pk=clue_id)
        if kind_id == u'2':
            clue = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if kind_id == u'3':
            clue = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if not clue[0].is_token:
            clue.update(adviser_id=adviser,is_token=True)
            res['res'] = 'get'
    except Exception as e:
        LOG.error('Error happened when get the order. %s' % e)
    return res


# 任务详情
@page
@require_adviser_login
def mission_index(request):
    res = []
    try:
        level = request.GET.get('level', None)
        kind_id = request.GET.get('kind_id',None)
        adviser = request.app_user
        if kind_id == u"1":
            clue_obj = CarInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                                 Q(status=True), Q(is_token=True)).order_by('-created_at')
        if kind_id == u"2":
            clue_obj = CarUpkeepInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                                       Q(status=True), Q(is_token=True)).order_by('-created_at')
        if kind_id == u"3":
            clue_obj = CarUpkeepInquiry.objects.filter(Q(adviser=adviser), Q(status=True), Q(content__isnull=False),
                                                       Q(is_token=False)).order_by('-created_at')
        if level == 'O':
            clue = clue_obj.filter(is_order=True)
        elif level == 'Z':
            clue = clue_obj.filter(is_defeat=True)
        else:
            clue = clue_obj.filter(level=level)
        if clue:
            for cl in clue:
                cl_params = {}
                cl_params['id'] = cl.id
                cl_params['name'] = cl.customer.name
                cl_params['car'] = cl.customer.car.series.name + " " + cl.customer.car.produce_year + u"款" + cl.customer.car.name
                cl_params['sex'] = cl.customer.sexual
                cl_params['area'] = cl.area.city
                cl_params['created_date'] = cl.created_at.strftime("%Y-%m-%d")
                cl_params['created_time'] = cl.created_at.strftime('%H:%M:%S')
                cl_params['kind_id'] = int(kind_id)
                res.append(cl_params)
    except Exception as e:
        LOG.error('Error happened when mission index be loaded. %s' % e)
    return res


@require_adviser_login
def mission_index_nums(request):
        res = {"car_clues_num": 0, "fix_clues_num": 0, "paint_clues_num": 0,  "insurance_num": 0}
        try:
            level = request.GET.get('level', None)
            adviser = request.app_user
            car_obj = CarInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                                 Q(status=True), Q(is_token=True)).order_by('-created_at')
            fix_obj = CarUpkeepInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                                       Q(status=True), Q(is_token=True)).order_by('-created_at')
            paint_obj = CarUpkeepInquiry.objects.filter(Q(adviser=adviser), Q(status=True), Q(content__isnull=False),
                                                        Q(is_token=False)).order_by('-created_at')
            if level == 'O':
                car = car_obj.filter(is_order=True)
                fix = fix_obj.filter(is_order=True)
                paint = paint_obj.filter(is_order=True)
            elif level == 'Z':
                car = car_obj.filter(is_defeat=True)
                fix = fix_obj.filter(is_defeat=True)
                paint = paint_obj.filter(is_defeat=True)
            else:
                car = car_obj.filter(level=level)
                fix = fix_obj.filter(level=level)
                paint = paint_obj.filter(level=level)
            res['car_clues_num'] = len(car)
            res['fix_clues_num'] = len(fix)
            res['paint_clues_num'] = len(paint)
        except Exception as e:
            LOG.error('Error happened when mission num be gotten. %s' % e)
        return res


def clue_detail_append_content(request):
    res = {"res": "fail"}
    try:
        #kind_id:购车/保养/做漆/保险
        clue_id = request.GET.get('clue_id', 1)
        kind_id = request.GET.get('kind_id', 1)
        # text_mail = Textmail.objects.filter(Q(usefor=kind_id),Q(status=True))
        if kind_id == u'1':
            car_clues = CarInquiry.objects.filter(pk=clue_id)
        if kind_id == u'2':
            car_clues = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if kind_id == u'3':
            car_clues = CarUpkeepInquiry.objects.filter(pk=clue_id)
        if car_clues:
            res['res'] = 'success'
            res['name'] = car_clues[0].customer.name
            res['sex'] = car_clues[0].customer.sexual
            res['phone'] = car_clues[0].customer.phone
            res['area'] = car_clues[0].area.city
            res['resource'] = car_clues[0].resource
            res['car'] = car_clues[0].customer.car.series.name + " " + car_clues[0].customer.car.produce_year + u"款" + " " +car_clues[0].customer.car.name
            content = car_clues[0].content
            res['content'] = content.split('#%$#%$#%#')
    except Exception as e:
        LOG.error('Error happened when enter clue detail. %s' % e)
    return res


@require_adviser_login
def recommend_new_car(request):
    res = {'res': 'fail'}
    clue = None
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            name = info.get("name", None)
            car = info.get('car', 36000)
            sex = info.get('sex', 1)
            phone = info.get('phone', None)
#             类型
            kind_id = info.get('kind_id', None)
            adviser = request.app_user
            area = adviser.producer.area
            resource = u'内部'
            if kind_id == 1:
                customer = CustomerInfo.objects.create(name=name, sexual=sex, phone=phone, car_id=car)
                clue = CarInquiry.objects.create(area=area, adviser=adviser, created_at=timezone.now(), customer=customer, resource=resource)
            if kind_id == 2:
                tag = info.get('tag', None)
                buy_year = info.get('buy_year', None)
                customer = CustomerInfo.objects.create(name=name, sexual=sex, phone=phone, car_id=car, tag=tag, buy_year=buy_year)
                clue = CarUpkeepInquiry.objects.create(area=area, adviser=adviser, created_at=timezone.now(), customer=customer, type_id=1, resource=resource)
            if kind_id == 3:
                tag = info.get('tag', None)
                buy_year = info.get('buy_year', None)
                customer = CustomerInfo.objects.create(name=name, sexual=sex, phone=phone, car_id=car, tag=tag, buy_year=buy_year)
                clue = CarUpkeepInquiry.objects.create(area=area, adviser=adviser, created_at=timezone.now(), customer=customer, type_id=2,  resource=resource)
            if clue:
                res['res'] = 'success'
        except Exception as e:
            LOG.error('Error happened logging data of new customers. %s' % e)
    return res


@require_adviser_login
def adviser_detail(request):
    res = {'res': 'fail'}
    try:
        adviser = request.app_user
        res['producer'] = adviser.producer.username
        res['name'] = adviser.username
        res['phone'] = adviser.phone
        res['real_name'] = adviser.information.name
        res['res'] = 'success'
    except Exception as e:
        LOG.error('Error happened logging data of user. %s' % e)
    return res


@require_adviser_login
def text_mail(request):
    res = {'res': 'fail'}
    try:
        kind_id = request.GET.get('kind_id', None)
        clue_id = request.GET.get('clue_id', None)
        adviser = request.app_user
        if kind_id == u'1':
            clue_obj = CarInquiry.objects.get(pk=clue_id)
            mail = TEXT_MAIL_1
        if kind_id == u'2':
            clue_obj = CarUpkeepInquiry.objects.get(pk=clue_id)
            mail = TEXT_MAIL_1
        if kind_id == u'3':
            clue_obj = CarUpkeepInquiry.objects.get(pk=clue_id)
            mail = TEXT_MAIL_1
        if kind_id == u'4':
            mail = TEXT_MAIL_1
            return res
        custom = clue_obj.customer.name
        custom_sex = u'先生' if clue_obj.customer.sexual == 1 else u'女士'
        custom_car = clue_obj.customer.car.series.name + " " + clue_obj.customer.car.produce_year + u"款" + " " + clue_obj.customer.car.name
        adviser_shop = adviser.producer.information.name if adviser.producer.information else u'我店'
        adviser_name = adviser.information.name if adviser.information else adviser.username
        adviser_phone = adviser.phone
        if adviser.producer.information:
            if adviser.producer.information.addresses:
                adviser_add = adviser.producer.information.addresses.all()[0].address
        else:
            adviser_add = ''
        mail_text = mail.replace("##customer##", custom).replace("##sex##", custom_sex).replace("##car##", custom_car).replace("##shop##", adviser_shop).\
            replace("##adviser_phone##", adviser_phone).replace("##address##", adviser_add).replace("##adviser_name##", adviser_name)
        res['res'] = 'success'
        res['mail'] = mail_text
    except Exception as e:
        LOG.error('Error happened logging mail text. %s' % e)
    return res


@require_adviser_login
def customer_info_complete(request):
    res = {'res': 'fail'}
    try:
        info = simplejson.loads(request.body, encoding="utf-8")
        name = info.get("name", None)
        car = info.get('car', 36000)
        sex = info.get('sex', 1)
        kind_id = info.get('kind_id', 1)
        clue_id = info.get('clue_id', 1)
        phone = info.get('phone', '')
        tel = info.get('tel', '')
        if kind_id == 1:
            customer_id = CarInquiry.objects.get(pk=clue_id).customer_id
        if kind_id ==2 or kind_id == 3:
            customer_id = CarUpkeepInquiry.objects.get(pk=clue_id).customer_id
        if kind_id == 4:
            return res
        cus = CustomerInfo.objects.filter(pk=customer_id)
        new_info = cus.update(name=name, car_id=car, sexual=sex, phone=phone)
        if tel:
            if cus[0].user:
                Users.objects.filter(pk=cus[0].user.id).update(phone=tel)
            else:
                user = Users.objects.create(phone=tel)
                cus.update(user=user)
        if new_info:
            res['res'] = 'success'
    except Exception as e:
        LOG.error('Error happened when update customer information. %s' % e)
    return res


@require_adviser_login
def order_exist_remind(request):
    res = {'res': 'false'}
    try:
        adviser = request.app_user
        car_clue = CarInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                             Q(status=True), Q(is_token=True))
        upkeep_clue = CarUpkeepInquiry.objects.filter(Q(adviser=adviser), Q(content__isnull=False),
                                             Q(status=True), Q(is_token=True))
        if car_clue:
            for clue in car_clue:
                if clue.level == 'A':
                    level_days = 15
                if clue.level == 'B':
                    level_days = 30
                if clue.level == 'C':
                    level_days = 90
                if clue.level == 'H':
                    level_days = 7
                second_el = (timedelta(days=level_days)-(timezone.now() - clue.record_date) - timedelta(days=REMIND_TIME)).total_seconds()
                if second_el < 0:
                    res['res'] = 'true'
                break
        if upkeep_clue:
            for clue in upkeep_clue:
                if clue.level == 'A':
                    level_days = 15
                if clue.level == 'B':
                    level_days = 30
                if clue.level == 'C':
                    level_days = 90
                if clue.level == 'H':
                    level_days = 7
                second_el = (timedelta(days=level_days) - (timezone.now() - clue.record_date) - timedelta(days=REMIND_TIME)).total_seconds()
                if second_el < 0:
                    res['res'] = 'true'
                break
    except Exception as e:
        LOG.error('Error happened when remind if the emergency order exist. %s' % e)
    return res