# -*- coding:utf-8 -*-
import simplejson
import logging
import hashlib

from django.db.models import Q, Count
from django.utils import timezone
from hibiscus.decorators import page
from hibiscus.models import Producer, SeriesUpkeepQuote, UpkeepPackage, UpkeepSeries, \
    SeriesUpkeepInfo, Upkeep, CarQuote, UpkeepMaterialModel, ProducerCarSeries, Series, Car

from hibiscus.mobile.series.views import get_series_car_price_min_and_max

BRAND_LEVEL = {
    1: 'high',
    2: 'middle',
    3: 'low'
}
LOG = logging.getLogger(__name__)


@page
def get_producers_by_series(request):
    """
    producer_type: 4 4s, c compost, z direct store
    sort: price
    :param request:
    :return:
    """
    series_id = request.GET.get("series_id", None)
    area_id = request.GET.get("area_id", 1)
    type_id = request.GET.get("type", 0)
    car_id = request.GET.get("car_id", None)
    producer_type = request.GET.get("producer_type", 0)
    sort_type = request.GET.get("sort", "")
    producer_list = []
    #4s
    #zonghe
    #jiage
    if series_id and car_id:
        series = Series.objects.filter(pk=series_id)
        if producer_type and producer_type != 0:
            producers = Producer.objects.filter(Q(area_id=area_id), Q(is_active=True)). filter(type=producer_type).\
                filter(producercarseries__type=type_id).filter(producercarseries__brand__id=series[0].brand.id)
        else:
            producers = Producer.objects.filter(Q(area_id=area_id), Q(is_active=True)).\
                filter(producercarseries__type=type_id).filter(producercarseries__brand__id=series[0].brand.id)
        quote_obj = CarQuote.objects.filter(car__id=car_id)
        producer_ids = {}
        for q in quote_obj:
            producer_ids[q.producer.id] = q.sale_price
        for producer in producers:
            p = {}
            if producer.id in producer_ids:
                p['price'] = producer_ids[producer.id]
            else:
                car = Car.objects.filter(pk=car_id)
                p['price'] = car[0].price if car else 0
            p['id'] = producer.id
            p['area'] = producer.area.city
            p['name'] = producer.information.name if producer.information else "未设置"
            p['full_name'] = producer.full_name
            p['short_name'] = producer.short_name
            p['portrait'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                        request.META["HTTP_HOST"],
                                       producer.information.portrait.picture.path) if producer.information and producer.information.portrait else "",

            p['tel'] = producer.information.tel if producer.information else "未设置"
            p['address'] = []
            if producer.information:
                if producer.information.addresses:
                    p['address'] = [{"address": address.address,
                                    "is_default": address.is_default,
                                   "id": address.id} for address in producer.information.addresses.all()]
            p['markets'] = [{"id": adviser.id,
                          "name": adviser.information.name if adviser.information else "未设置",
                          "weight": adviser.weight} for adviser in
                         producer.adviser_set.all().filter(Q(type=type_id), Q(is_active=True)).order_by("-weight")]
            producer_list.append(p)
        if sort_type == "price":
            # res['cars'] = sorted(cars_list, key=lambda s: s['price']['price_max'], reverse=True)
            producer_list = sorted(producer_list, key=lambda s: s['price'])
        return producer_list


@page
def get_quote_producers_by_car(request):
    car_id = request.GET.get("car_id", None)
    area_id = request.GET.get("area_id", 1)
    type_id = request.GET.get("type", 0)
    producers = []
    if car_id:
        try:
            car_quotes = CarQuote.objects.filter(Q(producer__is_active=True),
                                                 Q(car_id=car_id),
                                                 Q(producer__area_id=area_id)).order_by("-weight")
            for quote in car_quotes:
                qp = {}
                qp['id'] = quote.producer.id
                qp['area'] = quote.producer.area.city
                qp['address'] = ""
                if quote.producer.information:
                    if quote.producer.information.addresses:
                        adr = quote.producer.information.addresses.all().filter(is_default=True)
                        if adr:
                            qp['address'] = adr[0].address
                        else:
                            adr_obj = quote.producer.information.addresses.all()
                            if adr_obj:
                                qp['address'] = adr_obj[0].address
                qp['name'] = quote.producer.information.name if quote.producer.information else "未设置"
                qp['tel'] = quote.producer.information.tel if quote.producer.information else "未设置"
                qp['portrait'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                     request.META["HTTP_HOST"],
                                                     quote.producer.information.portrait.picture.path) \
                                        if quote.producer.information and quote.producer.information.portrait else ""
                qp['full_name'] = quote.producer.full_name
                qp['short_name'] = quote.producer.short_name
                qp['producer_price'] = quote.price
                producers.append(qp)
        except Exception as e:
            LOG.debug("get_quote_producers_by_car failed! error is %s" % e)

    return producers


def get_banjin_price_by_package_and_producer(packages_id, producer_id, upobj):
    prices = []
    try:
        level = upobj.series.brand.level.name
        series_upkeep_quotes = SeriesUpkeepQuote.objects.filter(Q(producer_id=producer_id),Q(package_id__in=packages_id))
        if series_upkeep_quotes:
            prices = [{"package_id": series_upkeep_quote.package_id, "package_name":series_upkeep_quote.package.name, "price": getattr(series_upkeep_quote, "sale_price_%s" % level)}
                      for series_upkeep_quote in series_upkeep_quotes]
    except Exception as e:
        LOG.debug("get_banjin_price_by_package_and_producer failed!, error is %s" % e)
    return prices


def get_producers_by_upkeep(request):
    res = {}
    if request.body:
        info = simplejson.loads(request.body, encoding="utf-8")
        packages_id = info.get("packages_id", None)
        upkeep_series_id = info.get("upkeep_series_id", None)
        try:
            if packages_id and upkeep_series_id:
                producers_info = SeriesUpkeepQuote.objects.filter(package__id__in=packages_id). \
                    values("producer__id"). \
                    annotate(pcount=Count("package__id"))
                # producers_info = SeriesUpkeepQuote.objects.filter(package__id__in=packages_id). \
                #     filter(series_upkeep_info__upkeep_series__id=upkeep_series_id).values("producer__id").\
                #     annotate(pcount=Count("package__id"))
                res = {"packages_id": packages_id,
                       "upkeep_series_id": upkeep_series_id,
                       "producers": []}
                upobj = UpkeepSeries.objects.get(pk=upkeep_series_id)
                if producers_info:
                    for st in producers_info:
                        stp = {}
                        producer = Producer.objects.get(pk=st['producer__id'])
                        stp['id'] = producer.id
                        stp['prices'] = get_banjin_price_by_package_and_producer(packages_id, producer.id, upobj)
                        stp['name'] = producer.information.name if producer.information else ""
                        if producer.information:
                            if producer.information.addresses:
                                stp['address'] = [{"address": address.address,
                                                 "is_default": address.is_default,
                                                 "id": address.id} for address in producer.information.addresses.all()]
                        else:
                            stp['address'] = ""

                        if producer.information and producer.information.portrait:
                            stp['portrait'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"],
                                                             producer.information.portrait.picture.path)
                        else:
                            stp['portrait'] = ""
                        # stp['price'] = st['price']
                        res['producers'].append(stp)
        except Exception as e:
            LOG.debug("Error when get_producers_by_upkeep , error is %s" % e)
            res = {}
    return res


def get_quote_by_producer(request):
    res = {}
    if request.body:
        res['all_data'] = []
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            packages_id = info.get("packages_id", None)
            upkeep_series_id = info.get("upkeep_series_id", None)
            producer_id = info.get("producer_id", None)
            for pk_id in packages_id:
                pk_data = pkg_upkeep_quote_detail(producer_id, pk_id, upkeep_series_id, request)
                if pk_data:
                    res['all_data'].append(pk_data[0])
        except Exception as e:
            LOG.error('Can not get quote by producer. %s' % e)
            res = {}
    return res


def pkg_upkeep_quote_detail(producer_id, pkg_id, upkeep_series_id, request):
    try:
        upsobj = UpkeepSeries.objects.get(pk=upkeep_series_id)
        level = BRAND_LEVEL[upsobj.series.brand.level.id]
        all_data = []
        pkg = UpkeepPackage.objects.get(pk=pkg_id)
        if pkg.type.id == 1:
            pkg_params = {}
            pkg_params['upkeep_series_id'] = upkeep_series_id
            pkg_params['type'] = 1
            pkg_params['pkg_id'] = pkg.id
            pkg_params['pkg_name'] = pkg.name
            pkg_params['upkeep_data'] = []
            upkeeps = pkg.upkeep_set.all().values("id", "name", "labor_price_original", "labor_price_general",
                                                  ).annotate(pcount=Count(id))
            upmm = UpkeepMaterialModel.objects.filter(material__upkeep_id=pkg.upkeep_set.all()[0].id)
            for up in upkeeps:
                quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id). \
                    filter(series_upkeep_info__upkeep_series__id=upkeep_series_id).filter(package__id=pkg_id)
                if not quote:
                    continue
                up_data = {}
                up_data['upkeep_id'] = up['id']
                up_data['upkeep_name'] = up['name']
                up_data['price'] = up['labor_price_general']
                up_data['num'] = up['pcount']
                pkg_params['upkeep_data'].append(up_data)
                upobj = Upkeep.objects.get(pk=up['id'])
                materials = upobj.upkeepmaterial_set.all()
                if materials:
                    up_data['material_name'] = materials[0].brand
                else:
                    up_data['material_name'] = ""
                model_objs = UpkeepMaterialModel.objects.filter(material=materials[0])
                if model_objs:
                    up_data['model_id'] = model_objs[0].id
                    up_data['model_name'] = model_objs[0].name
                    up_data['model_code'] = model_objs[0].code
                    up_data['model_model'] = model_objs[0].model
                else:
                    up_data['model_id'] = ""
                    up_data['model_name'] = ""
                    up_data['model_code'] = ""
                    up_data['model_model'] = ""
                up_data['material_id'] = materials[0].id
                up_data['material_pic'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                         request.META["HTTP_HOST"],
                                                         materials[0].picture.path)

            all_data.append(pkg_params)
        else:
            pkg_p = {}
            pkg_p['pkg_id'] = pkg.id
            pkg_p['pkg_name'] = pkg.name
            pkg_p['upkeep_data'] = []
            upkeeps = pkg.upkeep_set.all()
            for up in upkeeps:
                up_params = {}
                up_params['upkeep_id'] = up.id
                up_params['upkeep_name'] = up.name
                quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id).filter(series_upkeep_info__upkeep=up)
                if quote:
                    up_params['labor'] = getattr(quote[0], "sale_price_%s" % level)
                else:
                    up_params['labor'] = ""
                pkg_p['upkeep_data'].append(up_params)
            all_data.append(pkg_p)
    except Exception as e:
        LOG.error('Can not get pkg upkeeps .%s' % e)
    return all_data


def get_banjin_projects_by_brand(request):
    res = {}
    res['all_data'] = []
    try:
        upseries_id = request.GET.get("upkeep_series_id", None)
        series_upkeep_info = SeriesUpkeepInfo.objects.filter(upkeep_series__id=upseries_id)
        for sui in series_upkeep_info:
            level = sui.upkeep_series.series.brand.level.name
            quotes = sui.seriesupkeepquote_set.all()
            if quotes:
                up = {}
                up['upkeep_name'] = sui.upkeep.name
                up['price'] = getattr(quotes[0], "sale_price_%s" % level)
                up['upkeep_id'] = sui.upkeep.id
                res['all_data'].append(up)
    except Exception as e:
        LOG.error('Can not get quote by producer. %s' % e)
        res = {}
    return res


def get_producer_detail(request):
    res = {}
    try:
        producer_id = request.GET.get("producer_id", "")
        producer_obj = Producer.objects.filter(pk=producer_id)
        if producer_obj:
            p = producer_obj[0]
            res['username'] = p.username
            res['phone'] = p.phone
            res['address'] = ""
            if p.information:
                if p.information.addresses:
                    a = p.information.addresses.all()
                    res['address'] = a[0].address
    except Exception as e:
        LOG.error('Error happened.get_producer_detail %s' % e)
    return res


def producer_hot_series(request):
    res = []
    try:
        series_list = []
        producer_id = request.GET.get("producer_id", "")
        producer = Producer.objects.filter(pk=producer_id)
        if producer:
            pobj = producer[0]
            brands = pobj.producercarseries_set.filter(type=0)
            for b in brands:
                series = Series.objects.filter(brand__id=b.brand.id, is_hot__id=3)
                for s in series:
                    sp = {}
                    sp['id'] = s.id
                    sp['name'] = s.name
                    sp['brand_name'] = s.brand.name
                    sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                request.META["HTTP_HOST"],
                                                s.logo.picture.path)
                    sp['created_at'] = timezone.localtime(s.created_at).strftime('%Y-%m-%d %H:%M:%S')
                    sp['price'] = get_series_car_price_min_and_max(s.id)
                    res.append(sp)
    except Exception as e:
        LOG.error('Error happened, %s' % e)
    return res

