import logging

from django.db.models import Q

from hibiscus.models import Adviser

LOG = logging.getLogger(__name__)


def get_adviser_by_producer(request):
    producer_id = request.GET.get("producer_id", None)
    type = request.GET.get("type", "0")
    res = []
    if producer_id:
        advisers = Adviser.objects.filter(producer_id=producer_id).filter(Q(type=type),Q(is_active=True)).order_by("-weight")
        if advisers:
            res = [{"id": adviser.id,
                    "name": adviser.information.name if adviser.information else '',
                    } for adviser in advisers]
    return res

