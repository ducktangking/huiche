import logging

from django.utils import timezone
from hibiscus.decorators import require_app_login

LOG = logging.getLogger(__name__)


@require_app_login
def get_user_by_id(request):
    res = {}
    app_token = request.app_token
    app_user = request.app_user
    if app_token and app_user:
        try:
            res = {"username": app_user.username,
                   "phone": app_user.phone,
                   "name": app_user.information.name if app_user.information else "",
                   "email": app_user.information.email if app_user.information else "",
                   "sex": app_user.information.sex if app_user.information else "",
                   "birthday": timezone.localtime(app_user.information.birthday).strftime('%Y-%m-%d') if app_user.information and app_user.information.birthday else "",
                   "tel": app_user.information.tel if app_user.information else "",
                   "qq": app_user.information.qq if app_user.information else "",
                   "weibo": app_user.information.weibo if app_user.information else "",
                   "weixin":app_user.information.weixin if app_user.information else "",
                   "portrait": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                               request.META["HTTP_HOST"],
                                               app_user.information.portrait.picture.path
                                               ) if app_user.information and app_user.information.portrait else "",
                   "addresses": [{"address":address.address, "is_default": address.is_default}
                                  for address in app_user.information.addresses.all() if app_user.information]}
        except Exception as e:
            print e
    return res