# -*- coding:utf-8 -*-
import simplejson
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.http import require_GET, require_POST

from hibiscus.mobile.area.views import get_city, get_province
from hibiscus.mobile.brand.views import get_hot_brands, get_series_brands
from hibiscus.mobile.ad.adetail.views import get_ads
from hibiscus.mobile.article.views import get_article_type, get_articles, get_article, search_articles, \
    get_latest_articles, get_user_article_types, set_user_article_types, get_anonymous_article_types
from hibiscus.mobile.user.producer.views import get_producers_by_series, get_producers_by_upkeep, \
    get_quote_by_producer, get_banjin_projects_by_brand, get_quote_producers_by_car, get_producer_detail, \
    producer_hot_series
from hibiscus.mobile.brand.views import get_second_brands, get_top_brands, get_brands, \
    get_country_brands, get_brands_by_level, get_brand_details, get_second_brands_and_series
from hibiscus.mobile.series.views import search, get_series_by_brand_id, get_upkeep_series_by_series_id, \
    get_hot_series, recommend_series, set_user_fav_series, get_user_fav_series
from hibiscus.mobile.user.adviser.views import get_adviser_by_producer
from hibiscus.mobile.user.users.views import get_user_by_id
from hibiscus.mobile.upkeep.package.views import get_packages_by_type
from hibiscus.mobile.upkeep.upkeep.views import get_upkeep_by_package, new_upkeep_orders
from hibiscus.mobile.upkeep.upkeepseries.views import get_banjin_upkeep_producer
from hibiscus.mobile.group.views import get_group_index, get_groups, user_add, get_group_articles, get_hot_series_groups, \
    get_hot_brand_groups
from hibiscus.mobile.auth.views import login as app_login, register as app_register, \
    logout as app_logout, reset_password as app_reset_password, feedback as app_feedback, adviser_register, \
    adviser_login
from hibiscus.mobile.upkeep.material.views import get_upkeep_material, get_material_models
from hibiscus.mobile.picture.views import new as picture_new
from hibiscus.mobile.upkeep.material.views import get_upkeep_material
from hibiscus.mobile.car.views import car_inquiry, cars_by_price, cars_by_grade, get_all_car_pic, get_car_special_pic, \
    orders, get_colors, set_user_fav_car, get_user_fav_cars
from hibiscus.mobile.clue.views import get_all_clues, get_adviser_clues, index_nums, clue_detail, complete_information,\
    remark_and_record, take_order, is_order_token, mission_index, clue_detail_append_content, recommend_new_car,\
    adviser_detail, text_mail, mission_index_nums, customer_info_complete, order_exist_remind


@require_GET
def index(request):
    if request.user.is_anonymous():
        return HttpResponseRedirect(reverse("mobile-login"))
    else:
        return render(request, "mobile/index.html")


@require_GET
def set_csrf(request):
    return render(request, "mobile/csrf.html")


@require_GET
def cars(request):
    res = get_hot_series(request)
    res["brand_hot"] = get_hot_brands(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def ads(request):
    res = get_ads(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def article_types(request):
    res = get_article_type(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def articles(request):
    res = get_articles(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def articles_search(request):
    res = search_articles(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def article(request):
    res = get_article(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def articles_latest(request):
    res = get_latest_articles(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def brands(request):
    res = get_brands(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def top_brands(request):
    res = get_top_brands(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def second_brands(request):
    res = get_second_brands(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def second_brands_series(request):
    res = get_second_brands_and_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def brand(request):
    res = get_brand_details(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def brand_series(request):
    res = get_series_by_brand_id(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def compost_brands(request):
    res = get_country_brands(request)
    # res.extend(get_brands_by_level(request))
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def producers(request):
    res = get_producers_by_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def car_quote_producers(request):
    res = get_quote_producers_by_car(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def producer_advisers(request):
    res = get_adviser_by_producer(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def packages(request):
    res = get_packages_by_type(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def items(request):
    res = get_upkeep_by_package(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def group_index(request):
    res = get_group_index(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def groups(request):
    res = get_groups(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def register(request):
    res = app_register(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def login(request):
    res = app_login(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def logout(request):
    res = app_logout(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def group_user_add(request):
    res = user_add(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def reset_password(request):
    res = app_reset_password(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def feedback(request):
    res = app_feedback(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def user(request):
    res = get_user_by_id(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


@require_GET
def series_search(request):
    res = search(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_series(request):
    res = get_upkeep_series_by_series_id(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_producers(request):
    res = get_producers_by_upkeep(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def pic_upload(request):
    res = picture_new(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_proj_weixiu_price(request):
    res = get_quote_by_producer(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_material(request):
    res = get_upkeep_material(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_banjin_projects(request):
    res = get_banjin_projects_by_brand(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def upkeep_banjin_producers(request):
    res = get_banjin_upkeep_producer(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def material_models(request):
    res = get_material_models(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def deal_car_inquiry(request):
    res = car_inquiry(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def create_upkeep_orders(request):
    res = new_upkeep_orders(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_search_by_price(request):
    res = cars_by_price(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_search_by_grade(request):
    res = cars_by_grade(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_recommend_series(request):
    res = recommend_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def user_article_types(request):
    res = get_user_article_types(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def set_article_types(request):
    res = set_user_article_types(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def anonymous_article_types(request):
    res = get_anonymous_article_types(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def set_fav_series(request):
    res = set_user_fav_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def user_fav_series(request):
    res = get_user_fav_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_pics(request):
    res = get_all_car_pic(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_special_pic(request):
    res = get_car_special_pic(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def producer_detail(request):
    res = get_producer_detail(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_orders(request):
    res = orders(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_producer_hot_series(request):
    res = producer_hot_series(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def all_clues(request):
    res = get_all_clues(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def adviser_clues(request):
    res = get_adviser_clues(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def register_for_adviser(request):
    res = adviser_register(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def login_for_adviser(request):
    res = adviser_login(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def series_brands(request):
    res = get_series_brands(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def set_user_fav_cars(request):
    res = set_user_fav_car(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def user_fav_cars(request):
    res = get_user_fav_cars(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def group_articles(request):
    res = get_group_articles(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def hot_series_groups(request):
    res = get_hot_series_groups(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def hot_brand_groups(request):
    res = get_hot_brand_groups(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def clue_index_nums(request):
    res = index_nums(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def detail_for_index_clues(request):
    res = clue_detail(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def clue_complete_information(request):
    res = complete_information(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def create_record(request):
    res = remark_and_record(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def order_by_take(request):
    res = take_order(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_the_order(request):
    res = is_order_token(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def show_all_mission(request):
    res = mission_index(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def detail_append_content(request):
    res = clue_detail_append_content(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def recommend_new_customer(request):
    res = recommend_new_car(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def car_colors(request):
    res = get_colors(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def supported_province(request):
    res = get_province(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def supported_city(request):
    res = get_city(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def adviser_user_detail(request):
    res = adviser_detail(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def adviser_text_mail(request):
    res = text_mail(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def mission_top_nums(request):
    res = mission_index_nums(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def customer_information_detail(request):
    res = customer_info_complete(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def order_remind(request):
    res = order_exist_remind(request)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")
