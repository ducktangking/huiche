from django.conf.urls import url

from hibiscus.mobile.car import views

urlpatterns = [
    url(r'^search$', views.search, name="mobile-car-data"),
]