import logging

import simplejson
from django.db.models import Q, ObjectDoesNotExist
from django.http import HttpResponse
from hibiscus.decorators import user_authorized, require_app_login, login_info, page
from hibiscus.models import Car, CarInquiry, Adviser, Users, SupportedArea, Producer, Series, CarQuote, CarPicture, \
    CarTypePicture, UpkeepOrders, Brand, CarTypeColor

from hibiscus.mobile.series.views import get_series_car_cc, get_series_car_pic_num

LOG = logging.getLogger(__name__)


@login_info
def search(request):
    series_id = request.GET.get("series_id", "")
    start = int(request.GET.get("start", "0"))
    end = int(request.GET.get("end", "10"))
    res = {}
    if series_id != "":
        user = getattr(request, "app_user", None)
        fav_cars = []
        if user:
            fav_cars = list(user.fav_cars.all())
        cars = Car.objects.filter(series__id=series_id).all()[start:end]
        res = {
            "series_id": series_id,
            "num": len(cars),
            "start": start,
            "end": end,
            "default_pic": get_series_default_pic(series_id, request),
            "data": [{"name": car.name,
                      "full_name": car.series.brand.name + ' ' + car.series.name + ' ' + car.name,
                      "id": car.id,
                      "produce_year": car.produce_year,
                      "gearbox": car.gearbox.name,
                      "price": car.price,
                      "price_max": car.price_max,
                      "price_min": car.price_min,
                      "producer_price": get_car_quote_price(car),
                      "cc": car.real_cc,
                      "power": car.power,
                      "drive": car.drive.name,
                      "grade": car.grade.name,
                      "stall": car.stall.name,
                      "is_fav": True if car in fav_cars else False,
                      "pics": [{"name": pic.name,
                                "path":  "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                        request.META["HTTP_HOST"],
                                                        pic.path)
                                } for pic in car.pictures.all()]} for car in cars]
        }

    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_series_default_pic(series_id, request):
    pic = ""
    try:
        cars = Car.objects.filter(series__id=series_id)
        for c in cars:
            pics = c.carpicture_set.filter(picture__name__contains='45')
            if pics:
                pic = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                request.META["HTTP_HOST"],
                                pics[0].picture.path)
                break
    except Exception as e:
        LOG.error('Error happened get_series_default_pic. %s' % e)
    return pic


def get_car_quote_price(car):
    producer_price = {"min_price": 0, "max_price": 0}
    quote_cars = CarQuote.objects.filter(car=car)
    if quote_cars:
        quotes_list = [quote.sale_price for quote in quote_cars]
        producer_price['min_price'] = min(quotes_list)
        producer_price['max_price'] = max(quotes_list)
    return producer_price


def get_hot_cars(request):
    sts = Car.objects.filter(is_hot=True)
    return [{"id": st.id,
             "name": st.name,
             "icon": st.logo.picture.path} for st in sts]


def car_inquiry(request):
    res = {}
    res['status'] = "success"
    try:
        if request.body:
            info = simplejson.loads(request.body, encoding="utf-8")
            adviser_id = info.get('adviser_id', "")
            res1, user = user_authorized(request)
            username = info.get('username', "")
            phone = info.get('phone', "")
            car_id = info.get('car_id', "")
            content = info.get('content', "")
            area_id = info.get('area_id', "")
            producer_id = info.get('producer_id', [])
            car = Car.objects.filter(pk=car_id)
            if car:
                car = car[0]
                carci = CarInquiry.objects.create(car=car,  name=username, phone=phone, content=content)
                if adviser_id:
                    adviser_obj = Adviser.objects.filter(pk=adviser_id)
                    if adviser_obj:
                        carci.adviser = adviser_obj[0]
                        carci.save()
                if res1:
                    carci.user = user
                    carci.save()
                if area_id:
                    arobj = SupportedArea.objects.filter(pk=area_id)
                    if arobj:
                        carci.area = arobj[0]
                        carci.save()
                if producer_id:
                    producer_objs = Producer.objects.filter(pk__in=producer_id)
                    if producer_objs:
                        for po in producer_objs:
                            carci.producer.add(po)
                        carci.save()
            else:
                res['reason'] = 'No car found.'
    except ObjectDoesNotExist:
        res['status'] = "fail"
        res['reason'] = "No object found."

    except Exception as e:
        res['status'] = "fail"
        res['reason'] = "Error happened"
        LOG.error('Can not submit car inquiry. %s' % e)
    return res


@page
def cars_by_price(request):
    res = {"cars": []}
    cars_list = []
    try:
        #jiaoche, SUV, MPV
        price_list = request.GET.get("price", [])
        sort_order = request.GET.get('sort', 0)
        price_list = price_list.split(",")
        min_price = int(price_list[0])
        max_price = int(price_list[1])
        car_quotes = CarQuote.objects.filter(price__range=(min_price, max_price))
        series_list = {}
        for cq in car_quotes:
            series = cq.car.series
            if series in series_list:
                series_list[series].append(cq.sale_price)
            else:
                series_list[series] = [cq.sale_price]
        for key, value in series_list.items():
            sp = {}
            sp['producer_price'] = {"producer_min_price": 0, "producer_max_price": 0}
            if value:
                sp['producer_price']['producer_min_price'] = min(value)
                sp['producer_price']['producer_max_price'] = max(value)

                sp['price'] = get_series_car_price_min_and_max(key.id)
                sp['id'] = key.id
                sp['name'] = key.name
                sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                            request.META["HTTP_HOST"],
                                            key.logo.picture.path)
                sp["cc"] = get_series_car_cc(key.id)
                sp['pic_num'] = get_series_car_pic_num(key.id)
                sp['country'] = key.brand.country.name if key.brand.country else ""
                cars_list.append(sp)
        #jiaoche 2
        # MPV 7
        # SUV 8
        if int(sort_order) != 0:
            for c in cars_list:
                cars = Car.objects.filter(series__id=c['id'], grade__id=int(sort_order))
                if cars:
                    res['cars'].append(c)

        if int(sort_order) == 0:
            res['cars'] = cars_list
    except Exception as e:
        LOG.error('Can not get cars by price. cars_by_price %s' % e)
    return res['cars']


def cars_by_grade(request):
    res = {"cars": []}
    cars_list = []
    grade = request.GET.get("grade", 2)
    try:
        sort_order = request.GET.get('sort', 0)
        car_quotes = CarQuote.objects.filter(car__grade__id=grade)
        series_list = {}
        for cq in car_quotes:
            series = cq.car.series
            if series in series_list:
                series_list[series].append(cq.sale_price)
            else:
                series_list[series] = [cq.sale_price]
        for key, value in series_list.items():
            sp = {}
            sp['producer_price'] = {"producer_min_price": 0, "producer_max_price": 0}
            if value:
                sp['producer_price']['producer_min_price'] = min(value)
                sp['producer_price']['producer_max_price'] = max(value)
                sp['price'] = get_series_car_price_min_and_max(key.id)
                sp['id'] = key.id
                sp['name'] = key.name
                sp['brand_id'] = key.brand.id
                sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                            request.META["HTTP_HOST"],
                                            key.logo.picture.path)
                sp["cc"] = get_series_car_cc(key.id)
                sp['pic_num'] = get_series_car_pic_num(key.id)
                sp['country'] = key.brand.country.name if key.brand.country else ""
                cars_list.append(sp)
                # Descending
        #1 zizhu
        #2 hezi
        #3 jinkou
        if int(sort_order) != 0:
            for c in cars_list:
                brand_id = c['brand_id']
                brand = Brand.objects.filter(pk=brand_id, type__id=int(sort_order))
                if brand:
                    res['cars'].append(c)
        if int(sort_order) == 0:
            res['cars'] = cars_list

    except Exception as e:
        LOG.error('Can not get cars by price. cars_by_grade %s' % e)
    return res


def get_series_car_quote_min_and_max(series_id, min_price, max_price, sort_order):
    data = {"producer_min_price": 0, "producer_max_price": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        quotes = CarQuote.objects.filter(car__in=list(cars)).filter(price__range=(min_price, max_price))
        if quotes:
            quotes_list = [quote.sale_price for quote in quotes]
            data['producer_min_price'] = min(quotes_list) if quotes_list else 0
            data['producer_max_price'] = max(quotes_list) if quotes_list else 0
    except Exception as e:
        LOG.error('get_series_car_quote_min_and_max. %s' % e)
    return data


def get_series_car_quote_by_grade(series_id, grade):
    data = {"producer_min_price": 0, "producer_max_price": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        quotes = CarQuote.objects.filter(car__in=list(cars)).filter(car__grade__id=grade)
        if quotes:
            quotes_list = [quote.price for quote in quotes]
            data['producer_min_price'] = min(quotes_list)
            data['producer_max_price'] = max(quotes_list)
    except Exception as e:
        LOG.error('Error happened when get_series_car_quote_by_grade. %s' % e)
    return data


def get_all_car_pic(request):
    res = []
    try:
        series_id = request.GET.get('series_id', "")
        car_id = request.GET.get('car_id', "")
        color_id = request.GET.get('color_id', "")
        car_type_pics = CarTypePicture.objects.all()
        for c in car_type_pics:
            cp = {}
            cp['name'] = c.name
            cp['id'] = c.id
            cp['pictures'] = []

            if car_id and color_id:
                pics = CarPicture.objects.filter(type__id=c.id).filter(car__series__id=series_id).filter(color__id=color_id).filter(car__id=car_id).order_by('-id')[:6]
            elif color_id:
                pics = CarPicture.objects.filter(type__id=c.id).filter(car__series__id=series_id).filter(color__id=color_id).order_by('-id')[:6]
            elif car_id:
                pics = CarPicture.objects.filter(type__id=c.id).filter(car__series__id=series_id).filter(car__id=car_id).order_by('-id')[:6]
            else:
                pics = CarPicture.objects.filter(type__id=c.id).filter(car__series__id=series_id).order_by('-id')[:6]

            for p in pics:
                pp = {}
                pp['url'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                            request.META["HTTP_HOST"],
                                           p.picture.path)
                cp['pictures'].append(pp)
            res.append(cp)
    except Exception as e:
        LOG.error('Error happened when get_all_car_pic. %s' % e)
    return res


def get_car_special_pic(request):
    res = {"type_id": "", "pictures": []}
    try:
        type_id = request.GET.get("type_id", "")
        series_id = request.GET.get("series_id", "")
        car_id = request.GET.get('car_id', "")
        color_id = request.GET.get('color_id', "")
        if type_id:
            if car_id and color_id:
                pics = CarPicture.objects.filter(type__id=type_id).filter(car__series__id=series_id).filter(
                    car__id=car_id).filter(color__id=color_id)
            elif car_id:
                pics = CarPicture.objects.filter(type__id=type_id).filter(car__series__id=series_id).filter(
                    car__id=car_id)
            elif color_id:
                pics = CarPicture.objects.filter(type__id=type_id).filter(car__series__id=series_id).filter(
                    color__id=car_id)
            else:
                pics = CarPicture.objects.filter(type__id=type_id).filter(car__series__id=series_id)
            # pics = CarPicture.objects.filter(type__id=type_id, car__series__id=series_id)
            res['type_id'] = type_id
            for p in pics:
                pp = {}
                pp['url'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                           request.META["HTTP_HOST"],
                                           p.picture.path)
                res['pictures'].append(pp)
    except Exception as e:
        LOG.error('Error happened get_car_special_pic. %s' % e)
    return res


def get_colors(request):
    res = []
    try:
        colors = CarTypeColor.objects.all()
        for c in colors:
            cp = {}
            cp['id'] = c.id
            cp['name'] = c.name
            cp['rgb'] = c.rgb
            res.append(cp)
    except Exception as e:
        LOG.error('Error happened. get_colors. %s' % e)
    return res


@require_app_login
def orders(request):
    res = {"xunjia": [], "yangche": []}
    try:
        name = request.GET.get("name", "")
        user = request.app_user
        if name == "xunjia":
            xunjia_orders = CarInquiry.objects.filter(user=user)
            for x in xunjia_orders:
                xp = {}
                xp['car'] = x.car.name
                xp['series_name'] = x.car.series.name
                xp['brand_name'] = x.car.series.brand.name
                xp['content'] = x.content
                xp['status'] = x.status
                xp['username'] = x.name
                xp['phone'] = x.phone
                res['xunjia'].append(xp)
        if name == "yangche":
            yangche_orders = UpkeepOrders.objects.filter(user=user)
            for y in yangche_orders:
                yp = {}
                yp['username'] = y.name
                yp['phone'] = y.phone
                yp['in_time'] = y.in_time
                yp['out_time'] = y.out_time
                yp['status'] = y.status
                res['yangche'].append(yp)
    except Exception as e:
        LOG.error('Error happened orders. %s' % e)
    return res


@require_app_login
def set_user_fav_car(request):
    res = {"res": "fail"}
    try:
        car_id = request.GET.get("car_id", "")
        if car_id:
            car_obj = Car.objects.filter(pk=car_id)
            if car_obj:
                # user = Users.objects.get(pk=2)
                user = request.app_user
                own_cars = list(user.fav_cars.all())
                if car_obj[0] in own_cars:
                    own_cars.remove(car_obj[0])
                else:
                    own_cars.append(car_obj[0])
                for s in own_cars:
                    user.fav_cars.add(s)
                user.save()
                res = {"res": "success"}
    except Exception as e:
        res = "fail"
        print e
    return res


@require_app_login
def get_user_fav_cars(request):
    res = []
    try:
        user = request.app_user
        cars = user.fav_cars.all()
        for c in cars:
            cp = {}
            cp['series_id'] = c.series.id
            cp['name'] = c.name
            cp['id'] = c.id
            cp['produce_year'] = c.produce_year
            cp['gearbox'] = c.gearbox.name
            cp['price'] = c.price
            cp['price_max'] = c.price_max
            cp['price_min'] = c.price_min
            cp['producer_price'] = get_car_quote_price(c)
            cp['cc'] = c.real_cc
            cp['power'] = c.power
            cp['drive'] = c.drive.name
            cp['grade'] = c.drive.name
            cp['stall'] = c.stall.name
            cp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                        request.META["HTTP_HOST"],
                                        c.series.logo.picture.path)
            cp['pics'] = [{"name": pic.name, "path": pic.path} for pic in c.pictures.all()]
            res.append(cp)
    except Exception as e:
        LOG.error('Error happened. get_user_fav_cars %s' % e)
    return res


def get_series_car_price_min_and_max(series_id):
    data = {"price_min": 0, "price_max": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        price = [c.price for c in cars]
        if price:
            data['price_min'] = min(price)
            data['price_max'] = max(price)
    except Exception as e:
        LOG.error('get_series_car_quote_min_and_max. %s' % e)
    return data