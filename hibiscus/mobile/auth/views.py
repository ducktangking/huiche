# -*- coding:utf-8 -*-
from django.shortcuts import render

# Create your views here.

import simplejson
import base64
import uuid
import hashlib
import logging
from datetime import timedelta

from hibiscus.models import Users, UserToken, UserInformation, FeedBack, ArticleType, Adviser, AdviserToken
from django.db.models import Q
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail

from hibiscus.decorators import require_app_login
from hibiscus.utils import make_password
from hibiscus.background.public import pwd_hash
LOG = logging.getLogger(__name__)

LOGIN_TIMEOUT_DAYS = 15
DEFAULT_ARTICLE_TYPE = [1, 2, 3]


def login(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            username = info.get("username", None)
            password = info.get("password", None)
            if username and password:
                username = base64.decodestring(username)
                password = base64.decodestring(password)
                pwd = hashlib.sha256(password).hexdigest()
                user = Users.objects.filter(Q(username=username))
                if user:
                    user = Users.objects.filter(Q(username=username), Q(password=pwd))
                    if user:
                        user_token = UserToken.objects.filter(Q(user=user[0]), Q(status=True))
                        if user_token:
                            user_token[0].timeout_at = timezone.now()
                            user_token[0].status = False
                            user_token[0].save()
                        user_new_token = UserToken.objects.create(user=user[0], token=uuid.uuid1().hex,
                                timeout_at=timezone.now()+timedelta(days=LOGIN_TIMEOUT_DAYS))
                        user_new_token.save()
                        res["token"] = user_new_token.token
                        res["res"] = "success"
                    else:
                        res["description"] = "密码错误"
                else:
                    res["description"] = "用户不存在"
            else:
                res["description"] = "参数不全或格式错误"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when app login, error is %s" % e)
    return res


def logout(request):
    try:
        token = request.META.get(settings.HUICHE_X_TOKEN, None)
        if token:
            user_token = UserToken.objects.filter(token=token)
            if user_token:
                user_token[0].status = False
                user_token[0].save()
    except Exception as e:
        LOG.debug("Error when app logout, error is %s" % e)

    return {"res": "success"}


def register(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            username = info.get("username", None)
            password = info.get("password", None)
            email = info.get("email", None)
            if username and password and email:
                username = base64.decodestring(username)
                password = base64.decodestring(password)
                pwd = hashlib.sha256(password).hexdigest()
                user = Users.objects.filter(Q(username=username))
                if not user:
                    email = base64.decodestring(email)
                    emails = UserInformation.objects.filter(email=email)
                    if not emails:
                        user_info = UserInformation(email=email)
                        user_info.save()
                        user = Users(username=username, password=pwd, information=user_info)
                        user.save()
                        # Add default article types
                        article_types = ArticleType.objects.filter(pk__in=DEFAULT_ARTICLE_TYPE)
                        for a in article_types:
                            user.article_types.add(a)
                        user.save()
                        res["res"] = "success"
                    else:
                        res["description"] = "邮箱已注册过"
                else:
                    res["description"] = "用户已存在"
            else:
                res["description"] = "参数不全或格式错误"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when app login, error is %s" % e)
    return res


def reset_password(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            username = info.get("username", None)
            email = info.get("email", None)
            if username and email:
                username = base64.decodestring(username)
                email = base64.decodestring(email)
                user = Users.objects.filter(Q(username=username), Q(information__email=email))
                if user:
                    password = make_password()
                    user[0].password = hashlib.sha256(password).hexdigest()
                    user[0].save()
                    send_mail(
                        '慧车客服中心',
                        '您的新密码是：%s' % password,
                        settings.EMAIL_HOST_USER,
                        [user[0].information.email],
                        fail_silently=True,
                    )
                    res["res"] = "success"
                else:
                    res["description"] = "用户和邮箱不匹配"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when find password, error is %s" % e)
    return res


@require_app_login
def feedback(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            content = info.get("content", None)
            if content:
                user = request.app_user
                if user:
                    feed_back = FeedBack.objects.create(user=user, content=content)
                    feed_back.save()
                    res["res"] = "success"
                else:
                    res["description"] = "用户异常"
            else:
                res["description"] = "请填写内容"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when find password, error is %s" % e)
    return res


def adviser_login(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            username = info.get("username", None)
            password = info.get("password", None)
            if username and password:
                username = base64.decodestring(username)
                password = base64.decodestring(password)
                pwd = pwd_hash(password)
                user = Adviser.objects.filter(Q(username=username))
                if user:
                    user = Adviser.objects.filter(Q(username=username), Q(password=pwd))
                    if user:
                        user_token = AdviserToken.objects.filter(Q(user=user[0]), Q(status=True))
                        if user_token:
                            user_token[0].timeout_at = timezone.now()
                            user_token[0].status = False
                            user_token[0].save()
                        user_new_token = AdviserToken.objects.create(user=user[0], token=uuid.uuid1().hex,
                                                                  timeout_at=timezone.now() + timedelta(
                                                                      days=LOGIN_TIMEOUT_DAYS))
                        user_new_token.save()
                        res["token"] = user_new_token.token
                        res["res"] = "success"
                    else:
                        res["description"] = "密码错误"
                else:
                    res["description"] = "用户不存在"
            else:
                res["description"] = "参数不全或格式错误"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when app login, error is %s" % e)
    return res


def adviser_register(request):
    res = {"res": "fail"}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            username = info.get("username", None)
            password = info.get("password", None)
            email = info.get("email", None)
            if username and password and email:
                username = base64.decodestring(username)
                password = base64.decodestring(password)
                pwd = pwd_hash(password)
                user = Adviser.objects.filter(Q(username=username))
                if not user:
                    email = base64.decodestring(email)
                    emails = Adviser.objects.filter(email=email)
                    if not emails:
                        user_info = Adviser(email=email)
                        user_info.save()
                        user = Adviser(username=username, password=pwd, information=user_info)
                        user.save()
                        res["res"] = "success"
                    else:
                        res["description"] = "邮箱已注册过"
                else:
                    res["description"] = "用户已存在"
            else:
                res["description"] = "参数不全或格式错误"
        except Exception as e:
            res = {"res": "fail", "description": "服务器错误"}
            LOG.debug("Error when app login, error is %s" % e)
    return res

