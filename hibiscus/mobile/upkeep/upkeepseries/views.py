import simplejson
import logging

from django.db.models import Count

from hibiscus.models import SeriesUpkeepQuote, UpkeepSeries, Upkeep, Producer

LOG = logging.getLogger(__name__)
BRAND_LEVEL = {
    1: 'high',
    2: 'middle',
    3: 'low'
}


def get_banjin_upkeep_producer(request):
    res = {}
    if request.body:
        try:
            info = simplejson.loads(request.body, encoding="utf-8")
            upseries_id = info.get("upkeep_series_id", None)
            upkeep_list = info.get("upkeep_list", None)
            upsobj = UpkeepSeries.objects.get(pk=upseries_id)
            level = BRAND_LEVEL[upsobj.series.brand.level.id]
            quotes = SeriesUpkeepQuote.objects.filter(series_upkeep_info__upkeep_series__id=upseries_id).\
                        filter(series_upkeep_info__upkeep__id__in=upkeep_list).values("producer__id").\
                        annotate(pcount=Count("producer__id"))
            producer_ids = [q['producer__id'] for q in quotes]
            producer_data = []
            for p_id in producer_ids:
                producer_price = []
                original_price = []
                producer_params = {}
                pobj = Producer.objects.get(pk=p_id)
                producer_params['username'] = pobj.username
                producer_params['id'] = pobj.id
                for upid in upkeep_list:
                    qs = SeriesUpkeepQuote.objects.filter(series_upkeep_info__upkeep_series__id=upseries_id).\
                        filter(producer__id=p_id).filter(series_upkeep_info__upkeep__id=upid)
                    if qs:
                        producer_price.append(getattr(qs[0], "sale_price_%s" % level))
                        # producer_price.append(qs[0].price)
                    upobj = Upkeep.objects.get(pk=upid)
                    if level == "high":
                        original_price.append(upobj.price_high)
                    if level == "low":
                        original_price.append(upobj.price_low)
                    if level == "middle":
                        original_price.append(upobj.price_middle)
                producer_money = sum(producer_price)
                original_money = sum(original_price)
                hui_price = original_money - producer_money
                if hui_price < 0:
                    hui_price = 0
                producer_params['hui_price'] = hui_price
                producer_data.append(producer_params)
                res['producer_data'] = producer_data
        except Exception as e:
            LOG.error('Can not get quote by producer. %s' % e)
            res = {}
    return res
