import simplejson
import logging

from hibiscus.models import UpkeepMaterial, SeriesUpkeepQuote

LOG = logging.getLogger(__name__)


def get_upkeep_material(request):
    res = {}
    try:
        upkeep_id = request.GET.get("upkeep_id", None)
        material_data_list = []
        if upkeep_id:
            materials = UpkeepMaterial.objects.filter(upkeep__id=upkeep_id)
            for m in materials:
                mp = {}
                mp['brand'] = m.brand
                mp['pic'] = ""
                mp['id'] = m.id
                mp['model_data'] = []
                models = m.upkeepmaterialmodel_set.all()
                for d in models:
                    dp = {}
                    dp['model_id'] = d.id
                    dp['code'] = d.code
                    dp['model'] = d.model
                    dp['name'] = d.name
                    dp['price'] = d.material_price
                    dp['pic'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                               request.META["HTTP_HOST"],
                                               d.picture.path)
                    mp['model_data'].append(dp)
                material_data_list.append(mp)
            res['material_data'] = material_data_list

    except Exception as e:
        LOG.error('Can not get producers by pkg. %s' % e)
    return res


def get_material_models(request):
    res = {}
    try:
        res['all_data'] = []
        if request.body:
            info = simplejson.loads(request.body, encoding="utf-8")
            upseries_id = info.get("upkeep_series_id", None)
            producer_id = info.get("producer_id", None)
            material_id = info.get("material_id", None)
            series_quote = SeriesUpkeepQuote.objects.filter(producer__id=producer_id).\
                filter(series_upkeep_info__upkeep_series__id=upseries_id).\
                filter(series_upkeep_info__upkeep_material_model__material__id=material_id)
            if series_quote:
                for sq in series_quote:
                    sqp = {}
                    sqp['model_id'] = sq.series_upkeep_info.upkeep_material_model.id
                    sqp['model_name'] = sq.series_upkeep_info.upkeep_material_model.name
                    sqp['labor'] = sq.sale_labor_price
                    sqp['price'] = sq.sale_material_price
                    res['all_data'].append(sqp)
    except Exception as e:
        LOG.error('Can not get material models. %s' % e)
    return res

