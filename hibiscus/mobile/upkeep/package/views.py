import simplejson
import logging
from django.shortcuts import render
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.contrib import messages
from django.utils.translation import ugettext as _

from hibiscus.models import UpkeepPackage, UpkeepType, UpkeepSeries
from hibiscus.decorators import data_table

LOG = logging.getLogger(__name__)

BRAND_LEVEL = {
    1: 'high',
    2: 'middle',
    3: 'low'
}


def get_packages_by_type(request):
    type_id = request.GET.get("type_id", "1")
    upkeep_series_id = request.GET.get("upkeep_series_id", None)
    type_id = type_id if type_id in ("1", "2") else "1"
    packages = UpkeepPackage.objects.filter(type=type_id)
    res = []
    try:
        if type_id == "1":
            res = [{"id": package.id,
                    "name": package.name} for package in packages]
        else:
            level = 'middle'
            if upkeep_series_id:
                upsobj = UpkeepSeries.objects.get(pk=upkeep_series_id)
                level = BRAND_LEVEL[upsobj.series.brand.level.id]
            res = [{"id": package.id,
                    "name": package.name,
                    "price": [{
                         "price": getattr(upkeep, "price_%s" % level)} for upkeep in package.upkeep_set.all()]
                     } for package in packages]
    except Exception as e:
        LOG.debug("get_packages_by_type failed! error is %s" % e)
        res = []

    return res

