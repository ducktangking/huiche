import logging
import simplejson
import datetime

from hibiscus.models import Upkeep, Adviser, UpkeepOrders, Series, Producer, UpkeepMaterialModel, UpkeepSeries, UpkeepPackage
from hibiscus.decorators import user_authorized, require_app_login


LOG = logging.getLogger(__name__)


def get_upkeep_by_package(request):
    package_id = request.GET.get("package_id", None)
    res = []
    if package_id:
        upkeeps = Upkeep.objects.filter(package=package_id)
    return res


def new_upkeep_orders(request):
    res = {"status": "success", "reason": "success"}
    try:
        if request.body:
            # user = request.app_user
            login_status, user = user_authorized(request)
            info = simplejson.loads(request.body, encoding="utf-8")
            name = info['name']
            phone = info['phone']
            car_license = info['car_license']

            adviser_id = info.get('adviser_id', "")
            in_time = info['in_time']
            in_time = datetime.datetime.strptime(in_time, "%Y-%m-%dT%H:%M:%S.%fZ")
            pkgs = info.get("packages", "")
            if pkgs:
                payment = info['payment']
                out_time = info['out_time']
                out_time = datetime.datetime.strptime(out_time, "%Y-%m-%dT%H:%M:%S.%fZ")
            producer_id = info['producer_id']
            series_id = info['series_id']

            material_models = info.get("material_models", "")
            adviser = ""
            if adviser_id:
                adviser = Adviser.objects.filter(pk=adviser_id)

            series_obj = UpkeepSeries.objects.get(pk=series_id)
            producer = Producer.objects.get(pk=producer_id)

            if pkgs:
                up = UpkeepOrders.objects.create(name=name, phone=phone, producer=producer,
                                                 upkeep_series=series_obj,
                                                 car_license=car_license, payment=payment,
                                                 in_time=in_time, out_time=out_time)
                for uid in pkgs:
                    upobj = UpkeepPackage.objects.filter(pk=uid)
                    if upobj:
                        up.upkeeps_pkgs.add(upobj[0])
                # up.save()
            if material_models:
                up = UpkeepOrders.objects.create(name=name, phone=phone, producer=producer,
                                                 upkeep_series=series_obj,
                                                 car_license=car_license,
                                                 in_time=in_time)
                for mm in material_models:
                    model_obj = UpkeepMaterialModel.objects.filter(pk=mm)
                    if model_obj:
                        up.material_model.add(model_obj[0])

            if login_status:
                up.user = user
                up.save()

            if adviser:
                up.adviser = adviser[0]
                # up.save()
            up.save()

    except Exception as e:
        res['status'] = "fail"
        res['reason'] = "Error happened."
        LOG.error('Error happened when insert upkeep orders. %s' % e)
    return res

