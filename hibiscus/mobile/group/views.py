# -*-coding:utf8 -*-
import simplejson
import logging
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.core.urlresolvers import reverse

from hibiscus.models import Logo, Picture, Article, Brand, SupportedArea, Car, Series, ArticleType, GroupPurchase, GroupPurchaseUser
from hibiscus.decorators import data_table
from hibiscus.background.public import deal_with_upload_file
from hibiscus.background.brand.views import brand_car
from hibiscus.decorators import page

LOG = logging.getLogger(__name__)


def get_group_index(request):
    groups = GroupPurchase.objects.filter(is_timeout=False).order_by("-id")[:3]
    return [{"id": group.id,
             "name": group.name,
             "series_pic": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], group.series.logo.picture.path),
             'brand_id': group.series.brand.id,
             "series_id": group.series.id,
             "brand_name": group.series.brand.name,
             "brand_icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], group.series.brand.logo.picture.path),
             "time_begin": timezone.localtime(group.time_begin).strftime('%Y-%m-%d %H:%M:%S'),
             "time_end": timezone.localtime(group.time_end).strftime('%Y-%m-%d %H:%M:%S'),
             "persons": group.grouppurchaseuser_set.count()}
            for group in groups]


@page
def get_groups(request):
    groups = GroupPurchase.objects.order_by("-id")
    return [{"id": group.id,
             "name": group.name,
             "series_id": group.series.id,
             "series_pic": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], group.series.logo.picture.path),
             "time_begin": timezone.localtime(group.time_begin).strftime('%Y-%m-%d %H:%M:%S'),
             "time_end": timezone.localtime(group.time_end).strftime('%Y-%m-%d %H:%M:%S'),
             "is_timeout": group.is_timeout,
             "status": group.status}
            for group in groups]


def user_add(request):
    res = {"res": "fail"}
    if request.body:
        info = simplejson.loads(request.body, encoding="utf-8")
        group_id = info.get("group_id", None)
        name = info.get("name", None)
        phone = info.get("phone", None)
        city = info.get("city", None)
        buy_type = info.get("buy_type", None)

        if group_id and name and phone and city and buy_type:
            user = GroupPurchaseUser.objects.create(name=name, phone=phone,
                                                    city=city, buy_type=buy_type, group_purchase_id=group_id)
            user.save()
            res["res"] = "success"
    return res


@page
def get_group_articles(request):
    res = []
    try:
        groups = GroupPurchase.objects.filter(is_timeout=False, article__status=True)
        for g in groups:
            gp = {}
            ar = g.article
            gp['article_content'] = ar.content
            gp['article_title'] = ar.title
            gp['brand'] = g.series.brand.name
            gp['time_begin'] = timezone.localtime(g.time_begin).strftime('%Y-%m-%d %H:%M:%S')
            gp['time_end'] = timezone.localtime(g.time_end).strftime('%Y-%m-%d %H:%M:%S')
            gp['peple_count'] = g.grouppurchaseuser_set.all().count()
            pics = g.picture.all()
            gp['pics'] = [{"pic_name": p.name, "path": p.path} for p in pics]
            gp['brand_icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], g.series.brand.logo.picture.path)
            res.append(gp)
    except Exception as e:
        LOG.error('Error happened get_group_articles. %s' % e)
    return res


@page
def get_hot_series_groups(request):
    res = []
    try:
        series = Series.objects.filter(is_hot__id=3)
        if series:
            for s in series:
                g = GroupPurchase.objects.filter(series__id=series[0].id, is_timeout=False)
                gp = {}
                if g:
                    ar = g[0].article
                    gp['article_content'] = ar.content
                    gp['article_title'] = ar.title
                    gp['id'] = g[0].series.id
                    # id 是指车型的 id
                    gp['brand'] = g[0].series.brand.name
                    gp['time_begin'] = timezone.localtime(g[0].time_begin).strftime('%Y-%m-%d %H:%M:%S')
                    gp['time_end'] = timezone.localtime(g[0].time_end).strftime('%Y-%m-%d %H:%M:%S')
                    gp['peple_count'] = g[0].grouppurchaseuser_set.all().count()
                    pics = g[0].picture.all()
                    gp['series_icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], g[0].series.logo.picture.path)
                    gp['name'] = g[0].series.name
                    gp['pics'] = [{"pic_name": p.name, "path": p.path} for p in pics]
                    gp['brand_icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], g[0].series.brand.logo.picture.path)
                    res.append(gp)
    except Exception as e:
        LOG.error('Error happened. %s' % e)
    return res


@page
def get_hot_brand_groups(request):
    res = []
    try:
        brands = Brand.objects.filter(is_hot__id=1)
        series_list = []
        for b in brands:
            series = b.series_set.all()
            for s in series:
                series_list.append(s)
        for s in series_list:
            g = GroupPurchase.objects.filter(series__id=s.id, is_timeout=False)
            if g:
                gp = {}
                ar = g[0].article
                gp['article_content'] = ar.content
                gp['article_title'] = ar.title
                gp['id'] = g[0].series.brand.id
                # id是二级品牌id
                gp['brand'] = g[0].series.brand.name
                gp['time_begin'] = timezone.localtime(g[0].time_begin).strftime('%Y-%m-%d %H:%M:%S')
                gp['time_end'] = timezone.localtime(g[0].time_end).strftime('%Y-%m-%d %H:%M:%S')
                gp['peple_count'] = g[0].grouppurchaseuser_set.all().count()
                pics = g[0].picture.all()
                gp['pics'] = [{"pic_name": p.name, "path": p.path} for p in pics]
                gp['brand_icon'] = "%s://%s%s" % (
                request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], g[0].series.brand.logo.picture.path)
                res.append(gp)

    except Exception as e:
        LOG.error('Error happened. get_hot_brand_groups. %s' % e)
    return res

