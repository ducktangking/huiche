import simplejson
import logging

from django.utils import timezone
from django.db.models import Q
from django.http import HttpResponse

from hibiscus.decorators import page, require_app_login
from hibiscus.models import Series, UpkeepSeries, Car, CarQuote

LOG = logging.getLogger(__name__)
SERIES_COLUMNS = ["name"]


@page
def search(request):
    #input price
    res = []
    key_word = request.GET.get("keywords", "")
    if key_word != "":
        sts = Series.objects.filter(Q(name__contains=key_word) | Q(name__contains=key_word.upper()) |
                                    Q(name__contains=key_word.lower()))
    else:
        sts = Series.objects.all()
    for st in sts:
        stp = {}
        stp['name'] = st.name
        stp['id'] = st.id
        stp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                      request.META["HTTP_HOST"],
                                      st.logo.picture.path)
        stp['price'] = get_series_car_price_min_and_max(st.id)
        stp['producer_price'] = get_series_car_quote_price(st.id)
        stp['cc'] = get_series_car_cc(st.id)
        stp['pic_num'] = get_series_car_pic_num(st.id)
        stp['country'] = ""
        if st.brand.country:
            stp['country'] = st.brand.country.name
        res.append(stp)
    return res


def get_series_car_price_min_and_max(series_id):
    data = {"price_min": 0, "price_max": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        price = [c.price for c in cars]
        if price:
            data['price_min'] = min(price)
            data['price_max'] = max(price)
    except Exception as e:
        LOG.error('get_series_car_quote_min_and_max. %s' % e)
    return data


def get_series_car_quote_price(series_id):
    data = {'producer_min_price': 0, 'producer_max_price': 0}
    try:
        cars = CarQuote.objects.filter(car__series__id=series_id)
        producer_price = [c.sale_price for c in cars]
        if producer_price:
            data['producer_min_price'] = min(producer_price)
            data['producer_max_price'] = max(producer_price)
    except Exception as e:
        LOG.error('Error happened when get_series_car_quote_price. %s' % e)
    return data


def get_hot_series(request):
    sts = Series.objects.filter(is_app=True)
    res = {"series_hot":[], "series_primary":[], "series_pinxingjingkou":[], "series_new": [], "series_recommend": [], "series_favorite": []}
    for st in sts:
        series_type = st.is_hot.all()
        for s in series_type:
            if s.id == 3:
                res["series_hot"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
            if s.id == 4:
                res["series_primary"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
            if s.id == 6:
                res["series_pinxingjingkou"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
            if s.id == 1:
                res["series_new"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
            if s.id == 2:
                res["series_recommend"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
            if s.id == 5:
                res["series_favorite"].append({"id": st.id, "name": st.name, "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"], request.META["HTTP_HOST"], st.logo.picture.path)})
    return res


def get_series_by_brand_id(request):
    brand_id = request.GET.get("brand_id", None)
    res = []
    if brand_id:
        series = Series.objects.filter(brand_id=brand_id)
        if series:
            res = [{"id": st.id, "name": st.name,
                    "icon": "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                           request.META["HTTP_HOST"], st.logo.picture.path),
                    "price": get_series_car_price_min_and_max(st.id),
                    "producer_price": get_series_car_quote_price(st.id),
                    "pic_num": get_series_car_pic_num(st.id),
                    "country": st.brand.country.name if st.brand.country else ""
                    }
                   for st in series]
    return res


def get_upkeep_series_by_series_id(request):
    res = []
    series_id = request.GET.get("series_id", None)
    if series_id:
        try:
            upkeep_series = UpkeepSeries.objects.filter(series_id=series_id)
            if upkeep_series:
                res = [{"id": st.id,
                        "produce_year": st.produce_year.year if st.produce_year else "",
                        "cc": st.cc} for st in upkeep_series]
        except Exception as e:
            res = []
            LOG.debug("Error when get_upkeep_series_by_series_id, error is %s" % e)
    return res


def recommend_series(request):
    res = []
    try:
        series = Series.objects.filter(is_hot__id=2).order_by("-created_at")[0:6]
        for s in series:
            sp = {}
            sp['id'] = s.id
            sp['name'] = s.name
            sp['brand_name'] = s.brand.name
            sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                                     request.META["HTTP_HOST"],
                                                     s.logo.picture.path)
            sp['created_at'] = timezone.localtime(s.created_at).strftime('%Y-%m-%d %H:%M:%S')
            sp["price"] = get_series_car_price_min_and_max(s.id)
            sp["producer_price"] = get_series_car_quote_price(s.id)
            sp["cc"] = get_series_car_cc(s.id)
            sp['pic_num'] = get_series_car_pic_num(s.id)
            sp['country'] = s.brand.country.name if s.brand.country else ""
            res.append(sp)
    except Exception as e:
        LOG.error('Error happened when get recommend series. %s' % e)
    return res


@require_app_login
def set_user_fav_series(request):
    res = {"res": "fail"}
    try:
        series_id = request.GET.get("series_id", "")
        if series_id:
            series_obj = Series.objects.filter(pk=series_id)
            if series_obj:
                # user = Users.objects.get(pk=2)
                user = request.app_user
                own_series = list(user.fav_series.all())
                if series_obj[0] in own_series:
                    own_series.remove(series_obj[0])
                else:
                    own_series.append(series_obj[0])
                for s in own_series:
                    user.fav_series.add(s)
                user.save()
                res = {"res": "success"}
    except Exception as e:
        res = "fail"
        print e
    return res


@require_app_login
def get_user_fav_series(request):
    res = []
    try:
        # user = Users.objects.get(pk=2)
        user = request.app_user
        series = user.fav_series.all()
        for s in series:
            sp = {}
            sp['id'] = s.id
            sp['name'] = s.name
            sp['brand_name'] = s.brand.name
            sp['icon'] = "%s://%s%s" % (request.META["wsgi.url_scheme"],
                                        request.META["HTTP_HOST"],
                                        s.logo.picture.path)
            sp['price'] = get_series_car_price_min_and_max(s.id)
            sp['producer_price'] = get_series_car_quote_price(s.id)
            sp["cc"] = get_series_car_cc(s.id)
            sp['pic_num'] = get_series_car_pic_num(s.id)
            sp['country'] = s.brand.country.name if s.brand.country else ""
            sp['created_at'] = timezone.localtime(s.created_at).strftime('%Y-%m-%d %H:%M:%S')
            res.append(sp)
    except Exception as e:
        LOG.error("Error happened when get_user_fav_series. %s" % e)
    return res


def get_series_car_cc(series_id):
    #Fuel consumption
    data = {"min_cc": 0, "max_cc": 0}
    try:
        cars = Car.objects.filter(series__id=series_id)
        cc = [c.real_cc for c in cars]
        if cc:
            data['min_cc'] = min(cc)
            data['max_cc'] = max(cc)
    except Exception as e:
        LOG.error('Error happened when get_series_car_cc. %s' % e)
    return data


def get_series_car_pic_num(series_id):
    pic_num = 0
    try:
        cars = Car.objects.filter(series__id=series_id)
        for c in cars:
            pic_num += c.carpicture_set.all().count()
    except Exception as e:
        LOG.error('Error happened when get_series_car_pic_num. %s' % e)
    return pic_num
