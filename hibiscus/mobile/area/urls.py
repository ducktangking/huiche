# -*- coding:utf-8 -*-
from django.conf.urls import url
from hibiscus.mobile.area import views

urlpatterns = [
    url(r'^index$', views.index, name="mobile-area-index"),
    url(r'^set$', views.update_city, name='mobile-user-area-set'),
    url(r'^id$', views.area_id, name='mobile-get-area-id'),
]