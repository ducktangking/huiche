#-*- utf8 -*-

__author__ = 'gmj'

import simplejson
import logging

from django.http import HttpResponse
from django.db.models import Q

from hibiscus.models import SupportedArea, UserInformation
from hibiscus.decorators import require_app_login


LOG = logging.getLogger(__name__)


def index(request):
    return ajax_data(request)


def ajax_data(request):
    area_data = []
    try:
        area = SupportedArea.objects.all()
        for a in area:
            a_params = {"id": a.id, "province": a.province, "city": a.city}
            area_data.append(a_params)
    except Exception as e:
        LOG.error('Can not get area data. %s' % e)

    return HttpResponse(simplejson.dumps(area_data), content_type="application/json")


@require_app_login
def update_city(request):
    res = {"status": "success", "reason": "success"}
    try:
        if request.body:
            info = simplejson.loads(request.body, encoding="utf-8")
            province = info.get("province", None)
            city = info.get("city", None)
            support_area = SupportedArea.objects.filter(province=province, city=city)
            if support_area:
                user = request.app_user
                if user.information:
                    user.information.city_info = support_area[0]
                    user.information.save()
                    user.save()
                else:
                    uinfo = UserInformation.objects.create(city_info = support_area[0])
                    user.information = uinfo
                    user.save()
            else:
                res['status'] = "fail"
                res['reason'] = "Not support this area."
    except Exception as e:
        LOG.error('Error happened when update city info. %s' % e)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def area_id(request):
    res = {"id": 0}
    try:
        if request.body:
            info = simplejson.loads(request.body, encoding="utf-8")
            province = info.get("province", "")
            city = info.get("city", "")
            area = SupportedArea.objects.filter(province__contains=province).filter(city__contains=city)
            if area:
                res['id'] = area[0].id
    except Exception as e:
        LOG.error('Error happened when get area id in get_area_id.' % e)
    return HttpResponse(simplejson.dumps(res), content_type="application/json")


def get_province(request):
    res = []
    try:
        provinces = SupportedArea.objects.all()
        for p in provinces:
            pp = {}
            pp['id'] = p.id
            pp['province_initial'] = p.province_initial
            pp['province'] = p.province
            res.append(pp)
    except Exception as e:
        LOG.error('Error happened. get_province. %s' % e)
    return res


def get_city(request):
    res = []
    try:
        province_id = request.GET.get("province_id", "")
        area = SupportedArea.objects.filter(pk=province_id)
        if area:
            cities = SupportedArea.objects.filter(province=area[0].province)
            for c in cities:
                cp = {}
                cp['id'] = c.id
                cp['city'] = c.city
                cp['city_initial'] = c.city_initial
                res.append(cp)
    except Exception as e:
        LOG.error('Error happened get_city. %s' % e)
    return res
