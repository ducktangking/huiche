# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2017-10-30 08:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hibiscus', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArticleShow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('desc', models.CharField(max_length=256)),
                ('position', models.IntegerField(null=True)),
                ('type', models.IntegerField(null=True)),
                ('page', models.IntegerField(null=True)),
                ('is_app', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('area', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SupportedArea')),
            ],
        ),
        migrations.CreateModel(
            name='GiftBag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('car_good', models.BooleanField(default=False)),
                ('gift_value', models.FloatField()),
                ('oil_card', models.FloatField(null=True)),
                ('business_insurance', models.IntegerField(null=True)),
                ('traffic_insurance', models.IntegerField(null=True)),
                ('buy_tax', models.IntegerField(choices=[(1, '50%'), (2, '100%')], null=True)),
                ('upkeep_way', models.IntegerField(choices=[(1, 'money'), (2, 'times'), (3, 'year divide 10kkm')], null=True)),
                ('upkeep_val', models.FloatField(null=True)),
                ('else_sale', models.TextField(null=True)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='GiftBagType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gift_name', models.CharField(max_length=16)),
                ('has_way', models.BooleanField()),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='ProducerPreSale',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sale_start', models.DateTimeField(default=django.utils.timezone.now)),
                ('sale_end', models.DateTimeField(default=django.utils.timezone.now)),
                ('keep_up_in_buttom', models.BooleanField(default=True)),
                ('company_adr_in_buttom', models.BooleanField(default=True)),
                ('map_in_buttom', models.BooleanField(default=True)),
                ('sale_tel_in_buttom', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='SaleCarAndColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('origin_price', models.FloatField(null=True)),
                ('discount_price', models.FloatField(null=True)),
                ('finaly_price', models.FloatField(null=True)),
                ('inventory', models.IntegerField(choices=[(0, 'shortage'), (1, 'need order'), (2, 'enough car')])),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hibiscus.Car')),
                ('color', models.ManyToManyField(to='hibiscus.CarTypeColor')),
            ],
        ),
        migrations.CreateModel(
            name='SaleSeriesAndColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('car_and_color', models.ManyToManyField(to='hibiscus.SaleCarAndColor')),
                ('series', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hibiscus.Series')),
            ],
        ),
        migrations.CreateModel(
            name='SeriesCharacterShow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('row', models.IntegerField()),
                ('row_name', models.CharField(max_length=32, null=True)),
                ('col', models.IntegerField()),
                ('sheet', models.CharField(max_length=32)),
                ('page', models.IntegerField()),
                ('about_price', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='SeriesKind',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('parent', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SeriesKind')),
            ],
        ),
        migrations.CreateModel(
            name='SeriesNewCar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
                ('position', models.IntegerField()),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='SeriesPictureShow',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sheet', models.CharField(max_length=32)),
                ('page', models.IntegerField()),
                ('about_price', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='SeriesPortalAd',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.CharField(max_length=512)),
                ('position', models.IntegerField()),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('pic', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.Picture')),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='on_top',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='cartypeenergy',
            name='parent',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.CarTypeEnergy'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='article',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='hibiscus.Article'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='bind_series',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SaleSeriesAndColor'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='gift',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.GiftBag'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='hd_pic',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hd_pic', to='hibiscus.Picture'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='producer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hibiscus.Producer'),
        ),
        migrations.AddField(
            model_name='producerpresale',
            name='small_pic',
            field=models.ManyToManyField(related_name='small_pic', to='hibiscus.Picture'),
        ),
        migrations.AddField(
            model_name='article',
            name='article_show',
            field=models.ManyToManyField(default=None, to='hibiscus.ArticleShow'),
        ),
        migrations.AddField(
            model_name='series',
            name='kind',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SeriesKind'),
        ),
        migrations.AddField(
            model_name='series',
            name='new_car',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SeriesNewCar'),
        ),
        migrations.AddField(
            model_name='series',
            name='pc_character',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SeriesCharacterShow'),
        ),
        migrations.AddField(
            model_name='series',
            name='pc_picture',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hibiscus.SeriesPictureShow'),
        ),
        migrations.AddField(
            model_name='series',
            name='series_ad',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='hibiscus.SeriesPortalAd'),
        ),
    ]
