
import threading
import time
from datetime import timedelta
import logging
from django.db.models import Q
from hibiscus.decorators import singleton
from hibiscus.models import GroupPurchase, Ad, CarInquiry, CarUpkeepInquiry
from django.utils import timezone

LOG = logging.getLogger(__name__)

DEFAULT_TIME_OUT = 3600
ORDER_REFRESH_TIME = 600
# choose once of 10 minutes as refresh frequency
ORDER_TIME_OUT = 1
# 1 day later, means the order could be toke by anyone


class LoopTask(threading.Thread):
    def __init__(self, task, timeout=DEFAULT_TIME_OUT):
        threading.Thread.__init__(self)
        self._timeout = timeout
        self._task = task

    def run(self):
        while True:
            if self._task():
                time.sleep(self._timeout)
            else:
                break

@singleton
class HibiscusTask(object):

    def __init__(self):
        self.__timers = []
        self.__start = False
        self.tasks_add(self.group_refresh)
        self.tasks_add(self.ad_refresh)

    def tasks_add(self, func):
        self.__timers.append(LoopTask(func))

    def tasks_start(self):
        for timer in self.__timers:
            timer.start()

    def run(self):
        if not self.__start:
            self.tasks_start()
            self.__start = True

    @staticmethod
    def group_refresh():
        groups = GroupPurchase.objects.filter(is_timeout=False)
        for group in groups:
            seconds_el = (timezone.localtime(group.time_end) - timezone.now()).total_seconds()
            if seconds_el <= 0:
                group.is_timeout = True
                group.save()
        return True

    @staticmethod
    def ad_refresh():
        ads = Ad.objects.filter(is_timeout=False)
        for ad in ads:
            seconds_el = (timezone.now() - (timezone.localtime(ad.created_at)+timedelta(ad.time))).total_seconds()
            if seconds_el > 0:
                ad.is_timeout = True
                ad.save()
        return True


class LoopTask2(threading.Thread):
    def __init__(self, task, timeout=ORDER_REFRESH_TIME):
        threading.Thread.__init__(self)
        self._timeout = timeout
        self._task = task

    def run(self):
        while True:
            if self._task():
                time.sleep(self._timeout)
            else:
                break

@singleton
class HibiscusTask2(object):

    def __init__(self):
        self.__timers = []
        self.__start = False
        self.tasks_add(self.order_refresh)

    def tasks_add(self, func):
        self.__timers.append(LoopTask2(func))

    def tasks_start(self):
        for timer in self.__timers:
            timer.start()

    def run(self):
        if not self.__start:
            self.tasks_start()
            self.__start = True

    @staticmethod
    def order_refresh():
        order1_obj = CarInquiry.objects.filter(Q(is_order=True), Q(is_token=True), Q(is_defeat=False))
        order2_obj = CarUpkeepInquiry.objects.filter(Q(is_order=True), Q(is_token=True), Q(is_defeat=False))
        if order1_obj:
            for order1 in order1_obj:
                if order1.level == 'A':
                    second_el = (timezone.now() - (timezone.localtime(order1.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order1.is_token = False
                        order1.order_created_at = timezone.now()
                        order1.save()
                if order1.level == 'B':
                    second_el = (timezone.now() - (timezone.localtime(order1.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order1.is_token = False
                        order1.order_created_at = timezone.now()
                        order1.save()
                if order1.level == 'C':
                    second_el = (timezone.now() - (timezone.localtime(order1.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order1.is_token = False
                        order1.order_created_at = timezone.now()
                        order1.save()
                if order1.level == 'H':
                    second_el = (timezone.now() - (timezone.localtime(order1.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order1.is_token = False
                        order1.order_created_at = timezone.now()
                        order1.save()
        if order2_obj:
            for order2 in order2_obj:
                if order2.level == 'A':
                    second_el = (timezone.now() - (timezone.localtime(order2.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order2.is_token = False
                        order2.order_created_at = timezone.now()
                        order2.save()
                if order2.level == 'B':
                    second_el = (timezone.now() - (timezone.localtime(order2.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order2.is_token = False
                        order2.order_created_at = timezone.now()
                        order2.save()
                if order2.level == 'C':
                    second_el = (timezone.now() - (timezone.localtime(order2.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order2.is_token = False
                        order2.order_created_at = timezone.now()
                        order2.save()
                if order2.level == 'H':
                    second_el = (timezone.now() - (timezone.localtime(order2.record_date) +
                                                   timedelta(days=15) + timedelta(days=ORDER_TIME_OUT))).total_seconds()
                    if second_el <= 0:
                        order2.is_token = False
                        order2.order_created_at = timezone.now()
                        order2.save()
