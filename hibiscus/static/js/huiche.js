/**
 * Created by gmj on 4/28/16.
 */

function area_info() {
    //{"province": "", "city": ["", "", "","", "", "","", "", "","", "", "", "", "", "", "", "", ""]}
    var info = [
        {
            "province": "河北",
            "city": ["石家庄", "唐山", "秦皇岛", "邯郸", "邢台", "保定", "张家口", "承德", "沧州", "廊坊", "衡水"]
        },
        {
            "province": "山西",
            "city": ["太原", "大同", "阳泉", "长治", "晋城", "朔州", "晋中", "运城", "忻州", "临汾", "吕梁"]
        },
        {
            "province": "内蒙古",
            "city": ["呼和浩特", "包头", "乌海", "赤峰", "通辽", "鄂尔多斯", "呼伦贝尔", "巴彦淖尔", "乌兰察布", "兴安",
                "锡林郭勒", "阿拉善"]
        },
        {
            "province": "辽宁",
            "city": ["沈阳", "大连", "鞍山", "抚顺", "本溪", "丹东", "锦州", "营口", "阜新", "辽阳", "盘锦", "铁岭",
                "朝阳", "葫芦岛"]
        },
        {
            "province": "吉林",
            "city": ["长春", "吉林", "四平", "辽源", "通化", "白山", "松原", "白城", "延边"]
        },
        {
            "province": "黑龙江",
            "city": ["哈尔滨", "齐齐哈尔", "鸡西", "鹤岗", "双鸭山", "大庆", "伊春", "佳木斯", "七台河", "牡丹江",
                "黑河", "绥化", "大兴安岭"]
        },
        {
            "province": "江苏省",
            "city": ["南京", "无锡", "徐州", "常州", "苏州", "南通", "连云港", "淮安", "盐城", "扬州", "镇江", "泰州", "宿迁"]
        }
    ];
    return info;
}

function reduce_el(obj) {
    $(obj).parent().remove();
}

function change_package() {
    var obj = $("#upPkg").find("option:selected");
    if (obj.attr("type_id") == 1) {
        $("#upParams1").css("display", "none");
        $("#upParams2").css("display", "block");
    } else {
        $("#upParams2").css("display", "none");
        $("#upParams1").css("display", "block");
    }
}

function select_proj_name() {
    var proj_name = $("#projName").find("option:selected").val();
    if (proj_name == "机油") {
        $("#isOil").css("display", "block");
    } else {
        $("#isOil").css("display", "none");
    }
}

function select_upkeep_type() {
    $("#upPkg").empty();
    var type_id = $("#upT").find("option:selected").val();
    var _url = $("#pkgUrl").val();
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "json",
        data: {"type_id": type_id},
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                var t1 = "<input type='checkbox' name='upPkg' value='" + data[i]["type_id"] + "'>" + data[i]["name"];
                $("#upPkg").append(t1);
            }
        }
    });
    if (type_id == 1) {
        $("#upParams1").css("display", "none");
        $("#upParams2").css("display", "block");
    } else {
        $("#upParams2").css("display", "none");
        $("#upParams1").css("display", "block");
    }
}

function up_series_new_data(obj) {
    $("#upSeries").empty();
    $("#carYear").empty();
    $("#carCc").empty();
    var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

    var brandId = $(obj).val();
    var _url = $("#newUpSeriesDataUrl").val();
    $.ajax({
        url: _url,
        method: 'get',
        dataType: 'json',
        data: {"brandId": brandId},
        success: function (data) {
            if ($.isEmptyObject(data)) {
                return false;
            }
            var series_obj = data['series'];
            var series_param_obj = data['series_one_param'];


            for (var i = 0; i < series_obj.length; i++) {
                var t = "<option value='" + series_obj[i]["s_id"] + "'>" + series_obj[i]["name"] + "</option>";
                $("#upSeries").append(t);
            }
            //year
            var car_obj = series_param_obj["year_data"];
            for (var j = 0; j < car_obj.length; j++) {
                var txt = "<input type='checkbox' name='upseriesYear' value='" + car_obj[j] + "'>" + car_obj[j];
                $("#carYear").append(txt);
            }
            $("#carYear").prepend(select_all_year);
            var cc_obj = series_param_obj["cc_data"];
            for (var k = 0; k < cc_obj.length; k++) {
                var tx = "<option value='" + cc_obj[k] + "'>" + cc_obj[k] + "</option>";
                $("#carCc").append(tx);
            }
        }  //end of success
    });
}

function model_brand_all_series() {
    var _url = $("#modelBrandData").val();
    var brand_id = $("#upSeriesModelBrand").val();
    $("#modelSeriesPlace").empty();
    if (brand_id != '-1') {
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"brand_id": brand_id},
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var t = "<input style='margin-left: 10px;' type='checkbox' name='modelSeries' value='" + data[i]['id'] + "'>" + data[i]['name'];
                    $("#modelSeriesPlace").append(t);
                }
            }
        });
    }
}

function ma_series_new_data(obj) {
    $("#maSeries").empty();
    var brandId = $(obj).val();
    var _url = $("#newMaSeriesDataUrl").val();
    $.ajax({
        url: _url,
        method: 'get',
        dataType: 'json',
        data: {"brandId": brandId},
        success: function (data) {
            if ($.isEmptyObject(data)) {
                return false;
            }
            var series_obj = data['series'];
            var series_param_obj = data['series_one_param'];


            for (var i = 0; i < series_obj.length; i++) {
                var t = "<option value='" + series_obj[i]["s_id"] + "'>" + series_obj[i]["name"] + "</option>";
                $("#maSeries").append(t);
            }
        }  //end of success
    });
}

function up_series_new_data_ex(obj) {
    $("#upSeries").empty();
    var brandId = $(obj).val();
    var initial = $("#brandInitinal").val();
    var _url = $("#newUpSeriesDataUrl").val();
    $.ajax({
        url: _url,
        method: 'get',
        dataType: 'json',
        data: {
            "brandId": brandId,
            "brand_initinal": initial
        },
        success: function (data) {
            if ($.isEmptyObject(data)) {
                return false;
            }
            var series_obj = data['series'];
            var series_param_obj = data['series_one_param'];


            for (var i = 0; i < series_obj.length; i++) {
                var t = "<option value='" + series_obj[i]["s_id"] + "'>" + series_obj[i]["name"] + "</option>";
                $("#upSeries").append(t);
            }
        }  //end of success
    });
}

function check_upkeep_type(obj) {
    var type_id = $(obj).attr("id");
    var _url = $("#projDataUrl").val();
    var weixiu = $("#weixiuPos");
    $.ajax({
        url: _url,
        method: "get",
        dataType: "html",
        data: {"type_id": type_id},
        success: function (data) {
            if (data.length == 0) {
                return false;
            }
            if (type_id == 1) {
                $("#weixiu_proj").empty();
                $("#weixiu_proj").html(data);
            } else {
                $("#banjinPos").empty();
                $("#banjinPos").html(data);
            }

            // if(type_id == "1"){
            //     $("#weixiu_proj").empty();
            //     for(var i=0; i<data.length; i++){
            //         //项目名称
            //         var t = "<div style='border:solid 1px red;height: 200px;width; 500px;'>" +
            //             "<div style='margin-top: 30px;float:left; ' up_id='"+data[i]["up_id"]+"'>" +
            //             "<input name='proj' type='checkbox' value='"+data[i]['up_id']+"'>"+data[i]["up_name"]+"" +
            //             "</div><div style='float: left; margin-left: 20px;' id='brand_"+data[i]["up_id"]+"'></div>" +
            //             "<div style='float: left;margin-left:20px;' id='model_"+data[i]["up_id"]+"'></div></div>";
            //         $("#weixiu_proj").append(t);
            //         for(var j=0; j<data[i]["m_data"].length; j++){
            //             var m_obj = data[i]["m_data"];
            //             //材料品牌
            //             var m_brand_t = "<div>" +
            //                 "<input type='checkbox' name='brandName' value='"+m_obj[j]['m_brand']+"'>"+m_obj[j]['m_brand']+"</div>";
            //                 $("#brand_" + data[i]["up_id"]).append(m_brand_t);
            //             //材料型号
            //             for(var k=0; k<m_obj[j]['m_model'].length; k++){
            //                 var model_obj = m_obj[j]['m_model'];
            //                 var m_material_t = "<div id='brand_model_'"+data[i]["up_id"]+">" +
            //                     "<input type='checkbox' name='brandModel' value='"+model_obj[k]+"'>"+model_obj[k]+"</div>";
            //                 $("#model_" + data[i]["up_id"]).append(m_material_t);
            //             }
            //
            //         }
            //     }
            //
            // }
            // if(type_id == '2'){
            //     $("#banjinPos").empty();
            //     for(var i=0; i<data.length; i++){
            //         var t = "<div><input type='checkbox' name='banjinProj' value='"+data[i]["up_id"]+"'>"+data[i]["up_name"]+"</div>"
            //         $("#banjinPos").append(t);
            //     }
            // }
        }
    });

}

function up_series_car(obj) {
    var seriesId = $(obj).val();
    var _url = $("#newUpSeriesDataUrl").val();
    $("#carYear").empty();
    $("#carCc").empty();
    var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

    $.ajax({
        url: _url,
        method: 'get',
        dataType: 'json',
        data: {"seriesId": seriesId},
        success: function (data) {
            console.log(data);
            var cc_data = data['cc_data'];
            var year_data = data['year_data'];
            for (var i = 0; i < year_data.length; i++) {
                var txt = "<input type='checkbox' name='upseriesYear' value='" + year_data[i] + "'>" + year_data[i];
                $("#carYear").append(txt);
            }
            $("#carYear").prepend(select_all_year);
            for (var i = 0; i < cc_data.length; i++) {
                var txt = "<option value='" + cc_data[i] + "'>" + cc_data[i] + "</option>";
                $("#carCc").append(txt);
            }
        }
    });
}


function up_series_year(obj) {
    var seriesId = $("#upSeries").val();
    var _url = $("#newUpSeriesDataUrl").val();
    var cc = $(obj).val();
    $("#carYear").empty();
    var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

    $.ajax({
        url: _url,
        method: 'get',
        dataType: 'json',
        data: {
            "seriesId": seriesId,
            "cc": cc
        },
        success: function (data) {
            var cc_data = data['cc_data'];
            for (var i = 0; i < cc_data.length; i++) {
                var txt = "<input type='checkbox' name='upseriesYear' value='" + cc_data[i] + "'>" + cc_data[i];
                $("#carYear").append(txt);
            }
            $("#carYear").prepend(select_all_year);
        }
    });
}


function bind_material_area(obj) {
    var province = $(obj).val();
    var area_data = area_info();
    // var default_t = '<option value="-1">全国</option>';
    $("#bindCity").empty();
    // $("#bindCity").append(default_t);
    for (var i = 0; i < area_data.length; i++) {
        if (area_data[i]["province"] == province) {
            var cities = area_data[i]['city'];
            for (var j = 0; j < cities.length; j++) {
                var t = "<option>" + cities[j] + "</option>";
                $("#bindCity").append(t);
            }
        }
    }
    $("#MaterialAreaDataTable tbody").empty();
    $.ajax({
        url: $("#ChangeDataUrl").val(),
        dataType: "json",
        method: "get",
        data: {
            "province": province,
            "city": $("#bindCity").val()
        },
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i]["is_default"]) {
                    var t = "<tr><td>" + data[i]["brand"] + "</td>" +
                        "<td>" + data[i]['code'] + "</td>" +
                        "<td>" + data[i]["model"] + "</td>" +
                        "<td>" + data[i]["price"] + "</td>" +
                        "<td><span style='color: red;'>默认显示</span></td></tr>";

                } else {
                    var t = "<tr><td>" + data[i]["brand"] + "</td>" +
                        "<td>" + data[i]['code'] + "</td>" +
                        "<td>" + data[i]["model"] + "</td>" +
                        "<td>" + data[i]["price"] + "</td>" +
                        "<td><div value='" + data[i]["DT_RowId"] + "' onclick='set_material_default_area(this)'>设为默认</div></td></tr>";
                }


                $("#MaterialAreaDataTable tbody").append(t);
            }

        }
    });
}


function bind_material_area_city() {
    var province = $("#bindProvince").val();
    var city = $("#bindCity").val();
    $("#MaterialAreaDataTable tbody").empty();

    $.ajax({
        url: $("#ChangeDataUrl").val(),
        dataType: "json",
        method: "get",
        data: {
            "province": province,
            "city": city
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                if (data[i]["is_default"] == "True") {
                    var t = "<tr><td>" + data[i]["brand"] + "</td>" +
                        "<td>" + data[i]['code'] + "</td>" +
                        "<td>" + data[i]["model"] + "</td>" +
                        "<td>" + data[i]["price"] + "</td>" +
                        "<td style='color: red;'>默认显示</td></tr>";

                } else {
                    var t = "<tr><td>" + data[i]["brand"] + "</td>" +
                        "<td>" + data[i]['code'] + "</td>" +
                        "<td>" + data[i]["model"] + "</td>" +
                        "<td>" + data[i]["price"] + "</td>" +
                        "<td><div value='" + data[i]["DT_RowId"] + "' onclick='set_material_default_area(this)'>设为默认</div></td></tr>";
                }

                $("#MaterialAreaDataTable tbody").append(t);
            }
        }
    });
}

function set_material_default_area(model_id) {
    var _url = $("#prioriyAreaUrl").val();
    $("#materialModelDefaultProvince").empty();
    $("#materialModelDefaultCity").empty();
    $.ajax({
        url: _url,
        dataType: "json",
        method: 'get',
        data: {"model_id": model_id},
        success: function (data) {
            var t1 = "";
            var t2 = "";
            var default_province = data['default_province'];
            var default_city = data['default_city'];
            if (default_province != "") {
                if (default_province == '-1') {
                    default_province = "全国";
                }
                if (default_city == '-1') {
                    default_city = "全国";
                }
                $("#existArea").append(default_province + "  " + default_city);
            }
            // else{
            for (var i = 0; i < data['province'].length; i++) {
                var province_name = data['province'][i];
                if (province_name == "-1") {
                    province_name = "全国";
                }
                t1 = "<option>" + province_name + "</option>";
                $("#materialModelDefaultProvince").append(t1);

            }
            for (var j = 0; j < data['city'].length; j++) {
                var city_name = data['city'][j]['city'];
                if (city_name == '-1') {
                    city_name = "全国";
                }
                var t2 = "<option valud='" + data['city'][j]['id'] + "'>" + city_name + "</option>";
                $("#materialModelDefaultCity").append(t2);
            }
            // }
            $("#materialModelId").val(data['model_id']);
            $("#areaSetModel").modal("show");
        }
    });
}

function subQuote(obj) {
    var car_id = $(obj).attr('carId');
    var action_url = $("#carQuoteAction").val();
    $.ajax({
        url: action_url,
        dataType: "json",
        method: "POST",
        data: $("#carQuoteForm_" + car_id).serialize(),
        success: function (data) {
            if (data['res'] == "success") {
                var index_url = $("#carQuoteIndexUrl").val();
                window.location.href = index_url;
            }
            else {
                alert("error happened");
            }
        }

    });
}

function delQuote(obj) {
    var url = $("#carQuoteDel").val();
    var role = $("#actionRole").val();
    var index_url = "";
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "carId": $(obj).attr("carId"),
            "producerId": $(obj).attr("producerId")
        },
        success: function (data) {
            if (data['res'] == "success") {
                if (role == "producer") {
                    index_url = $("#carQuoteIndexUrl").val();

                } else {
                    index_url = $("#adminCarQuoteIndex").val();
                }
                location.href = index_url;

            } else {
                alert("error happened when delete car quote");
            }
        }
    });
}


function producer_car_quotes(obj) {
    var producer_id = $(obj).val();
    var url = $("#adminCarQuoteChangeProducer").val();
    // $("#adminCarQuoteP").empty();
    $("#carQuoteTable tbody").empty();
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        data: {"producer_id": producer_id},
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var t = "<tr>" +
                    "<td>" + data[i]["series_name"] + "</td>" +
                    "<td>" + data[i]["car_name"] + "</td>" +
                    "<td>" + data[i]["quote"] + "</td>" +
                    "<td>" + data[i]["weight"] + "</td>" +
                    "<td><button class='adminCarQuoteDelete btn btn-primary btn-xs' onclick='delQuote(this)'  " +
                    "carId='" + data[i]["car_id"] + "' producerId='" + data[i]["producer_id"] + "' " +
                    ">删除</button></td></tr>";
                $("#carQuoteTable tbody").append(t);
            }
        }
    });
}

function select_second_brands(obj) {
    var brand_id = $(obj).val();
    var original_t = '<option value="-1">全部品牌</option>';
    var original_car_t = '<option value="-1">全部车型</option>';
    $("#articleBrand").empty();
    $("#carticleCar").empty();
    $("#articleBrand").append(original_t);
    $("#carticleCar").append(original_car_t);
    var url = $("#secondBrandsUrl").val();
    console.log(brand_id);
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"brand_id": brand_id},
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var t = "<option value='" + data[i]["id"] + "'>" + data[i]["name"] + "</option>";
                $("#articleBrand").append(t);
            }

        }
    });
}

function select_second_brands_up(obj) {
    var brand_id = $(obj).val();
    $("#upSeriesBrand").empty();
    $("#upSeries").empty();
    $("#carYear").empty();
    $("#carCc").empty();
    var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

    var url = $("#secondBrandsUrl").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "brand_id": brand_id,
            "upseries": true
        },
        success: function (data) {
            var brand_data = data['brand_data'];
            var series_data = data['series_data'];
            var series_param_obj = data['cc_year_data'];
            for (var i = 0; i < brand_data.length; i++) {
                var t = "<option value='" + brand_data[i]["id"] + "'>" + brand_data[i]["name"] + "</option>";
                $("#upSeriesBrand").append(t);
            }
            for (var i = 0; i < series_data.length; i++) {
                var t = "<option value='" + series_data[i]["id"] + "'>" + series_data[i]["name"] + "</option>";
                $("#upSeries").append(t);
            }

            //year
            var car_obj = series_param_obj["year_data"];
            for (var j = 0; j < car_obj.length; j++) {
                var txt = "<input type='checkbox' name='upseriesYear' value='" + car_obj[j] + "'>" + car_obj[j];
                $("#carYear").append(txt);
            }
            $("#carYear").prepend(select_all_year);
            var cc_obj = series_param_obj["cc_data"];
            for (var k = 0; k < cc_obj.length; k++) {
                var tx = "<option value='" + cc_obj[k] + "'>" + cc_obj[k] + "</option>";
                $("#carCc").append(tx);
            }
        }
    });
}


function select_second_brands_model(obj) {
    var brand_id = $(obj).val();
    $("#modelSeriesPlace").empty();
    $("#upSeriesModelBrand").empty();
    var all_second_brand = "<option value='-1'>全部品牌</option>";
    $("#upSeriesModelBrand").append(all_second_brand);
    $("#upModelSeries").val("-1");
    var url = $("#secondBrandsUrl").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "brand_id": brand_id,
            "model": true
        },
        success: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                var t = "<option value='" + data[i]["id"] + "'>" + data[i]["name"] + "</option>";
                $("#upSeriesModelBrand").append(t);
            }
        }
    });
}

function select_second_brands_ma(obj) {
    var brand_id = $(obj).val();
    $("#maSeriesBrand").empty();
    $("#maSeries").empty();
    var url = $("#secondBrandsUrl").val();
    console.log(brand_id);
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "brand_id": brand_id,
            "upseries": true
        },
        success: function (data) {
            var brand_data = data['brand_data'];
            var series_data = data['series_data'];
            for (var i = 0; i < brand_data.length; i++) {
                var t = "<option value='" + brand_data[i]["id"] + "'>" + brand_data[i]["name"] + "</option>";
                $("#maSeriesBrand").append(t);
            }
            for (var i = 0; i < series_data.length; i++) {
                var t = "<option value='" + series_data[i]["id"] + "'>" + series_data[i]["name"] + "</option>";
                $("#maSeries").append(t);
            }
        }
    });
}

function select_brand_car(obj) {
    var brand_id = $(obj).val();
    var original_t = '<option value="-1">全部车型</option>';
    $("#carticleCar").empty();
    if (brand_id == "-1") {
        $("#carticleCar").append(original_t);
        return;
    }
    var url = $("#brandCarUrl").val();
    console.log(brand_id);
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"brand_id": brand_id},
        success: function (data) {
            $("#carticleCar").append(original_t);
            for (var i = 0; i < data.length; i++) {
                var t = "<option value='" + data[i]["id"] + "'>" + data[i]["name"] + "</option>";
                $("#carticleCar").append(t);
            }

        }
    });

}

function brand_cars_for_car_pic(obj) {
    $("#carPicCar").empty();
    $("#carPicYear").empty();
    var data_url = $("#carPicChangeBrandData").val();
    $.ajax({
        url: data_url,
        method: "GET",
        dataType: "json",
        data: {"brand_id": $(obj).val()},
        success: function (data) {
            var cars = data[0];
            var years = data[1];
            for (var i = 0; i < cars.length; i++) {
                var t = "<option value='" + cars[i]["car_name"] + "'>" + cars[i]["car_name"] + "</option>";
                $("#carPicCar").append(t);
            }

            for (var j = 0; j < years.length; j++) {
                var t = "<option value='" + years[j] + "'>" + years[j] + "</option>";
                $("#carPicYear").append(t);
            }
        }
    });
}

function set_weight(obj_id) {
    $("#weightSetModal").modal('show');
    $("#carQuoteId").val(obj_id);
}

function upkeepEdit(obj) {
    $(".hasQuoteForm").submit(function (event) {
        event.preventDefault();
        var action_url = $("#upkeepQuoteAction").val();
        $.ajax({
            url: action_url,
            method: "post",
            dataType: "json",
            data: $(this).serialize(),
            success: function (data) {
                if (data['res'] == 'success') {
                    location.href = $("#upkeepQuoteIndex").val();
                }
            },
            error: function (data) {
                alert("ggggggggggggggggg");
                alert(data);
                console.log(data);
            }
        });
    });
}

function upkeepQuoteDelete(obj) {
    var up_series_quote_id = $(obj).attr("upkeepQuoteId");
    var del_url = $("#upkeepQuoteDelete").val();
    $.ajax({
        url: del_url,
        method: "GET",
        dataType: "json",
        data: {"up_series_quote_id": up_series_quote_id},
        success: function (data) {
            if (data['res'] == "success") {
                location.href = $("#upkeepQuoteIndex").val();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(XMLHttpRequest.readyState);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function article_type_level_two(obj) {
    var type_id = $(obj).val();
    var url = $("#TypeLevelTwoData").val();
    $("#articleTwo").empty();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"type_id": type_id},
        success: function (data) {
            var one_t = '<option value="' + type_id + '">' + data["one_level_name"] + '</option>';
            $("#articleTwo").append(one_t);
            for (var i = 0; i < data['two_level_data'].length; i++) {
                var t = "<option value='" + data['two_level_data'][i]["id"] + "'>" + data['two_level_data'][i]["name"] + "</option>";
                $("#articleTwo").append(t);
                if ($('#operatorId').val()) {
                    $('#articleTwo option:contains(销售促销)').remove();
                    $('#articleTwo option:contains(售后促销)').remove();
                }
            }
        }
    });

}

function get_producer_type_series(obj) {
    $("#adviserTypeS div").removeClass("btn-primary").addClass("btn-warning");
    $(obj).removeClass("btn-warning").addClass("btn-primary");
    var producer_type = $(obj).attr("btn_type");
    var url = $("#producerCarSeriesUrl").val();
    var producer_id = $("#producerId").val();
    $("#adviserType").val(producer_type);
    // $("#no_bind_series").empty();
    var chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    console.log(chars.length);
    for (var i = 0; i < chars.length; i++) {
        console.log('sdfffff');
        $("#" + chars[i] + "_pos").empty();
    }
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "producer_id": producer_id,
            "producer_series_type": producer_type
        },
        success: function (data) {
            var other_series = data[1];
            if (other_series.length != 0) {
                for (var j = 0; j < other_series.length; j++) {
                    var t = "<div style='float:left;margin-left: 15px;'><input type='checkbox' name='other_series' " +
                        "value='" + other_series[j]["series_id"] + "'>" + other_series[j]["series_name"] + "</div>";
                    $("#" + other_series[j]['initial'] + "_pos").append(t);
                    $("#" + other_series[j]['initial'] + "_all").attr('style', "display:block");
                }
            }
        }
    });
}


function cc_input(obj) {
    var cc = $(obj).val();
    if (cc == "-1") {
        $("#manuCC").show();
        $("#carCC").removeAttr("disabled");
    } else {
        $("#manuCC").hide();
        $("#carCC").attr("disabled", "disabled");
    }
}


function checkall_year() {
    $("input[name=upseriesYear]").prop("checked", $("#checkall_year_o").prop("checked"));
}
checkall_year();

function reset_user_pwd(user_id) {
    $.ajax({
        url: "/background/user/users/pwd/reset/",
        dataType: "json",
        method: "GET",
        data: {"admin_id": user_id},
        success: function (data) {
            if (data["res"] == "success") {
                alert("初始化经销商密码成功，原始密码为 123456, 请悉知!");
                return;
            } else {
                alert("重置密码失败!");
                return;
            }
        }
    });
}

function reset_admin_pwd(admin_id) {
    $.ajax({
        url: "/background/user/admin/pwd/reset/",
        dataType: "json",
        method: "GET",
        data: {"admin_id": admin_id},
        success: function (data) {
            if (data["res"] == "success") {
                alert("初始化经销商密码成功，原始密码为 123456, 请悉知!");
                return;
            } else {
                alert("重置密码失败!");
                return;
            }
        }
    });
}

function reset_producer_pwd(producer_id) {
    $.ajax({
        url: "/background/user/producer/password/reset/",
        dataType: "json",
        method: "GET",
        data: {"producer_id": producer_id},
        success: function (data) {
            if (data["res"] == "success") {
                alert("初始化经销商密码成功，原始密码为 123456, 请悉知!");
                return;
            } else {
                alert("重置密码失败!");
                return;
            }
        }
    });
}

/*
 function pkg_detail(obj) {
 $("#pkgList a").removeClass("btn-primary").addClass("btn-warning");
 $(obj).removeClass("btn-warning").addClass("btn-primary");
 var _url = $(obj).attr("url");
 $.ajax({
 url: _url,
 method: "GET",
 dataType: "html",
 success: function(data){
 $("#projDetail").html(data);
 }
 });
 }
 */

function pkg_detail() {
    var _url = $('#upkeepQuoteAjaxUrl').val();
    $.ajax({
        url: _url,
        method: "GET",
        data: {'type_id': $('#typeId').val()},
        dataType: "html",
        async: false,
        success: function (data) {
            $("#projDetail").html(data);
        }
    });
}

function subUpkeepQuote(obj) {
    var form = $(obj).parent().parent();
    var pkg_id = form.attr('model');
    var data = '';
    form.parent().find("tr[class='model_" + pkg_id + "']").each(function () {
        var model_obj = $(".model_" + pkg_id);
        var sale_labor_price = $(this).find('input[name=sale_labor_price]').val();
        var sale_material_price = $(this).find('input[name=sale_material_price]').val();
        var upkeepId = $(this).find('input[name=upkeepId]').val();
        var mtype = $(this).find('input[name=materialType]').val();
        var model_id = $(this).find('input[name=modelId]').val();
        // var pkgId = $("#pkgId").val();

        //1 材料型号id； 2 项目id; 3 材料类型id, G 通用；O 原厂; 4 工时优惠价 5 材料优惠价
        var pdata = model_id + "," + upkeepId + "," + mtype + "," + sale_labor_price + "," + sale_material_price;
        data += pdata + "#";
    });
    console.log(data);
    // return false;
    var _url = $("#upkeepQuoteActionUrl").val();
    $.ajax({
        url: _url,
        method: "POST",
        dataType: "json",
        data: {
            "model_data": data,
            "pkgId": pkg_id,
        },
        success: function (data) {
            if (data['res'] == "success") {
                alert("报价成功!");
                var jump_url = $('#upkeepFixQuoteIndex').val();
                window.location.href = jump_url;
                //return false;
            } else {
                alert("报价失败!");
                console.log(data['msg']);
                return false;
            }
        }
    });
}

function subUpkeepQuotebanjin(obj) {
    var form = $(obj).parent().parent();
    var pkg_id = form.attr('banjin');
    var data = '';
    console.log(pkg_id);
    var i = 1;
    var sale_price_low = '';
    var sale_price_middle = '';
    var sale_price_high = '';
    form.parent().find("tr[class='banjin_" + pkg_id + "']").each(function () {
        var model_obj = $(".banjin_" + $(this).val());
        sale_price_low = $(this).find('input[name=sale_price_low]').val() || sale_price_low;
        sale_price_middle = $(this).find('input[name=sale_price_middle]').val() || sale_price_middle;
        sale_price_high = $(this).find('input[name=sale_price_high]').val() || sale_price_high;
        var upkeepId = $(this).find('input[name=banjinUpkeepId]').val();
        if (i % 3 == 0) {
            //1 低端优惠价；2 中端优惠价； 3 高端优惠价; 4 项目id;
            var pdata = sale_price_low + "," + sale_price_middle + "," + sale_price_high + "," + upkeepId;
            data += pdata + "#";
        }
        i += 1;
    });
    console.log(data);
    // return false;
    var _url = $("#banjinQuoteActionUrl").val();
    $.ajax({
        url: _url,
        method: "POST",
        dataType: "json",
        data: {
            "items": form.serialize(),
            "model_data": data,
            "upkeepSeriesId": $("#upkeepSeriesId").val(),
            "pkgId": $("#pkgId").val()
        },
        success: function (data) {
            if (data['res'] == "success") {
                alert("报价成功!");
                var jump_url = $('#upkeepPaintQuoteIndex').val();
                window.location.href = jump_url;
                //return false;
            } else {
                alert("报价失败!");
                console.log(data['msg']);
                return false;
            }
        }
    });
}

function validate_username() {
    var username = $("#username").val();
    var user_reg = /^[a-zA-Z0-9_-]{1,}$/;

    if (!user_reg.test(username)) {
        alert("用户名只能包含数字，字母，_， -");
        return false;
    }
    var res = "";
    var _url = $("#nameValidateUrl").val();
    $.ajax({
        url: _url,
        method: "GET",
        async: false,
        dataType: "text",
        data: {
            "user_name": username,
            "action": "edit",
            "id": $("#user_id").val()
        },
        success: function (data) {
            res = data;
        }
    });
    if (res == "exists") {
        alert("相同用户名系统中已存在");
        return false;
    }
    if (res == "error") {
        alert("用户名验证失败");
        return false;
    }
    if (res == "no") {
        return true;
    }
}

function brand_set_pc(brand_id) {
    $.ajax({
        url: $("#setPc").val(),
        method: "GET",
        dataType: "json",
        data: {"brandId": brand_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置pc成功");
                window.location.href = $("#brandTwoIndex").val();
            } else {
                alert("设置pc失败");
            }
        }
    });
}

function brand_set_app(brand_id) {
    $.ajax({
        url: $("#setApp").val(),
        method: "GET",
        dataType: "json",
        data: {"brandId": brand_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置app成功");
                window.location.href = $("#brandTwoIndex").val();
            } else {
                alert("设置app失败");
            }
        }
    });
}

function series_set_pc(series_id, brand_id, adjuge) {
    $.ajax({
        url: $("#seriesSetPc").val(),
        method: "GET",
        dataType: "json",
        data: {"series_id": series_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置pc成功");
                if (adjuge) {
                    window.location.href = $("#seriesIndex").val() + "&brandId=" + brand_id;
                } else {
                    window.location.href = $("#seriesIndex").val();
                }
            } else {
                alert("设置pc失败");
            }
        }
    });
}

function series_set_app(series_id, brand_id, adjuge) {
    $.ajax({
        url: $("#seriesSetApp").val(),
        method: "GET",
        dataType: "json",
        data: {"series_id": series_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置app成功");
                if (adjuge == 1) {
                    window.location.href = $("#seriesIndex").val() + "&brandId=" + brand_id;
                } else {
                    window.location.href = $("#seriesIndex").val();
                }
            } else {
                alert("设置app失败");
            }
        }
    });
}

function series_set_ad8(series_id, brand_id, adjuge) {
    $.ajax({
        url: $("#seriesSetAd8").val(),
        method: "GET",
        dataType: "json",
        data: {"series_id": series_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置广告栏成功");
                if (adjuge) {
                    window.location.href = $("#seriesIndex").val() + "&brandId=" + brand_id;
                } else {
                    window.location.href = $("#seriesIndex").val();
                }
            } else if (data['res'] == 'enough') {
                alert("广告栏仅限八个");
            } else {
                alert("设置广告栏失败");
            }
        }
    });
}

function series_set_un(series_id, brand_id, adjuge) {
    $.ajax({
        url: $("#seriesSetun").val(),
        method: "GET",
        dataType: "json",
        data: {"series_id": series_id},
        success: function (data) {
            if (data['res'] == "success") {
                alert("设置车型推送成功");
                if (adjuge) {
                    window.location.href = $("#seriesIndex").val() + "&brandId=" + brand_id;
                } else {
                    window.location.href = $("#seriesIndex").val();
                }
            } else if (data['res'] == 'exist') {
                alert("车型推送已更新");
                window.location.href = $("#seriesIndex").val();
            }
            else {
                alert("设置车型推送失败");
            }
        }
    });
}

/*

 function series_set_newcar(series_id) {
 $.ajax({
 url: $("#seriesSetnewcar").val(),
 method: "GET",
 dataType: "json",
 data: {"series_id": series_id},
 success: function(data){
 if(data['res'] == "success"){
 alert("设置新车推送成功");
 window.location.href = $("#seriesIndex").val();
 }else if(data['data'] == "enough"){
 alert("新车栏位仅限五个");
 }
 else{
 alert("设置新车推送失败");
 }
 }
 });
 }
 */

function check_all_year() {
    if ($("#checkAllYear").prop("checked")) {
        $("input[name=manageProduceYear]").each(function () {
            $(this).prop("checked", true);
        });
    } else {
        $("input[name=manageProduceYear]").each(function () {
            $(this).prop("checked", false);
        });
    }
}

function article_preview(o) {
    $("#myModalContent").empty();
    $("#myModalContent").empty();
    $.ajax({
        url: $(o).attr("href"),
        method: "GET",
        dataType: "json",
        success: function (data) {
            $("#myModalLabel").html(data.title + "<br/>");
            var author_info = "";
            if (data.created_type == 'o') {
                author_info = "<span style='font-size: 14px;'> 慧车 作者 " + data.author + " " + data.created_at + "</span>";
            } else {
                author_info = "<span style='font-size: 14px;'>" + data.reprint_addr + " 作者 转载 " + data.created_at + "</span>";
            }
            $("#myModalLabel").append(author_info);
            $("#myModalContent").html(data.content);
            $("#myModal").modal("show");
        },
        error: function (data) {
            console.debug(data);
            $("#myModalLabel").html("文章预览失败！");
            $("#myModalContent").html("文章预览失败！");
            $("#myModal").modal("show");
        }
    });

    return false;
}

function get_initinal_brand(initial) {
    /* var inital = $("#brandInitinal").val();
     $("#upSeries").empty();
     $("#upSeriesBrand").empty();
     $.ajax({
     url: $("#initinalBrand").val(),
     method: "GET",
     dataType: "json",
     data: {"brand_initial": inital},
     success: function (data) {
     console.log(data['brand_list']);
     if ($.isEmptyObject(data)) {
     return false;
     }
     var brand_list = data['brand_list'];
     var series_obj = data['series'];
     for(var i=0; i < brand_list.length; i++){
     var t = "<option value='"+brand_list[i]['id']+"'>"+brand_list[i]["name"]+"</option>";
     $("#upSeriesBrand").append(t);
     }
     for (var i = 0; i < series_obj.length; i++) {
     var t = "<option value='" + series_obj[i]["s_id"] + "'>" + series_obj[i]["name"] + "</option>";
     $("#upSeries").append(t);
     }
     }
     });*/

    $("#maSeriesBrand").empty();
    $("#maSeries").empty();
    $("#maSeriesTopBrand").empty();
    var url = $("#managesecondBrandsUrl").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"initial": initial},
        success: function (data) {
            var brand_data = data['brand_data'];
            var series_data = data['series_data'];
            var top_brands = data['top_brands'];
            //一级品牌
            for (var i = 0; i < top_brands.length; i++) {
                var t = "<option value='" + top_brands[i]["id"] + "'>" + top_brands[i]["name"] + "</option>";
                $("#maSeriesTopBrand").append(t);
            }
            //二级品牌
            for (var i = 0; i < brand_data.length; i++) {
                var t = "<option value='" + brand_data[i]["id"] + "'>" + brand_data[i]["name"] + "</option>";
                $("#maSeriesBrand").append(t);
            }
            for (var i = 0; i < series_data.length; i++) {
                var t = "<option value='" + series_data[i]["id"] + "'>" + series_data[i]["name"] + "</option>";
                $("#maSeries").append(t);
            }
        }
    });
}

function get_upkeep_m_list() {
    var up_id = $("#upkeep_proj_for_material").val();
    window.location.href = "/background/upkeep/material/?SecondMenu=upMaterialManage&up_id=" + up_id;
}


function select_data_by_brand_initial(initial) {
    $("#upSeriesBrand").empty();
    $("#upSeries").empty();
    $("#carYear").empty();
    $("#carCc").empty();
    $("#upSeriesTopBrand").empty();
    var select_all_year = "<div id='checkall_year' onclick='checkall_year()'>全选<input type='checkbox' name='checkall_year' id='checkall_year_o'/></div>";

    var url = $("#secondBrandsUrl").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "initial": initial,
            "upseries": true
        },
        success: function (data) {
            var brand_data = data['brand_data'];
            var series_data = data['series_data'];
            var series_param_obj = data['cc_year_data'];
            var top_brands = data['top_brands'];
            //一级品牌
            for (var i = 0; i < top_brands.length; i++) {
                var t = "<option value='" + top_brands[i]["id"] + "'>" + top_brands[i]["name"] + "</option>";
                $("#upSeriesTopBrand").append(t);
            }
            //二级品牌
            for (var i = 0; i < brand_data.length; i++) {
                var t = "<option value='" + brand_data[i]["id"] + "'>" + brand_data[i]["name"] + "</option>";
                $("#upSeriesBrand").append(t);
            }
            for (var i = 0; i < series_data.length; i++) {
                var t = "<option value='" + series_data[i]["id"] + "'>" + series_data[i]["name"] + "</option>";
                $("#upSeries").append(t);
            }

            //year
            var car_obj = series_param_obj["year_data"];
            for (var j = 0; j < car_obj.length; j++) {
                var txt = "<input type='checkbox' name='upseriesYear' value='" + car_obj[j] + "'>" + car_obj[j];
                $("#carYear").append(txt);
            }
            $("#carYear").prepend(select_all_year);
            var cc_obj = series_param_obj["cc_data"];
            for (var k = 0; k < cc_obj.length; k++) {
                var tx = "<option value='" + cc_obj[k] + "'>" + cc_obj[k] + "</option>";
                $("#carCc").append(tx);
            }
        }
    });
}

function model_data_by_brand_initial(initial) {
    var _url = $("#modelTopBrandUrl").val();
    var all = '<option value="-1">全部品牌</option>';
    $("#upSeriesTopBrand").empty();
    $("#modelSeriesPlace").empty();
    $("#upSeriesBrand").empty();
    $("#upSeriesBrand").append(all);
    $("#upSeries").val("-1");
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "json",
        data: {"initial": initial},
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var t = "<option value='" + data[i]['id'] + "'>" + data[i]['name'] + "</option>";
                $("#upSeriesTopBrand").append(t);
            }
            $("#upSeriesTopBrand").append(all);
        }
    });
}

$(document).ready(function () {
    var top_menu = $("#TopMenu").val();
    var second_menu = $("#SecondMenu").val();
    var third_menu = $("#ThirdMenu").val();
    if (top_menu != "") {
        $("[class*=top_menu]").each(function () {
            $(this).removeClass("active");
        });
        $("#" + top_menu).addClass("active");
    }
    if (second_menu != "") {
        $("[class*=second-menu]").each(function () {
            $(this).removeClass("active2");
        });
        $("#" + second_menu).addClass("active2");
    }

    if (third_menu != "") {
        $("[class*=third-menu]").each(function () {
            $(this).removeClass("active3");
        });
        $("#" + third_menu).addClass("active3");
        $("#" + third_menu + " a").attr("style", "color: #f1f1f1");
    }

    $("#originalImg").click(function () {
        if ($("#parentLogo").prop("checked")) {
            $("#tbrandImg").attr("disabled", "disabled");
        } else {
            $("#tbrandImg").removeAttr("disabled");
        }
    });

    function birthdaydate() {

        var str1, str2, str3, year_num, month_num, day_num;
        var birthday_date = $('#birthday_get').val().split(" ")[0];
        year_num = birthday_date.split("-")[0] || 2016;
        month_num = birthday_date.split("-")[1] || 1;
        day_num = birthday_date.split("-")[2] || 1;
        str1 = str2 = str3 = "";
        for (var i = 1950; i < 2050; i++) {
            if (i == year_num)
                str1 += "<option value=" + i + " selected='selected'>" + i + "</option>";
            else
                str1 += "<option value=" + i + ">" + i + "</option>";
        }
        for (var j = 1; j < 13; j++) {
            if (j == month_num)
                str2 += "<option value=" + j + " selected='selected'>" + j + "</option>";
            else
                str2 += "<option value=" + j + ">" + j + "</option>";
        }
        for (var k = 1; k < 32; k++) {
            if (k == day_num)
                str3 += "<option value=" + k + " selected='selected'>" + k + "</option>";
            else
                str3 += "<option value=" + k + ">" + k + "</option>";
        }
        $('#year').html(str1);
        $('#month').html(str2);
        $('#day').html(str3);

        $('#birthday').html("<input type='hidden' name='birthday' value=" + year_num + "-" + month_num + "-" + day_num + ">");

        $('#year,#month,#day').bind('focus change', function () {
            var day29 = $('#day :nth-child(29)').html();
            var day30 = $('#day :nth-child(30)').html();
            var day31 = $('#day :nth-child(31)').html();
            if (($('#month').val()) == 2) {
                if (($('#year').val() % 4) == 0) {
                    $('#day').children('option[value="30"]').remove();
                    $('#day').children('option[value="31"]').remove();
                    if (!day29)
                        $('#day').append("<option value='29'>29</option>");
                }
                else {
                    $('#day').children('option[value="29"]').remove();
                    $('#day').children('option[value="30"]').remove();
                    $('#day').children('option[value="31"]').remove();
                }
            }
            else {
                if (($('#month').val()) == 4 || ($('#month').val()) == 6 || ($('#month').val()) == 9 || ($('#month').val()) == 11) {
                    if (!day29)
                        $('#day').append("<option value='29'>29</option>");
                    if (!day30)
                        $('#day').append("<option value='30'>30</option>");
                    $('#day').children('option[value=31]').remove();

                }
                else {
                    if (!day29)
                        $('#day').append("<option value='29'>29</option>");
                    if (!day30)
                        $('#day').append("<option value='30'>30</option>");
                    if (!day31) {
                        $('#day').append("<option value='31'>31</option>");
                    }

                }
            }
            var year = $('#year').val();
            var month = $('#month').val();
            var day = $('#day').val();
            $('#birthday').html("<input type='hidden' name='birthday' value=" + year + "-" + month + "-" + day + ">");
        });

    };
});


function birthday_date() {

    var str1, str2, str3, year_num, month_num, day_num;
    var birthday_date = $('#birthday_get').val().split(" ")[0];
    year_num = birthday_date.split("-")[0] || 2016;
    month_num = birthday_date.split("-")[1] || 1;
    day_num = birthday_date.split("-")[2] || 1;
    str1 = str2 = str3 = "";
    for (var i = 1950; i < 2050; i++) {
        if (i == year_num)
            str1 += "<option value=" + i + " selected='selected'>" + i + "</option>";
        else
            str1 += "<option value=" + i + ">" + i + "</option>";
    }
    for (var j = 1; j < 13; j++) {
        if (j == month_num)
            str2 += "<option value=" + j + " selected='selected'>" + j + "</option>";
        else
            str2 += "<option value=" + j + ">" + j + "</option>";
    }
    for (var k = 1; k < 32; k++) {
        if (k == day_num)
            str3 += "<option value=" + k + " selected='selected'>" + k + "</option>";
        else
            str3 += "<option value=" + k + ">" + k + "</option>";
    }
    $('#year').html(str1);
    $('#month').html(str2);
    $('#day').html(str3);

    $('#birthday').html("<input type='hidden' name='birthday' value=" + year_num + "-" + month_num + "-" + day_num + ">");

    $('#year,#month,#day').bind('focus change', function () {
        var day29 = $('#day :nth-child(29)').html();
        var day30 = $('#day :nth-child(30)').html();
        var day31 = $('#day :nth-child(31)').html();
        if (($('#month').val()) == 2) {
            if (($('#year').val() % 4) == 0) {
                $('#day').children('option[value="30"]').remove();
                $('#day').children('option[value="31"]').remove();
                if (!day29)
                    $('#day').append("<option value='29'>29</option>");
            }
            else {
                $('#day').children('option[value="29"]').remove();
                $('#day').children('option[value="30"]').remove();
                $('#day').children('option[value="31"]').remove();
            }
        }
        else {
            if (($('#month').val()) == 4 || ($('#month').val()) == 6 || ($('#month').val()) == 9 || ($('#month').val()) == 11) {
                if (!day29)
                    $('#day').append("<option value='29'>29</option>");
                if (!day30)
                    $('#day').append("<option value='30'>30</option>");
                $('#day').children('option[value=31]').remove();

            }
            else {
                if (!day29)
                    $('#day').append("<option value='29'>29</option>");
                if (!day30)
                    $('#day').append("<option value='30'>30</option>");
                if (!day31) {
                    $('#day').append("<option value='31'>31</option>");
                }

            }
        }
        var year = $('#year').val();
        var month = $('#month').val();
        var day = $('#day').val();
        $('#birthday').html("<input type='hidden' name='birthday' value=" + year + "-" + month + "-" + day + ">");
    });
};


function selectArticleStyle() {
    var style_name = $("#articleStyle").val();
    console.log(style_name);
    // if(style_name == "0"){
    //     $("#editor").attr("style", "display:block;margin-top:10px;");
    //     $("#picContent").attr("style", "display:none;");
    //     $("#videoContent").attr("style", "display:none;");
    // }
    // if(style_name == "1"){
    //     $("#editor").attr("style", "display:none;margin-top:10px;");
    //     $("#picContent").attr("style", "display:block;");
    //     $("#videoContent").attr("style", "display:none;");
    // }
    if (style_name == '2') {
        $("#editor").attr("style", "display:none;margin-top:10px;");
        $("#picContent").attr("style", "display:none;");
        $("#videoContent").attr("style", "display:block;");
    }
}

function selectArticleType() {
    var at = $("#arType").val();
    if (at == "r") {
        $("#reprint_addr").removeAttr("style");
        $("#reprint_addr").attr("style", "display:block");
    } else {
        $("#reprint_addr").removeAttr("style");
        $("#reprint_addr").attr("style", "display:none;");
    }
}


function get_province_city() {
    var _url = $("#provinceCityUrl").val();
    var province_name = $("#materialModelDefaultProvince").val();
    if (province_name == "全国") {
        province_name = '-1';
    }
    $("#materialModelDefaultCity").empty();
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "json",
        data: {"province_name": province_name},
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var city;
                if (data[i]['city'] == '-1') {
                    city = "全国";
                } else {
                    city = data[i]['city'];
                }
                var t = "<option value='" + data[i]['id'] + "'>" + city + "</option>";
                $("#materialModelDefaultCity").append(t);
            }
        }
    });

}


function get_special_series(obj) {
    var level_id = $(obj).val();
    var _url = $("#specialSeriesUrl").val();
    if ($(obj).prop("checked")) {
        if (level_id == "5") {
            if (!$('input[name="avaLevel"]').eq(0).prop('checked')) {
                $('#chosenSeries').show();
                if ($('#secBrand').val() != -1) {
                    $('#addSeriesBox').show();
                } else {
                    $('#addSeriesBox').hide();
                }
            }
        }
        if (level_id == "6") {
            $("#chosenBrands").css("display", "block");
        }
    } else {
        $('#chosenSeries').hide();
        $("#chosenBrands").css("display", "none");
    }
    if (level_id == '5' && !$(obj).prop("checked")) {
        $("#specialSeriesPlace").empty();
    }
}


function model_second_series(obj) {
    $("#modelSeriesPlace").empty();
    $("#upModelSeries").val("-1");
}


function select_second_brands_model1(obj) {
    var brand_id = $(obj).val();
    $("#modelSeriesPlace").empty();
    $(".upSeriesModelBrand").empty();
    var all_second_brand = "<option value='-1'>全部品牌</option>";
    var parent_obj = $(obj).parent().parent();

    $(".upSeriesModelBrand").append(all_second_brand);
    $("#upModelSeries").val("-1");
    var url = $("#secondBrandsUrl").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {
            "brand_id": brand_id,
            "model": true
        },
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var t = "<option value='" + data[i]["id"] + "'>" + data[i]["name"] + "</option>";
                parent_obj.find(".upSeriesModelBrand").append(t);
                // $("#upSeriesModelBrand").append(t);
            }
        }
    });
}


function model_second_series1(obj) {
    $("#modelSeriesPlace").empty();
    $("#upModelSeries").val("-1");
}

function model_brand_all_series1(obj) {
    var _url = $("#modelBrandData").val();
    var parent_obj = $(obj).parent().parent();

    var brand_id = parent_obj.find(".upSeriesModelBrand").val();
    console.log(brand_id);
    parent_obj.find(".modelSeriesPlace").empty();
    if (brand_id != '-1') {
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"brand_id": brand_id},
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var t = "<input style='margin-left: 10px;' type='checkbox' name='modelSeries' value='" + data[i]['id'] + "'>" + data[i]['name'];
                    parent_obj.find(".modelSeriesPlace").append(t);
                }
            }
        });
    }
}


function model_brand_all_series2() {
    var _url = $("#modelBrandData").val();
    var brand_id = $("#upSeriesModelBrand").val();
    $("#modelSeriesPlace").empty();
    if (brand_id != '-1') {
        $.ajax({
            url: _url,
            method: "GET",
            dataType: "json",
            data: {"brand_id": brand_id},
            success: function (data) {
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var t = "<input style='margin-left: 10px;' type='checkbox' name='modelSeries' value='" + data[i]['id'] + "'>" + data[i]['name'];
                    $("#modelSeriesPlace").append(t);
                }
            }
        });
    }
}

function get_relation_series() {
    var _url = $("#relationDataUrl").val();
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "html",
        success: function (data) {
            $("#relationSeries").append(data);
        }
    })
}

function related_series_ajax(initial, topbrandid, secbrandid) {
    if ($('#relatedSeries option:eq(0)').val()) {
        var txt = $('#relatedSeries option:eq(0)').val();
        for (var i = 1; i < $('#relatedSeries').children().length; i++) {
            txt += ',' + $('#relatedSeries option:eq(' + i + ')').val();
        }
        $('#relatedfor').attr({existseries: txt});
    }
    else {
        $('#relatedfor').attr({existseries: ''});
    }
    $.ajax({
        url: $('#relatedSeriesUrl').val(),
        method: 'GET',
        type: 'json',
        data: {
            'initial': initial,
            'topbrand_id': topbrandid,
            'secbrand_id': secbrandid,
            'existseries_id': $('#relatedfor').attr('existseries')
        },
        success: function (data) {
            console.log(data);
            if (data['topbrand']) {
                $('#topBrand').empty();
                for (var i = 0; i < data['topbrand'].length; i++) {
                    var t = "<option value='" + data['topbrand'][i]['id'] + "'>" + data['topbrand'][i]['name'] + "</option>";
                    $('#topBrand').append(t);
                }
            }
            if (data['secbrand']) {
                $("#secBrand").empty();
                for (var j = 0; j < data['secbrand'].length; j++) {
                    var tx = "<option value='" + data['secbrand'][j]['id'] + "'>" + data['secbrand'][j]['name'] + "</option>";
                    $("#secBrand").append(tx);
                }
            }
            if (data['series']) {
                $('#addSeries').empty();
                for (var k = 0; k < data['series'].length; k++) {
                    var txt = "<option value='" + data['series'][k]['id'] + "'>" + data['series'][k]['name'] + "</option>";
                    $('#addSeries').append(txt);
                }
            }
        }
    })
}


function upkeep_bind_series_ajax(initial, topbrandid, secbrandid) {
    if ($('#bindFor option:eq(0)').val()) {
        var txt = $('#bindFor option:eq(0)').val();
        for (var i = 1; i < $('#bindFor').children().length; i++) {
            txt += ',' + $('#bindFor option:eq(' + i + ')').val();
        }
        $('#bindSeries').attr({existseries: txt});
        console.log(txt)
    }
    else {
        $('#bindSeries').attr({existseries: ''});
    }
    $.ajax({
        url: $('#bindSeriesChoiceUrl').val(),
        method: 'GET',
        type: 'json',
        data: {
            'initial': initial,
            'topbrand_id': topbrandid,
            'secbrand_id': secbrandid,
            'existseries_id': $('#bindSeries').attr('existseries')
        },
        success: function (data) {
            if (data['topbrand']) {
                $('#topBrand').empty();
                for (var i = 0; i < data['topbrand'].length; i++) {
                    var t = "<option value='" + data['topbrand'][i]['id'] + "'>" + data['topbrand'][i]['name'] + "</option>";
                    $('#topBrand').append(t);
                }
            }
            if (data['secbrand']) {
                $("#secBrand").empty();
                for (var j = 0; j < data['secbrand'].length; j++) {
                    var tx = "<option value='" + data['secbrand'][j]['id'] + "'>" + data['secbrand'][j]['name'] + "</option>";
                    $("#secBrand").append(tx);
                }
                $('#secBrand').prepend("<option value='-1'>所有品牌</option>");
            }
            if (data['series']) {
                $('#addSeries').empty();
                for (var k = 0; k < data['series'].length; k++) {
                    var txt = "<option value='" + data['series'][k]['id'] + "'>" + data['series'][k]['name'] + "</option>";
                    $('#addSeries').append(txt);
                }
            }
        }
    })
}

//用于车型新增/编辑页面，关联车型
function related_series_by_brand_initial(initial) {
    $('#topBrand').empty();
    $('#secBrand').empty();
    $('#addSeries').empty();
    related_series_ajax(initial, null, null);
}

function related_series_by_topbrand() {
    $('#secBrand').empty();
    $('#addSeries').empty();
    related_series_ajax(null, $('#topBrand').val(), null);
}
function related_series_by_secbrand() {
    $('#addSeries').empty();
    related_series_ajax(null, null, $('#secBrand').val());
}


//用于材料型号新增/绑定页面 车型绑定
function bind_series_by_brand_initial(initial) {
    $('#topBrand').empty();
    $('#secBrand').empty();
    $('#addSeries').empty();
    upkeep_bind_series_ajax(initial, null, null);
}

function bind_series_by_topbrand() {
    $('#secBrand').empty();
    $('#addSeries').empty();
    upkeep_bind_series_ajax(null, $('#topBrand').val(), null);
}
function bind_series_by_secbrand() {
    if ($('#secBrand').val() == -1 || $('#allAndChosen').val() == -1) {
        upkeep_bind_series_ajax(null, null, $('#secBrand').val());
        $('#addSeriesBox').hide();
    }
    else {
        $('#addSeriesBox').show();
        $('#addSeries').empty();
        upkeep_bind_series_ajax(null, null, $('#secBrand').val());
    }
}


function get_producer_adviser(clue_id) {
    var _url = $("#producerAdvisers").val();
    $.ajax({
        url: _url,
        method: "GET",
        dataType: "json",
        success: function (data) {
            var t;
            for (var i = 0; i < data.length; i++) {
                t = "<option value='" + data[i]["id"] + "'>" + data[i]['name'] + "</option>";
                $("#producerAdviser").append(t);
            }
        }
    });
    $("#clueId").val(clue_id);
    $("#adviserModel").modal("show");
}


function judge_all_and_chosen() {
    if ($('#secBrand').val() == -1 || $('#allAndChosen').val() == -1) {
        $('#addSeriesBox').hide();
    } else {
        $('#addSeriesBox').show();
    }
}

function bind_brand_by_initinal(initinal){
    var url = $("#topBrandByInitial").val();
    $.ajax({
        url: url,
        method: "get",
        dataType: "json",
        data: {"initial": initinal},
        success: function(data){
            $("#topBrand1").empty();
            for(var i=0; i<data.length; i++){
                var t = "<option value='"+data[i]['id']+"'>"+data[i]['name']+"</option>";
                $("#topBrand1").append(t);
            }
            if(data.length != 0){
                bind_brand_by_topbrand1(data[0]['id']);
            }
        }
    })
}


function bind_brand_by_topbrand(obj){
    var topBrandId = $(obj).val();

    var url = $("#secondBrandByTopBrand").val();
    $.ajax({
        url: url,
        data: {"brand_id": topBrandId},
        method: "get",
        dataType: "json",
        success: function(data){
            $("#addBrands").empty();
            for(var i=0;i<data.length;i++){
                var t = "<option value='"+data[i]['id']+"'>"+data[i]['name']+"</option>";
                 $("#addBrands").append(t);
            }
        }
    })
}

function bind_brand_by_topbrand1(obj){
    var url = $("#secondBrandByTopBrand").val();
    $.ajax({
        url: url,
        data: {"brand_id": obj},
        method: "get",
        dataType: "json",
        success: function(data){
            $("#addBrands").empty();
            for(var i=0;i<data.length;i++){
                var t = "<option value='"+data[i]['id']+"'>"+data[i]['name']+"</option>";
                 $("#addBrands").append(t);
            }
        }
    })
}


function write_in_series_kind() {
    var data_json_str =$("#series_kind_value").val();
    if(data_json_str != ""){
        var data_json = JSON.parse(data_json_str);
        var text = "";
        var chosen_kind =  parseInt($("#series_kind_parent").val());
        for(var i=0;i<data_json.length; i++)    {
            var data = data_json[i];
            console.log(data['id'] + chosen_kind);
            if (data['id'] == chosen_kind){
                var child_data_arr = data['kind'];
                for(var j=0;j<child_data_arr.length; j++) {
                    var child_data = child_data_arr[j];
                    var txt = "<option value='" + child_data['id'] + "'> " + child_data['name'] + "</option>";
                    text += txt;
                }
            }
        }
        console.log(text);
        $("#series_kind").html(text);
    }
}




// 促销页面 --- 确认单个颜色编辑窗口内容更改
function ensure_car_color(obj) {
    var a_id = $(obj).attr("bind_id");
    var parent_div = $(obj).parent().parent();
    var checkbox_arr = parent_div.find(".single_color");
    var txt = "";
    // console.log(checkbox_arr);
    for(var i=0; i<checkbox_arr.length; i++){
        var checkbox = checkbox_arr[i];
        if($(checkbox).prop("checked")){
            var id = $(checkbox).val();
            txt += id + ","
        }
    }
    // console.log(txt);
    if(txt != ""){
        $("#" + a_id).popover("hide");
        var text = txt.substring(0, txt.length-1);
        // console.log(text);
        var data_str = $("#color_json").val();
        var data = JSON.parse(data_str);
        $("#" + a_id).attr("chosen_color", text);
        $("#" + a_id).attr("data-content", change_popover_html("#" + a_id, data));

        var control_box = parent_div.find(".all_color");
        var a_parent = $("#" + a_id).parent().parent();
        var mark = a_parent.find(".show-color");
        var character = a_parent.find(".single_car_all_color");
        single_car_color_popover("#" + a_id, data);
        if(control_box.prop("checked")){
            mark.hide();
            character.show();
        }else{
            character.hide();
            mark.show();
        }
    }
}


// 促销页面 --- 计算促销价格
function caculate_car_price(obj_temp) {
    var obj = $(obj_temp);
    var _parent = obj.parents("tr");
    var origin = _parent.find(".origin_price");
    var finaly = _parent.find(".finally_price");
    var discount_price = obj.val();
    var origin_price = origin.text();
    finaly.text((origin_price-discount_price).toFixed(2));
}


// 促销页面 --- 展开图库
function show_pic_bank(obj) {
    var pic_div = $(obj).parent().parent();
    var pic_type = pic_div.attr("pic_type");
    var pic_id = pic_div.attr("pic_id");
    console.log(pic_type + "-----" + pic_id);
    // stop_default_event(obj.event)

    get_pic_page(series_id,1,1,0, true);
    get_pic_page(series_id,1,2,0, true);
    get_pic_page(series_id,1,3,0, true);
    get_pic_page(series_id,1,4,0, true);
    get_pic_page(series_id,1,5,0, true);
    get_pic_page(series_id,1,6,0, true);
    $("#pic_bank_model").modal('show');
    $("#pic_sure").attr("pic_type", pic_type);
    $("#pic_sure").attr("pic_id", pic_id);
    // var producer_id = $("#producer_id").val();
}

//促销页面 --- 获取车型图片: 车型id，分页第几页，车型图片类型, 图片个数， 是否需要设置页面分页参数
function get_pic_page(series_id, page, type, total, pic_div) {
    $.ajax({
        url:$("#pic_bank_url").val(),
        dataType:"json",
        method:"get",
        data:{
            "series_id":series_id,
            "page":page,
            "pic_type" :type,
            "pic_len": total,
            "page_size":12,
            "big_pic":$("#big_pic_val").val(),
            "small_pic":$("#small_pic_val").val()
        },
        success:function (data) {
            console.log(data);
            var pic_list = data['pic'];
            var text = "";
            for(var i=0;i<pic_list.length; i++){
                var pic_obj = pic_list[i];
                var txt = "<div class='col-sm-3' style='height: 110px; padding-bottom: 4px' onclick='choose_pic(this)'><a href='javascript:void(0)' class='thumbnail' style='height: 100%;'><img src='"
                    + pic_obj['path']+ "' style='height: inherit; width: 100%' pic_id='" + pic_obj['id'] + "'></a></div>";
                text += txt;
            }

            $("#pic_bank_type_" + type).html(text);
            if(pic_div){
                $("#page_div_" + type).paging({
                    totalPage: data['args']['last_page'],
                    totalSize: data['args']['total'],
                    callback: function(num) {
                        get_pic_page(series_id, num, type, data['args']['total'], false)
                    }
                })
            }

        }
    })
}




// 促销页面 --- 拼接车款信息结构
function make_up_car_string(){
    var tr_arr = $("#sale_car_table tr");
    var text = "";
    for(var i=0; i<tr_arr.length; i++){
        var tr_obj = tr_arr[i];
        var input_obj = $(tr_obj).find("input[id^='car_']");

        if(input_obj.prop("checked")){
            var car_id = input_obj.val();
            var origin_price = $(tr_obj).find(".origin_price").text();
            var sale_price = $(tr_obj).find(".car_discount_price").val();
            var finaly_price = $(tr_obj).find(".finally_price").text();
            var inventory = $(tr_obj).find(".car_inventory").val();
            var color = $(tr_obj).find(".edit-car-color").attr("chosen_color");
            var txt = car_id + "#" + origin_price + "#" +sale_price + "#" + finaly_price + "#" +inventory  + "#" +color;
            text += txt + "@"
        }
    }
    return text
}





// 促销页面 --- 单个颜色的编辑窗口 全选取消全选的编辑设置
function single_car_color_control(obj){
    var popover_div = $(obj).parent().parent();
    var color_list = popover_div.find(".single_color");
    if($(obj).prop("checked")){
        color_list.prop("checked", true)
    }else{
        color_list.prop("checked", false)
    }
}


//  促销页面 --- 单个颜色编辑窗口
function single_car_ati_color_control(obj) {
    var popover_div = $(obj).parent().parent();
    var all_control = popover_div.find(".all_color");
    if($(obj).prop("checked")){
        var input_div = $(obj).parent().siblings();
        var input_list = input_div.find(".single_color");
        var num = 0;
        for(var i=0; i<input_list.length; i++){
            var input_obj = input_list[i];
            if($(input_obj).prop("checked")){
                num += 1
            }
        }
        if(num == input_list.length){
            all_control.prop("checked", true)
        }
    }else{
        all_control.prop("checked", false)
    }
}


// 促销页面 --- 图库页面切换图片类型
function switch_pic_type(obj) {
    $(obj).addClass("active");
    $(obj).siblings().removeClass("active");
    var type = $(obj).attr("typeid");
    $("div[id^='pic_bank_type_']").hide();
    $("#pic_bank_type_" + type).show();

    $("div[id^='page_div_']").hide();
    $("#page_div_" + type).show();
    // console.log(type);
}



//促销页面 --- 选定更改图片
function choose_pic(obj){
    // $(obj).siblings().children(0).removeClass("active");
    $("div[id^='pic_bank_type_'] a").removeClass("active");
    $(obj).children(0).addClass("active");
    var img_obj = $(obj).children(0).children(0);
    var img_path = img_obj.prop("src");
    var img_id = img_obj.attr("pic_id");
    console.log(img_path + "     " + img_id);
    $("#chosen_pic").val(img_path);
    $("#chosen_pic").attr("pic_id", img_id);
}

// 促销页面 --- 更改图片，及图片相关的一系列的参数
function change_default_pic(obj) {
    var img_path = $("#chosen_pic").val();
    if(img_path){
        var pic_type = $(obj).attr("pic_type");
        var pic_id = $(obj).attr("pic_id");
        var change_img_obj = $("#" + pic_type + "_"+ pic_id);
        var change_img_id = $("#chosen_pic").attr("pic_id");
        if(pic_type == "big_pic"){
            $("#big_pic_val").val(change_img_id)
        }else {
            var origin_small_pic = $("#small_pic_val").val();
            var origin_pic_list = origin_small_pic.split(",").slice(0, -1);
            console.log("origin_pic_list:" + origin_pic_list);
            var new_pic_str = "";
            for (var i=0;i<origin_pic_list.length; i++){
                if(pic_id == origin_pic_list[i]){
                    new_pic_str += change_img_id + ","
                }else{
                    new_pic_str += origin_pic_list[i] + ","
                }
            }
            $("#small_pic_val").val(new_pic_str);
        }
        // 更改图片
        change_img_obj.prop("src", img_path);
        // 更改div图片id
        change_img_obj.parent().attr("pic_id", change_img_id);
        // 更改img标签id
        change_img_obj.prop("id", pic_type + "_" + change_img_id);
        $("#pic_bank_model").modal('hide');
    }
}
