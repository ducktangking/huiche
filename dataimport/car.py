# -*- coding: utf-8 -*-

import sys
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import xlrd
import MySQLdb
import time

"""
国别
德系	1
日系	2
韩系	3
美系	4
欧系	5
国产	6 (非日系 改)
法系	7 (增加)
"""

"""
排挡
4档	1
5档	2
6档	3
7档	4
8档	5
9档	6
无极变速  7 (增加)
"""

"""
变速箱
手动	1
半自动(AMT)	2
自动(AT)	3
手自一体	4
无极变速(CVT)	5
双离合(DSG)	6
电动车单速变速箱	7 (增加)
"""

"""
驱动
前置前驱	1 (改)
前置后驱	2 (改)
前置全时四驱	3 (改)
前置分时四驱	4 (改)
前置适时四驱	5 (改)
后置后驱	6 (增加)
后置四驱	7 (增加)
前置四驱	8 (增加)
中置后驱	9 (增加)
中置四驱	10 (增加)
"""

"""
排量范围选择
1.3L以下	1
1.3-1.6L	2
1.7-2L	3
2.1-3L	4
3.1-5L	5
5L以上	6
电动	7 (增加)
"""
"""
级别
微型车	1
小型车	2
紧凑型车	3
中型车	4
中大型车	5
豪华车	6
MPV	7
SUV	8
跑车	9
面包车	10
电动车	11
混动车	12
皮卡	13
进口车	14
轿车 15 (增加)
"""


def task():
    data = xlrd.open_workbook(sys.argv[1])
    table = data.sheet_by_index(0)
    conn = MySQLdb.connect(host='localhost', user='root', passwd='123qwe', db='huiche', port=3306, charset='utf8')
    cur = conn.cursor()
    sql_series = u"SELECT id FROM hibiscus_series WHERE name='%s'"
    sql = u"INSERT INTO hibiscus_car(body_id,discharge_id,door_id,energy_id,seat_id" \
          u",series_id,name,country_id,produce_year,real_cc,power," \
          u"price,price_min,price_max,stall_id,gearbox_id,drive_id,cc_id,grade_id,created_at) " \
          u"VALUES(1,6,1,1,1,%d,'%s',%d,'%s','%s','%s',%f,%f,%f,%d,%d,%d,%d,%d,'%s')"
    for i in range(3, table.nrows):
        initial = table.cell(i, 0).value
        brand = table.cell(i, 1).value
        country = table.cell(i, 2).value
        country = 6 if country == "" else country
        producer = table.cell(i, 3).value
        series = table.cell(i, 4).value
        produce_year = table.cell(i, 5).value
        real_cc = table.cell(i, 6).value
        power = table.cell(i, 7).value
        name = table.cell(i, 8).value
        price = table.cell(i, 9).value
        try:
            price = float(price)
        except Exception as e:
            price = 0.0
        stall = table.cell(i, 10).value
        stall = 10 if stall == "" else stall
        stall = int(stall)-3
        gearbox = table.cell(i, 11).value
        gearbox = 3 if gearbox == "" else gearbox
        drive = table.cell(i, 12).value
        drive = 1 if drive == "" else drive
        cc = table.cell(i, 13).value
        cc = 2 if cc == "" else cc
        grade = table.cell(i, 14).value
        grade = 15 if grade == "" else grade

        if produce_year != "" and real_cc != "" and power != "":
            try:
                ss_sql = sql_series % series.encode('utf8')
                print repr(ss_sql)
                nums = cur.execute(ss_sql)
                if nums:
                    series_id = cur.fetchone()
                    s_sql = sql % (series_id[0], name, country, produce_year, real_cc, power,
                                   price, price * 0.9, price * 1.1,
                                   stall, gearbox, drive, cc, grade, time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time.time())))
                    print repr(s_sql)
                    cur.execute(s_sql)
                    conn.commit()
                else:
                    print "Series not exist! car is %s" % series.encode('utf8')
            except Exception as e:
                print "Insert car failed! error is %s" % e

    cur.close()
    conn.close()


if __name__ == "__main__":
    task()
