BEGIN;
--
-- Create model GiftBag
--
CREATE TABLE `hibiscus_giftbag` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `car_good` bool NOT NULL, `gift_value` double precision NOT NULL, `created_at` datetime(6) NOT NULL);
--
-- Create model GiftBagType
--
CREATE TABLE `hibiscus_giftbagtype` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `gift_name` varchar(16) NOT NULL, `has_way` bool NOT NULL, `created_at` datetime(6) NOT NULL);
--
-- Create model GiftBagValue
--
CREATE TABLE `hibiscus_giftbagvalue` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `ways` integer NULL, `text_value` varchar(512) NULL, `created_at` datetime(6) NOT NULL, `gift_type_id` integer NOT NULL);
--
-- Create model ProducerPreSale
--
CREATE TABLE `hibiscus_producerpresale` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `sale_start` datetime(6) NOT NULL, `sale_end` datetime(6) NOT NULL, `sale_style` integer NOT NULL, `sale_num` double precision NOT NULL, `keep_up_in_buttom` bool NOT NULL, `company_adr_in_buttom` bool NOT NULL, `map_in_buttom` bool NOT NULL, `sale_tel_in_buttom` bool NOT NULL, `created_at` datetime(6) NOT NULL, `article_id` integer NOT NULL UNIQUE);
--
-- Create model SaleCarAndColor
--
CREATE TABLE `hibiscus_salecarandcolor` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `price` double precision NOT NULL, `inventory` integer NOT NULL, `created_at` datetime(6) NOT NULL, `car_id` integer NOT NULL);
CREATE TABLE `hibiscus_salecarandcolor_color` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `salecarandcolor_id` integer NOT NULL, `cartypecolor_id` integer NOT NULL);
--
-- Create model SaleSeriesAndColor
--
CREATE TABLE `hibiscus_saleseriesandcolor` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `year` varchar(256) NOT NULL, `created_at` datetime(6) NOT NULL, `series_id` integer NOT NULL);
CREATE TABLE `hibiscus_saleseriesandcolor_car_and_color` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `saleseriesandcolor_id` integer NOT NULL, `salecarandcolor_id` integer NOT NULL);
CREATE TABLE `hibiscus_saleseriesandcolor_series_color` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `saleseriesandcolor_id` integer NOT NULL, `cartypecolor_id` integer NOT NULL);
--
-- Add field bind_series to producerpresale
--
ALTER TABLE `hibiscus_producerpresale` ADD COLUMN `bind_series_id` integer NOT NULL;
ALTER TABLE `hibiscus_producerpresale` ALTER COLUMN `bind_series_id` DROP DEFAULT;
--
-- Add field gift to producerpresale
--
ALTER TABLE `hibiscus_producerpresale` ADD COLUMN `gift_id` integer NULL;
ALTER TABLE `hibiscus_producerpresale` ALTER COLUMN `gift_id` DROP DEFAULT;
--
-- Add field hd_pic to producerpresale
--
ALTER TABLE `hibiscus_producerpresale` ADD COLUMN `hd_pic_id` integer NOT NULL;
ALTER TABLE `hibiscus_producerpresale` ALTER COLUMN `hd_pic_id` DROP DEFAULT;
--
-- Add field producer to producerpresale
--
ALTER TABLE `hibiscus_producerpresale` ADD COLUMN `producer_id` integer NOT NULL;
ALTER TABLE `hibiscus_producerpresale` ALTER COLUMN `producer_id` DROP DEFAULT;
--
-- Add field small_pic to producerpresale
--
CREATE TABLE `hibiscus_producerpresale_small_pic` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `producerpresale_id` integer NOT NULL, `carpicture_id` integer NOT NULL);
--
-- Add field gift_bag_type to giftbag
--
CREATE TABLE `hibiscus_giftbag_gift_bag_type` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `giftbag_id` integer NOT NULL, `giftbagvalue_id` integer NOT NULL);
ALTER TABLE `hibiscus_giftbagvalue` ADD CONSTRAINT `hibiscus_giftba_gift_type_id_2f6b0d52_fk_hibiscus_giftbagtype_id` FOREIGN KEY (`gift_type_id`) REFERENCES `hibiscus_giftbagtype` (`id`);
ALTER TABLE `hibiscus_producerpresale` ADD CONSTRAINT `hibiscus_producerpres_article_id_53f5fa82_fk_hibiscus_article_id` FOREIGN KEY (`article_id`) REFERENCES `hibiscus_article` (`id`);
ALTER TABLE `hibiscus_salecarandcolor` ADD CONSTRAINT `hibiscus_salecarandcolor_car_id_6f7c68ef_fk_hibiscus_car_id` FOREIGN KEY (`car_id`) REFERENCES `hibiscus_car` (`id`);
ALTER TABLE `hibiscus_salecarandcolor_color` ADD CONSTRAINT `hibis_salecarandcolor_id_540f9b62_fk_hibiscus_salecarandcolor_id` FOREIGN KEY (`salecarandcolor_id`) REFERENCES `hibiscus_salecarandcolor` (`id`);
ALTER TABLE `hibiscus_salecarandcolor_color` ADD CONSTRAINT `hibiscus_sa_cartypecolor_id_43e3fbb3_fk_hibiscus_cartypecolor_id` FOREIGN KEY (`cartypecolor_id`) REFERENCES `hibiscus_cartypecolor` (`id`);
ALTER TABLE `hibiscus_salecarandcolor_color` ADD CONSTRAINT `hibiscus_salecarandcolor_color_salecarandcolor_id_83a3272d_uniq` UNIQUE (`salecarandcolor_id`, `cartypecolor_id`);
ALTER TABLE `hibiscus_saleseriesandcolor` ADD CONSTRAINT `hibiscus_saleseriesandc_series_id_8cd00e89_fk_hibiscus_series_id` FOREIGN KEY (`series_id`) REFERENCES `hibiscus_series` (`id`);
ALTER TABLE `hibiscus_saleseriesandcolor_car_and_color` ADD CONSTRAINT `f937081136ace781dc5194f990185a3a` FOREIGN KEY (`saleseriesandcolor_id`) REFERENCES `hibiscus_saleseriesandcolor` (`id`);
ALTER TABLE `hibiscus_saleseriesandcolor_car_and_color` ADD CONSTRAINT `hibis_salecarandcolor_id_088078dc_fk_hibiscus_salecarandcolor_id` FOREIGN KEY (`salecarandcolor_id`) REFERENCES `hibiscus_salecarandcolor` (`id`);
ALTER TABLE `hibiscus_saleseriesandcolor_car_and_color` ADD CONSTRAINT `hibiscus_saleseriesandcolor__saleseriesandcolor_id_2b0ea980_uniq` UNIQUE (`saleseriesandcolor_id`, `salecarandcolor_id`);
ALTER TABLE `hibiscus_saleseriesandcolor_series_color` ADD CONSTRAINT `f29295a3f1b5109721ebd6d837fb5b0b` FOREIGN KEY (`saleseriesandcolor_id`) REFERENCES `hibiscus_saleseriesandcolor` (`id`);
ALTER TABLE `hibiscus_saleseriesandcolor_series_color` ADD CONSTRAINT `hibiscus_sa_cartypecolor_id_27934580_fk_hibiscus_cartypecolor_id` FOREIGN KEY (`cartypecolor_id`) REFERENCES `hibiscus_cartypecolor` (`id`);
ALTER TABLE `hibiscus_saleseriesandcolor_series_color` ADD CONSTRAINT `hibiscus_saleseriesandcolor__saleseriesandcolor_id_c7fa3f32_uniq` UNIQUE (`saleseriesandcolor_id`, `cartypecolor_id`);
CREATE INDEX `hibiscus_producerpresale_7de7219e` ON `hibiscus_producerpresale` (`bind_series_id`);
ALTER TABLE `hibiscus_producerpresale` ADD CONSTRAINT `hibisc_bind_series_id_4618a8bc_fk_hibiscus_saleseriesandcolor_id` FOREIGN KEY (`bind_series_id`) REFERENCES `hibiscus_saleseriesandcolor` (`id`);
CREATE INDEX `hibiscus_producerpresale_addd09df` ON `hibiscus_producerpresale` (`gift_id`);
ALTER TABLE `hibiscus_producerpresale` ADD CONSTRAINT `hibiscus_producerpresale_gift_id_c81a171e_fk_hibiscus_giftbag_id` FOREIGN KEY (`gift_id`) REFERENCES `hibiscus_giftbag` (`id`);
CREATE INDEX `hibiscus_producerpresale_057bcac4` ON `hibiscus_producerpresale` (`hd_pic_id`);
ALTER TABLE `hibiscus_producerpresale` ADD CONSTRAINT `hibiscus_producerpr_hd_pic_id_bb8a9aa0_fk_hibiscus_carpicture_id` FOREIGN KEY (`hd_pic_id`) REFERENCES `hibiscus_carpicture` (`id`);
CREATE INDEX `hibiscus_producerpresale_ffd0e4bf` ON `hibiscus_producerpresale` (`producer_id`);
ALTER TABLE `hibiscus_producerpresale` ADD CONSTRAINT `hibiscus_producerpr_producer_id_671e3b87_fk_hibiscus_producer_id` FOREIGN KEY (`producer_id`) REFERENCES `hibiscus_producer` (`id`);
ALTER TABLE `hibiscus_producerpresale_small_pic` ADD CONSTRAINT `hibis_producerpresale_id_afd975f2_fk_hibiscus_producerpresale_id` FOREIGN KEY (`producerpresale_id`) REFERENCES `hibiscus_producerpresale` (`id`);
ALTER TABLE `hibiscus_producerpresale_small_pic` ADD CONSTRAINT `hibiscus_produc_carpicture_id_b7fa2887_fk_hibiscus_carpicture_id` FOREIGN KEY (`carpicture_id`) REFERENCES `hibiscus_carpicture` (`id`);
ALTER TABLE `hibiscus_producerpresale_small_pic` ADD CONSTRAINT `hibiscus_producerpresale_small__producerpresale_id_db0236c1_uniq` UNIQUE (`producerpresale_id`, `carpicture_id`);
ALTER TABLE `hibiscus_giftbag_gift_bag_type` ADD CONSTRAINT `hibiscus_giftbag_gift_giftbag_id_9af58d60_fk_hibiscus_giftbag_id` FOREIGN KEY (`giftbag_id`) REFERENCES `hibiscus_giftbag` (`id`);
ALTER TABLE `hibiscus_giftbag_gift_bag_type` ADD CONSTRAINT `hibiscus_gi_giftbagvalue_id_628bfc2e_fk_hibiscus_giftbagvalue_id` FOREIGN KEY (`giftbagvalue_id`) REFERENCES `hibiscus_giftbagvalue` (`id`);
ALTER TABLE `hibiscus_giftbag_gift_bag_type` ADD CONSTRAINT `hibiscus_giftbag_gift_bag_type_giftbag_id_e2450bfc_uniq` UNIQUE (`giftbag_id`, `giftbagvalue_id`);

COMMIT;

