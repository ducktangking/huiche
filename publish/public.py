__author__ = 'server'
import time
import os
import logging

from django.conf import settings
from django.utils.crypto import salted_hmac

from hibiscus.models import Picture, Portrait, UserAddress, UserInformation

LOGO_PATH = settings.MEDIA_ROOT
LOG = logging.getLogger(__name__)

DEFAULT_PASSWORD = '123456'


def deal_with_upload_file(f):
    if f.content_type in ["image/pjpeg", "image/jpeg", "image/gif", "image/bmp", "image/x-png", "image/png"]:
        try:
            img_t = f.name.split(".")[-1]
            t = str(time.time()).replace(".", "")
            new_name = t + "." + img_t
            logo_path = os.path.join(LOGO_PATH, new_name)
            real_path = os.path.join("/home/hc/huiche/background/huiche/collectedstatic/upload", new_name)
            destination = open(real_path, 'wb+')
            for chunk in f.chunks():
                destination.write(chunk)
            a = logo_path.find("static")
            b = int(a) - 1
            rel_path = logo_path[b:]
            return rel_path
        except Exception, e:
            LOG.error('Error happened when deal with upload files. %s' % e)
            print e
    return False


def action_info_new(request):
    info1 = None
    try:
        por = request.FILES.get("portrait", "")
        por_obj = ""
        if por:
            por_path = deal_with_upload_file(por)
            pic_obj = Picture.objects.create(name=por.name, path=por_path)
            # portrait obj
            por_obj = Portrait.objects.create(picture=pic_obj)

        name = request.POST.get("infoName", None)
        sex = request.POST.get("sex", None)
        birthday = request.POST.get("birthday", None)
        tel = request.POST.get("tel", None)
        qq = request.POST.get('qq', None)
        weibo = request.POST.get("weibo", None)
        weixin = request.POST.get("weixin", None)
        if por_obj or name or birthday or tel or qq or weibo or weixin:
            if por_obj:
                info1 = UserInformation(name=name, portrait=por_obj, sex=sex,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            else:
                info1 = UserInformation(name=name, sex=sex,
                                        tel=tel, qq=qq, weibo=weibo, weixin=weixin)
            if birthday and birthday != "None":
                info1.birthday = birthday
            info1.save()
            if "InfoAddr" in request.POST:
                addr = request.POST["InfoAddr"]
                if addr:
                    a1 = UserAddress.objects.create(address=addr)
                    info1.addresses.add(a1)
            if "addrList" in request.POST:
                ids = request.POST.getlist("addrList")
                uads = UserAddress.objects.filter(id__in=ids)
                for uad in uads:
                    info1.addresses.add(uad)
    except Exception as e:
        LOG.error('Error happened when create user info. %s' % e)

    return info1


def pwd_hash(password):
    key_salt = "django.contrib.auth.models.AbstractBaseUser.get_session_auth_hash"
    return salted_hmac(key_salt, password).hexdigest()