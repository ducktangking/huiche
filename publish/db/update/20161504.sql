BEGIN;
--
-- Create model InsuranceActivity
--
CREATE TABLE `hibiscus_insuranceactivity` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `name` varchar(64) NOT NULL, `content` varchar(256) NOT NULL);
--
-- Create model InsuranceCompany
--
CREATE TABLE `hibiscus_insurancecompany` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `name` varchar(64) NOT NULL, `logo_id` integer NOT NULL);
--
-- Alter field real_cc on car
--
ALTER TABLE `hibiscus_car` MODIFY `real_cc` varchar(16) NOT NULL;
--
-- Add field company to insuranceactivity
--
ALTER TABLE `hibiscus_insuranceactivity` ADD COLUMN `company_id` integer NOT NULL;
ALTER TABLE `hibiscus_insuranceactivity` ALTER COLUMN `company_id` DROP DEFAULT;
ALTER TABLE `hibiscus_insurancecompany` ADD CONSTRAINT `hibiscus_insurancecompan_logo_id_ad46e41f_fk_hibiscus_picture_id` FOREIGN KEY (`logo_id`) REFERENCES `hibiscus_picture` (`id`);
CREATE INDEX `hibiscus_insuranceactivity_447d3092` ON `hibiscus_insuranceactivity` (`company_id`);
ALTER TABLE `hibiscus_insuranceactivity` ADD CONSTRAINT `hibiscus_ins_company_id_d904b618_fk_hibiscus_insurancecompany_id` FOREIGN KEY (`company_id`) REFERENCES `hibiscus_insurancecompany` (`id`);

COMMIT;


BEGIN;
--
-- Rename field labor on seriesupkeepquote to sale_labor_price
--
ALTER TABLE `hibiscus_seriesupkeepquote` CHANGE `labor` `sale_labor_price` integer NULL;
--
-- Rename field price on seriesupkeepquote to sale_material_price
--
ALTER TABLE `hibiscus_seriesupkeepquote` CHANGE `price` `sale_material_price` integer NULL;
--
-- Remove field is_newcar from series
--
ALTER TABLE `hibiscus_series` DROP COLUMN `is_newcar` CASCADE;
--
-- Remove field sale_labor from seriesupkeepquote
--
ALTER TABLE `hibiscus_seriesupkeepquote` DROP COLUMN `sale_labor` CASCADE;
--
-- Remove field sale_price from seriesupkeepquote
--
ALTER TABLE `hibiscus_seriesupkeepquote` DROP COLUMN `sale_price` CASCADE;
--
-- Remove field general_price from upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` DROP COLUMN `general_price` CASCADE;
--
-- Remove field original_price from upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` DROP COLUMN `original_price` CASCADE;
--
-- Remove field price from upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` DROP COLUMN `price` CASCADE;
--
-- Remove field price_max from upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` DROP COLUMN `price_max` CASCADE;
--
-- Remove field price_min from upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` DROP COLUMN `price_min` CASCADE;

COMMIT;
