BEGIN;
--
-- Rename field labor on seriesupkeepquote to sale_labor_price
--
ALTER TABLE `hibiscus_seriesupkeepquote` CHANGE `labor` `sale_labor_price` integer NULL;
--
-- Rename field price on seriesupkeepquote to sale_material_price
--
ALTER TABLE `hibiscus_seriesupkeepquote` CHANGE `price` `sale_material_price` integer NULL;
--
-- Remove field sale_labor from seriesupkeepquote
--
ALTER TABLE `hibiscus_seriesupkeepquote` DROP COLUMN `sale_labor` CASCADE;
--
-- Remove field sale_price from seriesupkeepquote
--
ALTER TABLE `hibiscus_seriesupkeepquote` DROP COLUMN `sale_price` CASCADE;

COMMIT;
