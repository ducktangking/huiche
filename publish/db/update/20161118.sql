BEGIN;
--
-- Add field article_types to users
--
CREATE TABLE `hibiscus_users_article_types` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `users_id` integer NOT NULL, `articletype_id` integer NOT NULL);
ALTER TABLE `hibiscus_users_article_types` ADD CONSTRAINT `hibiscus_users_article_ty_users_id_fea73fcf_fk_hibiscus_users_id` FOREIGN KEY (`users_id`) REFERENCES `hibiscus_users` (`id`);
ALTER TABLE `hibiscus_users_article_types` ADD CONSTRAINT `hibiscus_user_articletype_id_a9ab5d31_fk_hibiscus_articletype_id` FOREIGN KEY (`articletype_id`) REFERENCES `hibiscus_articletype` (`id`);
ALTER TABLE `hibiscus_users_article_types` ADD CONSTRAINT `hibiscus_users_article_types_users_id_06424797_uniq` UNIQUE (`users_id`, `articletype_id`);

COMMIT;



BEGIN;
--
-- Create model RelationSeries
--
CREATE TABLE `hibiscus_relationseries` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `is_all` bool NOT NULL, `level_one_brand_id` integer NULL, `level_two_brand_id` integer NULL);
CREATE TABLE `hibiscus_relationseries_detail_series` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `relationseries_id` integer NOT NULL, `series_id` integer NOT NULL);
--
-- Add field rel_series to series
--
CREATE TABLE `hibiscus_series_rel_series` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `series_id` integer NOT NULL, `relationseries_id` integer NOT NULL);
ALTER TABLE `hibiscus_relationseries` ADD CONSTRAINT `hibiscus_relati_level_one_brand_id_f379bbdb_fk_hibiscus_brand_id` FOREIGN KEY (`level_one_brand_id`) REFERENCES `hibiscus_brand` (`id`);
ALTER TABLE `hibiscus_relationseries` ADD CONSTRAINT `hibiscus_relati_level_two_brand_id_8c5f204e_fk_hibiscus_brand_id` FOREIGN KEY (`level_two_brand_id`) REFERENCES `hibiscus_brand` (`id`);
ALTER TABLE `hibiscus_relationseries_detail_series` ADD CONSTRAINT `hibiscu_relationseries_id_7189a76e_fk_hibiscus_relationseries_id` FOREIGN KEY (`relationseries_id`) REFERENCES `hibiscus_relationseries` (`id`);
ALTER TABLE `hibiscus_relationseries_detail_series` ADD CONSTRAINT `hibiscus_relationseries_series_id_9d2934f1_fk_hibiscus_series_id` FOREIGN KEY (`series_id`) REFERENCES `hibiscus_series` (`id`);
ALTER TABLE `hibiscus_relationseries_detail_series` ADD CONSTRAINT `hibiscus_relationseries_detail_s_relationseries_id_ae2e9bb6_uniq` UNIQUE (`relationseries_id`, `series_id`);
ALTER TABLE `hibiscus_series_rel_series` ADD CONSTRAINT `hibiscus_series_rel_ser_series_id_1001ecd6_fk_hibiscus_series_id` FOREIGN KEY (`series_id`) REFERENCES `hibiscus_series` (`id`);
ALTER TABLE `hibiscus_series_rel_series` ADD CONSTRAINT `hibiscu_relationseries_id_4e9dddab_fk_hibiscus_relationseries_id` FOREIGN KEY (`relationseries_id`) REFERENCES `hibiscus_relationseries` (`id`);
ALTER TABLE `hibiscus_series_rel_series` ADD CONSTRAINT `hibiscus_series_rel_series_series_id_614a1744_uniq` UNIQUE (`series_id`, `relationseries_id`);

COMMIT;
