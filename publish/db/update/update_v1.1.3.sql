-- 车价轮询将地区设为可为空
BEGIN;
--
-- Alter field area on carinquiry
--
ALTER TABLE `hibiscus_carinquiry` DROP FOREIGN KEY `hibiscus_carinquir_area_id_ee5f7160_fk_hibiscus_supportedarea_id`;
ALTER TABLE `hibiscus_carinquiry` MODIFY `area_id` integer NULL;
ALTER TABLE `hibiscus_carinquiry` ADD CONSTRAINT `hibiscus_carinquir_area_id_ee5f7160_fk_hibiscus_supportedarea_id` FOREIGN KEY (`area_id`) REFERENCES `hibiscus_supportedarea` (`id`);

COMMIT;

-- 将车款中排量改为整型

BEGIN;
--
-- Alter field real_cc on car
--
ALTER TABLE `hibiscus_car` MODIFY `real_cc` integer NOT NULL;

COMMIT;

BEGIN;
--
-- Add field fav_series to users
--
CREATE TABLE `hibiscus_users_fav_series` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `users_id` integer NOT NULL, `series_id` integer NOT NULL);
COMMIT;



-- 增加资讯样式
BEGIN;
--
-- Create model ArticleStyleType
--
CREATE TABLE `hibiscus_articlestyletype` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `name` varchar(30) NOT NULL);
--
-- Add field article_style_type to article
--
ALTER TABLE `hibiscus_article` ADD COLUMN `article_style_type` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_article` ALTER COLUMN `article_style_type` DROP DEFAULT;
--
-- Add field fav_series to users
--
CREATE TABLE `hibiscus_users_fav_series` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `users_id` integer NOT NULL, `series_id` integer NOT NULL);
ALTER TABLE `hibiscus_users_fav_series` ADD CONSTRAINT `hibiscus_users_fav_series_users_id_3478a36b_fk_hibiscus_users_id` FOREIGN KEY (`users_id`) REFERENCES `hibiscus_users` (`id`);
ALTER TABLE `hibiscus_users_fav_series` ADD CONSTRAINT `hibiscus_users_fav_seri_series_id_7c6869d8_fk_hibiscus_series_id` FOREIGN KEY (`series_id`) REFERENCES `hibiscus_series` (`id`);
ALTER TABLE `hibiscus_users_fav_series` ADD CONSTRAINT `hibiscus_users_fav_series_users_id_a1c2ded6_uniq` UNIQUE (`users_id`, `series_id`);

COMMIT;

-- 增加转载地址
BEGIN;
--
-- Add field reprint_addr to article
--
ALTER TABLE `hibiscus_article` ADD COLUMN `reprint_addr` varchar(512) DEFAULT NULL;
ALTER TABLE `hibiscus_article` ALTER COLUMN `reprint_addr` DROP DEFAULT;

COMMIT;

-- 将维修保养项目的价格设置移到材料型号
BEGIN;
--
-- Add field general_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `general_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `general_price` DROP DEFAULT;
--
-- Add field original_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `original_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `original_price` DROP DEFAULT;

COMMIT;

-- 创建材料型号适用级别表并与材料型号建立关联关系
BEGIN;
--
-- Create model UpkeepMaterialLevel
--
CREATE TABLE `hibiscus_upkeepmateriallevel` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `name` varchar(30) NOT NULL);
--
-- Add field level to upkeepmaterialmodel
--
CREATE TABLE `hibiscus_upkeepmaterialmodel_level` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `upkeepmaterialmodel_id` integer NOT NULL, `upkeepmateriallevel_id` integer NOT NULL);
ALTER TABLE `hibiscus_upkeepmaterialmodel_level` ADD CONSTRAINT `D197033e1d2a2814e3d5a76f482e8dac` FOREIGN KEY (`upkeepmaterialmodel_id`) REFERENCES `hibiscus_upkeepmaterialmodel` (`id`);
ALTER TABLE `hibiscus_upkeepmaterialmodel_level` ADD CONSTRAINT `D1ccbc0ad18244f770f931fcf26aa8f6` FOREIGN KEY (`upkeepmateriallevel_id`) REFERENCES `hibiscus_upkeepmateriallevel` (`id`);
ALTER TABLE `hibiscus_upkeepmaterialmodel_level` ADD CONSTRAINT `hibiscus_upkeepmaterialmode_upkeepmaterialmodel_id_181a7720_uniq` UNIQUE (`upkeepmaterialmodel_id`, `upkeepmateriallevel_id`);

COMMIT;

