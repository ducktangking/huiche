
ALTER TABLE `hibiscus_seriesupkeepinfo` DROP COLUMN `brands_id` CASCADE;
--
-- Add field brands to seriesupkeepinfo
--
CREATE TABLE `hibiscus_seriesupkeepinfo_brands` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `seriesupkeepinfo_id` integer NOT NULL, `brand_id` integer NOT NULL);
ALTER TABLE `hibiscus_seriesupkeepinfo_brands` ADD CONSTRAINT `hib_seriesupkeepinfo_id_915d3d56_fk_hibiscus_seriesupkeepinfo_id` FOREIGN KEY (`seriesupkeepinfo_id`) REFERENCES `hibiscus_seriesupkeepinfo` (`id`);
ALTER TABLE `hibiscus_seriesupkeepinfo_brands` ADD CONSTRAINT `hibiscus_seriesupkeepinfo_brand_id_9539df28_fk_hibiscus_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `hibiscus_brand` (`id`);
ALTER TABLE `hibiscus_seriesupkeepinfo_brands` ADD CONSTRAINT `hibiscus_seriesupkeepinfo_bran_seriesupkeepinfo_id_2f578c3d_uniq` UNIQUE (`seriesupkeepinfo_id`, `brand_id`);

