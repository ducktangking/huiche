-- 经销商顾问添加级别
BEGIN;
--
-- Add field level to adviser
--
ALTER TABLE `hibiscus_adviser` ADD COLUMN `level` integer DEFAULT 1 NOT NULL;
ALTER TABLE `hibiscus_adviser` ALTER COLUMN `level` DROP DEFAULT;

COMMIT;

-- 保养订单提交接口，增加维修保养的提交： 材料型号
BEGIN;
--
-- Add field material_model to upkeeporders
--
CREATE TABLE `hibiscus_upkeeporders_material_model` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `upkeeporders_id` integer NOT NULL, `upkeepmaterialmodel_id` integer NOT NULL);
ALTER TABLE `hibiscus_upkeeporders_material_model` ADD CONSTRAINT `hibiscus_up_upkeeporders_id_a83998ed_fk_hibiscus_upkeeporders_id` FOREIGN KEY (`upkeeporders_id`) REFERENCES `hibiscus_upkeeporders` (`id`);
ALTER TABLE `hibiscus_upkeeporders_material_model` ADD CONSTRAINT `D4e2dfc65d48d87eb7ae8f0e313ac750` FOREIGN KEY (`upkeepmaterialmodel_id`) REFERENCES `hibiscus_upkeepmaterialmodel` (`id`);
ALTER TABLE `hibiscus_upkeeporders_material_model` ADD CONSTRAINT `hibiscus_upkeeporders_material_mod_upkeeporders_id_ee50fe6a_uniq` UNIQUE (`upkeeporders_id`, `upkeepmaterialmodel_id`);

COMMIT;


-- 订单提交的车型为保养车型管理的车型

BEGIN;
--
-- Remove field series from upkeeporders
--
ALTER TABLE `hibiscus_upkeeporders` DROP COLUMN `series_id` CASCADE;
--
-- Add field upkeep_series to upkeeporders
--
ALTER TABLE `hibiscus_upkeeporders` ADD COLUMN `upkeep_series_id` integer NULL;
ALTER TABLE `hibiscus_upkeeporders` ALTER COLUMN `upkeep_series_id` DROP DEFAULT;
CREATE INDEX `hibiscus_upkeeporders_3553cd1d` ON `hibiscus_upkeeporders` (`upkeep_series_id`);
ALTER TABLE `hibiscus_upkeeporders` ADD CONSTRAINT `hibiscus_u_upkeep_series_id_9b0128b4_fk_hibiscus_upkeepseries_id` FOREIGN KEY (`upkeep_series_id`) REFERENCES `hibiscus_upkeepseries` (`id`);

COMMIT;



-- 板金做漆订单提交时，提交的为package id
BEGIN;
--
-- Remove field upkeeps from upkeeporders
--
DROP TABLE `hibiscus_upkeeporders_upkeeps` CASCADE;
--
-- Add field upkeeps_pkgs to upkeeporders
--
CREATE TABLE `hibiscus_upkeeporders_upkeeps_pkgs` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `upkeeporders_id` integer NOT NULL, `upkeeppackage_id` integer NOT NULL);
ALTER TABLE `hibiscus_upkeeporders_upkeeps_pkgs` ADD CONSTRAINT `hibiscus_up_upkeeporders_id_ebfea308_fk_hibiscus_upkeeporders_id` FOREIGN KEY (`upkeeporders_id`) REFERENCES `hibiscus_upkeeporders` (`id`);
ALTER TABLE `hibiscus_upkeeporders_upkeeps_pkgs` ADD CONSTRAINT `hibiscus__upkeeppackage_id_e0a0f69c_fk_hibiscus_upkeeppackage_id` FOREIGN KEY (`upkeeppackage_id`) REFERENCES `hibiscus_upkeeppackage` (`id`);
ALTER TABLE `hibiscus_upkeeporders_upkeeps_pkgs` ADD CONSTRAINT `hibiscus_upkeeporders_upkeeps_pkgs_upkeeporders_id_6c4e3c35_uniq` UNIQUE (`upkeeporders_id`, `upkeeppackage_id`);

COMMIT;

