BEGIN;
--
-- Add field city_initial to supportedarea
--
ALTER TABLE `hibiscus_supportedarea` ADD COLUMN `city_initial` varchar(4) DEFAULT NULL;
ALTER TABLE `hibiscus_supportedarea` ALTER COLUMN `city_initial` DROP DEFAULT;
--
-- Add field province_initial to supportedarea
--
ALTER TABLE `hibiscus_supportedarea` ADD COLUMN `province_initial` varchar(4) DEFAULT NULL;
ALTER TABLE `hibiscus_supportedarea` ALTER COLUMN `province_initial` DROP DEFAULT;

COMMIT;
