BEGIN;
--
-- Add field fav_cars to users
--
CREATE TABLE `hibiscus_users_fav_cars` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `users_id` integer NOT NULL, `car_id` integer NOT NULL);
ALTER TABLE `hibiscus_users_fav_cars` ADD CONSTRAINT `hibiscus_users_fav_cars_users_id_29c6eab3_fk_hibiscus_users_id` FOREIGN KEY (`users_id`) REFERENCES `hibiscus_users` (`id`);
ALTER TABLE `hibiscus_users_fav_cars` ADD CONSTRAINT `hibiscus_users_fav_cars_car_id_5269fb1f_fk_hibiscus_car_id` FOREIGN KEY (`car_id`) REFERENCES `hibiscus_car` (`id`);
ALTER TABLE `hibiscus_users_fav_cars` ADD CONSTRAINT `hibiscus_users_fav_cars_users_id_80ee3f2b_uniq` UNIQUE (`users_id`, `car_id`);

COMMIT;
