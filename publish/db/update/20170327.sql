BEGIN;
--
-- Add field car_color to car
--
CREATE TABLE `hibiscus_car_car_color` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `car_id` integer NOT NULL, `cartypecolor_id` integer NOT NULL);
ALTER TABLE `hibiscus_car_car_color` ADD CONSTRAINT `hibiscus_car_car_color_car_id_6918a71d_fk_hibiscus_car_id` FOREIGN KEY (`car_id`) REFERENCES `hibiscus_car` (`id`);
ALTER TABLE `hibiscus_car_car_color` ADD CONSTRAINT `hibiscus_ca_cartypecolor_id_18f38c2a_fk_hibiscus_cartypecolor_id` FOREIGN KEY (`cartypecolor_id`) REFERENCES `hibiscus_cartypecolor` (`id`);
ALTER TABLE `hibiscus_car_car_color` ADD CONSTRAINT `hibiscus_car_car_color_car_id_4032af3e_uniq` UNIQUE (`car_id`, `cartypecolor_id`);

COMMIT;
