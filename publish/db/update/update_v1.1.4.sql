BEGIN;
--
-- Add field material_max_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `material_max_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `material_max_price` DROP DEFAULT;
--
-- Add field material_min_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `material_min_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `material_min_price` DROP DEFAULT;
--
-- Add field material_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `material_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `material_price` DROP DEFAULT;
--
-- Add field time_labor_max_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `time_labor_max_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `time_labor_max_price` DROP DEFAULT;
--
-- Add field time_labor_min_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `time_labor_min_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `time_labor_min_price` DROP DEFAULT;
--
-- Add field time_labor_price to upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ADD COLUMN `time_labor_price` integer DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `time_labor_price` DROP DEFAULT;
--
-- Alter field price_max on upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `price_max` SET DEFAULT 0;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `price_max` DROP DEFAULT;
--
-- Alter field price_min on upkeepmaterialmodel
--
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `price_min` SET DEFAULT 0;
ALTER TABLE `hibiscus_upkeepmaterialmodel` ALTER COLUMN `price_min` DROP DEFAULT;

COMMIT;




BEGIN;
--
-- Add field all_series to seriesupkeepinfo
--
ALTER TABLE `hibiscus_seriesupkeepinfo` ADD COLUMN `all_series` bool DEFAULT 0 NOT NULL;
ALTER TABLE `hibiscus_seriesupkeepinfo` ALTER COLUMN `all_series` DROP DEFAULT;
--
-- Alter field upkeep_series on seriesupkeepinfo
--
ALTER TABLE `hibiscus_seriesupkeepinfo` DROP FOREIGN KEY `hibiscus_s_upkeep_series_id_59d72afb_fk_hibiscus_upkeepseries_id`;
ALTER TABLE `hibiscus_seriesupkeepinfo` MODIFY `upkeep_series_id` integer NULL;
ALTER TABLE `hibiscus_seriesupkeepinfo` ADD CONSTRAINT `hibiscus_s_upkeep_series_id_59d72afb_fk_hibiscus_upkeepseries_id` FOREIGN KEY (`upkeep_series_id`) REFERENCES `hibiscus_upkeepseries` (`id`);

COMMIT;


BEGIN;
--
-- Add field brands to seriesupkeepinfo
--
ALTER TABLE `hibiscus_seriesupkeepinfo` ADD COLUMN `brands_id` integer NULL;
ALTER TABLE `hibiscus_seriesupkeepinfo` ALTER COLUMN `brands_id` DROP DEFAULT;
CREATE INDEX `hibiscus_seriesupkeepinfo_d5b4acb9` ON `hibiscus_seriesupkeepinfo` (`brands_id`);
ALTER TABLE `hibiscus_seriesupkeepinfo` ADD CONSTRAINT `hibiscus_seriesupkeepinf_brands_id_2617cc86_fk_hibiscus_brand_id` FOREIGN KEY (`brands_id`) REFERENCES `hibiscus_brand` (`id`);

COMMIT;



BEGIN;
--
-- Remove field series from seriesupkeepinfo
--
--
-- Add field series to seriesupkeepinfo
--
CREATE TABLE `hibiscus_seriesupkeepinfo_series` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `seriesupkeepinfo_id` integer NOT NULL, `series_id` integer NOT NULL);
ALTER TABLE `hibiscus_seriesupkeepinfo_series` ADD CONSTRAINT `hib_seriesupkeepinfo_id_cafc714d_fk_hibiscus_seriesupkeepinfo_id` FOREIGN KEY (`seriesupkeepinfo_id`) REFERENCES `hibiscus_seriesupkeepinfo` (`id`);
ALTER TABLE `hibiscus_seriesupkeepinfo_series` ADD CONSTRAINT `hibiscus_seriesupkeepin_series_id_fee4f881_fk_hibiscus_series_id` FOREIGN KEY (`series_id`) REFERENCES `hibiscus_series` (`id`);
ALTER TABLE `hibiscus_seriesupkeepinfo_series` ADD CONSTRAINT `hibiscus_seriesupkeepinfo_seri_seriesupkeepinfo_id_2a93642c_uniq` UNIQUE (`seriesupkeepinfo_id`, `series_id`);

COMMIT;
