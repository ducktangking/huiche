"""
WSGI config for huiche project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

#import os

#from django.core.wsgi import get_wsgi_application

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "huiche.settings")

#application = get_wsgi_application()


import os

import sys

from django.core.wsgi import get_wsgi_application

path = '/home/hc/huiche/background/huiche'

if path not in sys.path:

    sys.path.insert(0, '/home/hc/huiche/background/huiche')

os.environ['DJANGO_SETTINGS_MODULE'] = 'huiche.settings'

application = get_wsgi_application()
