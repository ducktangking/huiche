from distutils.core import setup

setup(
    name='huiche',
    version='1.0.3',
    packages=['huiche', 'hibiscus', 'hibiscus.migrations'],
    url='http://www.huiche.com',
    license='BSD',
    author='tomjun',
    author_email='65151907@qq.com',
    description='A django app to conduct web-apps huiche',
    install_requires=["django>=1.9", "simplejson", "MySQL-python", "pytz"],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 1.9',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ]
)
